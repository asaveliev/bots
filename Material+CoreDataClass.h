//
//  Material+CoreDataClass.h
//  
//
//  Created by Anton Savelev on 07.05.17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Material : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Material+CoreDataProperties.h"
