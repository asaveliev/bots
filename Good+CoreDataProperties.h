//
//  Good+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 01.01.16.
//
//

#import "Good+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Good (CoreDataProperties)

+ (NSFetchRequest<Good *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSNumber *strategy;
@property (nullable, nonatomic, copy) NSString *goodDescription;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSString *price;
@property (nullable, nonatomic, copy) NSString *link;

@end

NS_ASSUME_NONNULL_END
