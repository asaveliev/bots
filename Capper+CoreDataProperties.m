//
//  Capper+CoreDataProperties.m
//  
//
//  Created by Anton Savelev on 17.04.17.
//
//

#import "Capper+CoreDataProperties.h"

@implementation Capper (CoreDataProperties)

+ (NSFetchRequest<Capper *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Capper"];
}

@dynamic uid;
@dynamic name;
@dynamic balance;
@dynamic imageUrl;
@dynamic created;

@end
