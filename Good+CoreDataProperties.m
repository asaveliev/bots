//
//  Good+CoreDataProperties.m
//  
//
//  Created by Anton Savelev on 01.01.16.
//
//

#import "Good+CoreDataProperties.h"

@implementation Good (CoreDataProperties)

+ (NSFetchRequest<Good *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Good"];
}

@dynamic uid;
@dynamic strategy;
@dynamic goodDescription;
@dynamic imageUrl;
@dynamic price;
@dynamic link;

@end
