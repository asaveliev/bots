//
//  UserCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 27.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

@objc public class UserCell: UITableViewCell {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 22.0)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .white
        return label
    }()
    
    lazy var avaView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 30.0
        return image
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = UIColor.tkColorFromHex(0x8a8a8f)
        label.textColor = UIColor(red:0.54, green:0.54, blue:0.56, alpha:1)
        return label
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = UIColor.white
        
        let views = [
            titleLabel,
            subtitleLabel,
            avaView,
            bottomLine
        ]
        views.forEach {
            contentView.addSubview($0)
        }
        
        selectionStyle = .none
        
        setNeedsUpdateConstraints()
        
        backgroundColor = UIColor.tkColorFromHex(0x2e3035)
        contentView.backgroundColor = UIColor.tkColorFromHex(0x2e3035)
    }
    
    override public func updateConstraints() {
        avaView.snp.remakeConstraints { make in
            make.top.leading.equalToSuperview().inset(16)
            make.height.width.equalTo(60)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.height.greaterThanOrEqualTo(28)
            make.leading.equalTo(avaView.snp.trailing).offset(15)
            make.top.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-15)
        }
        
        subtitleLabel.snp.remakeConstraints { make in
            make.height.greaterThanOrEqualTo(16)
            make.leading.equalTo(avaView.snp.trailing).offset(15)
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.trailing.equalToSuperview().offset(-15)
            make.bottom.equalToSuperview().offset(-24)
        }
        
        super.updateConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
