//
//  DCGraphView.m
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCGraphView.h"
#import "DCMoneyFormatter.h"

const CGFloat DCGraphViewDefaultOffset = 8.0;
const CGFloat DCGraphViewDefaultWidth = 16.0;

@interface DCGraphView () <SKGraphElementDelegateProtocol>

@property (nonatomic, strong) NSArray<NSNumber *>   *values;

@property (nonatomic, strong) UIScrollView          *scrollView;
@property (nonatomic, assign) CGFloat               contentWith;
@property (nonatomic, strong) UILabel               *valueLabel;

@property (nonatomic, strong) NSLayoutConstraint    *labelCenterConstraint;

@end

@implementation DCGraphView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _scrollView = [UIScrollView new];
        self.scrollView.showsHorizontalScrollIndicator = self.scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:self.scrollView];
        
        _valueLabel = [UILabel new];
        self.valueLabel.font = [UIFont regularFontOfSize:13.0];
        self.valueLabel.textAlignment = NSTextAlignmentCenter;
        self.valueLabel.textColor = [UIColor whiteColor];
        [self.scrollView addSubview:self.valueLabel];
        
        self.valueLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-25)-[label]" options:0 metrics:nil views:@{ @"label": self.valueLabel }]];
        
        _elements = [NSMutableArray new];
        
        self.scrollView.clipsToBounds = NO;
        self.clipsToBounds = NO;
        
        _offset = DCGraphViewDefaultOffset;
        _elementWidth = DCGraphViewDefaultWidth;
    }
    
    return self;
}

- (void)setGraphElementsWithValues:(NSArray *)values titles:(NSArray *)titles {
    self.contentWith = 0.0;
    for (UIView *v in self.scrollView.subviews) {
        if (v == self.valueLabel) {
            continue;
        }
        
        [v removeFromSuperview];
    }
    
    self.valueLabel.hidden = YES;
    
    [self.elements removeAllObjects];
    
    NSMutableArray *absoluteValues = [values mutableCopy];
    for (int i = 0; i < values.count; i++) {
        absoluteValues[i] = @(fabs([values[i] doubleValue]));
    }
    double maxValue = [[absoluteValues sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }].lastObject doubleValue];
    CGRect prevRect = CGRectZero;
    prevRect.origin.x = -self.offset;
    for (NSInteger i = 0; i < values.count; i++) {
        NSNumber *absoluteValue = absoluteValues[i];
        NSNumber *value = values[i];
        SKGraphElementView *elementView = [[SKGraphElementView alloc] initWithFrame:(CGRect) {2.0 * self.offset + prevRect.origin.x + prevRect.size.width, 0.0, self.elementWidth, self.bounds.size.height}];
        elementView.clipsToBounds = NO;
        elementView.negative = value.doubleValue < 0.0;
        elementView.titleLabel.text = titles[i];
        elementView.progress = maxValue ? [absoluteValue doubleValue] / maxValue : 0.0;
        elementView.delegate = self;
        
        [self.elements addObject:elementView];
        [self.scrollView addSubview:elementView];
        
        prevRect = elementView.frame;
        
        self.contentWith = CGRectGetMaxX(elementView.frame) + self.offset;
    }
    
    self.values = [values copy];
    
    self.scrollView.contentInset = UIEdgeInsetsZero;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = (CGSize) { self.contentWith, self.bounds.size.height };
}

- (NSInteger)selecteIndex {
    NSInteger index = [self.elements indexOfObject:self.selectedElement];
    return index;
}

#pragma mark - SKGraphElementDelegateProtocol

- (void)graphElementViewDidTapped:(SKGraphElementView *)graphView {
    if (![self.delegate graphViewCanSelectElementAtIndex:[self.elements indexOfObject:graphView]]) {
        return;
    }
    for (SKGraphElementView *view in self.elements) {
        view.selected = NO;
    }
    
    graphView.selected = YES;
    
    self.valueLabel.text = [[DCMoneyFormatter new] stringFromAmount:self.values[[self.elements indexOfObject:graphView]]];
    self.valueLabel.textColor = graphView.titleLabel.textColor;
    [self.scrollView removeConstraint:self.labelCenterConstraint];
    self.labelCenterConstraint = [NSLayoutConstraint constraintWithItem:self.valueLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:graphView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [self.scrollView addConstraint:self.labelCenterConstraint];
    [self setNeedsLayout];
    [self updateConstraints];
    self.valueLabel.hidden = NO;
    
    if (graphView == self.elements.firstObject) {
        self.scrollView.contentInset = UIEdgeInsetsMake(0.0, [self.valueLabel.text sizeWithAttributes:@{ NSFontAttributeName: self.valueLabel.font }].width / 2.0, 0.0, 0.0);
        [self.scrollView setContentOffset:CGPointMake(- [self.valueLabel.text sizeWithAttributes:@{ NSFontAttributeName: self.valueLabel.font }].width / 2.0, 0.0) animated:YES];
    } else if (graphView == self.elements.lastObject) {
        self.scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, [self.valueLabel.text sizeWithAttributes:@{ NSFontAttributeName: self.valueLabel.font }].width / 2.0);
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            self.scrollView.contentInset = UIEdgeInsetsZero;
        }];
    }
    
    if (self.delegate) {
        [self.delegate graphViewDidSelectElementAtIndex:[self.elements indexOfObject:graphView]];
    }
}

@end
