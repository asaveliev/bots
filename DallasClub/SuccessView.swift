//
//  SuccessView.swift
//  DallasClubProd
//
//  Created by Anton on 30.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

import UIKit

class SuccessView: UIView {
    lazy var doneImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "doneIcon")
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = .white
        label.text = "Отправлено"
        return label
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 51/255, green: 70/255, blue: 89/255, alpha: 1)
        view.clipsToBounds = true
        view.layer.cornerRadius = 8
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(doneImageView)
        
        containerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(120)
        }
        
        doneImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(32)
            make.centerX.equalToSuperview()
            make.size.equalTo(CGSize(width: 49, height: 38))
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(doneImageView.snp.bottom).offset(17)
        }
        
        backgroundColor = UIColor.black.withAlphaComponent(0.9)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
