//
//  DCAudioManager.m
//  DallasClub
//
//  Created by Anton Savelev on 08.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAudioManager.h"
#import "ExtAudioConverter.h"
#import "EZAudio.h"

@interface DCAudioManager ()<EZMicrophoneDelegate>

@property (nonatomic, readonly) EZMicrophone        *microphone;
@property (nonatomic, strong)   ExtAudioConverter   *converter;

@end

@implementation DCAudioManager

- (instancetype)initWithFilePath:(NSString *)path {
    if (self = [super init]) {
        _filePath = path;
        
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&error];
        if (error) {
            NSLog(@"Error setting up audio session category: %@", error.localizedDescription);
        }
        [session setActive:YES error:&error];
        if (error) {
            NSLog(@"Error setting up audio session active: %@", error.localizedDescription);
        }
    
        _player = [EZAudioPlayer audioPlayer];
        
        [session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
        if (error) {
            NSLog(@"Error overriding output to the speaker: %@", error.localizedDescription);
        }
    }
    
    return self;
}

- (void)startRecording {
    [self.player pause];
    
    _microphone = [EZMicrophone microphoneWithDelegate:self startsImmediately:YES];
    _recorder = [EZRecorder recorderWithURL:[NSURL URLWithString:self.filePath] clientFormat:[self.microphone audioStreamBasicDescription] fileType:EZRecorderFileTypeM4A];
}

- (void)stopRecording {
    [self.microphone stopFetchingAudio];
    [self.recorder closeAudioFile];
}

- (void)playAudio {
    [self stopRecording];
    
    EZAudioFile *file = [[EZAudioFile alloc] initWithURL:[NSURL URLWithString:self.filePath]];
    [self.player playAudioFile:file];
}

- (NSData *)generateMp3Data {
    self.converter = [ExtAudioConverter new];
    self.converter.inputFile = self.filePath;
    NSString *uuid = [[[NSUUID UUID] UUIDString] stringByAppendingPathExtension:@"mp3"];
    self.converter.outputFile = [[self.filePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:uuid];
    self.converter.outputFormatID = kAudioFormatMPEGLayer3;
    self.converter.outputFileType = kAudioFileMP3Type;
    [self.converter convert];
    NSData *data = [NSData dataWithContentsOfFile:self.converter.outputFile];
    return data;
}

#pragma mark - EZMicrophoneDelegate

- (void)microphone:(EZMicrophone *)microphone hasBufferList:(AudioBufferList *)bufferList withBufferSize:(UInt32)bufferSize withNumberOfChannels:(UInt32)numberOfChannels {
    [self.recorder appendDataFromBufferList:bufferList withBufferSize:bufferSize];
}

@end
