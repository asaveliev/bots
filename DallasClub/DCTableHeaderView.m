//
//  DCTableHeaderView.m
//  DallasClub
//
//  Created by Anton on 19/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCTableHeaderView.h"

@implementation DCTableHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _bgImageView = [UIImageView new];
        self.bgImageView.image = [UIImage imageNamed:@"card"];
        [self addSubview:self.bgImageView];
        
        _titleLabel  = [UILabel new];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont statsHeaderTitleFont];
        [self addSubview:self.titleLabel];
        
        _subtitleLabel  = [UILabel new];
        self.subtitleLabel.textAlignment = NSTextAlignmentCenter;
        self.subtitleLabel.textColor = [UIColor statsHeaderSubtitleColor];
        self.subtitleLabel.font = [UIFont statsHeaderSubtitleFont];
        [self addSubview:self.subtitleLabel];
        
        _infoView = [UIView new];
        self.infoView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.infoView];
        
        _successLabel = [UILabel new];
        [self.infoView addSubview:self.successLabel];
        _failedLabel = [UILabel new];
        [self.infoView addSubview:self.failedLabel];
        _returnLabel = [UILabel new];
        [self.infoView addSubview:self.returnLabel];
        self.returnLabel.textColor = self.failedLabel.textColor = self.successLabel.textColor = [UIColor whiteColor];
        self.returnLabel.font = self.failedLabel.font = self.successLabel.font = [UIFont statsHeaderLabelsFont];
        
        _successImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bet_check_icon"]];
        _failedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bet_fail_icon"]];
        _returnImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bet_return_icon"]];
        [self.infoView addSubview:self.successImageView];
        [self.infoView addSubview:self.failedImageView];
        [self.infoView addSubview:self.returnImageView];
        
        self.subtitleLabel.text = @"Нажми на меня";
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"bg": self.bgImageView,
                            @"title": self.titleLabel,
                            @"subtitle": self.subtitleLabel,
                            @"success": self.successLabel,
                            @"failed": self.failedLabel,
                            @"ret": self.returnLabel,
                            @"successImg": self.successImageView,
                            @"failedImg": self.failedImageView,
                            @"retImg": self.returnImageView,
                            @"info": self.infoView
                            };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-14-[bg]-14-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[bg(210)]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-55-[title]-6-[subtitle]-25-[info]-30-|" options:0 metrics:nil views:views]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:1.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.subtitleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:1.0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self.infoView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[successImg(20)]-5-[success(>=40@1000)]-5-[retImg(20)]-5-[ret(>=40@1000)]-5-[failedImg(20)]-5-[failed(>=28@1000)]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[success]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[failed]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[ret]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[successImg(20)]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[retImg(20)]-0-|" options:0 metrics:nil views:views]];
    [self.infoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[failedImg(20)]-0-|" options:0 metrics:nil views:views]];
}

@end
