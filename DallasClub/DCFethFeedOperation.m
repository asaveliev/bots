//
//  DCFethFeedOperation.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFethFeedOperation.h"
#import "Post.h"
#import "Capper.h"

@interface DCFethFeedOperation ()

@property (nonatomic, readonly) NSString *capperName;

@end

@implementation DCFethFeedOperation

- (instancetype)initWithPageNumber:(NSNumber *)page capperName:(NSString *)name {
    if (self = [super initWithPageNumber:page]) {
        _capperName = name;
    }
    
    return self;
}

- (void)main {
    NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
    NSNumber *capperId;
    Capper *capper = [Capper getByField:@"name" equalsTo:self.capperName fromContext:context];
    capperId = capper.uid;
    
    [[DCNetworkCore sharedInstance].feedProvider fetchFeedPage:self.page ?: @1 capperId:capperId withCompletion:^(NSArray *items, NSError *error) {
        if (error || ![items isKindOfClass:[NSArray class]]) {
            [self completeOperationWithError:error];
            return;
        }
        
        [context performBlock:^{
            NSMutableArray *existing = [[Post getAllInContext:context] mutableCopy];
            for (NSDictionary *data in items) {
                Post *item = [Post getByField:@"uid" equalsTo:data[@"id"] fromContext:context];
                if (!item) {
                    item = [Post createInContext:context];
                }
                [item setDictionaryRepresentation:data];
                
                [existing removeObject:item];
            }
            
            if ((self.page.integerValue == 1 || !self.page) && !self.capperName.length) {
                for (Post *toDelete in existing) {
                    [context deleteObject:toDelete];
                }
            }
            
            NSError *error = nil;
            [context save:&error];
            
            [self completeOperationWithError:error];
        }];
    }];
}

@end
