//
//  DCFeedCell.h
//  DallasClub
//
//  Created by Anton on 20/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "TKNewRoundedCardCell.h"
#import "KILabel.h"

@interface DCFeedCell : TKNewRoundedCardCell

@property (strong, nonatomic) KILabel       *messageLabel;
@property (strong, nonatomic) UIImageView   *avatarImageView;
@property (strong, nonatomic) UILabel       *timeLabel;
@property (strong, nonatomic) UILabel       *nameLabel;

@end
