//
//  DCTableHeaderCell.m
//  DallasClub
//
//  Created by Anton Savelev on 22.01.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCTableHeaderCell.h"
#import "DCNetworkCore.h"

@interface DCTableHeaderCell ()

@property (nonatomic, strong) UIView *dividerView;

@end

@implementation DCTableHeaderCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _dividerView = [UIView new];
        self.dividerView.backgroundColor = UIColorFromHex(0xdce1e4);
        [self addSubview:self.dividerView];
        
        _titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:36.0 weight:UIFontWeightHeavy];
        [self addSubview:self.titleLabel];
        
        _dateLabel = [UILabel new];
        self.dateLabel.font = [UIFont regularFontOfSize:12.0];
        self.dateLabel.textColor = UIColorFromHex(0x929292);
        [self addSubview:self.dateLabel];
        
        _aboutButton = [UIButton new];
        [self.contentView addSubview:self.aboutButton];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.aboutButton setImage:[UIImage imageNamed:[DCNetworkCore sharedInstance].authProvider.isAdmin ? @"add-message-icon" : @"info_icon"] forState:UIControlStateNormal];
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"date": self.dateLabel,
        @"divider": self.dividerView,
        @"label": self.titleLabel,
        @"about": self.aboutButton
    };
    
    NSDictionary *metrics = @{
        @"leftOffset": @16,
        @"dividerHeight": @1.5
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-11-[date]-1-[label]-8-[divider(==dividerHeight)]-0-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-leftOffset-[date]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[about]-20-|" options:0 metrics:metrics views:views]];
    
    [NSLayoutConstraint activateConstraints:@[[NSLayoutConstraint constraintWithItem:self.aboutButton attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]]];
}

@end
