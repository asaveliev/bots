//
//  DCAddBetPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAddBetPolicy.h"

@implementation DCAddBetPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    NSDictionary *dict = @{
                           @"amount": [self.percent stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"% "]],
                           @"number": self.numberString,
                           @"status": self.status,
                           @"coefficient": self.coefficient,
                           @"date": self.dateString
                           };
    
    [[DCNetworkCore sharedInstance].statsProvider createBetWithDictionary:@{ @"bet": dict } completion:^(NSArray *items, NSError *error) {
        completion(error);
    }];
}

@end
