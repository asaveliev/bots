//
//  DCFeedViewController.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedViewController.h"
#import "DCStatisticsView.h"
#import "DCFeedCell.h"
#import "DCFeedView.h"
#import "DCFeedDataSource.h"
#import "DCCappersDataSource.h"
#import "DCAddFeedViewController.h"
#import "DCAboutViewController.h"
#import "DCCapperDetailsViewController.h"
#import "DCCappersHeaderView.h"

#import "DCRemoveFeedPolicy.h"
#import "DCEditFeedPolicy.h"
#import "DCCreateFeedPolicy.h"

#import "DCImporter.h"

#import "Capper.h"
#import "DCCapperDetailsViewController.h"

#import "RMUniversalAlert.h"
#import "SVProgressHUD.h"
#import "SDImageCache.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>

@interface DCFeedViewController () <DCFeedDataSourceDelegate>

@property (strong, nonatomic) DCFeedView            *mainView;

@property (strong, nonatomic) DCFeedDataSource      *dataSource;

@property (strong, nonatomic) NSNumber              *postId;
@property (assign, nonatomic) BOOL                  notFirstLaunch;

@end

@implementation DCFeedViewController

- (instancetype)initWithPostId:(NSNumber *)uid {
    if (self = [super init]) {
        _postId = uid;
    }
    
    return self;
}

- (void)loadView {
    self.view = self.mainView = [[DCFeedView alloc] initWithSelectedItem:self.postId != nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.1)];
    self.mainView.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.1)];
    
    _dataSource = [[DCFeedDataSource alloc] initWithTableView:self.mainView.tableView postId:self.postId];
    [self.dataSource.refreshControl addTarget:self action:@selector(refreshFeedData) forControlEvents:UIControlEventValueChanged];
    self.dataSource.delegate = self;
    self.dataSource.cappersDataSource.delegate = self;
    
    [self.mainView.headerView.rightButton addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.headerView.leftButton addTarget:self action:@selector(leftButtonAction) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.headerView.leftButton.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRefreshControl) name:@"DCApplicationDidRecieveNotification" object:nil];
}


- (void)refreshFeedData {
    [[DCImporter importer] updateFeed:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.dataSource.refreshControl endRefreshing];
        });
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mainView.tableView reloadData];
    self.mainView.headerView.rightButton.hidden = ![DCNetworkCore sharedInstance].authProvider.isAdmin;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.tabBarController.title = @"Сигналы";
    
    if (!self.postId && !self.notFirstLaunch) {
        [self refreshFeedData];
    }
    
    self.notFirstLaunch = YES;
}

- (void)showRefreshControl {
    [self.dataSource.refreshControl beginRefreshing];
    [self.mainView.tableView setContentOffset:CGPointMake(0, -self.dataSource.refreshControl.frame.size.height) animated:YES];
    [self refreshFeedData];
}

- (void)leftButtonAction {
    if (self.postId) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    NSString *text = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texts" ofType:@"plist"]][@"feed"];
    [self.navigationController pushViewController:[[DCAboutViewController alloc] initWithText:text] animated:YES];
}

- (void)add {
    DCCreateFeedPolicy *policy = [DCCreateFeedPolicy new];
    [self.navigationController pushViewController:[[DCAddFeedViewController alloc] initWithPolicy:policy] animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - DCFeedDataSourceDelegate

- (void)dataSource:(id)dataSource didSelectObjectAtIndexPath:(NSIndexPath *)indexPath {
    if ([dataSource isKindOfClass:[DCCappersDataSource class]]) {
        Capper *model = [dataSource objectAtIndexPath:indexPath];
        DCCapperDetailsViewController *vc = [[DCCapperDetailsViewController alloc] initWithCapperModel:model];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    Post *post = [self.dataSource objectAtIndexPath:indexPath];
    Capper *model = [Capper getByField:@"name" equalsTo:post.capper_name fromContext:[DCPersistentStack sharedInstance].context];
    if (![DCNetworkCore sharedInstance].authProvider.isAdmin || model.uid.integerValue != [DCNetworkCore sharedInstance].authProvider.userId) {
        return;
    }
    
    [RMUniversalAlert showActionSheetInViewController:self withTitle:@"" message:@"Выберите действие" cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@[@"Изменить", @"Удалить"] popoverPresentationControllerBlock:^(RMPopoverPresentationController * _Nonnull popover) {
        
    } tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
        DCFeedPolicy *policy;
        if (buttonIndex == 2) {
            policy = [[DCEditFeedPolicy alloc] initWithPostId:post.uid];
            policy.image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:post.imageUrl];
            [self.navigationController pushViewController:[[DCAddFeedViewController alloc] initWithPolicy:policy] animated:YES];
        } else if (buttonIndex == 3) {
            policy = [[DCRemoveFeedPolicy alloc] initWithPostId:post.uid];
            
            [SVProgressHUD show];
            [policy performActionWithCompletion:^(NSError *error) {
                [SVProgressHUD dismiss];
                if (!error) {
                    [[DCImporter importer] updateFeed];
                    return;
                }
                
                [RMUniversalAlert showAlertInViewController:self withTitle:@"Ошибка!" message:@"Попробуйте позже" cancelButtonTitle:@"Ок" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                    
                }];
            }];
        }
    }];
}

- (void)dataSource:(DCFeedDataSource *)dataSource didSelectPhoto:(NSString *)link {
    if (!link.length) {
        return;
    }
    
    IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:link]];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo]];
    browser.displayToolbar = NO;
    [self presentViewController:browser animated:YES completion:nil];
}
    
- (void)dataSource:(DCFeedDataSource *)dataSource didSelectLink:(NSURL *)link {
    if (!link) {
        return;
    }
    
    [[UIApplication sharedApplication] openURL:link];
}

- (void)dataSourceDidSelectInfo {
    if ([DCNetworkCore sharedInstance].authProvider.isAdmin) {
        [self add];
        return;
    }
    
    NSString *text = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texts" ofType:@"plist"]][@"feed"];
    [self.navigationController pushViewController:[[DCAboutViewController alloc] initWithText:text] animated:YES];
}

- (void)scrollToTop {
    if (![self.dataSource numberOfSectionsInTableView:self.mainView.tableView]) {
        return;
    }
    
    if (![self.dataSource tableView:self.mainView.tableView numberOfRowsInSection:0]) {
        return;
    }
    [self.mainView.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

@end
