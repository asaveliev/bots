//
//  DCMaterialsView.h
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCMaterialsView : DCBaseView

@property (nonatomic, readonly) UITableView *tableView;

@end
