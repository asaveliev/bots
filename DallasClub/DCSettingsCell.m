
//
//  DCSettingsCell.m
//  DallasClub
//
//  Created by Anton on 19/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCSettingsCell.h"

@interface DCSettingsCell ()

@property (nonatomic, readonly) UIView *bottomLine;
@property (nonatomic, readonly) UIView *topLine;
@property (nonatomic, readonly) UIView *titleContainerView;

@end

@implementation DCSettingsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _titleContainerView = [UIView new];
        self.titleContainerView.backgroundColor = UIColorFromHex(0x2e3035);
        [self.contentView addSubview:self.titleContainerView];
        
        _titleLabel = [KILabel new];
        self.titleLabel.font = [UIFont regularFontOfSize:17.0];
        self.titleLabel.textColor = [UIColor whiteColor];
        [self.titleContainerView addSubview:self.titleLabel];
        
        _switchControl = [UISwitch new];
        [self.switchControl setOnTintColor:UIColorFromHex(0x737886)];
        [self.titleContainerView addSubview:self.switchControl];
        
        _subtitleLabel = [UILabel new];
        self.subtitleLabel.font = [UIFont regularFontOfSize:12.0];
        self.subtitleLabel.numberOfLines = 0;
        self.subtitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.subtitleLabel.textColor = UIColorFromHex(0x798f9b);
        [self.contentView addSubview:self.subtitleLabel];
        
        _bottomLine = [UIView new];
        [self.titleContainerView addSubview:self.bottomLine];
        
        _topLine = [UIView new];
        [self.titleContainerView addSubview:self.topLine];
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.topLine.backgroundColor = self.bottomLine.backgroundColor = [UIColor clearColor];
        
        self.contentView.backgroundColor = self.backgroundColor = UIColorFromHex(0x161616);
        
        [self setConstraints];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"container": self.titleContainerView,
                            @"title": self.titleLabel,
                            @"subtitle": self.subtitleLabel,
                            @"switch": self.switchControl,
                            @"bottom": self.bottomLine
                            };
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[container(48)]-15-[subtitle]-5-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[container]-0-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[subtitle]-15-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottom]-0-|" options:0 metrics:nil views:views]];
    
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[title(47)]-0-[bottom(0.5)]-0-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[title]-30-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[switch]-15-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleContainerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.switchControl attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

- (void)layoutSubviews {
    [super layoutSubviews];

    self.topLine.frame = (CGRect){ 0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 0.5 };
}

@end
