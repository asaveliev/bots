//
//  DCAudioStreamer.h
//  DallasClub
//
//  Created by Anton Savelev on 12.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCSlider.h"

@class STKAudioPlayer;
@protocol DCstreamerDelegate <NSObject>

- (void)streamerDidEnd;

@end

@interface DCAudioStreamer : NSObject

@property (nonatomic, strong)   NSString                    *playingAudioUrl;
@property (nonatomic, strong)   STKAudioPlayer              *player;
@property (nonatomic, strong)   DCSlider                    *slider;
@property (nonatomic, assign)   BOOL                        paused;
@property (nonatomic, weak)     id<DCstreamerDelegate>      delegate;

- (void)playAudioWithUrl:(NSString *)url;

@end
