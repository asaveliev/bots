//
//  NSDictionary+NSNullSafe.m
//  RCDOptics
//
//  Created by Anton on 07/04/16.
//  Copyright © 2016 Robot Dream. All rights reserved.
//

#import "NSDictionary+NSNullSafe.h"
#import "NSArray+NSNullSafe.h"

@implementation NSDictionary (NSNullSafe)

- (NSDictionary *)dictionaryByReplacingNull {
    NSMutableDictionary *result = [self mutableCopy];
    for (id key in self) {
        if (result[key] == [NSNull null]) {
            result[key] = @"";
        }
        else if ([result[key] isKindOfClass:[NSArray class]]) {
            result[key] = [result[key] arrayByReplacingNull];
        }
        else if ([result[key] isKindOfClass:[NSDictionary class]]) {
            result[key] = [result[key] dictionaryByReplacingNull];
        }
    }
    
    return result;
}

@end
