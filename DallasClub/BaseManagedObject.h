//
//  BaseManagedObject.h
//  StocksApp
//
//  Created by Anton on 09/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <CoreData/CoreData.h>

@protocol Serializable <NSObject>

-(void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation;

@end

@interface BaseManagedObject : NSManagedObject

+ (NSString *)entityName;
+ (instancetype)createInContext:(NSManagedObjectContext *)context;
+ (instancetype)getByField:(NSString *)fieldName equalsTo:(id)value fromContext:(NSManagedObjectContext *)context;
+ (NSArray *)getAllByField:(NSString *)fieldName equalsTo:(id)value fromContext:(NSManagedObjectContext *)context;
+ (NSArray *)getAllInContext:(NSManagedObjectContext *)context;

@end
