//
//  ShopColectionViewCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SDWebImage

class OfferBadge: UIView {
    
    lazy var percentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 11.0)
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(percentLabel)
        
        backgroundColor = UIColor.tkColorFromHex(0xFF2D55)
        clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = bounds.height / 2.0
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        percentLabel.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ShopCollectionViewCell: UICollectionViewCell, AdapterProtocol {
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let cellAdapter = adapter as? GoodAdapter else {
                return
            }
            
            titleLabel.text = cellAdapter.subtitle
            priceLabel.attributedText = cellAdapter.price
            typeLabel.text = cellAdapter.title
            
            badge.isHidden = cellAdapter.hasOffer == false
            badge.percentLabel.text = cellAdapter.percentOffer
            
            if let url = URL(string: cellAdapter.imageUrl) {
                titleImageView.sd_setImage(with: url)
            }
        }
    }
    
    lazy var titleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        imageView.layer.cornerRadius = 4.0
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.tkColorFromHex(0x8A8A8F)
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15.0)
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        return label
    }()
    
    lazy var badge: OfferBadge = OfferBadge()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(titleImageView)
        contentView.addSubview(typeLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(badge)
    }
    
    override open class var requiresConstraintBasedLayout: Bool {
        return true
    }
    
    override func updateConstraints() {
        titleImageView.snp.remakeConstraints { [unowned self] make in
            make.width.equalTo(self.contentView)
            make.height.equalTo(self.titleImageView.snp.width)
            make.top.equalTo(self.contentView)
            make.leading.trailing.equalTo(self.contentView)
        }
        
        typeLabel.snp.remakeConstraints { [unowned self] make in
            make.leading.trailing.equalTo(self.contentView)
            make.top.equalTo(self.titleImageView.snp.bottom).offset(2.0)
        }
        
        titleLabel.snp.remakeConstraints { [unowned self] make in
            make.leading.trailing.equalTo(self.contentView)
            make.top.equalTo(self.typeLabel.snp.bottom).offset(2.0)
        }
        
        priceLabel.snp.remakeConstraints { [unowned self] make in
            make.leading.trailing.equalTo(self.contentView)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(2.0)
        }
        
        badge.snp.remakeConstraints { make in
            make.top.leading.equalToSuperview().inset(7)
            make.height.width.equalTo(33)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
