//
//  DCMaterialsTableViewCell.h
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCMaterialsTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel       *title;
@property (nonatomic, strong) UILabel       *subtitle;
@property (nonatomic, strong) UIImageView   *thumbImageView;
@property (nonatomic, strong) UIView        *separatorView;

@end
