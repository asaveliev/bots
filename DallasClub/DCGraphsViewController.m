//
//  DCGraphsViewController.m
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCGraphsViewController.h"
#import "DCGraphicsContentView.h"
#import "DCMonthControlViewModel.h"
#import "NSDate+Utilities.h"
#import "DCStatisticsDataSource.h"
#import "SVProgressHUD.h"
#import "Capper.h"

@interface DCGraphsViewController () <DCGraphicsContentViewDelegate, DCStatisticsDataSourceDelegate>

@property (strong, nonatomic) DCGraphicsContentView     *contentView;

@property (strong, nonatomic) DCStatisticsDataSource    *dataSource;
@property (readonly, nonatomic) NSString                *capperName;
@property (strong, nonatomic) NSArray                   *models;
@property (nonatomic, readonly) Capper                  *capper;

@end

@implementation DCGraphsViewController

- (instancetype)initWithCapper:(Capper *)capper {
    _capper = capper;
    _capperName = capper.name;
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)loadView {
    self.view = _contentView = [DCGraphicsContentView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.contentView.headerView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.contentView.monthsControl.models = [self models];
    
    [self.contentView.segmentedControl addTarget:self action:@selector(changeMode:) forControlEvents:UIControlEventValueChanged];
    
    _dataSource = [[DCStatisticsDataSource alloc] initWithTableView:self.contentView.tableView graphView:self.contentView.graphView capper:self.capper];
    self.dataSource.delegate = self;
    self.contentView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSInteger index = [NSDate date].month - 1 < 12 ? [NSDate date].month : ([NSDate date].month - 1) % 12 + 1;
    self.contentView.monthsControl.selectedIndex = index + 13;
}

- (NSArray *)models {
    if (_models) {
        return _models;
    }
    
    NSDateComponents *components = [NSDateComponents new];
    components.month = 1;
    components.year = [NSDate date].year - 1;
    NSMutableArray *monthModels = [NSMutableArray new];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *date = [calendar dateFromComponents:components];
    for (int i = 0; i < 24; i++) {
        if (i % 12 == 0) {
            DCMonthControlViewModel *yearModel = [[DCMonthControlViewModel alloc] initWithDate:date];
            yearModel.selectable = NO;
            yearModel.type = DCMonthControlElementTypeYear;
            [monthModels addObject:yearModel];
        }
        [monthModels addObject:[[DCMonthControlViewModel alloc] initWithDate:date]];
        
        date = [date dateByAddingMonths:1];
    }
    
    _models = monthModels;
    return monthModels;
}

- (void)changeMode:(UISegmentedControl *)sender {
    [SVProgressHUD show];
    
    self.dataSource.mode = sender.selectedSegmentIndex;
    
    self.contentView.monthsHeightConstraint.constant = self.contentView.segmentedControl.selectedSegmentIndex == 1 ? 0 : 40;
    [self.contentView updateConstraints];
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
}

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - DCGraphicsContentViewDelegate

- (void)graphsViewDidSelectMonthAtIndex:(NSInteger)month andGraphElementAtIndex:(NSInteger)element {
    DCMonthControlViewModel *selectedModel = self.models[month];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    components.month = selectedModel.date.month;
    components.year = selectedModel.date.year;
    components.day = element == NSNotFound ? 1 : element + 1;
    
    [SVProgressHUD show];
    self.dataSource.selectedDay = [calendar dateFromComponents:components];
}

- (BOOL)graphViewCanSelectElementAtIndex:(NSInteger)index {
    return self.dataSource.mode == DCGraphModeDays || index % 13 != 0;
}

#pragma mark - DCStatisticsDataSourceDelegate

- (void)statisticsDataSourceDidUpdate {
    [SVProgressHUD dismiss];
    
    self.contentView.monthsControl.hidden = self.contentView.segmentedControl.selectedSegmentIndex == 1;
}

@end

