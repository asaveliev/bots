//
//  DCGraphicsContentView.h
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"
#import "DCGraphView.h"
#import "DCMonthsControl.h"

@protocol DCGraphicsContentViewDelegate <NSObject>

- (void)graphsViewDidSelectMonthAtIndex:(NSInteger)month andGraphElementAtIndex:(NSInteger)element;
- (BOOL)graphViewCanSelectElementAtIndex:(NSInteger)index;

@end

@interface DCGraphicsContentView : DCBaseView

@property (strong, nonatomic) DCGraphView           *graphView;
@property (strong, nonatomic) DCMonthsControl       *monthsControl;
@property (strong, nonatomic) UITableView           *tableView;
@property (strong, nonatomic) UISegmentedControl    *segmentedControl;

@property (strong, nonatomic) NSLayoutConstraint    *monthsHeightConstraint;

@property (weak, nonatomic)   id<DCGraphicsContentViewDelegate> delegate;

@end
