//
//  NSDateFormatter+ISO8601.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#define AM_PM_FORMAT @"a"

#define MILLISECOND_OF_DAY_FORMAT @"A"

#define DAY_OF_WEEK_FORMAT_NUM @"e"
#define DAY_OF_WEEK_FORMAT_ABR @"EEE"
#define DAY_OF_WEEK_FORMAT @"EEEE"

#define DAY_OF_MONTH_FORMAT_NUM @"dd"
#define DAY_OF_YEAR_FORMAT @"D"
#define WEEK_OF_MONTH_FORMAT @"F"

#define JULIAN_DAY_FORMAT_NUM @"g"
#define BC_AD_FORMAT_ABR @"GGG"
#define BC_AD_FORMAT_FULL @"GGGG"

#define HOUR_FORMAT @"h"
#define HOUR_MILITARY_FORMAT @"HH"
#define MINUTE_FORMAT @"mm"
#define SECOND_FORMAT @"ss"

#define MONTH_FORMAT_NUM @"LL"
#define MONTH_FORMAT_ABR @"LLL"
#define MONTH_FORMAT @"LLLL"

#define QUARTER_FORMAT_NUM @"qq"
#define QUARTER_FORMAT_ABR @"qqq"
#define QUARTER_FORMAT_FULL @"qqqq"

#define YEAR_FORMAT_2 @"yy"
#define YEAR_FORMAT_4 @"yyyy"

#define TIMEZONE_FORMAT_NUM @"Z"
#define TIMEZONE_FORMAT_ABR @"zz"
#define TIMEZONE_FORMAT @"zzzz"

#import "NSDateFormatter+ISO8601.h"

@implementation NSDateFormatter (ISO8601)

+(id)newWithFormat:(NSString*)format, ... {
    va_list ap;
    va_start(ap, format);
    NSString *formatterString = [[NSString alloc] initWithFormat:format arguments:ap];
    va_end(ap);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatterString];
    
    return formatter;
}

+(id)newForISO8601 {
    // 2016-07-25T20:54:54.562Z
   // NSLog(@"%@-%@-%@'T'%@:%@.%@%@", YEAR_FORMAT_4, MONTH_FORMAT_NUM, DAY_OF_MONTH_FORMAT_NUM, HOUR_MILITARY_FORMAT, MINUTE_FORMAT, SECOND_FORMAT, TIMEZONE_FORMAT_NUM);
    return [NSDateFormatter newWithFormat:@"%@-%@-%@'T'%@:%@:%@.SSSZZZZ", YEAR_FORMAT_4, MONTH_FORMAT_NUM, DAY_OF_MONTH_FORMAT_NUM, HOUR_MILITARY_FORMAT, MINUTE_FORMAT, SECOND_FORMAT, TIMEZONE_FORMAT];
}

+(NSString*)stringFromDateForISO8601:(NSDate*)date {
    NSDateFormatter *formatter = [NSDateFormatter newForISO8601];
    NSString *string = [formatter stringFromDate:date];
    return string;
}

+(NSDate*)dateFromStringForISO8601:(NSString*)dateString {
    NSDateFormatter *formatter = [NSDateFormatter newForISO8601];
    NSDate *date = [formatter dateFromString:dateString];
    return date;
}

@end
