//
//  DCStatisticsView.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatisticsView.h"

@implementation DCStatisticsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor =  UIColorFromHex(0x161616);
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentView addSubview:self.tableView];
        
        _tableHeader = [[DCTableHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width - 24.0, 0)];
        self.tableView.tableHeaderView = self.tableHeader;
        
        [self.rightButton setImage:[UIImage imageNamed:@"plus_icon"] forState:UIControlStateNormal];
        [self.leftButton setImage:[UIImage imageNamed:@"info_icon"] forState:UIControlStateNormal];
        
        [self setConstraints];
        self.backgroundColor = UIColorFromHex(0x161616);
       // self.tableView.contentInset = UIEdgeInsetsMake(0, 0, -20, 0);
    }
    
    return self;
}

- (void)setConstraints {
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[v]-0-|" options:0 metrics:nil views:@{ @"v": self.tableView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[v]-49-|" options:0 metrics:nil views:@{ @"v": self.tableView }]];
}

@end
