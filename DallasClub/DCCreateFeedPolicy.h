//
//  DCCreateFeedPolicy.h
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedPolicy.h"

@interface DCCreateFeedPolicy : DCFeedPolicy

@property (nonatomic, strong) NSData *audioData;

@end
