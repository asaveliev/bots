//
//  DCAddBetView.h
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"
#import "DCDateTextField.h"

@interface DCAddBetView : DCBaseView

@property (nonatomic, readonly) UILabel *statusTitleLabel;
@property (nonatomic, readonly) UILabel *percentTitleLabel;
@property (nonatomic, readonly) UILabel *numberTitleLabel;
@property (nonatomic, readonly) UILabel *coefTitleLabel;
@property (nonatomic, readonly) UILabel *dateTitleLabel;

@property (nonatomic, readonly) DCTextField     *statusTextField;
@property (nonatomic, readonly) DCTextField     *percentTextField;
@property (nonatomic, readonly) DCTextField     *numberTextField;
@property (nonatomic, readonly) DCTextField     *coefTextField;
@property (nonatomic, readonly) DCDateTextField *dateTextField;

@end
