//
//  DCAboutView.h
//  DallasClub
//
//  Created by Anton Savelev on 25/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCAboutView : DCBaseView

@property (nonatomic, readonly) UITableView *tableView;

@end
