//
//  SettingsIconCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 18.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class SettingsIconCell: UITableViewCell {
    
    lazy var arrowView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "elements_arrow_icon_right"))
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var topLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return view
    }()
    
    lazy var bottomLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 17.0)
        return label
    }()
    
    lazy var iconView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(containerView)
        let views = [
            topLine,
            bottomLine,
            titleLabel,
            iconView,
            arrowView
        ]
        views.forEach {
            containerView.addSubview($0)
        }
        
        backgroundColor = UIColor.tkColorFromHex(0x2e3035)
        contentView.backgroundColor = UIColor.tkColorFromHex(0x2e3035)
        selectionStyle = .none
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        arrowView.snp.remakeConstraints { make in
            make.width.equalTo(7)
            make.height.equalTo(12)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-16)
        }
        
        containerView.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.bottom.equalToSuperview().offset(-15)
            make.leading.trailing.equalToSuperview()
        }
        
        iconView.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview().inset(6)
            make.width.height.equalTo(29)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.leading.equalTo(iconView.snp.trailing).offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.bottom.equalToSuperview().inset(11)
        }
        
        let divederHeight = 1 / UIScreen.main.scale
        topLine.snp.remakeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(divederHeight)
        }
        
        bottomLine.snp.remakeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(divederHeight)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
