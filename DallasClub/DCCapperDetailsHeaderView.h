//
//  DCCapperDetailsHeaderView.h
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HMSegmentedControl/HMSegmentedControl.h>

@interface DCCapperDetailsHeaderView : UIView

@property (nonatomic, readonly) UIImageView         *avatarImage;
@property (nonatomic, readonly) UILabel             *titleLabel;
@property (nonatomic, readonly) UILabel             *descriptionLabel;
@property (nonatomic, readonly) HMSegmentedControl  *segmentedControl;

@end
