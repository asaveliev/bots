//
//  DCAddFeedViewController.h
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DCFeedPolicy;

@interface DCAddFeedViewController : UIViewController

- (instancetype)initWithPolicy:(DCFeedPolicy *)policy;

@end
