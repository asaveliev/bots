//
//  Bet.m
//  
//
//  Created by Anton on 24/07/16.
//
//

#import "Bet.h"
#import "NSDateFormatter+ISO8601.h"
#import "NSDate+Utilities.h"

@implementation Bet

- (void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    self.created = [NSDateFormatter dateFromStringForISO8601:dictionaryRepresentation[@"date"]];
    self.uid = dictionaryRepresentation[@"id"];
    self.number = @([dictionaryRepresentation[@"number"] doubleValue]);
    self.coefficient = @([dictionaryRepresentation[@"coefficient"] doubleValue]);
    self.amout = @([dictionaryRepresentation[@"amount"] doubleValue]);
    self.balance_change = @([dictionaryRepresentation[@"balance_change"] doubleValue]);
    self.statusString = dictionaryRepresentation[@"status"];
    self.capper_name = dictionaryRepresentation[@"capper_name"];
    
    self.ordering = [self.created stringWithFormat:@"yyyy-MM-dd"];
}

- (void)setStatusString:(NSString *)statusString {
    /*successful: ставка прошла
     failed: ставка не прошла
     returned: ставка отменена*/
    
    if ([statusString isEqualToString:@"successful"]) {
        self.state = @(DCBetStateSuccess);
    } else if ([statusString isEqualToString:@"failed"]) {
        self.state = @(DCBetStateFailed);
    } else {
        self.state = @(DCBetStateReturned);
    }
}

- (NSString *)statusString {
    NSInteger stateValue = self.state.integerValue;
    
    switch (stateValue) {
        case DCBetStateSuccess:
            return @"successful";
            break;
        case DCBetStateFailed:
            return @"failed";
            break;
        case DCBetStateReturned:
            return @"returned";
            break;
        default:
            break;
    }
    
    return nil;
}

- (NSString *)readableStatus {
    NSInteger stateValue = self.state.integerValue;
    
    switch (stateValue) {
        case DCBetStateSuccess:
            return @"Cтавка прошла 💪";
            break;
        case DCBetStateFailed:
            return @"Cтавка не прошла 😢";
            break;
        case DCBetStateReturned:
            return @"Возврат 🤔";
            break;
        default:
            break;
    }
    
    return nil;
}

// Insert code here to add functionality to your managed object subclass

@end
