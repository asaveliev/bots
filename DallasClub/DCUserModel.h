//
//  DCUserModel.h
//  DallasClub
//
//  Created by Anton Savelev on 11.06.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "BaseManagedObject.h"
#import <Foundation/Foundation.h>

@interface DCUserModel : NSObject <Serializable, NSSecureCoding>

@property (nonatomic, readonly) NSNumber    *userId;
@property (nonatomic, readonly) NSNumber    *isSuperUser;
@property (nonatomic, readonly) NSString    *avatarUrl;
@property (nonatomic, readonly) NSString    *name;
@property (nonatomic, strong) NSString      *password;
@property (nonatomic, readwrite) NSString   *token;
@property (nonatomic, strong) NSString      *login;
@property (nonatomic, strong) NSNumber      *isCapper;

- (void)removeFromKeychain;

@end
