//
//  DCTabBarController.h
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTabBarController : UITabBarController

extern NSString *const DCUpdateTabBarNotification;

@end
