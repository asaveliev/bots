//
//  DCClassicHeaderView.m
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCClassicHeaderView.h"

@interface DCClassicHeaderView ()

@property (strong, nonatomic) UIView *bottomLine;

@end

@implementation DCClassicHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _bottomLine = [UIView new];
        self.bottomLine.backgroundColor = UIColorFromHex(0xC3CDD4);
        [self addSubview:self.bottomLine];
        self.bottomLine.translatesAutoresizingMaskIntoConstraints = NO;
        [self.leftButton setContentEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
        [self.leftButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, -10.0, 0.0, 0.0)];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[line]-0-|" options:0 metrics:nil views:@{ @"line": self.bottomLine }]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[line(1)]-0-|" options:0 metrics:nil views:@{ @"line": self.bottomLine }]];
        
        self.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
    
    return self;
}

@end
