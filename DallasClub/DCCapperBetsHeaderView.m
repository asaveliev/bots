//
//  DCCapperBetsHeaderView.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCapperBetsHeaderView.h"

@implementation DCCapperBetsHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _headerView = [DCTableHeaderView new];
        [self.contentView addSubview:self.headerView];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    self.headerView.translatesAutoresizingMaskIntoConstraints = NO;
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[v]-12-|" options:0 metrics:nil views:@{ @"v": self.headerView }]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[v]-25-|" options:0 metrics:nil views:@{ @"v": self.headerView }]];
}

@end
