//
//  UIFont+DCFonts.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "UIFont+DCFonts.h"

@implementation UIFont (DCFonts)

+ (instancetype)statsTitleFontRegular {
    return [UIFont fontWithName:@"SFUIText-Regular" size:15.0];
}

+ (instancetype)statsTitleFontMedium {
    return [UIFont fontWithName:@"SFUIText-Medium" size:15.0];
}

+ (instancetype)statsPriceFont {
    return [UIFont fontWithName:@"SFUIText-Regular" size:18.0];
}

+ (instancetype)statsPercentFont {
    return [UIFont fontWithName:@"SFUIText-Medium" size:12.0];
}

+ (instancetype)statsHeaderTitleFont {
    return [UIFont systemFontOfSize:42.0 weight:UIFontWeightLight];
}

+ (instancetype)statsHeaderSubtitleFont {
    return [UIFont fontWithName:@"SFUIText-Regular" size:12.0];
}

+ (instancetype)statsHeaderLabelsFont {
    return [UIFont fontWithName:@"SFUIText-Regular" size:16.0];
}

+ (instancetype)headerButtonsFont {
    return [UIFont fontWithName:@"SFUIText-Regular" size:17.0];
}

+ (instancetype)headerViewTitleFont {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:17.0];
}

+ (instancetype)sectionHeaderFont {
    return [UIFont fontWithName:@"SFUIText-Semibold" size:11.0];
}

+ (instancetype)feedFont {
    return [self statsTitleFontRegular];
}

+ (instancetype)regularFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIText-Regular" size:size];
}

+ (instancetype)lightFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIDisplay-Light" size:size];
}

+ (instancetype)ultralightFontOfSize:(CGFloat)size {
    return [UIFont fontWithName:@"SFUIDisplay-Ultralight" size:size];
}

@end
