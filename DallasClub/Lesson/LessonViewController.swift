//
//  LessonViewController.swift
//  DallasClub
//
//  Created by Anton Savelev on 31.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import Pageboy

class LessonViewController: UIViewController, PageboyViewControllerDataSource, PageboyViewControllerDelegate {
    let mainView = LessonView()
    let model: Lesson
    let pageController = PageboyViewController()
    let controllers: [StepViewController]
    var currentPageIndex: Int = 0
    let completionProvider = CompleteLessonsProvider()
    
    init(withModel model: Lesson) {
        self.model = model
        
        if let steps = model.steps {
            controllers = steps.sorted(by: { ($0 as! Step).ordering!.intValue < ($1 as! Step).ordering!.intValue })
                .flatMap {
                    print(($0 as! Step).ordering!)
                    return StepViewController(withModel: $0 as! Step)
            }
        } else {
            controllers = []
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.headerTitleLabel.text = model.title
        mainView.leftButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        if let steps = model.steps {
            let highlighted = steps.sorted(by: { ($0 as! Step).ordering!.intValue < ($1 as! Step).ordering!.intValue }).flatMap {
                ($0 as! Step).completed?.boolValue == true ? ($0 as! Step).ordering?.intValue : nil
            }
            mainView.pageControl = PageControl(indicatorsCount: model.steps?.count ?? 0, highlighted: highlighted)
        } else {
            mainView.pageControl = PageControl(indicatorsCount: model.steps?.count ?? 0, highlighted: [])
        }
        mainView.contentView.addSubview(mainView.pageControl!)
        
        pageController.willMove(toParentViewController: self)
        addChildViewController(pageController)
        mainView.contentView.addSubview(pageController.view)
        pageController.view.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-64)
            make.top.equalTo(mainView.pageControl!.snp.bottom)
        }
        mainView.pageControl?.snp.remakeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(20)
        }
        pageController.dataSource = self
        pageController.delegate = self
        pageController.didMove(toParentViewController: self)
        
        mainView.nextButton.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        
        if let color = model.purchase?.color {
            let selectedColor = UIColor.hexStringToUIColor(hex: color)
            mainView.pageControl?.selectedColor = selectedColor
        }
    }
    
    @objc private func backAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func nextAction() {
        if currentPageIndex + 1 == model.steps?.count {
            if
                let steps = model.steps,
                let step = steps.sorted(by: { ($0 as! Step).ordering!.intValue < ($1 as! Step).ordering!.intValue })[currentPageIndex] as? Step,
                step.completed?.boolValue == false
            {
                completionProvider.markStepAsComplete(byId: step.id)
                DCPersistentStack.sharedInstance().context.perform {
                    step.completed = NSNumber(value: true)
                    if let currentCompleted = step.lesson?.itemsCompleted?.intValue {
                        step.lesson?.itemsCompleted = NSNumber(value: currentCompleted + 1)
                    } else {
                        step.lesson?.itemsCompleted = NSNumber(value: 1)
                    }
                    
                    try? DCPersistentStack.sharedInstance().context.save()
                }
            }
            _ = navigationController?.popViewController(animated: true)
            return
        }
        
        pageController.scrollToPage(.next, animated: true)
    }
    
    func pageboyViewController(_ pageboyViewController: PageboyViewController, didScrollToPageAtIndex index: Int, direction: PageboyViewController.NavigationDirection, animated: Bool) {
        mainView.pageControl?.selectedIndex = index
        if let steps = model.steps {
            let step = steps.sorted(by: { ($0 as! Step).ordering!.intValue < ($1 as! Step).ordering!.intValue })[currentPageIndex] as! Step
            if step.completed?.boolValue == false {
                mainView.pageControl?.highlighted.append(currentPageIndex)
                mainView.pageControl?.updateAppearance()
                completionProvider.markStepAsComplete(byId: step.id)
                
                DCPersistentStack.sharedInstance().context.perform {
                    step.completed = NSNumber(value: true)
                    if let currentCompleted = step.lesson?.itemsCompleted?.intValue {
                        step.lesson?.itemsCompleted = NSNumber(value: currentCompleted + 1)
                    } else {
                        step.lesson?.itemsCompleted = NSNumber(value: 1)
                    }
                    
                    try? DCPersistentStack.sharedInstance().context.save()
                }
            }
        }
        currentPageIndex = index
    }
    
    func viewControllers(forPageboyViewController pageboyViewController: PageboyViewController) -> [UIViewController]? {
        return controllers
    }
    
    func defaultPageIndex(forPageboyViewController pageboyViewController: PageboyViewController) -> PageboyViewController.PageIndex? {
        // use default index
        return nil
    }
}
