//
//  PageControl.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PageControl: UIView {
    
    var selectedColor = UIColor.tkColorFromHex(0x4C4079) {
        didSet {
            updateAppearance()
        }
    }
    private let highlightedColor = UIColor.tkColorFromHex(0x4CAF50)
    private let defaultColor = UIColor.tkColorFromHex(0x3C93CE)
    
    let buttons: [UIButton]
    var highlighted: [Int]
    var selectedIndex: Int = 0 {
        didSet {
            updateAppearance()
        }
    }
    
    init(indicatorsCount: Int, highlighted: [Int]) {
        self.highlighted = highlighted
        var buttons = [UIButton]()
        if indicatorsCount <= 0 {
            self.buttons = buttons
            super.init(frame: CGRect.zero)
            return
        }
        for _ in 0...indicatorsCount - 1 {
            let button = UIButton()
            button.clipsToBounds = true
            button.layer.cornerRadius = 2.0
            buttons.append(button)
            
        }
        self.buttons = buttons
        super.init(frame: CGRect.zero)
        
        buttons.forEach {
            addSubview($0)
        }
        updateAppearance()
        
        translatesAutoresizingMaskIntoConstraints = false
        setNeedsUpdateConstraints()
    }
    
    func updateAppearance() {
        buttons.enumerated().forEach { index, button in
            if index == selectedIndex {
                button.backgroundColor = selectedColor
                return
            }
            
            button.backgroundColor = highlighted.contains(index) ? highlightedColor : defaultColor
        }
    }
    
    override func updateConstraints() {
        buttons.enumerated().forEach { index, button in
            button.snp.remakeConstraints { make in
                make.height.equalTo(4)
                make.centerY.equalToSuperview()
                if index == 0 {
                    make.leading.equalToSuperview().offset(11)
                    return
                }
                
                let prevButton = buttons[index - 1]
                make.leading.equalTo(prevButton.snp.trailing).offset(12)
                make.width.equalTo(prevButton)
                
                if index + 1 == buttons.count {
                    make.trailing.equalToSuperview().offset(-11)
                }
                
            }
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
