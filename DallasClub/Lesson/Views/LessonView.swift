//
//  LessonView.swift
//  DallasClub
//
//  Created by Anton Savelev on 31.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class LessonView: DCBaseView {
    
    var pageControl: PageControl?
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.setTitle("Я все понял".uppercased(), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.black, for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.tkColorFromHex(0x6D5CAE)
        headerView.headerTitleLabel.textColor = UIColor.white
        headerView.leftButton.tintColor = UIColor.white
        headerView.leftButton.setImage(#imageLiteral(resourceName: "back_button.png").withRenderingMode(.alwaysTemplate), for: .normal)
        
        contentView.addSubview(nextButton)
    }
    
    override func updateConstraints() {
        nextButton.snp.remakeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(64)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
