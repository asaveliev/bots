//
//  StepViewController.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import RMUniversalAlert
import MediaPlayer

class StepView: TableContentView {
    override func headerViewClass() -> AnyClass! {
        return nil
    }
}

class StepViewController: UIViewController {
    let mainView = StepView()
    var dataSource: StepDataSource?
    let model: Step
    
    let banProvider = BanProvider()
    
    init(withModel model: Step) {
        self.model = model
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var adapters = [TKBaseAdapter]()
        if model.imageUrl != nil {
            let imageAdapter = StepImageAdapter(withImage: nil)
            adapters += [
                imageAdapter
            ]
        }
        if model.thumbnailUrl != nil {
            let thumbnailAdapter = StepVideoAdapter(withImage: nil, cellClass: StepVideoCell.self)
            adapters += [
                thumbnailAdapter
            ]
        }
        if model.audioUrl != nil {
            let audioAdapter = TKBaseAdapter(model: model.audioUrl, cellClass: StepAudioCell.self)
            adapters += [
                audioAdapter
            ]
        }
        adapters += [
            StepTextAdapter(model: model, cellClass: StepTextCell.self)
        ]
        dataSource = StepDataSource(tableView: mainView.tableView, adapters: adapters)
        dataSource?.delegate = self
        
        loadMedia()
        
       // NotificationCenter.default.addObserver(self, selector: #selector(screenDidConnected), name: NSNotification.Name.UIScreenDidConnect, object: nil)
        let mainQueue = OperationQueue.main
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationUserDidTakeScreenshot,
                                               object: nil,
                                               queue: mainQueue,
                                               using: { notification in
                                                self.banProvider.warnUser(completion: { count in
                                                    if count > 1 {
                                                        RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Ваш аккаунт забанен. По всем вопросам пишите в группу ВК.", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                                                        _ = self.navigationController?.popViewController(animated: true)
                                                        return
                                                    }
                                                    RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Предупреждение! За повторный скриншот ваш аккаунт будет забанен!", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                                                })
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        restrictRotation(false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        dataSource?.streamer.paused = true
        dataSource?.streamer.playingAudioUrl = nil
        
        mainView.tableView.reloadData()
    }
    
    func screenDidConnected() {
        RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Ваш аккаунт забанен. По всем вопросам пишите в группу ВК.", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func loadMedia() {
        if
            let imageUrlString = model.imageUrl,
            let imageUrl = URL(string: imageUrlString),
            let adapters = self.dataSource?.adapters
        {
            let imageAdapter = StepImageAdapter(withImage: nil)
            SDWebImageManager.shared().downloadImage(with: imageUrl, options: [], progress: nil, completed: { (image, _, _, _, _) in
                guard let image = image else {
                    return
                }
                
                let updatedAdapter = StepImageAdapter(withImage: image)
                self.dataSource?.adapters = self.replace(adapter: imageAdapter, withAdapter: updatedAdapter, inAdapters: adapters)
                self.reloadAdapter(withType: StepImageAdapter.self)
            })
        }
        
        if
            let imageUrlString = model.thumbnailUrl,
            let imageUrl = URL(string: imageUrlString),
            let adapters = self.dataSource?.adapters
        {
            let imageAdapter = StepVideoAdapter(withImage: nil)
            SDWebImageManager.shared().downloadImage(with: imageUrl, options: [], progress: nil, completed: { (image, _, _, _, _) in
                guard let image = image else {
                    return
                }
                
                let updatedAdapter = StepVideoAdapter(withImage: image, cellClass: StepVideoCell.self)
                self.dataSource?.adapters = self.replace(adapter: imageAdapter, withAdapter: updatedAdapter, inAdapters: adapters)
                self.reloadAdapter(withType: StepVideoAdapter.self)
            })
        }
    }
    
    func reloadAdapter(withType: TKBaseAdapter.Type) {
        guard let index = dataSource?.adapters.index(where: { type(of: $0) == withType }) else {
            return
        }
        mainView.tableView.beginUpdates()
        mainView.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        mainView.tableView.endUpdates()
    }
    
    func replace(adapter: TKBaseAdapter, withAdapter otherAdapter: TKBaseAdapter, inAdapters: [TKBaseAdapter]) -> [TKBaseAdapter] {
        return inAdapters.flatMap { existingAdapter -> TKBaseAdapter in
            if type(of: adapter) == type(of: existingAdapter) {
                return otherAdapter
            }
            return existingAdapter
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        mainView.backgroundColor = UIColor.clear
        mainView.tableView.backgroundColor = UIColor.clear
        mainView.tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        mainView.tableView.allowsSelection = true
        view = mainView
    }
}

extension StepViewController: ResultsDataSourceDelegate {
    func didSelectRow(at indexPath: IndexPath) {
        guard
            let videoIndex = dataSource?.adapters.index(where: { $0.cellClass == StepVideoCell.self }),
            let videoString = model.videoUrl,
            let videoUrl = URL(string: videoString),
            videoIndex == indexPath.row,
            let vc = MPMoviePlayerViewController(contentURL: videoUrl)
        else {
            return
        }
        
        restrictRotation(true)
        navigationController?.presentMoviePlayerViewControllerAnimated(vc)
    }
}
