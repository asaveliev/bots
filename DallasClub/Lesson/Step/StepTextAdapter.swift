//
//  StepAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation
import Down

class StepTextAdapter: TKBaseAdapter {

    let attributedText: NSAttributedString
    
    required init(model: Any?, cellClass:UITableViewCell.Type) {
        if
            let model = model as? Step,
            let markdown = Optional<Down>(Down(markdownString: model.text ?? "")),
            let string = try? markdown.toAttributedString()
        {
            let mutableText = NSMutableAttributedString(attributedString: string)
            let range = NSRange(location: 0, length: mutableText.length)
            mutableText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: range)
            mutableText.enumerateAttribute(NSFontAttributeName, in: range, options: .longestEffectiveRangeNotRequired, using: { (value, range, stop) in
                guard let font = value as? UIFont else {
                    return
                }
                
                mutableText.removeAttribute(NSFontAttributeName, range: range)
                var newFont = font.withSize(16.0)
                var description = newFont.fontDescriptor
                description = description.addingAttributes([UIFontDescriptorFamilyAttribute: UIFont.systemFont(ofSize: newFont.pointSize).familyName])
                newFont = UIFont(descriptor: description, size: newFont.pointSize)
                mutableText.addAttribute(NSFontAttributeName, value: newFont, range: range)
            })
            mutableText.enumerateAttribute(NSLinkAttributeName, in: range, options: .longestEffectiveRangeNotRequired, using: { (value, range, stop) in
                mutableText.removeAttribute(NSForegroundColorAttributeName, range: range)
                mutableText.removeAttribute(NSUnderlineColorAttributeName, range: range)
                mutableText.removeAttribute(NSStrokeColorAttributeName, range: range)
                mutableText.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: range)
                mutableText.addAttribute(NSStrokeColorAttributeName, value: UIColor.white, range: range)
                mutableText.addAttribute(NSUnderlineColorAttributeName, value: UIColor.white, range: range)
            })
            
            attributedText = mutableText
        } else {
            attributedText = NSAttributedString()
        }
        
        super.init(model: model, cellClass: cellClass)
    }
    
}
