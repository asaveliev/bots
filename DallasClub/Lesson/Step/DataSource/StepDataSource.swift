//
//  StepDataSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class StepDataSource: ResultsDataSource, StepAudioCellDelegate {
    let streamer = DCAudioStreamer()
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let audioCell = cell as? StepAudioCell {
            audioCell.delegate = self
            if streamer.playingAudioUrl != nil {
                if audioCell.audioUrl == streamer.playingAudioUrl {
                    streamer.slider = audioCell.slider
                    audioCell.slider.isUserInteractionEnabled = true
                } else {
                    audioCell.slider.minLabel.text = nil
                    audioCell.slider.slider.value = 0.0
                    audioCell.playButton.isSelected = false
                }
            } else {
                audioCell.slider.minLabel.text = nil
                audioCell.slider.slider.value = 0.0
                audioCell.playButton.isSelected = false
                audioCell.playButton.imageView?.image = audioCell.playButton.image(for: .normal)
            }
        }
        
        return cell
    }
    
    func didPlay(url: String) {
        if streamer.playingAudioUrl != nil {
            if url == streamer.playingAudioUrl {
                streamer.paused = false
                return
            }
        }
        streamer.playAudio(withUrl: url)
        tableView.reloadData()
    }
    
    func didPause() {
        streamer.paused = true
    }
}
