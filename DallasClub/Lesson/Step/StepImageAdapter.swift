//
//  StepImageAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class StepImageAdapter: TKBaseAdapter {
    
    let image: UIImage?
    
    init(withImage: UIImage?, cellClass: UITableViewCell.Type = StepImageCell.self) {
        self.image = withImage
        
        super.init(model: nil, cellClass: cellClass)
    }
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        fatalError("init(model:cellClass:) has not been implemented")
    }
    
}

class StepVideoAdapter: StepImageAdapter {
    
}
