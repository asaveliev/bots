//
//  StepImageCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class StepImageCell: UITableViewCell, AdapterProtocol {
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard
                let imageAdapter = adapter as? StepImageAdapter,
                let image = imageAdapter.image
            else {
                setNeedsUpdateConstraints()
                return
            }
            
            pictureView.image = image
            setNeedsUpdateConstraints()
        }
    }
    
    let defaultRatio = 0.6
    
    lazy var pictureView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return imageView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(pictureView)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        guard
            let imageAdapter = adapter as? StepImageAdapter,
            let image = imageAdapter.image
        else {
            pictureView.snp.remakeConstraints { make in
                make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5.0, left: 15.0, bottom: 0.0, right: 15.0))
                make.height.equalTo(pictureView.snp.width).multipliedBy(defaultRatio)
            }
            super.updateConstraints()
            return
        }
        
        let imageRatio = image.size.height / image.size.width
        pictureView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5.0, left: 15.0, bottom: 0.0, right: 15.0))
            make.height.equalTo(pictureView.snp.width).multipliedBy(imageRatio)
        }        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
