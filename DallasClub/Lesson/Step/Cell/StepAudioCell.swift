//
//  StepAudioCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

protocol StepAudioCellDelegate: class {
    func didPlay(url: String)
    func didPause()
}

class StepAudioCell: UITableViewCell, AdapterProtocol {
    
    weak var delegate: StepAudioCellDelegate?
    var audioUrl: String?
    var adapter: TKBaseAdapter! {
        didSet {
            slider.slider.value = 0.0
            slider.slider.maximumValue = 1.0
            slider.slider.minimumValue = 0.0
            slider.slider.isUserInteractionEnabled = false
            slider.minLabel.isHidden = true
            slider.minLabel.text = "00:00"
            
            if let url = adapter.model as? String {
                audioUrl = url
            }
        }
    }
    
    lazy var playButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "audio_play_icon"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "audio_stop_icon"), for: .selected)
        return button
    }()
    
    lazy var slider: DCSlider = {
        let slider = DCSlider()
        slider.slider.minimumTrackTintColor = UIColor.tkColorFromHex(0x4C4079)
        slider.slider.maximumTrackTintColor = UIColor.white
        slider.minLabel.textColor = UIColor.white
        slider.minLabel.font = UIFont.systemFont(ofSize: 11.0)
        slider.minLabel.textAlignment = .right
        return slider
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(slider)
        contentView.addSubview(playButton)
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
        setNeedsUpdateConstraints()
        
        playButton.addTarget(self, action: #selector(playAction(sender:)), for: .touchUpInside)
    }
    
    override func updateConstraints() {
        
        playButton.snp.remakeConstraints { make in
            make.width.height.equalTo(30)
            make.leading.equalToSuperview().offset(13)
            make.centerY.equalToSuperview()
        }
        
        slider.snp.remakeConstraints { make in
            make.leading.equalTo(playButton.snp.trailing).offset(8)
            make.trailing.equalToSuperview().offset(-13)
            make.bottom.top.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    func playAction(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            delegate?.didPlay(url: audioUrl ?? "")
        } else {
            delegate?.didPause()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
