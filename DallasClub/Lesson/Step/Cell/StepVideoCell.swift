//
//  StepVideoCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class StepVideoCell: StepImageCell {
    
    lazy var playImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .center
        image.image = #imageLiteral(resourceName: "video_play_icon")
        return image
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(playImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        playImageView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
}
