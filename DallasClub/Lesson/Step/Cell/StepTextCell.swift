//
//  StepTextCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class StepTextCell: UITableViewCell, AdapterProtocol, TTTAttributedLabelDelegate {
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        UIApplication.shared.openURL(url)
    }
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let adapter = adapter as? StepTextAdapter else {
                return
            }
            
            titleLabel.attributedText = adapter.attributedText
        }
    }
    
    lazy var titleLabel: TTTAttributedLabel = {
        let label = TTTAttributedLabel(frame: CGRect.zero)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.inactiveLinkAttributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSStrokeColorAttributeName: UIColor.white
        ]
        label.linkAttributes = [
            NSForegroundColorAttributeName: UIColor.white,
        ]
        label.delegate = self
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(titleLabel)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        selectionStyle = .none
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        titleLabel.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(15)
        }
        
        super.updateConstraints()
    }
    
}
