//
//  Material+CoreDataClass.m
//  
//
//  Created by Anton Savelev on 07.05.17.
//
//

#import "Material.h"
#import "NSDateFormatter+ISO8601.h"

@implementation Material

- (void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    self.uid = dictionaryRepresentation[@"id"];
    self.caption = dictionaryRepresentation[@"caption"];
    self.image_url = dictionaryRepresentation[@"thumb_url"];
    self.url = dictionaryRepresentation[@"url"];
    self.created_at = [NSDateFormatter dateFromStringForISO8601:dictionaryRepresentation[@"created_at"]];
}

@end
