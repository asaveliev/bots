//
//  DCAuthProvider.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@class DCUserModel;

extern NSString *const DCUnathorizedNotificationKey;

@interface DCAuthProvider : DCApiProvider

@property (nonatomic, readonly) DCUserModel *currentUser;

@property (nonatomic, readonly) NSString    *token;
@property (nonatomic, readonly) BOOL        hasAuth;
@property (nonatomic, readonly) BOOL        isSuperAdmin;
@property (nonatomic, readonly) BOOL        isAdmin;
@property (nonatomic, readonly) NSArray     *users;
@property (nonatomic, assign)   NSInteger   userId;

- (void)removeUser:(DCUserModel *)model;
- (void)selectUser:(DCUserModel *)user completion:(void(^)(BOOL success))completion;
- (void)authWithLogin:(NSString *)login andPass:(NSString *)password completion:(void(^)(BOOL success))completion;
- (void)authUserWithLogin:(NSString *)login andPass:(NSString *)password completion:(void (^)(BOOL))completion;
- (void)registerUserWithLogin:(NSString *)login pass:(NSString *)password name:(NSString *)name completion:(void (^)(BOOL, NSString*))completion;
- (void)authorize;

@end
