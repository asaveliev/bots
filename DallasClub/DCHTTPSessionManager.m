//
//  DCHTTPSessionManager.m
//  DallasClub
//
//  Created by Anton on 07/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCHTTPSessionManager.h"

#import "NSArray+NSNullSafe.h"
#import "NSDictionary+NSNullSafe.h"

@implementation DCHTTPSessionManager

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request uploadProgress:(void (^ _Nullable)(NSProgress * _Nonnull))uploadProgressBlock downloadProgress:(void (^)(NSProgress * _Nonnull))downloadProgressBlock completionHandler:(void (^)(NSURLResponse * _Nonnull, id _Nullable, NSError * _Nullable))completionHandler {
    
    void(^completion)(NSURLResponse * _Nonnull, id _Nullable, NSError * _Nullable) = ^void(NSURLResponse * reponse, id responseObject, NSError * error) {
        if ([responseObject isKindOfClass:[NSArray class]]) {
            responseObject = [responseObject arrayByReplacingNull];
        } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
            responseObject = [responseObject dictionaryByReplacingNull];
        }
        
        if (completionHandler) {
            completionHandler(reponse, responseObject, error);
        }
    };
    
    return [super dataTaskWithRequest:request uploadProgress:uploadProgressBlock downloadProgress:downloadProgressBlock completionHandler:completion];
}

@end
