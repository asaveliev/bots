//
//  NSArray+NSNullSafe.m
//  RCDOptics
//
//  Created by Anton on 07/04/16.
//  Copyright © 2016 Robot Dream. All rights reserved.
//

#import "NSArray+NSNullSafe.h"
#import "NSDictionary+NSNullSafe.h"

@implementation NSArray (NSNullSafe)

- (NSArray *)arrayByReplacingNull {
    NSMutableArray *result = [self mutableCopy];
    
    for (int i = 0; i < result.count; i++) {
        if (result[i] == [NSNull null]) {
            result[i] = @"";
        }
        else if ([result[i] isKindOfClass:[NSArray class]]) {
            result[i] = [result[i] arrayByReplacingNull];
        }
        else if ([result[i] isKindOfClass:[NSDictionary class]]) {
            result[i] = [result[i] dictionaryByReplacingNull];
        }
    }
    
    return  result;
}

@end
