//
//  DCNetworkingConsoleLogger.h
//  DallasClub
//
//  Created by Anton Savelev on 22.11.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>

@interface DCNetworkingConsoleLogger : NSObject <AFNetworkActivityLoggerProtocol>

@property (nonatomic, strong) NSPredicate *filterPredicate;
@property (nonatomic, assign) AFHTTPRequestLoggerLevel level;


@end
