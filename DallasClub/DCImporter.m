//
//  DCImporter.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCImporter.h"
#import "DCFethFeedOperation.h"
#import "DCFetchBetsOperation.h"
#import "DCFetchAccountDetailsOperation.h"
#import "DCFetchCappersOperation.h"
#import "DCFetchMaterialsOperation.h"

@interface DCImporter ()

@property (strong, nonatomic) NSOperationQueue  *queue;

@property (strong, nonatomic) NSNumber          *currentFeedPage;
@property (strong, nonatomic) NSNumber          *currentBetsPage;

@end

@implementation DCImporter

+ (instancetype)importer {
    static DCImporter *importer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        importer = [DCImporter new];
    });
    
    return importer;
}

+ (instancetype)betsImporter {
    static DCImporter *importer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        importer = [DCImporter new];
    });
    
    return importer;
}

+ (instancetype)feedImporter {
    static DCImporter *importer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        importer = [DCImporter new];
    });
    
    return importer;
}

- (instancetype)init {
    if (self = [super init]) {
        _queue = [NSOperationQueue new];
        _currentFeedPage = @1;
        _currentBetsPage = @1;
    }
    
    return self;
}

- (void)updateFeed {
    [self updateFeed:nil];
}

- (void)updateFeed:(void (^ _Nullable)())completion {
    _currentFeedPage = @1;
    DCFethFeedOperation *feed = [DCFethFeedOperation new];
    [feed setCompletionBlock:completion];
    [self.queue addOperation:feed];
}

- (void)updateBets {
    _currentBetsPage = @1;
    DCFetchAccountDetailsOperation *account = [DCFetchAccountDetailsOperation new];
    DCFetchBetsOperation *bets = [DCFetchBetsOperation new];
    [self.queue addOperations:@[bets, account] waitUntilFinished:NO];
}

- (void)loadAccountWithCapperName:(NSString *)name {
    DCFetchAccountDetailsOperation *account = [[DCFetchAccountDetailsOperation alloc] initWithCapperName:name];
    [self.queue addOperations:@[account] waitUntilFinished:NO];
}

- (void)syncAll {
    _currentFeedPage = @1;
    _currentBetsPage = @1;
    DCFetchBetsOperation *bets = [DCFetchBetsOperation new];
    DCFethFeedOperation *feed = [DCFethFeedOperation new];
    DCFetchCappersOperation *cappers = [DCFetchCappersOperation new];
    [feed addDependency:bets];
    [bets addDependency:cappers];
    [feed addDependency:cappers];
    DCFetchAccountDetailsOperation *account = [DCFetchAccountDetailsOperation new];
    [account addDependency:cappers];
    DCFetchMaterialsOperation *materials = [DCFetchMaterialsOperation new];
    
    [self.queue addOperations:@[feed, bets, account, cappers, materials] waitUntilFinished:NO];
}

#pragma mark - pagination

- (void)loadFeedPage:(NSNumber *)page withCapperName:(NSString *)capperName {
    self.currentFeedPage = page;
    
    DCFethFeedOperation *feed = [[DCFethFeedOperation alloc] initWithPageNumber:page capperName:capperName];
    [self.queue addOperation:feed];
}

- (void)loadNextFeedPageWithCapperName:(NSString *)capperName {
    [self loadFeedPage:@(self.currentFeedPage.integerValue + 1) withCapperName:capperName];
}

- (void)loadBetsPage:(NSNumber *)page withCapperName:(NSString *)capperName {
    self.currentFeedPage = page;
    
    DCFetchBetsOperation *bets = [[DCFetchBetsOperation alloc] initWithPageNumber:page capperName:capperName];
    [self.queue addOperation:bets];
}

- (void)loadNextBetsPageWithCapperName:(NSString *)capperName {
    [self loadBetsPage:@(self.currentFeedPage.integerValue + 1) withCapperName:capperName];
}

- (void)loadFeedPage:(NSNumber *)page {
    self.currentFeedPage = page;
    
    DCFethFeedOperation *feed = [[DCFethFeedOperation alloc] initWithPageNumber:page];
    [self.queue addOperation:feed];
}

- (void)loadNextFeedPage {
    [self loadFeedPage:@(self.currentFeedPage.integerValue + 1)];
}

- (void)loadBetsPage:(NSNumber *)page {
    self.currentBetsPage = page;
    
    DCFetchBetsOperation *bets = [[DCFetchBetsOperation alloc] initWithPageNumber:page];
    [self.queue addOperation:bets];
}

- (void)loadNextBetsPage {
    [self loadBetsPage:@(self.currentBetsPage.integerValue + 1)];
}

@end
