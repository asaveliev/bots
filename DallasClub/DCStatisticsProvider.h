//
//  DCBetsProvider.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@interface DCStatisticsProvider : DCApiProvider

- (void)fetchStatisticsPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion;
- (void)fetchStatisticsPage:(NSNumber *)pageNumber capperId:(NSNumber *)capperId withCompletion:(void (^)(NSArray *, NSError *))completion;
- (void)fetchStatisticsWithCompletion:(void(^)(NSArray *items, NSError *error))completion;
- (void)createBetWithDictionary:(NSDictionary *)dict completion:(void (^)(NSArray *items, NSError *error))completion;
- (void)updateBetWithDictionary:(NSDictionary *)dict betId:(NSNumber *)betId completion:(void (^)(NSArray *items, NSError *error))completion;
- (void)removeBetWithId:(NSNumber *)betId completion:(void (^)(NSArray *items, NSError *error))completion;

- (void)getFullStatisticsWithcompletion:(void (^)(NSDictionary *items, NSError *error))completion;
- (void)getAccountWithCapperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getAccountWithcompletion:(void (^)(NSDictionary *items, NSError *error))completion;
- (void)getFullStatisticsWithCapperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion;

- (void)getStatisticsByMonths:(NSDate *)month completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getStatisticsByMonths:(NSDate *)month capperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion;

@end
