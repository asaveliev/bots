//
//  SKGraphElementView.m
//  SKeeper
//
//  Created by Anton on 19/05/16.
//  Copyright © 2016 Anton. All rights reserved.
//

#import "SKGraphElementView.h"

@interface SKGraphElementView ()

@end

@implementation SKGraphElementView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        
        _progressView = [UIView new];
        self.progressView.backgroundColor = [UIColor whiteColor];
        self.progressView.clipsToBounds = YES;
        [self addSubview:self.progressView];
        
        _titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont regularFontOfSize:13.0];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.lineBreakMode = NSLineBreakByClipping;
        [self addSubview:self.titleLabel];
        
        self.titleLabel.alpha = self.progressView.alpha = 0.3;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:tap];
        self.userInteractionEnabled = YES;
        
        [self setConstraints];
    }
    
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        self.clipsToBounds = YES;
        
        _progressView = [UIView new];
        self.progressView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.progressView];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)handleTap:(UIGestureRecognizer *)sender {
    if (self.delegate) {
        [self.delegate graphElementViewDidTapped:self];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = self.frame.size.width / 2.0;
    self.progressView.layer.cornerRadius = (self.frame.size.width - 2.0) / 2.0;
}

- (CGFloat)maximumSize {
    return 148.0;
}

- (void)setSelected:(BOOL)selected {
    _selected = selected;
    
    if (self.negative) {
        self.titleLabel.alpha = selected ? 1.0 : 0.3;
        self.progressView.backgroundColor = selected ? UIColorFromHex(0xffcdd2) : UIColorFromHex(0xe57373);
        return;
    }
    
    self.titleLabel.alpha = self.progressView.alpha = selected ? 1.0 : 0.3;
}

- (void)setNegative:(BOOL)negative {
    _negative = negative;
    
    self.progressView.alpha = negative ? 1.0 : 0.3;
    
    self.progressView.backgroundColor = negative ? UIColorFromHex(0xe57373) : [UIColor whiteColor];
}

- (void)setProgress:(CGFloat)progress {
    _progress = progress;
    
    [self layoutIfNeeded];
    
    CGFloat height = [self maximumSize] * progress;
    self.progressViewSize.constant = height;
    [self.progressView setNeedsUpdateConstraints];
    [self.progressView setNeedsLayout];
    [UIView animateWithDuration:0.4 animations:^{
        [self.progressView updateConstraintsIfNeeded];
        [self.progressView layoutIfNeeded];
    }];
}

- (void)setConstraints {
    self.progressView.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-1-[view]-1-|" options:0 metrics:nil views:@{@"view": self.progressView}]];
    [NSLayoutConstraint activateConstraints:@[[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.titleLabel attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-5-[title]-10-|" options:0 metrics:nil views:@{@"view": self.progressView, @"title": self.titleLabel}]];
    self.progressViewSize = [NSLayoutConstraint constraintWithItem:self.progressView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0];
    [self.progressView addConstraint:self.progressViewSize];
}

@end
