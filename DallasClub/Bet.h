//
//  Bet.h
//  
//
//  Created by Anton on 24/07/16.
//
//

#import <Foundation/Foundation.h>
#import "BaseManagedObject.h"

typedef enum : NSUInteger {
    DCBetStateSuccess,
    DCBetStateFailed,
    DCBetStateReturned,
} DCBetState;

NS_ASSUME_NONNULL_BEGIN

@interface Bet : BaseManagedObject <Serializable>

@property (nonatomic, readwrite) NSString *statusString;
@property (nonatomic, readonly)  NSString *readableStatus;

@end

NS_ASSUME_NONNULL_END

#import "Bet+CoreDataProperties.h"
