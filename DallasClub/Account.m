//
//  Account.m
//  
//
//  Created by Anton on 01/08/16.
//
//

#import "Account.h"

@implementation Account

- (void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    self.change = @([dictionaryRepresentation[@"overall_balance_change"] doubleValue]);
    self.returned_bets = dictionaryRepresentation[@"returned_bets"];
    self.failed_bets = dictionaryRepresentation[@"failed_bets"];
    self.success_bets = dictionaryRepresentation[@"successful_bets"];
    self.month_change = @([dictionaryRepresentation[@"month_balance_change"] doubleValue]);
    self.week_change = @([dictionaryRepresentation[@"week_balance_change"] doubleValue]);
}

@end
