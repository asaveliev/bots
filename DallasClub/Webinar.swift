//
//  Webinar.swift
//  DallasClubProd
//
//  Created by Anton on 21.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

//{
//    "name": "Заголовок",
//    "description": "Описание",
//    "video_url": "Ссылка на видос",
//    "button_caption": "Текст над кнопкой",
//    "button_text": "Текст кнопки",
//    "url": "Ссылка кнопки"
//}

struct Webinar: Decodable {
    let name: String
    let description: String
    let videoUrl: String
    let buttonCaption: String
    let buttonText: String
    let url: String
}
