//
//  DCTextField.h
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetPicker.h"

@interface AbstractActionSheetPicker ()

- (UIToolbar *)createPickerToolbarWithTitle:(NSString *)aTitle;

@end

@interface DCTextField : UITextField

@property (nonatomic, readonly) AbstractActionSheetPicker *picker;

@property (nonatomic, strong) NSArray   *titles;

@property (nonatomic, assign) NSInteger selectedPickerIndex;

@end
