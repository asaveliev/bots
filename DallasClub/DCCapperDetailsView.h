//
//  DCCapperDetailsView.h
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCBaseView.h"
#import "DCExpandableBackgroundView.h"

@interface DCCapperDetailsView : DCExpandableBackgroundView

@property (nonatomic, readonly) UITableView *tableView;
@property (nonatomic, readonly) UIButton    *plusButton;

@end
