//
//  DCEditBetPolicy.m
//  DallasClub
//
//  Created by Anton on 09/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCEditBetPolicy.h"

@implementation DCEditBetPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    NSDictionary *dict = @{
                           @"name": self.numberString,
                           @"amount": [self.percent stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"% "]],
                           @"number": self.numberString,
                           @"status": self.status,
                           @"coefficient": self.coefficient,
                           @"date": self.dateString
                           };
    [[DCNetworkCore sharedInstance].statsProvider updateBetWithDictionary:dict betId:self.originalNumber completion:^(NSArray *items, NSError *error) {
        completion(error);
    }];
}

@end
