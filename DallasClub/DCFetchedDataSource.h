//
//  DCFetchedDataSource.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@protocol DCDataSourceDelegate <NSObject>

- (void)dataSource:(id)dataSource didSelectObjectAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface DCFetchedDataSource : NSObject <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@property (strong, nonatomic)   UITableView                 *tableView;
@property (strong, nonatomic)   NSFetchedResultsController  *fetchedResultsController;

@property (nonatomic, readonly) NSString                    *reuseIdentifier;
@property (nonatomic, readonly) NSFetchRequest              *fetchRequest;
@property (nonatomic, readonly) NSString                    *sectionName;

@property (strong, nonatomic)   NSManagedObjectContext      *context;

@property (weak, nonatomic)     id<DCDataSourceDelegate>    delegate;

@property (strong, nonatomic) UIRefreshControl              *refreshControl;

- (void)registerClasses;
- (id)objectAtIndexPath:(NSIndexPath*)indexPath;
- (NSIndexPath*)indexPathForObject:(id)object;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

- (instancetype)initWithTableView:(UITableView *)tableView;

@end
