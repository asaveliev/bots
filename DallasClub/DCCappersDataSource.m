//
//  DCCappersDataSource.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>

#import "DCCappersDataSource.h"
#import "DCCapperCollectionViewCell.h"
#import "Capper.h"

@implementation DCCappersDataSource

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:[Capper entityName]];
    request.predicate = [NSPredicate predicateWithValue:YES];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"ordering" ascending:YES]];
    return request;
}

- (void)registerClasses {
    [self.collectionView registerClass:[DCCapperCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
}

- (void)configureCollectionCell:(DCCapperCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Capper *capper = [self objectAtIndexPath:indexPath];
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:capper.imageUrl] placeholderImage:[UIImage imageNamed:@"person"]];
}

@end
