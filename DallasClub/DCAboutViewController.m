
//
//  DCAboutViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 25/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAboutViewController.h"
#import "DCAboutView.h"

@interface DCAboutViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, readonly) DCAboutView *mainView;
@property (nonatomic, strong)   NSString    *text;

@end

@implementation DCAboutViewController

- (instancetype)initWithText:(NSString *)text {
    if (self = [super init]) {
        _text = text;
    }
    
    return self;
}

- (void)loadView {
    self.view = _mainView = [DCAboutView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.tableView.delegate = self;
    self.mainView.tableView.dataSource = self;
    
    self.mainView.headerTitleLabel.text = @"Описание";
    [self.mainView.headerView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [self.mainView.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.mainView.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Button"];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)buttonTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://hochuprognoz.ru/bets.html"]];
}

#pragma mark UITableView

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:(indexPath.row == 0 ? @"Cell" : @"Button") forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row == 0) {
        UILabel *label = [UILabel new];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.numberOfLines = 0;
        label.font = [UIFont regularFontOfSize:17.0];
        label.textColor = [UIColor blackColor];
        label.text = self.text;
        [cell.contentView addSubview:label];
        
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[l]-15-|" options:0 metrics:nil views:@{ @"l": label }]];
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[l]-10-|" options:0 metrics:nil views:@{ @"l": label }]];
    } else {
        UIButton *b = [UIButton new];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor okButtonColor];
        b.clipsToBounds = YES;
        b.layer.cornerRadius = 25.0;
        [b setTitle:@"Как ставить?" forState:UIControlStateNormal];
        b.titleLabel.textColor = [UIColor whiteColor];
        [cell.contentView addSubview:b];
        
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[l]-50-|" options:0 metrics:nil views:@{ @"l": b }]];
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[l(==50)]-10-|" options:0 metrics:nil views:@{ @"l": b }]];
        
        [b addTarget:self action:@selector(buttonTapped) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100.0;
}



@end
