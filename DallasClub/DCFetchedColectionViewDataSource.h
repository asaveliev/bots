//
//  DCFetchedColectionViewDataSource.h
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCFetchedDataSource.h"

@interface DCFetchedColectionViewDataSource : DCFetchedDataSource<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

- (void)configureCollectionCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end
