//
//  UIColor+DCColors.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "UIColor+DCColors.h"

@implementation UIColor (DCColors)

+ (instancetype)bgColor {
    return UIColorFromHex(0xefeff4);
}

+ (instancetype)statsPositiveBalanceColor {
    return UIColorFromHex(0x11A861);
}

+ (instancetype)statsNegativeBalanceColor {
    return UIColorFromHex(0xFF2D55);
}

+ (instancetype)statsPostiveBubbleColor {
    return UIColorFromHex(0x66BB6A);
}

+ (instancetype)statsNegativeBubbleColor {
    return UIColorFromHex(0xFF3863);
}

+ (instancetype)statsNeutralBubbleColor {
    return UIColorFromHex(0x8798A4);
}

+ (instancetype)statsHeaderSubtitleColor {
    return UIColorFromHex(0xC3CDD4);
}

+ (instancetype)tabBarTintColor {
    return UIColorFromHex(0x737886);
}

+ (instancetype)okButtonColor {
    return UIColorFromHex(0x0A60FF);
}

+ (instancetype)audioBlueColor {
    return UIColorFromHex(0x007AFF);
}

+ (instancetype)audioGrayColor {
    return UIColorFromHex(0xD8D8D8);
}

@end
