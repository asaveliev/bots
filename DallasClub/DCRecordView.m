//
//  DCRecordView.m
//  DallasClub
//
//  Created by Anton Savelev on 03.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRecordView.h"

@implementation DCRecordView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
        _recordButton = [UIButton new];
        [self.recordButton setImage:[UIImage imageNamed:@"rec_play"] forState:UIControlStateNormal];
        [self.recordButton setImage:[UIImage imageNamed:@"rec_stop"] forState:UIControlStateSelected];
        [self addSubview:self.recordButton];
        
        _timeLabel = [UILabel new];
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        self.timeLabel.textColor = [UIColor whiteColor];
        self.timeLabel.font = [UIFont ultralightFontOfSize:92.0];
        [self addSubview:self.timeLabel];
        
        _stateLabel = [UILabel new];
        self.stateLabel.textAlignment = NSTextAlignmentCenter;
        self.stateLabel.font = [UIFont statsTitleFontMedium];
        self.stateLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.stateLabel];
        
        _closeButton = [UIButton new];
        [self.closeButton setImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
        [self addSubview:self.closeButton];
        
        _doneButton = [UIButton new];
        self.doneButton.titleLabel.font = [UIFont statsTitleFontMedium];
        self.doneButton.backgroundColor = [UIColor whiteColor];
        [self.doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.doneButton.clipsToBounds = YES;
        self.doneButton.layer.cornerRadius = 27.0;
        [self.doneButton setTitle:@"Отправить" forState:UIControlStateNormal];
        [self addSubview:self.doneButton];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"record": self.recordButton,
        @"time": self.timeLabel,
        @"state": self.stateLabel,
        @"close": self.closeButton,
        @"done": self.doneButton
    };
    NSDictionary *metrics = @{
        @"closeButtonSize": @60,
        @"recordSize": @64,
        @"doneHeight": @54
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[close(==closeButtonSize)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[close(==closeButtonSize)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[state]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[time]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[record(==recordSize)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[record(==recordSize)]" options:0 metrics:metrics views:views]];
    NSLayoutConstraint *recordCenterConstraint = [NSLayoutConstraint constraintWithItem:self.recordButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[recordCenterConstraint]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-180-[state]-2-[time]-30-[record]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-75-[done]-75-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[done(==doneHeight)]-20-|" options:0 metrics:metrics views:views]];
}

@end
