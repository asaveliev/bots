//
//  Capper+CoreDataClass.m
//  
//
//  Created by Anton Savelev on 17.04.17.
//
//

#import "Capper.h"
#import "NSDateFormatter+ISO8601.h"

@implementation Capper

- (void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    self.uid = dictionaryRepresentation[@"id"];
    self.name = dictionaryRepresentation[@"name"];
    self.imageUrl = dictionaryRepresentation[@"photo_url"];
    self.balance = @([dictionaryRepresentation[@"current_balance"] doubleValue]);
    self.created = [NSDateFormatter dateFromStringForISO8601:dictionaryRepresentation[@"created_at"]];
    self.title = dictionaryRepresentation[@"title"];
    self.coverUrl = dictionaryRepresentation[@"cover_photo_url"];
    self.capper_description = dictionaryRepresentation[@"description"];
    if (![dictionaryRepresentation[@"ordering"] isKindOfClass:[NSNumber class]]) {
        return;
    }
    self.ordering = [dictionaryRepresentation[@"ordering"] stringValue];
}

@end
