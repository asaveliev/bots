//
//  DCAudioCell.m
//  DallasClub
//
//  Created by Anton Savelev on 10.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAudioCell.h"

@interface TKNewRoundedCardCell ()
@property (strong, nonatomic) UIView  *cardView;
@end

@implementation DCAudioCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _slider = [DCSlider new];
        [self.slider.slider setThumbImage:[UIImage new] forState:UIControlStateNormal];
        self.slider.slider.minimumTrackTintColor = [UIColor audioBlueColor];
        self.slider.slider.maximumTrackTintColor = [UIColor audioGrayColor];
        self.slider.slider.userInteractionEnabled = NO;
        [self.slider.slider addTarget:self action:@selector(sliderChanged) forControlEvents:UIControlEventValueChanged];
        [self.cardView addSubview:self.slider];
        
        _audioButton = [UIButton new];
        self.audioButton.backgroundColor = [UIColor audioBlueColor];
        self.audioButton.clipsToBounds = YES;
        self.audioButton.layer.cornerRadius = 20.0;
        [self.audioButton setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        [self.audioButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateSelected];
        [self.audioButton addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.cardView addSubview:self.audioButton];
        
        _timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont regularFontOfSize:14.0];
        self.timeLabel.textColor = UIColorFromHex(0xA6B2BC);
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        [self.cardView addSubview:self.timeLabel];
        
        _avatarImageView = [UIImageView new];
        self.avatarImageView.backgroundColor = [UIColor clearColor];
        [self.cardView addSubview:self.avatarImageView];
        self.avatarImageView.clipsToBounds = YES;
        self.avatarImageView.layer.cornerRadius = 15.0;
        self.avatarImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        _nameLabel = [UILabel new];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.cardView addSubview:self.nameLabel];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)sliderChanged {
    [self.delegate audioCellSliderChanged:self toValue:self.slider.slider.value];
}

- (void)playAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    if (sender.selected) {
        [self.delegate audioCellDidPlay:self];
    } else {
        [self.delegate audioCellDidPause:self];
    }
}

- (void)setConstraints {
    if (!self.slider) {
        return;
    }
    NSDictionary *views = @{
        @"slider": self.slider,
        @"button": self.audioButton,
        @"time": self.timeLabel,
        @"avatar": self.avatarImageView,
        @"card": self.cardView
    };
    NSDictionary *metrics = @{
        @"sliderSize": @30,
        @"buttonSize": @40
    };
    
    for (UIView *v in views.allValues) {
        v.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[avatar]-19-[slider(==sliderSize)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-13-[button(==buttonSize)]-7-[slider]-15-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[avatar(30)]-15-[button(==buttonSize)]-15-|" options:0 metrics:metrics views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[v(30)]-8-[name]-5-[time]-15-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{ @"v": self.avatarImageView, @"time": self.timeLabel, @"name": self.nameLabel }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[time]" options:0 metrics:nil views:views]];
}

@end
