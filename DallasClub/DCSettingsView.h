//
//  DCSettingsView.h
//  DallasClub
//
//  Created by Anton on 19/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCSettingsView : DCBaseView

@property (nonatomic, strong) UITableView *tableView;

@end
