//
//  DCStatsFetcher.h
//  DallasClub
//
//  Created by Anton Savelev on 17.09.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bet.h"

@interface DCStatsFetcher : NSObject

@property (nonatomic, strong)   NSDictionary    *changeByDays;
@property (nonatomic, readonly) double          monthChange;

@property (nonatomic, strong)   NSDictionary    *changeByMonths;

- (instancetype)initWithCapperName:(NSString *)name;

- (NSInteger)countOfBetsByDay:(NSDate *)day withState:(DCBetState)state;
- (void)fetchDataOfMonth:(NSDate *)month completion:(void (^)(NSError *error))completion;

- (void)fetchDataOfYears:(NSArray *)years completion:(void (^)(NSError *error))completion;

@end
