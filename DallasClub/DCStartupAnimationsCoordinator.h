//
//  DCStartupAnimationsCoordinator.h
//  DallasClub
//
//  Created by Anton Savelev on 09/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCStartupAnimationsCoordinator : NSObject

- (instancetype)initWithViews:(NSArray *)views;

- (void)performAnimationsWithCompletion:(void (^)())completion;
- (void)prepareViews;
- (void)appendViewsToAnimate:(NSArray *)views;

@end
