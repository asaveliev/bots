//
//  NSDateFormatter+ISO8601.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (ISO8601)

+(id)newWithFormat:(NSString*)format, ...;

+(NSString*)stringFromDateForISO8601:(NSDate*)date;
+(NSDate*)dateFromStringForISO8601:(NSString*)dateString;

@end
