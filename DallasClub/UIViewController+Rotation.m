//
//  UIViewController+Rotation.m
//  DallasClub
//
//  Created by Anton Savelev on 27.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "UIViewController+Rotation.h"
#import "AppDelegate.h"

@implementation UIViewController (Rotation)
    
- (void)restrictRotation:(BOOL) restriction {
    if (!restriction) {
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
    }
    
    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictRotation = restriction;
}
    
@end
