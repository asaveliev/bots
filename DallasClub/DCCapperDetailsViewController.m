//
//  DCCapperDetailsViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCapperDetailsViewController.h"
#import "DCCapperDetailsView.h"
#import "DCCapperDetailsHeaderView.h"
#import "DCFeedDataSource.h"
#import "DCImporter.h"
#import "DCMoneyFormatter.h"
#import "DCBetsDataSource.h"
#import "DCGraphsViewController.h"
#import "CapperInfoViewController.h"
#import "DCNetworkCore.h"

#import "DCAddFeedViewController.h"
#import "DCCreateFeedPolicy.h"
#import "DCAddBetViewController.h"
#import "DCAddBetPolicy.h"
#import "DCEditBetPolicy.h"
#import "DCRemoveBetPolicy.h"
#import "DCEditFeedPolicy.h"
#import "DCRemoveFeedPolicy.h"

#import "DCFeedViewController.h"
#import "RMUniversalAlert.h"
#import "SVProgressHUD.h"
#import "IDMPhotoBrowser.h"

#import "DCPersistentStack.h"

#import "Capper.h"
#import "Bet.h"
#import "Post.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface DCCapperDetailsViewController () <DCBetsDataSourceDelegate, DCFeedDataSourceDelegate>

@property (nonatomic, readonly) DCCapperDetailsView         *mainView;
@property (nonatomic, readonly) DCCapperDetailsHeaderView   *tableHeaderView;

@property (nonatomic, readonly) DCFeedDataSource            *feedDataSource;
@property (nonatomic, readonly) DCBetsDataSource            *betsDataSource;

@property (nonatomic, readonly) Capper                      *capper;

@end

@implementation DCCapperDetailsViewController

- (void)loadView {
    self.view = _mainView = [DCCapperDetailsView new];
}

- (instancetype)initWithCapperModel:(Capper *)capper {
    if (self = [super initWithNibName:nil bundle:nil]) {
        _capper = capper;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.clipsToBounds = YES;
    
    _tableHeaderView = [[DCCapperDetailsHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 190.0)];
    self.tableHeaderView.titleLabel.text = self.capper.name;
    DCMoneyFormatter *formatter = [DCMoneyFormatter new];
    self.tableHeaderView.descriptionLabel.text = [[NSString stringWithFormat:NSLocalizedString(@"Баланс: %@", nil), [formatter stringFromAmount:self.capper.balance]] uppercaseString];
    self.mainView.tableView.tableHeaderView = self.tableHeaderView;
    [self.mainView.backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.mainView bindTableView:self.mainView.tableView forFadeWithOffset:64.0];
    
    [self.tableHeaderView.avatarImage sd_setImageWithURL:[NSURL URLWithString:self.capper.imageUrl] placeholderImage:[UIImage imageNamed:@"person"]];
    
    _feedDataSource = [[DCFeedDataSource alloc] initWithTableView:self.mainView.tableView postId:nil capperName:self.capper.name];
    _betsDataSource = [[DCBetsDataSource alloc] initWithTableView:nil capperName:self.capper.name];
    self.betsDataSource.delegate = self;
    self.feedDataSource.delegate = self;
    [[DCImporter betsImporter] loadAccountWithCapperName:self.capper.name];
    [self.feedDataSource.refreshControl addTarget:self action:@selector(refreshFeedData) forControlEvents:UIControlEventValueChanged];
    CGFloat bgOffset = [UIScreen mainScreen].bounds.size.width * 0.55;
    [self.mainView.tableView setContentOffset:CGPointMake(0.0, -bgOffset) animated:YES];
    
    [self.tableHeaderView.segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    
    self.mainView.plusButton.hidden = ![DCNetworkCore sharedInstance].authProvider.isAdmin || self.capper.uid.integerValue != [DCNetworkCore sharedInstance].authProvider.userId;
    [self.mainView.plusButton addTarget:self action:@selector(plusAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.mainView.backgroundImageView.backgroundColor = UIColorFromHex(0xE6E8EB);
    [self.mainView.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:self.capper.coverUrl]];
    
    [self.mainView.infoButton addTarget:self action:@selector(infoAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.mainView.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
    [self.mainView.leftButton setTitle:NSLocalizedString(@"Прогнозы", nil) forState:UIControlStateNormal];
    [self.mainView.leftButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
    [self.mainView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.headerTitleLabel.text = NSLocalizedString(@"Профиль", nil);
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[DCImporter feedImporter] loadFeedPage:@1 withCapperName:self.capper.name];
    [[DCImporter betsImporter] loadBetsPage:@1 withCapperName:self.capper.name];
}

- (void)refreshFeedData {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.feedDataSource.refreshControl endRefreshing];
    });
    [[DCImporter feedImporter] loadFeedPage:@1 withCapperName:self.capper.name];
}

- (void)refreshBetsData {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.feedDataSource.refreshControl endRefreshing];
    });
    [[DCImporter betsImporter] loadBetsPage:@1 withCapperName:self.capper.name];
}

- (void)dealloc {
    [self.mainView.tableView removeObserver:self forKeyPath:@"contentOffset"];
    [self.mainView.tableView removeObserver:self.mainView forKeyPath:@"contentOffset"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.mainView.tableView.contentOffset.y > -62.0 ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"contentOffset"]) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

#pragma mark - Actions

- (void)infoAction {
    [self.navigationController pushViewController:[[CapperInfoViewController alloc] initWithCapperModel:self.capper] animated:YES];
}

- (void)plusAction {
    if (self.betsDataSource.tableView == nil) {
        DCCreateFeedPolicy *policy = [DCCreateFeedPolicy new];
        [self.navigationController pushViewController:[[DCAddFeedViewController alloc] initWithPolicy:policy] animated:YES];
    }
    
    if (self.feedDataSource.tableView == nil) {
        DCAddBetViewController *vc = [[DCAddBetViewController alloc] initWithPolicy:[DCAddBetPolicy new]];
        vc.needsUpdateAfterComplete = NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)needsToDisplayGraphs {
    [self presentViewController:[[DCGraphsViewController alloc] initWithCapper:self.capper] animated:YES completion:nil];
}

- (void)segmentedControlChangedValue {
    switch (self.tableHeaderView.segmentedControl.selectedSegmentIndex) {
        case 0:
            self.betsDataSource.tableView = nil;
            self.feedDataSource.tableView = self.mainView.tableView;
            [self.feedDataSource.refreshControl removeTarget:self action:@selector(refreshBetsData) forControlEvents:UIControlEventValueChanged];
            [self.feedDataSource.refreshControl addTarget:self action:@selector(refreshFeedData) forControlEvents:UIControlEventValueChanged];
            
            [self.mainView.plusButton setImage:[UIImage imageNamed:@"add-alt"] forState:UIControlStateNormal];
            break;
        case 1:
            self.feedDataSource.tableView = nil;
            self.betsDataSource.tableView = self.mainView.tableView;
            [self.feedDataSource.refreshControl removeTarget:self action:@selector(refreshFeedData) forControlEvents:UIControlEventValueChanged];
            [self.feedDataSource.refreshControl addTarget:self action:@selector(refreshBetsData) forControlEvents:UIControlEventValueChanged];
            [self.mainView.plusButton setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
}

- (void)backAction {
    self.mainView.backgroundImageView.clipsToBounds = YES;
    CGRect frame = self.mainView.backgroundImageView.frame;
    frame.size.height = [UIScreen mainScreen].bounds.size.width * 0.55;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    self.mainView.backgroundImageView.frame = frame;
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - DCDataSourceDelegate

- (void)dataSource:(id)dataSource didSelectObjectAtIndexPath:(NSIndexPath *)indexPath {
    if ([dataSource isKindOfClass:[DCBetsDataSource class]]) {
        Bet *bet = [self.betsDataSource objectAtIndexPath:indexPath];
        if (![DCNetworkCore sharedInstance].authProvider.isAdmin || self.capper.uid.integerValue != [DCNetworkCore sharedInstance].authProvider.userId) {
            return;
        }
        
        [RMUniversalAlert showActionSheetInViewController:self withTitle:@"" message:NSLocalizedString(@"Выберите действие", nil) cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"Изменить", nil), NSLocalizedString(@"Удалить", nil)] popoverPresentationControllerBlock:^(RMPopoverPresentationController * _Nonnull popover) {
            
        } tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
            DCBetsPolicy *policy;
            if (buttonIndex == 2) {
                policy = [[DCEditBetPolicy alloc] initWithBetId:bet.uid];
                DCAddBetViewController *vc = [[DCAddBetViewController alloc] initWithPolicy:policy];
                vc.needsUpdateAfterComplete = NO;
                [self.navigationController pushViewController:vc animated:YES];
            } else if (buttonIndex == 3) {
                policy = [[DCRemoveBetPolicy alloc] initWithBetId:bet.uid];
                
                [SVProgressHUD show];
                [policy performActionWithCompletion:^(NSError *error) {
                    [SVProgressHUD dismiss];
                    if (!error) {
                        [[DCPersistentStack sharedInstance].context performBlock:^{
                            [[DCPersistentStack sharedInstance].context deleteObject:bet];
                            [[DCPersistentStack sharedInstance].context save:nil];
                        }];
                        return;
                    }
                    
                    [RMUniversalAlert showAlertInViewController:self withTitle:NSLocalizedString(@"Ошибка", nil) message:NSLocalizedString(@"Попробуйте позже", nil) cancelButtonTitle:NSLocalizedString(@"Ок", nil) destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                        
                    }];
                }];
            }
        }];
        return;
    }
    
    Post *post = [self.feedDataSource objectAtIndexPath:indexPath];
    if (![DCNetworkCore sharedInstance].authProvider.isAdmin || self.capper.uid.integerValue != [DCNetworkCore sharedInstance].authProvider.userId) {
        return;
    }
    
    [RMUniversalAlert showActionSheetInViewController:self withTitle:@"" message:NSLocalizedString(@"Выберите действие", nil) cancelButtonTitle:NSLocalizedString(@"Отмена", nil) destructiveButtonTitle:nil otherButtonTitles:@[NSLocalizedString(@"Изменить", nil), NSLocalizedString(@"Удалить", nil)] popoverPresentationControllerBlock:^(RMPopoverPresentationController * _Nonnull popover) {
        
    } tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
        DCFeedPolicy *policy;
        if (buttonIndex == 2) {
            policy = [[DCEditFeedPolicy alloc] initWithPostId:post.uid];
            policy.image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:post.imageUrl];
            [self.navigationController pushViewController:[[DCAddFeedViewController alloc] initWithPolicy:policy] animated:YES];
        } else if (buttonIndex == 3) {
            policy = [[DCRemoveFeedPolicy alloc] initWithPostId:post.uid];
            
            [SVProgressHUD show];
            [policy performActionWithCompletion:^(NSError *error) {
                [SVProgressHUD dismiss];
                if (!error) {
                    [[DCPersistentStack sharedInstance].context performBlock:^{
                        [[DCPersistentStack sharedInstance].context deleteObject:post];
                        [[DCPersistentStack sharedInstance].context save:nil];
                    }];
                    return;
                }
                
                [RMUniversalAlert showAlertInViewController:self withTitle:NSLocalizedString(@"Ошибка!", nil) message:NSLocalizedString(@"Попробуйте позже", nil) cancelButtonTitle:NSLocalizedString(@"Ок", nil) destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                    
                }];
            }];
        }
    }];
}

- (void)dataSource:(DCFeedDataSource *)dataSource didSelectPhoto:(NSString *)link {
    if (!link.length) {
        return;
    }
    
    IDMPhoto *photo = [IDMPhoto photoWithURL:[NSURL URLWithString:link]];
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[photo]];
    browser.displayToolbar = NO;
    [self presentViewController:browser animated:YES completion:nil];
}

- (void)dataSource:(DCFeedDataSource *)dataSource didSelectLink:(NSURL *)link {
    if (!link) {
        return;
    }
    
    [[UIApplication sharedApplication] openURL:link];
}

- (void)dataSourceDidSelectInfo {
    
}


@end

