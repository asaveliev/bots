//
//  HAScrollableSegmentedControll.m
//  HistoryApp
//
//  Created by Anton on 14/05/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "HAScrollableSegmentedControll.h"

@interface HAScrollableSegmentedControll ()

@end

@implementation HAScrollableSegmentedControll

- (instancetype)init {
    if (self = [super init]) {
        _scrollView = [UIScrollView new];
        self.scrollView.showsHorizontalScrollIndicator = self.scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
        
        _buttons = [NSMutableArray new];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[scrollView]-0-|" options:0 metrics:nil views:@{ @"scrollView": self.scrollView }]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[scrollView]-0-|" options:0 metrics:nil views:@{ @"scrollView": self.scrollView }]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = 0.0;
    for (UIButton *b in self.buttons) {
        width += b.frame.size.width;
    }
    
    self.scrollView.contentSize = CGSizeMake(width, self.scrollView.frame.size.height - 20.0);
}

- (void)setButtonsWithTitles:(NSArray *)titles andSizes:(NSArray <NSDictionary *>*)sizes {
    UIButton *prevButton = nil;
    for (UIButton *b in self.buttons) {
        [b removeFromSuperview];
    }
    [self.buttons removeAllObjects];

    for (NSString *title in titles) {
        UIButton *button = [UIButton new];
        [button setTitle:title forState:UIControlStateNormal];
        
        CGFloat newOrigin = prevButton ? prevButton.frame.origin.x + prevButton.frame.size.width : 0;
        button.frame = CGRectMake(newOrigin, 0.0, [sizes[[titles indexOfObject:title]][@"width"] doubleValue], [sizes[[titles indexOfObject:title]][@"height"] doubleValue]);
        [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self configureButton:button];
        prevButton = button;
        
        [self.buttons addObject:button];
        [self.scrollView addSubview:button];
    }
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)buttonClicked:(UIButton *)button {
    for (UIButton *b in self.buttons) {
        b.selected = NO;
    }
    
    button.selected = YES;
    
    if (self.selectionBlock) {
        self.selectionBlock([self.buttons indexOfObject:button], button.selected);
    }
}

- (void)configureButton:(UIButton *)button {
    
}

@end
