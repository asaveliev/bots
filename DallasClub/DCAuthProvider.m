//
//  DCAuthProvider.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAuthProvider.h"
#import "DCUserModel.h"
#import "DCNetworkCore.h"
#import <Lockbox/Lockbox.h>
#import <Buglife/Buglife.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>

NSString *const DCTokenKey = @"DCToken";
NSString *const DCAuthEndpoint = @"cappers/sessions.json";
NSString *const DCUserAuthEndpoint = @"sessions.json";
/// HACK!!!!!
NSString *const DCUserAuthEndpointWithoutJson = @"sessions";
NSString *const DCRegistrationEndpoint = @"users/registrations";
NSString *const DCUsersKey = @"DCUsers";
NSString *const DCSelectedUserKey = @"DCSelectedUser";
NSString *const DCUnathorizedNotificationKey = @"DCUnathorizedNotificationKey";

@implementation DCAuthProvider

- (instancetype)init {
    if (self = [super init]) {
        _currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:DCSelectedUserKey]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(HTTPOperationDidFinish:) name:AFNetworkingTaskDidCompleteNotification object:nil];
    
    return self;
}

- (void)HTTPOperationDidFinish:(NSNotification *)notification {
    NSURLSessionTask *task = [notification object];
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
    BOOL skip = [response.URL.absoluteString containsString:DCAuthEndpoint] || [response.URL.absoluteString containsString:DCUserAuthEndpoint];
    if (skip) {
        return;
    }
    
    if (response.statusCode == 401) {
        if ([DCNetworkCore sharedInstance].authProvider.hasAuth) {
            DCAuthProvider *auth = [DCNetworkCore sharedInstance].authProvider;
            if (auth.currentUser.isCapper.boolValue) {
                [auth removeUser:[DCNetworkCore sharedInstance].authProvider.currentUser];
                if (!auth.users.lastObject) {
                    [[DCNetworkCore sharedInstance] updateAuthHeader];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:DCUnathorizedNotificationKey object:nil];
                    return;
                }
                [auth selectUser:auth.users.lastObject completion:^(BOOL success) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:DCUnathorizedNotificationKey object:nil];
                }];
                return;
            }
            [auth selectUser:nil completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:DCUnathorizedNotificationKey object:nil];
            return;
        }
    }
}

- (void)registerUserWithLogin:(NSString *)login pass:(NSString *)password name:(NSString *)name completion:(void (^)(BOOL, NSString*))completion {
    NSString *token = [[FIRInstanceID instanceID] token];
    NSDictionary *params = @{
                             @"user": @{
                                     @"email": login,
                                     @"password": password,
                                     @"name": name,
                                     @"firebase_token": token ?: @"",
                                     @"platform": @"ios"
                                     }
                             };
    [self.sessionManager POST:[DCApiRoot stringByAppendingString:DCRegistrationEndpoint] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(YES, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *resp = error.userInfo[@"com.alamofire.serialization.response.error.response"];
        if (resp.statusCode != 422) {
            completion(NO, nil);
            return;
        }
        NSData *response = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil][@"errors"];
        NSDictionary *map = @{
                              @"name": @"Имя",
                              @"email": @"Email",
                              @"password": @"Пароль"
                              };
        NSString *errorsString = [NSString new];
        NSInteger errorsCount = 0;
        for (NSString *key in dict.allKeys) {
            NSArray *errors = dict[key];
            for (NSString *error in errors) {
                errorsCount++;
                NSString *errorString = [NSString stringWithFormat:@"%ld. %@ %@\n", errorsCount, map[key], error];
                errorsString = [errorsString stringByAppendingString:errorString];
            }
        }
        completion(NO, [errorsString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]);
    }];
}

- (void)authUserWithLogin:(NSString *)login andPass:(NSString *)password completion:(void (^)(BOOL))completion {
    NSDictionary *params = @{
                             @"session": @{
                                     @"email": login,
                                     @"password": password,
                                     }
                             };
    __weak DCAuthProvider *weakSelf = self;
    [self.sessionManager POST:[DCApiRoot stringByAppendingString:DCUserAuthEndpoint] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DCUserModel *model = [DCUserModel new];
        [model setDictionaryRepresentation:responseObject];
        model.token = responseObject[@"auth_token"];
        model.password = password;
        model.login = login;
        model.isCapper = @(![responseObject[@"type"] isEqualToString: @"user"]);

        if (model.isCapper.boolValue) {
            [weakSelf appendUser:model];
        }
        
        [weakSelf selectUser:model completion:nil];
        completion([responseObject[@"auth_token"] length] > 0);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NO);
    }];

}

-(void)authWithLogin:(NSString *)login andPass:(NSString *)password completion:(void (^)(BOOL))completion {
    NSString *token = [[FIRInstanceID instanceID] token];
    NSDictionary *params = @{
                             @"session": @{
                                     @"email": login,
                                     @"password": password,
                                     @"firebase_token": token ?: @"",
                                     @"platform": @"ios"
                                 }
                             };
    __weak DCAuthProvider *weakSelf = self;
    [self.sessionManager POST:[DCApiRoot stringByAppendingString:DCAuthEndpoint] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        DCUserModel *model = [DCUserModel new];
        [model setDictionaryRepresentation:responseObject];
        model.token = responseObject[@"auth_token"];
        model.password = password;
        model.login = login;
        model.isCapper = @YES;
        [weakSelf appendUser:model];
        [weakSelf selectUser:model completion:nil];
        completion([responseObject[@"auth_token"] length] > 0);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(NO);
    }];
}

-(void)authorize {
    if (!self.currentUser) {
        return;
    }
    NSString *login = self.currentUser.login;
    NSString *password = self.currentUser.password;
    if (!login.length || !password.length) {
        return;
    }
    
    [self authWithLogin:login andPass:password completion:^(BOOL success) {
        
    }];
}

#pragma mark -

- (void)selectUser:(DCUserModel *)user completion:(void (^)(BOOL))completion {
    if (!completion) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
        _currentUser = user;
        [Buglife sharedBuglife].invocationOptions = self.isAdmin || self.isSuperAdmin ? LIFEInvocationOptionsShake : LIFEInvocationOptionsNone;
        [[DCNetworkCore sharedInstance] updateAuthHeader];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:DCSelectedUserKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return;
    }
    
    [self authWithLogin:user.login andPass:user.password completion:^(BOOL success) {
        if (!success) {
            completion(success);
            return;
        }
        _currentUser = user;
        [[DCNetworkCore sharedInstance] updateAuthHeader];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:user] forKey:DCSelectedUserKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DCUpdateTabBarNotification" object:nil];
        
        completion(success);
    }];
}

- (NSArray *)users {
    return [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] objectForKey:DCUsersKey]];
}

- (void)appendUser:(DCUserModel *)model {
    if ([self.users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"userId == %ld", model.userId.integerValue]].count) {
        return;
    }
    NSArray *newUsers = [self.users ?: @[] arrayByAddingObject:model];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newUsers] forKey:DCUsersKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)removeToken:(NSString *)token {
    if (token == nil) {
        return;
    }
    self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [self.sessionManager DELETE:[NSString stringWithFormat:@"%@/%@", [DCApiRoot stringByAppendingString:DCUserAuthEndpointWithoutJson], token] parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        NSLog(@"success");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        NSLog(@"fail");
    }];
}

- (void)removeUser:(DCUserModel *)model {
    [self removeToken:model.token];
    [model removeFromKeychain];
    NSMutableArray *newUsers = [self.users mutableCopy];
    [newUsers removeObject:model];
    [self selectUser:nil completion:nil];
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:newUsers] forKey:DCUsersKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)token {
    return self.currentUser.token;
}

- (BOOL)hasAuth {
    return self.currentUser.token.length > 0;
}

- (BOOL)isAdmin {
    return self.hasAuth && self.currentUser.isCapper.boolValue;
}

- (BOOL)isSuperAdmin {
    return self.currentUser.isSuperUser.boolValue;
}

- (NSInteger)userId {
    return self.currentUser.userId.integerValue;
}

@end
