
//
//  DCCapperDetailsView.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCapperDetailsView.h"
#import "DCClassicHeaderView.h"

@implementation DCCapperDetailsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentView addSubview:self.tableView];
        
        CGFloat bgOffset = [UIApplication sharedApplication].statusBarFrame.size.height > 20 ? [UIScreen mainScreen].bounds.size.width * 0.7 : [UIScreen mainScreen].bounds.size.width * 0.55;
        self.tableView.contentInset = UIEdgeInsetsMake(bgOffset, 0.0, 0.0, 0.0);
        
        _plusButton = [UIButton new];
        [self.plusButton setImage:[UIImage imageNamed:@"add-alt"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.plusButton];
        
        [self.contentView bringSubviewToFront:self.backButton];
        [self.contentView bringSubviewToFront:self.infoButton];
        
        self.contentView.clipsToBounds = NO;
        
        self.headerView.backgroundColor = [UIColor bgColor];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"backButton": self.backButton,
        @"bgImage": self.backgroundImageView,
        @"table": self.tableView,
        @"plus": self.plusButton,
        @"info": self.infoButton
    };
    
    NSDictionary *metrics = @{
        @"height": @([UIScreen mainScreen].bounds.size.width * 0.55),
        @"width": @([UIScreen mainScreen].bounds.size.width),
        @"headerHeight": @(-([UIApplication sharedApplication].statusBarFrame.size.height + 44.0)),
        @"topInset": @([UIApplication sharedApplication].statusBarFrame.size.height + 13.0)
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }

    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[plus(54)]-15-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[plus(54)]-15-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-headerHeight-[bgImage(==height)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[backButton(20)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-headerHeight-[table]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bgImage(==width)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[backButton(99)]-(>=0)-[info(50)]-12-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
    
}

- (Class)headerViewClass {
    return [DCClassicHeaderView class];
}

@end
