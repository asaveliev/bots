//
//  DCCapperCollectionViewCell.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCapperCollectionViewCell.h"

@implementation DCCapperCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _avatarImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 52.0, 52.0)];
        self.avatarImageView.clipsToBounds = YES;
        self.avatarImageView.translatesAutoresizingMaskIntoConstraints = NO;
        self.avatarImageView.layer.cornerRadius = 26.0;
        [self.contentView addSubview:self.avatarImageView];
        
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[avatar]-0-|" options:0 metrics:nil views:@{ @"avatar": self.avatarImageView }]];
        [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[avatar]-0-|" options:0 metrics:nil views:@{ @"avatar": self.avatarImageView }]];
    }
    
    return self;
}

@end
