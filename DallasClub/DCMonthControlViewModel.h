//
//  DCMonthViewModel.h
//  DallasClub
//
//  Created by Anton Savelev on 04.01.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    DCMonthControlElementTypeMonth,
    DCMonthControlElementTypeYear
} DCMonthControlElementType;

@interface DCMonthControlViewModel : NSObject

- (instancetype)initWithDate:(NSDate *)date;

@property (nonatomic, strong) NSDate    *date;
@property (nonatomic, assign) BOOL      selectable;
@property (nonatomic, assign) BOOL      isSelected;

@property (nonatomic, assign) DCMonthControlElementType type;

@end
