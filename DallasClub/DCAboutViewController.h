//
//  DCAboutViewController.h
//  DallasClub
//
//  Created by Anton Savelev on 25/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCAboutViewController : UIViewController

- (instancetype)initWithText:(NSString *)text;

@end
