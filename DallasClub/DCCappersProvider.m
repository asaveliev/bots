//
//  DCCappersProvider.m
//  DallasClub
//
//  Created by Anton Savelev on 17.04.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCappersProvider.h"

static NSString *const DCCappersEndpoint = @"cappers";
NSInteger const DCCappersPageSize = INT_MAX;

@implementation DCCappersProvider

- (void)fetchCappersPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion {
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(DCCappersPageSize) forKey:@"per_page"];
    [params setValue:pageNumber forKey:@"page"];
    [self.sessionManager GET:[DCApiRoot stringByAppendingString:DCCappersEndpoint] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
