//
//  DCBubbleView.m
//  DallasClub
//
//  Created by Anton on 18/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBubbleView.h"

@implementation DCBubbleView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.clipsToBounds = YES;
        
        _percentLabel = [UILabel new];
        self.percentLabel.font = [UIFont statsPercentFont];
        self.percentLabel.textColor = [UIColor whiteColor];
        self.percentLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.percentLabel];
        
        self.percentLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[label]-0-|" options:0 metrics:nil views:@{ @"label": self.percentLabel }]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:@{ @"label": self.percentLabel }]];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.frame.size.height / 2.0;
}

@end
