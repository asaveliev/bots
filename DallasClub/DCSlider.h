//
//  DCSlider.h
//  DallasClub
//
//  Created by Anton Savelev on 12.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCSlider : UIView

@property (nonatomic, readonly) UISlider    *slider;
@property (nonatomic, readonly) UILabel     *minLabel;

@end
