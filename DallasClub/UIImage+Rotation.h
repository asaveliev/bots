//
//  UIImage+Rotation.h
//  BuyNow
//
//  Created by Anton Savelev on 20.09.16.
//  Copyright © 2016 Technocratos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

- (UIImage *)rotateImageOnDegrees:(float)degrees;
- (UIImage *)removeRotationForImage:(UIImage*)image;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

@end
