//
//  DCAddFeedView.m
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAddFeedView.h"
#import "DCClassicHeaderView.h"

@interface DCAddFeedView ()

@property (nonatomic, readonly) UIView  *bottomLine;
@property (nonatomic, readonly) UIView  *topLine;

@property (nonatomic, assign) CGFloat keyboardHeight;

@end

@implementation DCAddFeedView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.contentView.clipsToBounds = YES;
        
        self.headerTitleLabel.text = @"Прогноз";
        
        [self.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
        [self.leftButton setTitle:@"Назад" forState:UIControlStateNormal];
        [self.leftButton setContentEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
        [self.leftButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
        [self.rightButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
        [self.rightButton setTitle:@"Отправить" forState:UIControlStateNormal];
        
        _textView = [UITextView new];
        self.textView.font = [UIFont feedFont];
        self.textView.textColor = [UIColor blackColor];
        self.textView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.textView];
        
        _dateTextField = [DCDateTextField new];
        self.dateTextField.font = [UIFont regularFontOfSize:17.0];
        self.dateTextField.textColor = [UIColor blackColor];
        self.dateTextField.leftView = [[UIView alloc] initWithFrame:(CGRect){ CGPointZero, 15.0, 48.0 }];
        self.dateTextField.leftViewMode = UITextFieldViewModeAlways;
        [self.contentView addSubview:self.dateTextField];
        
        _addPhotoView = [DCAddPhotoView new];
        [self.contentView addSubview:self.addPhotoView];
        
        _numberTitleLabel = [self titleLabelWithTitle:@"НОМЕР СТАВКИ"];
        [self.contentView addSubview:self.numberTitleLabel];
        
        _dateTitleLabel = [self titleLabelWithTitle:@"дата"];
        [self.contentView addSubview:self.dateTitleLabel];
        
        _photoTitleLabel = [self titleLabelWithTitle:@"фото"];
        [self.contentView addSubview:self.photoTitleLabel];
        
        _numberTextField = [self configureTextField];
        [self.contentView addSubview:self.numberTextField];
        
        _recordButton = [UIButton new];
        [self.recordButton setImage:[UIImage imageNamed:@"record_icon"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.recordButton];
        
        _settingsCell = [DCSettingsCell new];
        self.settingsCell.titleLabel.text = @"Скрытый пост";
        [self.contentView addSubview: self.settingsCell];
        
        [self setConstraints];
        
        self.textView.contentInset = UIEdgeInsetsMake(10.0, 0.0, 10.0, 0.0);
        self.textView.textContainerInset = UIEdgeInsetsMake(0.0, 12.0, 0.0, 12.0);
        self.textView.showsVerticalScrollIndicator = NO;
        _bottomLine = [UIView new];
        [self.contentView addSubview:self.bottomLine];
        self.bottomLine.backgroundColor = UIColorFromHex(0xC3CDD4);
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.bottomLine.frame = (CGRect){ 0.0, self.textView.bounds.size.height + self.textView.frame.origin.y - 1.0, self.textView.bounds.size.width, 1.0 };
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"text": self.textView,
                            @"number": self.numberTitleLabel,
                            @"numberField": self.numberTextField,
                            @"date": self.dateTitleLabel,
                            @"dateField": self.dateTextField,
                            @"photo": self.photoTitleLabel,
                            @"photoField": self.addPhotoView,
                            @"record": self.recordButton,
                            @"switch": self.settingsCell,
                            @"contentView": self.settingsCell.contentView
                            };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    NSDictionary *metrics = @{
                              @"vSpacing": @20.0,
                              @"fieldHeight": @48.0,
                              @"textHeight": @150.0,
                              @"labelSpacing": @9.0,
                              @"recordSize": @54
                              };
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.settingsCell.contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:UIScreen.mainScreen.bounds.size.width]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.settingsCell.contentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:65]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.settingsCell attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:65]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[text(==textHeight)]-vSpacing-[photo]-labelSpacing-[photoField(==fieldHeight)]-vSpacing-[number]-labelSpacing-[numberField(==fieldHeight)]-vSpacing-[date]-labelSpacing-[dateField(==fieldHeight)]-vSpacing-[switch]" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[switch]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[numberField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[photoField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[dateField]-0-|" options:0 metrics:metrics views:views]];
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[record(==recordSize)]-15-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[record(==recordSize)]-15-|" options:0 metrics:metrics views:views]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[text]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[number]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[date]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[photo]-12-|" options:0 metrics:metrics views:views]];
}

- (DCTextField *)configureTextField {
    DCTextField *field = [DCTextField new];
    field.font = [UIFont regularFontOfSize:17.0];
    field.textColor = [UIColor blackColor];
    field.leftView = [[UIView alloc] initWithFrame:(CGRect){ CGPointZero, 15.0, 48.0 }];
    field.leftViewMode = UITextFieldViewModeAlways;
    
    return field;
}

- (UILabel *)titleLabelWithTitle:(NSString *)title {
    UILabel *label = [UILabel new];
    label.textColor = UIColorFromHex(0x6D6D72);
    label.font = [UIFont regularFontOfSize:13.0];
    label.text = [title uppercaseString];
    
    return label;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (Class)headerViewClass {
    return [DCClassicHeaderView class];
}

@end
