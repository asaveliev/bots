//
//  DCBaseView.h
//  DallasClub
//
//  Created by Anton on 20/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCBaseHeaderView.h"

@interface DCBaseView : UIView

@property (strong, nonatomic)   DCBaseHeaderView    *headerView;
@property (readonly, nonatomic) UILabel             *headerTitleLabel;
@property (readonly, nonatomic) UIButton            *rightButton;
@property (readonly, nonatomic) UIButton            *leftButton;

@property (strong, nonatomic)   UIView              *contentView;

- (void)bindTableView:(UITableView *)table forFadeWithOffset:(CGFloat)offset;

- (Class)headerViewClass;

@end
