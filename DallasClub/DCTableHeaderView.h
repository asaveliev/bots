//
//  DCTableHeaderView.h
//  DallasClub
//
//  Created by Anton on 19/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTableHeaderView : UIView

@property (strong, nonatomic) UIImageView *bgImageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subtitleLabel;

@property (strong, nonatomic) UIView *infoView;
@property (strong, nonatomic) UILabel *successLabel;
@property (strong, nonatomic) UILabel *returnLabel;
@property (strong, nonatomic) UILabel *failedLabel;
@property (strong, nonatomic) UIImageView *successImageView;
@property (strong, nonatomic) UIImageView *returnImageView;
@property (strong, nonatomic) UIImageView *failedImageView;

@end
