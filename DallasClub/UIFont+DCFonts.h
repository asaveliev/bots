//
//  UIFont+DCFonts.h
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (DCFonts)

+ (instancetype)regularFontOfSize:(CGFloat)size;
+ (instancetype)lightFontOfSize:(CGFloat)size;
+ (instancetype)ultralightFontOfSize:(CGFloat)size;

// statistics

+ (instancetype)statsTitleFontRegular;
+ (instancetype)statsTitleFontMedium;
+ (instancetype)statsPriceFont;
+ (instancetype)statsPercentFont;
+ (instancetype)statsHeaderTitleFont;
+ (instancetype)statsHeaderSubtitleFont;
+ (instancetype)statsHeaderLabelsFont;

// header view

+ (instancetype)headerViewTitleFont;
+ (instancetype)headerButtonsFont;
+ (instancetype)sectionHeaderFont;

// feed

+ (instancetype)feedFont;

@end
