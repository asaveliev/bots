//
//  DCFetchAccountDetailsOperation.h
//  DallasClub
//
//  Created by Anton on 01/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "ASConcurrentOperation.h"

@interface DCFetchAccountDetailsOperation : ASConcurrentOperation

- (instancetype)initWithCapperName:(NSString *)name;

@end
