//
//  DCPersistentStack.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface DCPersistentStack : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *context;
@property (nonatomic, readonly) NSManagedObjectContext *backgroundContext;

+ (instancetype)sharedInstance;

@end
