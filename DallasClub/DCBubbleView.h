//
//  DCBubbleView.h
//  DallasClub
//
//  Created by Anton on 18/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCBubbleView : UIView

@property (strong, nonatomic, readonly) UILabel *percentLabel;

@end
