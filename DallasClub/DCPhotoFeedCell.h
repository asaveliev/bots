//
//  DCPhotoFeedCell.h
//  DallasClub
//
//  Created by Anton Savelev on 26.02.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCFeedCell.h"

@protocol DCPhotoFeedCellDelegate <NSObject>

- (void)photoCellDidSelectImageWithUrl:(NSString *)url;

@end

@interface DCPhotoFeedCell : DCFeedCell

@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic, weak) id<DCPhotoFeedCellDelegate> delegate;

@end
