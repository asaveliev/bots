//
//  DCFeedSectionHeaderView.h
//  DallasClub
//
//  Created by Anton on 17/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCFeedSectionHeaderView : UITableViewHeaderFooterView

@property (strong, nonatomic) UIView    *bubbleView;
@property (strong, nonatomic) UILabel   *dateLabel;

@end
