//
//  YandexMoneyController.swift
//  DallasClub
//
//  Created by Anton Savelev on 22.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

protocol YandexMoneyControllerDelegate: class {
    func didPay(withSuccess success: Bool, modelId: String)
}

class YandexMoneyController: UIViewController {
    weak var delegate: YandexMoneyControllerDelegate?

    var finished = false
    let modelId: String
    let webView = UIWebView()
    let orderNumber: String
    let shopId = "144876"
    #if DEV
    let scid = "555819"
    #else
    let scid = "107215"
    #endif
    let customerId: String
    let sum: Double
    
    let mainView = TableContentView()
    
    override func loadView() {
        view = mainView
    }

    
    init(modelId: String, orderId: String, customerId: String, sum: Double) {
        self.modelId = modelId
        orderNumber = orderId
        self.sum = sum
        self.customerId = customerId
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.contentView.addSubview(webView)
        webView.scalesPageToFit = true
        
        #if DEV
        let url = "https://demomoney.yandex.ru/eshop.xml?shopId=\(shopId)&scid=\(scid)&sum=\(sum)&orderNumber=\(orderNumber)&customerNumber=\(customerId)"
        #else
        let url = "https://money.yandex.ru/eshop.xml?shopId=\(shopId)&scid=\(scid)&sum=\(sum)&orderNumber=\(orderNumber)&customerNumber=\(customerId)"
        #endif
        webView.loadRequest(URLRequest(url: URL(string: url)!))
        webView.delegate = self
        
        mainView.headerView.leftButton.setTitle("Отмена", for: .normal)
        mainView.headerView.leftButton.setTitleColor(UIColor.black, for: .normal)
        mainView.headerView.leftButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        webView.frame = mainView.contentView.bounds
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func backAction() {
        dismiss(animated: true, completion: nil)
    }
    
}

extension YandexMoneyController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        guard let url = request.url?.absoluteString else {
            return true
        }
        if url.contains("success") {
            if finished == true {
                return true
            }
            finished = true
            delegate?.didPay(withSuccess: true, modelId: modelId)
            dismiss(animated: true, completion: nil)
        }
        if url.contains("fail") {
            delegate?.didPay(withSuccess: false, modelId: modelId)
        }
        
        return true
    }
}
