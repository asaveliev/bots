
//
//  DCMoneyFormatter.m
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCMoneyFormatter.h"

@interface DCMoneyFormatter ()

@property (nonatomic, strong) NSNumberFormatter *formatter;

@end

@implementation DCMoneyFormatter

- (instancetype)init {
    if (self = [super init]) {
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setUsesGroupingSeparator:YES];
        formatter.groupingSeparator = @" ";
        [formatter setGroupingSize:3];
        
        self.formatter = formatter;
    }
    
    return self;
}

- (NSString *)stringFromAmount:(NSNumber *)amount {
    return [[self.formatter stringFromNumber:amount] stringByAppendingString:@" ₽"];
}

@end
