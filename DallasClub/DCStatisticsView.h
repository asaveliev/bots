//
//  DCStatisticsView.h
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"
#import "DCTableHeaderView.h"

@interface DCStatisticsView : DCBaseView

@property (strong, nonatomic, readonly) UITableView         *tableView;
@property (strong, nonatomic, readonly) DCTableHeaderView   *tableHeader;

@end
