
//
//  DCGraphicsContentView.m
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCGraphicsContentView.h"

@interface DCGraphicsContentView ()<DCGraphViewDelegate>

@property (strong, nonatomic) UIImageView *bgImageView;

@end

@implementation DCGraphicsContentView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.headerView.leftButton setImage:[UIImage imageNamed:@"close_button"] forState:UIControlStateNormal];
        
        _bgImageView = [UIImageView new];
        self.bgImageView.image = [UIImage imageNamed:@"graphs_bg"];
        [self addSubview:self.bgImageView];
        
        _graphView = [DCGraphView new];
        self.graphView.backgroundColor = [UIColor clearColor];
        self.graphView.delegate = self;
        [self addSubview:self.graphView];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.contentInset = UIEdgeInsetsMake(5.0, 0.0, 5.0, 0.0);
        [self.tableView setContentOffset:CGPointMake(0.0, -5.0) animated:YES];
        [self addSubview:self.tableView];
        
        self.headerView.backgroundColor = [UIColor clearColor];
        self.headerTitleLabel.textColor = [UIColor whiteColor];
        
        _monthsControl = [DCMonthsControl new];
        
        __weak typeof (self) weakSelf = self;
        self.monthsControl.selectionBlock = ^void(NSInteger selectedIndex, BOOL selected) {
            if (weakSelf.delegate) {
                [weakSelf.delegate graphsViewDidSelectMonthAtIndex:selectedIndex andGraphElementAtIndex:weakSelf.graphView.selecteIndex];
            }
        };
        [self addSubview:self.monthsControl];
        
        [self bringSubviewToFront:self.headerView];
        
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"По дням", @"Банк"]];
        self.segmentedControl.tintColor = [UIColor whiteColor];
        self.segmentedControl.selectedSegmentIndex = 0;
        [self addSubview:self.segmentedControl];
        
        self.monthsControl.clipsToBounds = NO;
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"bg": self.bgImageView,
                            @"graph": self.graphView,
                            @"months": self.monthsControl,
                            @"table": self.tableView,
                            @"segmented": self.segmentedControl,
                            
                            };
    NSDictionary *metrics = @{
                              @"topInset": @([UIApplication sharedApplication].statusBarFrame.size.height > 20 ? 40.0 : 29.0)
                              };
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bg]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[months]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[bg(308)]-0-[table]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-102-[graph]-0-[months]-(-4)-[table]" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[segmented(25)]" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-65-[segmented]-65-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[graph]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:nil views:views]];
    
    self.monthsHeightConstraint = [NSLayoutConstraint constraintWithItem:self.monthsControl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1.0 constant:40.0];
    [self addConstraint:self.monthsHeightConstraint];
}

#pragma mark - DCGraphViewDelegate

- (void)graphViewDidSelectElementAtIndex:(NSInteger)index {
    if (self.delegate) {
        [self.delegate graphsViewDidSelectMonthAtIndex:self.monthsControl.selectedIndex andGraphElementAtIndex:index];
    }
}

- (BOOL)graphViewCanSelectElementAtIndex:(NSInteger)index {
    return [self.delegate graphViewCanSelectElementAtIndex:index];
}

@end
