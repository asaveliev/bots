//
//  DCGoodsProvider.m
//  
//
//  Created by Anton Savelev on 03.03.17.
//
//

#import "DCGoodsProvider.h"

@implementation DCGoodsProvider

- (void)getGoodsWithCompletion:(void (^)(NSArray *, NSError *))completion {
    [self.sessionManager GET:[DCApiRoot stringByAppendingString:@"goods"] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
