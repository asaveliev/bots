//
//  Bet+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 30/08/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bet.h"

@class Post;

NS_ASSUME_NONNULL_BEGIN

@interface Bet (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *amout;
@property (nullable, nonatomic, retain) NSNumber *balance_change;
@property (nullable, nonatomic, retain) NSNumber *coefficient;
@property (nullable, nonatomic, retain) NSDate *created;
@property (nullable, nonatomic, retain) NSNumber *number;
@property (nullable, nonatomic, retain) NSString *ordering;
@property (nullable, nonatomic, retain) NSString *capper_name;
@property (nullable, nonatomic, retain) NSNumber *state;
@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) Post *post;

@end

NS_ASSUME_NONNULL_END
