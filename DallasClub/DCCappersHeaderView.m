//
//  DCCappersHeaderView.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCappersHeaderView.h"

@interface DCCappersHeaderView ()

@property (nonatomic, readonly) UIView *separatorView;

@end

@implementation DCCappersHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.minimumInteritemSpacing = 16.0;
        layout.itemSize = CGSizeMake(52.0, 52.0);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.backgroundColor = [UIColor whiteColor];
        self.collectionView.contentInset = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0);
        self.collectionView.showsHorizontalScrollIndicator = NO;
        [self addSubview:self.collectionView];
        
        self.backgroundColor = [UIColor whiteColor];
        
        _separatorView = [UIView new];
        self.separatorView.backgroundColor = UIColorFromHex(0xC3CDD4);
        [self addSubview:self.separatorView];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"collection": self.collectionView,
        @"separator": self.separatorView
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[separator]-0-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[collection]-0-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[collection]-12-[separator(0.5)]-0-|" options:0 metrics:nil views:views]];
}

@end
