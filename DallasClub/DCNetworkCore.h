//
//  DCNetworkCore.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DCAuthProvider.h"
#import "DCFeedProvider.h"
#import "DCStatisticsProvider.h"
#import "DCNotificationsApiProvider.h"
#import "DCGoodsProvider.h"
#import "DCMaterialsProvider.h"
#import "DCCappersProvider.h"

@interface DCNetworkCore : NSObject

@property (nonatomic, readonly) DCAuthProvider              *authProvider;
@property (nonatomic, readonly) DCFeedProvider              *feedProvider;
@property (nonatomic, readonly) DCStatisticsProvider        *statsProvider;
@property (nonatomic, readonly) DCNotificationsApiProvider  *notificationsProvider;
@property (nonatomic, readonly) DCGoodsProvider             *goodsProvider;
@property (nonatomic, readonly) DCCappersProvider           *cappersProvider;
@property (nonatomic, readonly) DCMaterialsProvider         *materialsProvider;

@property (strong, nonatomic)   AFHTTPSessionManager        *sessionManager;

- (void)updateAuthHeader;
+ (instancetype)sharedInstance;

@end
