//
//  DCFetchAccountDetailsOperation.m
//  DallasClub
//
//  Created by Anton on 01/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

/* {
 "failed_bets":
 1,
 "month_balance_change":
 "72770.0",
 "overall_balance_change":
 "72770.0",
 "returned_bets":
 1,
 "successful_bets":
 6,
 "week_balance_change":
 "72770.0"
 } */

#import "DCFetchAccountDetailsOperation.h"
#import "Account.h"
#import "Capper.h"

@interface DCFetchAccountDetailsOperation()

@property (nonatomic, readonly)NSString *capperName;

@end

@implementation DCFetchAccountDetailsOperation

- (instancetype)initWithCapperName:(NSString *)name {
    if (self = [super init]) {
        _capperName = name;
    }
    
    return self;
}

- (void)main {
    NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
    NSNumber *capperId;
    Capper *capper = [Capper getByField:@"name" equalsTo:self.capperName fromContext:context];
    capperId = capper.uid;
    
    [[DCNetworkCore sharedInstance].statsProvider getFullStatisticsWithCapperId:capperId completion:^(NSDictionary *items, NSError *error) {
        if (error || ![items isKindOfClass:[NSDictionary class]]) {
            [self completeOperationWithError:error];
            return;
        }
        
        [context performBlock:^{
            Account *acc = [Account getByField:nil equalsTo:nil fromContext:context];
            if (!acc) {
                acc = [Account createInContext:context];
            }
            
            [acc setDictionaryRepresentation:items];
            acc.balance = capper.balance;
            
            NSError *err;
            [context save:&err];
            if (err) {
                [self completeOperationWithError:err];
                return;
            }
            
            
           /* [[DCNetworkCore sharedInstance].statsProvider getAccountWithCapperId:capperId completion:^(NSDictionary *items, NSError *error) {
                if (error || ![items isKindOfClass:[NSDictionary class]]) {
                    [self completeOperationWithError:error];
                    return;
                }
                [context performBlock:^{
                    acc.balance = @([items[@"current_balance"] doubleValue]);
                    
                    NSError *err;
                    [context save:&err];
                    if (err) {
                        [self completeOperationWithError:err];
                        return;
                    }
                }];
            }]; */
        }];
    }];
}

@end
