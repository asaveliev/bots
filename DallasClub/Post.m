//
//  Post.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "Post.h"
#import "Bet.h"
#import "NSDateFormatter+ISO8601.h"
#import "NSDate+Utilities.h"
#import "DCPersistentStack.h"
#import "DCApiProvider.h"
#import <SDWebImage/SDImageCache.h>

@implementation Post

-(void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    self.created = [NSDateFormatter dateFromStringForISO8601:dictionaryRepresentation[@"created_at"]];
    self.content = dictionaryRepresentation[@"content"];
    self.uid = dictionaryRepresentation[@"id"];
    self.capperPhotoUrl = dictionaryRepresentation[@"capper_photo"];
    self.capper_name = dictionaryRepresentation[@"capper_name"];
    if (![dictionaryRepresentation[@"number"] isKindOfClass:[NSNull class]]) {
        self.bet = [Bet getByField:@"number" equalsTo:dictionaryRepresentation[@"number"] fromContext:[DCPersistentStack sharedInstance].backgroundContext];
    }
    if (![dictionaryRepresentation[@"audio_url"] isKindOfClass:[NSNull class]]) {
        self.audioUrl = dictionaryRepresentation[@"audio_url"];
    }
    
    self.imageUrl = @"";
    NSDate *date = [NSDateFormatter dateFromStringForISO8601:dictionaryRepresentation[@"updated_at"]];
    if (![dictionaryRepresentation[@"photo_url"] isKindOfClass:[NSNull class]]) {
        if ([dictionaryRepresentation[@"photo_url"] length]) {
            NSString *imageUrl = dictionaryRepresentation[@"photo_url"];
            self.imageUrl = imageUrl;
            
            if (date.timeIntervalSince1970 > self.updated_at.timeIntervalSince1970) {
                [[SDImageCache sharedImageCache] removeImageForKey:self.imageUrl fromDisk:YES];
            }
        }
    }
    
    self.updated_at = date;
    
    self.ordering = [self.created stringWithFormat:@"yyyy-MM-dd"];
}

@end
