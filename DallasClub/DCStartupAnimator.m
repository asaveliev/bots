
//
//  DCStartupAnimator.m
//  DallasClub
//
//  Created by Anton Savelev on 08/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStartupAnimator.h"

@interface DCStartupAnimator ()

@property (strong, nonatomic) UIView *view;

@end

@implementation DCStartupAnimator

- (instancetype)initWithView:(UIView *)view {
    if (self = [super init]) {
        _view = view;
        _animationDuration = 0.8;
    }
    
    return self;
}

- (void)animateWithCompletion:(void (^)(BOOL finished))completion {
    CGRect targetFrame = self.view.frame;
    self.view.frame = (CGRect){ targetFrame.origin.x, targetFrame.origin.y + targetFrame.size.width / 8.0, targetFrame.size };
    self.view.alpha = 0.0;
    [UIView animateWithDuration:self.animationDuration delay:1.0 usingSpringWithDamping:0.5 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.view.alpha = 1.0;
        self.view.frame = targetFrame;
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

@end
