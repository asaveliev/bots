//
//  DCBubbleImageView.h
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBubbleView.h"

@interface DCBubbleImageView : DCBubbleView

@property (nonatomic, strong) UIImageView *imageView;

@end
