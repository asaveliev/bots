//
//  HABaseHeaderView.h
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCBaseHeaderView : UIView

@property (readonly, nonatomic) UILabel   *headerTitleLabel;
@property (readonly, nonatomic) UIButton  *rightButton;
@property (readonly, nonatomic) UIButton  *leftButton;

@end
