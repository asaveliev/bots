//
//  DCStartupView.h
//  DallasClub
//
//  Created by Anton Savelev on 07/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCStartupView : UIView

@property (readonly, nonatomic) UIImageView *avatarImageView;
@property (readonly, nonatomic) UITableView *tableView;
@property (readonly, nonatomic) UILabel     *nameLabel;
@property (readonly, nonatomic) UIButton    *okButton;

@end
