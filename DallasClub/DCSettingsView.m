//
//  DCSettingsView.m
//  DallasClub
//
//  Created by Anton on 19/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCSettingsView.h"
#import "DCClassicHeaderView.h"

@implementation DCSettingsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.tableView];
    }
    
    return self;
}

- (Class)headerViewClass {
    return nil;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.contentView.bounds;
    if (IPAD) {
        frame.origin.x += 100;
        frame.size.width -= 200;
    }
    frame.size.height -= 44.0;
    self.tableView.frame = frame;
}

@end
