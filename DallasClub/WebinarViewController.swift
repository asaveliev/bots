//
//  WebinarViewController.swift
//  DallasClubProd
//
//  Created by Anton on 19.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

import UIKit
import Down
import SVProgressHUD
import AVKit
import IQKeyboardManagerSwift

class WebinarViewController: UIViewController {
    let contentView = WebinarView()
    let provider = ModelProvider<Webinar>()
    var model: Webinar?
    
    let videoPlayer = AVPlayerViewController()
    var player: AVPlayer?
    
    let defaults = UserDefaults.standard
    
    override func loadView() {
        view = contentView
        contentView.actionButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(forName: .UIApplicationDidEnterBackground, object: nil, queue: .main) { _ in
            self.saveTime()
        }
        
        NotificationCenter.default.addObserver(forName: .UIApplicationDidBecomeActive, object: nil, queue: .main) { _ in
            self.seekIfNeeded()
            self.player?.play()
        }
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: .main) { _ in
            self.videoPlayer.player?.seek(to: kCMTimeZero)
            
            self.videoPlayer.showsPlaybackControls = true
            self.defaults.set(true, forKey: "videoDone")
            self.player?.pause()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShowed), name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardHidden), name: .UIKeyboardDidHide, object: nil)
        
        contentView.sendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
    }
    
    func setupVideo() {
        guard let urlString = model?.videoUrl, let url = URL(string: urlString) else { return }
        player = AVPlayer(url: url)
        videoPlayer.player = player
        addChildViewController(videoPlayer)
        videoPlayer.view.frame = contentView.videoContainerView.bounds
        videoPlayer.showsPlaybackControls = defaults.bool(forKey: "videoDone")
        contentView.videoContainerView.addSubview(videoPlayer.view)
        
        if defaults.bool(forKey: "videoDone") == false {
            player?.play()
        }
        seekIfNeeded()
    }
    
    func getSeekTime() -> CMTime? {
        guard
            let currentTime = defaults.object(forKey: "currentTime") as? Float64,
            let savedDate = defaults.object(forKey: "currentDate") as? Date
        else { return nil }
        
        let currentDate = Date()
        let dateInterval = currentDate.timeIntervalSince(savedDate)
        let seek = currentTime + dateInterval
        return CMTimeMakeWithSeconds(seek, 1000000)
    }
    
    @objc
    func buttonAction() {
        guard let url = URL(string: model?.url ?? "") else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        saveTime()
        player?.pause()
    }
    
    func saveTime() {
        guard defaults.bool(forKey: "videoDone") == false else { return }
        
        guard let time = player?.currentItem?.currentTime() else { return }
        defaults.set(CMTimeGetSeconds(time), forKey: "currentTime")
        defaults.set(Date(), forKey: "currentDate")
        
        guard let duration = player?.currentItem?.asset.duration, duration > kCMTimeZero else { return }
        
        defaults.set(CMTimeGetSeconds(duration), forKey: "duration")
    }
    
    func seekIfNeeded() {
        guard defaults.bool(forKey: "videoDone") == false else { return }
        
        guard let seekTime = getSeekTime() else { return }
        player?.seek(to: seekTime)
        
        guard let duration = defaults.object(forKey: "duration") as? Float64, duration > 0 else { return }
        let durationTime = CMTimeMakeWithSeconds(duration, 1000000)
        videoPlayer.showsPlaybackControls = seekTime > durationTime
        if seekTime > durationTime {
            defaults.set(true, forKey: "videoDone")
            player?.seek(to: CMTimeMakeWithSeconds(0, 1000000))
            player?.pause()
        } else {
            player?.play()
        }
    }
    
    @objc
    func sendAction() {
        guard let text = contentView.messageInput.messageInput.textView.text, !text.isEmpty else { return }
        
        contentView.endEditing(true)
        SVProgressHUD.show()
        contentView.messageInput.messageInput.textView.text = nil
        DCNetworkCore.sharedInstance().sessionManager.post(DCApiRoot.appending("webinar/contact?message=\(text.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"), parameters: nil, progress: nil, success: { _, _ in
            SVProgressHUD.dismiss()
            self.contentView.showSuccessView()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.contentView.hideSuccessView()
            })
        }) { _, _ in
            SVProgressHUD.dismiss()
            self.showTryLater()
        }
    }
    
    @objc
    func keyboardShowed(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            contentView.setSendButtonHidden(false, keyboardHeight: keyboardHeight)
        }
    }
    
    @objc
    func keyboardHidden() {
        contentView.setSendButtonHidden(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        contentView.actionButton.isHidden = true
        contentView.messageInput.isHidden = true
        if videoPlayer.view.superview != nil {
            contentView.actionButton.isHidden = false
            contentView.messageInput.isHidden = false
            seekIfNeeded()
            return
        }
        
        IQKeyboardManager.sharedManager().enable = false
        
        SVProgressHUD.show()
        provider.getModel(from: "webinar") { model, _ in
            self.model = model
            SVProgressHUD.dismiss()
            
            guard
                let model = model
            else {
                self.showTryLater()
                return
            }
            
            self.contentView.actionButton.isHidden = false
            self.contentView.messageInput.isHidden = false
            
            let markdown = Down(markdownString: model.description)
            self.contentView.titleLabel.text = model.name
            let text = NSMutableAttributedString(attributedString: (try? markdown.toAttributedString()) ?? NSAttributedString())
            text.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 16), range: NSMakeRange(0, text.length))
            self.contentView.textView.attributedText = text
            self.contentView.buttonTitleLabel.text = model.buttonCaption
            self.contentView.actionButton.setTitle(model.buttonText, for: .normal)
            
            self.setupVideo()
        }
    }
}
