//
//  DCInfoCell.m
//  DallasClub
//
//  Created by Anton Savelev on 10.06.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCInfoCell.h"

@interface DCSettingsCell ()

@property (nonatomic, readonly) UIView *bottomLine;
@property (nonatomic, readonly) UIView *topLine;
@property (nonatomic, readonly) UIView *titleContainerView;

@end

@implementation DCInfoCell

- (void)setConstraints {
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.titleLabel.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
    };
    
    self.switchControl.hidden = YES;
    NSDictionary *views = @{
                            @"container": self.titleContainerView,
                            @"title": self.titleLabel,
                            @"subtitle": self.subtitleLabel,
                            @"switch": self.switchControl,
                            @"bottom": self.bottomLine
                            };
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[subtitle]-15-[container(>=48)]-5-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[container]-0-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[subtitle]-15-|" options:0 metrics:nil views:views]];

    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bottom]-0-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-18-[title]-20-[bottom(0.5)]-0-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[title]-30-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[switch]-15-|" options:0 metrics:nil views:views]];
    [self.titleContainerView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleContainerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.switchControl attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

@end
