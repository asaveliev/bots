//
//  Post.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseManagedObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface Post : BaseManagedObject <Serializable>

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Post+CoreDataProperties.h"
