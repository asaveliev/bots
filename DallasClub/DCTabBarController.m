//
//  DCTabBarController.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCTabBarController.h"

#import "DCStatisticsViewController.h"
#import "DCFeedViewController.h"
#import "DCSettingsViewController.h"
#import "DCMaterialViewController.h"
#import <SDWebImage/SDWebImageManager.h>
#import "SVProgressHUD.h"
#import "RMUniversalAlert.h"
#import "UIImage+Additions.h"
#import "DCNetworkCore.h"
#import "DCUserModel.h"
#import "UIImage+Rotation.h"
#import "DallasClub-Swift.h"
#import "DCChatViewController.h"

NSString *const DCUpdateTabBarNotification = @"DCUpdateTabBarNotification";

@interface DCTabBarController () <UITabBarControllerDelegate>

@property (nonatomic, strong) UIViewController *prevSelectedViewController;
@property (nonatomic, strong) BanProvider *banProvider;

@end

@implementation DCTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    [self setViewControllers:@[[DCFeedViewController new], [DCSettingsViewController new]]];
    self.prevSelectedViewController = self.viewControllers.firstObject;
    [self setupTabBar];
    [self setupScreenshotBan];
    self.delegate = self;
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget: self action: @selector(longPressAction)];
    [self.tabBar addGestureRecognizer:longPressGesture];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupTabBar) name:DCUpdateTabBarNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLogin) name:DCUnathorizedNotificationKey object:nil];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DCFirstVideoLaunch"];
}

- (void)showLogin {
    [self presentViewController:[LoginViewController new] animated:true completion:nil];
}

- (void)setupScreenshotBan {
    self.banProvider = [BanProvider new];
    
    [[NSNotificationCenter defaultCenter]
     addObserverForName:UIApplicationUserDidTakeScreenshotNotification
     object:nil
     queue:nil
     usingBlock:^(NSNotification * _Nonnull note) {
         if ([DCNetworkCore sharedInstance].authProvider.currentUser == nil) {
             return;
         }
         [self.banProvider warnUserWithCompletion:^(NSInteger count) {
             if (count > 1) {
                [RMUniversalAlert showAlertInViewController:self
                                                  withTitle:@"Внимание!"
                                                    message:@"Ваш аккаунт забанен. По всем вопросам пишите в группу ВК."
                                          cancelButtonTitle:@"ОК"
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil
                                                   tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                                                       [self logout];
                                                    }
                ];
                return;
             }
             [RMUniversalAlert showAlertInViewController:self
                                               withTitle:@"Внимание!"
                                                 message:@"Предупреждение! За повторный скриншот ваш аккаунт будет забанен!"
                                       cancelButtonTitle:@"ОК"
                                  destructiveButtonTitle:nil
                                       otherButtonTitles:nil
                                                tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                                                }
              ];
         }];
     }];
    
    [self.banProvider checkBanWithCompletion:^(BOOL banned) {
        if (!banned) {
            return;
        }
        
        [RMUniversalAlert showAlertInViewController:self
                                          withTitle:@"Внимание!"
                                            message:@"Ваш аккаунт забанен. По всем вопросам пишите в группу ВК."
                                  cancelButtonTitle:@"ОК"
                             destructiveButtonTitle:nil
                                  otherButtonTitles:nil
                                           tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                                               [self logout];
                                           }
         ];
    }];
}

- (void)logout {
    if (![DCNetworkCore sharedInstance].authProvider.hasAuth) {
        return;
    }
    
    DCAuthProvider *auth = [DCNetworkCore sharedInstance].authProvider;
    [auth removeUser:[DCNetworkCore sharedInstance].authProvider.currentUser];
    if (auth.currentUser.isCapper.boolValue) {
        if (!auth.users.lastObject) {
            [[DCNetworkCore sharedInstance] updateAuthHeader];
            [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
            return;
        }
        [auth selectUser:auth.users.lastObject completion:^(BOOL success) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
        }];
        return;
    }
    [auth selectUser:nil completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
}

- (void)setupTabBar {
    NSArray *titles = @[@"Прогнозы", [DCNetworkCore sharedInstance].authProvider.currentUser ? @"Профиль" : @"Настройки"];
    NSArray *icons = @[@"tapbarBotIcon", @"tapbarSettingsIconPassive",];
    NSArray *active = @[@"tapbarBotIconActive", @"tapbarSettingsIconActive"];
    self.tabBar.tintColor = [UIColor tabBarTintColor];
    
    for (UITabBarItem *item in self.tabBar.items) {
        NSInteger index = [self.tabBar.items indexOfObject:item];
        item.title = titles[index];
        item.image = [[UIImage imageNamed:icons[index]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        item.selectedImage = [[UIImage imageNamed:active[index]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [item setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"SFUIText-Regular" size:10.0] } forState:UIControlStateNormal];
        
        if (index + 1 == self.tabBar.items.count && [DCNetworkCore sharedInstance].authProvider.currentUser) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[DCNetworkCore sharedInstance].authProvider.currentUser.avatarUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                CGFloat size = [UIImage imageNamed:icons[index]].size.height;
                UIImage *generated = [UIImage imageWithImage:image ?: [UIImage imageNamed:@"ava"] scaledToSize:CGSizeMake(size, size)];
                generated = [generated add_imageWithCornerRadius:size / 2.0];
                item.image = [generated imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                item.selectedImage = [generated imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            }];
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)longPressAction {
    if (self.presentedViewController || ![DCNetworkCore sharedInstance].authProvider.currentUser.isCapper.boolValue) {
        return;
    }
    
    __weak DCTabBarController *weakSelf = self;
    if (self.selectedIndex + 1 == self.tabBar.items.count && [DCNetworkCore sharedInstance].authProvider.currentUser) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Переключение аккаунта" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        NSMutableArray *users = [[DCNetworkCore sharedInstance].authProvider.users mutableCopy];
        for (int i = 0; i < [DCNetworkCore sharedInstance].authProvider.users.count; i++) {
            DCUserModel *user = [DCNetworkCore sharedInstance].authProvider.users[i];
            UIAlertAction *action = [UIAlertAction actionWithTitle:user.name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[DCNetworkCore sharedInstance].authProvider selectUser:user completion:^(BOOL success) {
                    if (!success) {
                        [RMUniversalAlert showAlertInViewController:weakSelf withTitle:@"Ошибка" message:@"Неверный логин или пароль" cancelButtonTitle:@"Oк" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
                        return;
                    }
                    
                    
                    [weakSelf setupTabBar];
                }];
            }];
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:user.avatarUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                CGFloat size = 35.0;
                UIImage *generated = [UIImage imageWithImage:image scaledToSize:CGSizeMake(size, size)];
                generated = [generated add_imageWithCornerRadius:size / 2.0];
                [action setValue:[generated imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
                [alert addAction:action];
                
                [users removeLastObject];
                if (!users.count) {
                    [alert addAction:[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil]];
                    [weakSelf presentViewController:alert animated:YES completion:nil];
                    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Добавить аккаунт" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Вход" message:nil preferredStyle:UIAlertControllerStyleAlert];
                        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                            textField.placeholder = @"Email";
                        }];
                        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                            textField.placeholder = @"Пароль";
                            textField.secureTextEntry = YES;
                        }];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Войти" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [SVProgressHUD show];
                            [[DCNetworkCore sharedInstance].authProvider authWithLogin:alert.textFields[0].text andPass:alert.textFields[1].text completion:^(BOOL success) {
                                [SVProgressHUD dismiss];
                                if (!success) {
                                    [RMUniversalAlert showAlertInViewController:weakSelf withTitle:@"Ошибка" message:@"Неверный логин или пароль" cancelButtonTitle:@"Oк" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
                                    return;
                                }
                                [weakSelf setupTabBar];
                                [alert dismissViewControllerAnimated:YES completion:nil];
                            }];
                        }]];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil]];
                        
                        [weakSelf presentViewController:alert animated:YES completion:nil];
                    }];
                    [alert addAction:action];
                }
            }];
        }
    }
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[DCFeedViewController class]] && self.prevSelectedViewController == viewController) {
        DCFeedViewController *feed = (DCFeedViewController *)viewController;
        [feed scrollToTop];
    }
    
    self.prevSelectedViewController = viewController;
}

@end
