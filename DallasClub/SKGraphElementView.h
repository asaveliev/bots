//
//  SKGraphElementView.h
//  SKeeper
//
//  Created by Anton on 19/05/16.
//  Copyright © 2016 Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SKGraphElementView;

@protocol SKGraphElementDelegateProtocol <NSObject>

- (void)graphElementViewDidTapped:(SKGraphElementView *)graphView;

@end

@interface SKGraphElementView : UIView

@property (strong, nonatomic, readonly) UIView              *progressView;
@property (strong, nonatomic, readonly) UILabel             *titleLabel;
@property (nonatomic)                   CGFloat             progress;
@property (strong, nonatomic) NSLayoutConstraint            *progressViewSize;

@property (assign, nonatomic) BOOL                          selected;
@property (nonatomic) id<SKGraphElementDelegateProtocol>    delegate;

@property (assign, nonatomic) BOOL                          negative;

- (void)setConstraints;
- (CGFloat)maximumSize;

@end
