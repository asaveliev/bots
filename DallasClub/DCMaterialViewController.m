//
//  DCMaterialViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMaterialViewController.h"
#import "DCMaterialsView.h"
#import "DCMaterialDataSource.h"
#import "Material.h"

@interface DCMaterialViewController () <DCDataSourceDelegate>

@property (nonatomic, readonly) DCMaterialsView *mainView;
@property (nonatomic, readonly) DCMaterialDataSource *dataSource;

@end

@implementation DCMaterialViewController

- (void)loadView {
    self.view = _mainView = [DCMaterialsView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.mainView.headerView.headerTitleLabel.text = @"Полезные материалы";
    _dataSource = [[DCMaterialDataSource alloc] initWithTableView:self.mainView.tableView];
    [self.dataSource.refreshControl removeFromSuperview];
    self.dataSource.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataSource:(id)dataSource didSelectObjectAtIndexPath:(NSIndexPath *)indexPath {
    Material *model = [dataSource objectAtIndexPath:indexPath];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.url]];
}

@end
