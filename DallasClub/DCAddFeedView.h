//
//  DCAddFeedView.h
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"
#import "DCDateTextField.h"
#import "DCAddPhotoView.h"
#import "DCSettingsCell.h"

@interface DCAddFeedView : DCBaseView

@property (nonatomic, readonly) UILabel         *numberTitleLabel;
@property (nonatomic, readonly) UILabel         *dateTitleLabel;
@property (nonatomic, readonly) UILabel         *photoTitleLabel;

@property (nonatomic, readonly) DCDateTextField *dateTextField;
@property (nonatomic, readonly) DCTextField     *numberTextField;
@property (nonatomic, readonly) DCAddPhotoView  *addPhotoView;
@property (nonatomic, readonly) DCSettingsCell  *settingsCell;
@property (nonatomic, readonly) UIButton        *recordButton;

@property (strong, nonatomic) UITextView        *textView;

@end
