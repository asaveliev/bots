//
//  DCTableHeaderCell.h
//  DallasClub
//
//  Created by Anton Savelev on 22.01.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCTableHeaderCell : UITableViewHeaderFooterView

@property (nonatomic, strong) UILabel   *titleLabel;
@property (nonatomic, strong) UILabel   *dateLabel;
@property (nonatomic, strong) UIButton  *aboutButton;

@end
