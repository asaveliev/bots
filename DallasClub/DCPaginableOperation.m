//
//  DCPaginableOperation.m
//  DallasClub
//
//  Created by Anton Savelev on 18.09.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCPaginableOperation.h"

@implementation DCPaginableOperation

- (instancetype)initWithPageNumber:(NSNumber *)page {
    if (self = [super init]) {
        _page = page ?: @1;
    }
    
    return self;
}

@end
