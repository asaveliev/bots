//
//  DCFetchBetsOperation.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCPaginableOperation.h"

@interface DCFetchBetsOperation : DCPaginableOperation

- (instancetype)initWithPageNumber:(NSNumber *)page capperName:(NSString *)name;

@end
