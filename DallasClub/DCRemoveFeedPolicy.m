//
//  DCRemoveFeedPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRemoveFeedPolicy.h"

@implementation DCRemoveFeedPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    [[DCNetworkCore sharedInstance].feedProvider removePostWithId:self.postId completion:^(NSError *error) {
        completion(error);
    }];
}

@end
