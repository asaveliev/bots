//
//  DCAudioStreamer.m
//  DallasClub
//
//  Created by Anton Savelev on 12.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAudioStreamer.h"
#import "STKAudioPlayer.h"

@interface DCAudioStreamer () <STKAudioPlayerDelegate>

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation DCAudioStreamer

- (instancetype)init {
    if (self = [super init]) {
        _player = [STKAudioPlayer new];
        self.player.delegate = self;
    }
    
    return self;
}

-(void) setupTimer {
    if (self.timer) {
        [self.timer invalidate];
    }
    _timer = [NSTimer timerWithTimeInterval:0.001 target:self selector:@selector(tick) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
}

-(void) tick {
    if (!self.player) {
        self.slider.slider.value = 0;
        return;
    }
    
    if (self.player.currentlyPlayingQueueItemId == nil) {
        self.slider.slider.value = 0;
        self.slider.slider.minimumValue = 0;
        self.slider.slider.maximumValue = 1.0;
        self.slider.minLabel.hidden = YES;
        return;
    }
    
    if (self.player.duration != 0 && self.playingAudioUrl) {
        self.slider.slider.minimumValue = 0;
        self.slider.slider.maximumValue = self.player.duration;
        self.slider.slider.value = self.player.progress;
        
        self.slider.minLabel.hidden = NO;
        
        self.slider.minLabel.text = [self formatTimeFromSeconds:self.player.duration - self.player.progress];
    }
    else {
        self.slider.minLabel.hidden = YES;
        self.slider.slider.value = 0;
        self.slider.slider.minimumValue = 0;
        self.slider.slider.maximumValue = 1.0;
    }
}

- (void)playAudioWithUrl:(NSString *)url {
    [self.timer invalidate];
    [self.player clearQueue];
    _playingAudioUrl = url;
    _paused = NO;
    
    [self.player play:url];
    [self setupTimer];
}

- (void)setSlider:(DCSlider *)slider {
    _slider = slider;
   // [self tick];
}

- (void)setPaused:(BOOL)paused {
    _paused = paused;
    
    if (paused) {
        [self.player pause];
    } else {
        [self.player resume];
    }
}


-(NSString*) formatTimeFromSeconds:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    if (hours) {
        return [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
    }
    
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

#pragma mark - player delegate 

/// Raised when an item has started playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didStartPlayingQueueItemId:(NSObject*)queueItemId {
    
}
/// Raised when an item has finished buffering (may or may not be the currently playing item)
/// This event may be raised multiple times for the same item if seek is invoked on the player
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject*)queueItemId {
    
}
/// Raised when the state of the player has changed
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState {
    if (state == STKAudioPlayerStateStopped) {
        _playingAudioUrl = nil;
        [self.delegate streamerDidEnd];
    }
}
/// Raised when an item has finished playing
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer didFinishPlayingQueueItemId:(NSObject*)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration {
    
}
/// Raised when an unexpected and possibly unrecoverable error has occured (usually best to recreate the STKAudioPlauyer)
-(void) audioPlayer:(STKAudioPlayer*)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode {
    
}

@end
