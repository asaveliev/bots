//
//  DCFeedSectionHeaderView.m
//  DallasClub
//
//  Created by Anton on 17/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedSectionHeaderView.h"

@interface DCFeedSectionHeaderView ()

@end

@implementation DCFeedSectionHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        _bubbleView = [UIView new];
        self.bubbleView.backgroundColor = UIColorFromHex(0x1e75ff);
        self.bubbleView.clipsToBounds = YES;
        [self.contentView addSubview:self.bubbleView];
        
        _dateLabel = [UILabel new];
        self.dateLabel.textColor = [UIColor whiteColor];
        self.dateLabel.font = [UIFont sectionHeaderFont];
        [self.bubbleView addSubview:self.dateLabel];
        
        self.backgroundColor = [UIColor clearColor];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.bubbleView.layer.cornerRadius = self.bubbleView.bounds.size.height / 2.0;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"bubble": self.bubbleView,
                            @"label": self.dateLabel
                           };
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[bubble]-4-|" options:0 metrics:nil views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bubble]-13-|" options:0 metrics:nil views:views]];
    
    [self.bubbleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:nil views:views]];
    [self.bubbleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[label]-4-|" options:0 metrics:nil views:views]];
}

@end
