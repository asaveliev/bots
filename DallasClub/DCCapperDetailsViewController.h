//
//  DCCapperDetailsViewController.h
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Capper;

@interface DCCapperDetailsViewController : UIViewController

- (instancetype)initWithCapperModel:(Capper *)capper;

@end
