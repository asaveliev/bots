//
//  DCStartupViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 07/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStartupViewController.h"
#import "DCStartupView.h"
#import "DCStartupCell.h"
#import "DCTabBarController.h"

#import "RMUniversalAlert.h"

#import "DCStartupAnimationsCoordinator.h"

@interface DCStartupViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) DCStartupView *mainView;

@property (strong, nonatomic) NSArray                           *titles;
@property (strong, nonatomic) DCStartupAnimationsCoordinator    *animationsCoordinator;
@property (assign, nonatomic) BOOL                              animationsComplete;

@end

@implementation DCStartupViewController

- (void)loadView {
    self.view = self.mainView = [DCStartupView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.tableView.delegate = self;
    self.mainView.tableView.dataSource = self;
    [self.mainView.tableView registerClass:[DCStartupCell class] forCellReuseIdentifier:@"Cell"];
    
    self.mainView.avatarImageView.image = [UIImage imageNamed:@"startup_ava"];
    self.mainView.nameLabel.text = @"Айрат Даллас";
    
    [self.mainView.okButton setTitle:@"Да, хочу! 💪💪💪" forState:UIControlStateNormal];
    [self.mainView.okButton addTarget:self action:@selector(openAppAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.mainView.tableView.userInteractionEnabled = NO;
    self.animationsComplete = NO;
    NSArray *viewsToAnimate = @[self.mainView.avatarImageView, self.mainView.nameLabel];
    _animationsCoordinator = [[DCStartupAnimationsCoordinator alloc] initWithViews:viewsToAnimate];
    [self.animationsCoordinator prepareViews];
    
    self.titles = @[@"Привет! Я Айрат Даллас! 🙂", @"Я профессионально занимаюсь ставками на ⚽️⚽️⚽️ более 6 лет! 🔥", @"Зарабатываю от $1,000 до $5,000 💰💰💰в месяц.", @"Хочешь также? 😏"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSMutableArray *views = [NSMutableArray new];
    for (DCStartupCell *cell in self.mainView.tableView.visibleCells) {
        [views addObject:cell.contentView];
    }
    [views addObject:self.mainView.okButton];
    [self.animationsCoordinator appendViewsToAnimate:views];
    __weak typeof(self) weakSelf = self;
    [self.animationsCoordinator performAnimationsWithCompletion:^{
        weakSelf.mainView.tableView.userInteractionEnabled = YES;
        weakSelf.animationsComplete = YES;
    }];
}

- (void)openAppAction {
    NSString *text = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texts" ofType:@"plist"]][@"agreement"];
    [RMUniversalAlert showAlertInViewController:self withTitle:@"Соглашение" message:text cancelButtonTitle:@"Я согласен" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"DCAgreeKey"];
        [defaults synchronize];
        [self.navigationController setViewControllers:@[[DCTabBarController new]] animated:YES];
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DCStartupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.titleLabel.text = self.titles[indexPath.row];
    cell.contentView.alpha = self.animationsComplete ? 1.0 : 0.0;
    return cell;
}

@end
