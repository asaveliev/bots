//
//  DCStatisticsDataSource.h
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCGraphView.h"

typedef enum : NSUInteger {
    DCGraphModeDays,
    DCGraphModeYears
} DCGraphMode;

@class Capper;

@protocol DCStatisticsDataSourceDelegate <NSObject>

- (void)statisticsDataSourceDidUpdate;

@end

@interface DCStatisticsDataSource : NSObject

@property (nonatomic, assign) DCGraphMode   mode;
@property (nonatomic, strong) NSDate        *selectedDay;
@property (nonatomic, strong) DCGraphView   *graphView;

@property (nonatomic, weak) id<DCStatisticsDataSourceDelegate> delegate;

- (instancetype)initWithTableView:(UITableView *)tableView graphView:(DCGraphView *)graph;
- (instancetype)initWithTableView:(UITableView *)tableView graphView:(DCGraphView *)graph capper:(Capper *)capper;

@end

