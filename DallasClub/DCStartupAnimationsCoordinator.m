//
//  DCStartupAnimationsCoordinator.m
//  DallasClub
//
//  Created by Anton Savelev on 09/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStartupAnimationsCoordinator.h"
#import "DCStartupAnimator.h"

@interface DCStartupAnimationsCoordinator ()

@property (strong, nonatomic) NSMutableArray *animators;
@property (strong, nonatomic) NSMutableArray *views;

@end

@implementation DCStartupAnimationsCoordinator

- (instancetype)initWithViews:(NSArray *)views {
    if (self = [super init]) {
        _views = [NSMutableArray new];
        _animators = [NSMutableArray new];
        
        [self appendViewsToAnimate:views];
    }
    
    return self;
}

- (void)appendViewsToAnimate:(NSArray *)views {
    [self.views addObjectsFromArray:views];
    for (UIView *view in views) {
        [self.animators addObject:[[DCStartupAnimator alloc] initWithView:view]];
    }
    
    [self prepareViews];
}

- (void)prepareViews {
    for (UIView *view in self.views) {
        view.alpha = 0.0;
    }
}

- (void)performAnimationsWithCompletion:(void (^)())completion {
    [self.animators enumerateObjectsUsingBlock:^(DCStartupAnimator *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(idx * obj.animationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [obj animateWithCompletion:^(BOOL finished) {
                if (idx + 1 == self.animators.count && completion) {
                    completion();
                }
            }];
        });
    }];
}

@end
