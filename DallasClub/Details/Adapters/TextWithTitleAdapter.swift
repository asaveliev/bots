//
//  TextWithTitleAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

enum ExpandState {
    case expanded
    case collapsed
}

class  TextWithTitleAdapter: TKBaseAdapter {
    var type: ExpandState = .collapsed
    let title: String
    let text: String
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        if let goodModel = model as? Good {
            title = "Описание"
            text = goodModel.goodDescription ?? ""
        } else if let faq = model as? Faq {
            title = faq.question ?? ""
            text = faq.answer ?? ""
        } else {
            title = ""
            text = ""
            super.init(model: model, cellClass: cellClass)
            return
        }
        
        super.init(model: model, cellClass: cellClass)
    }
    
}
