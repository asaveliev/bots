//
//  KeyValueAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class KeyValueAdapter: TKBaseAdapter {
    
    let keyInObject: String
    let keyTitle: String
    let value: String
    
    init?(model: Any?, cellClass: UITableViewCell.Type, keyInObject: String, keyTitle: String) {
        self.keyInObject = keyInObject
        self.keyTitle = keyTitle
        
        if let object = model as? AnyObject {
            
            if object.responds(to: Selector(keyInObject)) == false {
                value = "Стратегия"
                super.init(model: model, cellClass: cellClass)
                return
            }
            
            let keyValue = object.value(forKey: keyInObject)
            if let string = keyValue as? String {
                value = string
            }
            else if let num = keyValue as? NSNumber {
                if keyInObject == "support" || keyInObject == "bot" {
                    value = num.boolValue == true ? "Есть" : "Нет"
                } else {
                    value = "\(num) \(keyInObject == "lessonsCount" ? "": "шт")"
                }
            } else {
                value = ""
            }
        } else {
            value = ""
        }
        
        if value == "" {
            return nil
        }
        
        super.init(model: model, cellClass: cellClass)
        
    }
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        fatalError("init(model:cellClass:) has not been implemented")
    }
    
}
