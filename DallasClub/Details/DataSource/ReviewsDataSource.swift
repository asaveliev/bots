//
//  ReviewsDataSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class ReviewsDataSource: AdapterCollectionDataSource<TKBaseAdapter, PhotoCollectionViewCell> {
    
    let goodModel: Good
    
    init(withGood good: Good) {
        self.goodModel = good
        
        super.init()
    }
    
    override var fetchRequest: NSFetchRequest<NSFetchRequestResult>! {
        return Review.sortedFetchRequestWithPredicateFormat("good == %@", goodModel)
    }
    
}
