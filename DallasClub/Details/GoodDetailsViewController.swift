//
//  GoodDetailsViewController.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SVProgressHUD
import RMUniversalAlert
import IDMPhotoBrowser

class GoodDetailsViewController: UIViewController {
    
    let goodId: String
    var model: Good
    var dataSource: ResultsDataSource?
    let goodImporter = ManagedObjectsImporter<Good>()
    let purchasesImporter = ManagedObjectsImporter<Purchase>()
    
    let provider = OrdersProvider()
    
    let mainView = TableContentView()
    
    init(withGoodId id: String) {
        goodId = id
        let model: Good = DCPersistentStack.sharedInstance().context.getOrCreateById(goodId)
        self.model = model
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = mainView
        mainView.tableView.allowsSelection = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.headerView.leftButton.setTitle(" Назад", for: .normal)
        mainView.headerView.leftButton.setTitleColor(UIColor.tkColorFromHex(0x007AFF), for: .normal)
        mainView.headerView.leftButton.tintColor = UIColor.tkColorFromHex(0x007AFF)
        mainView.headerView.leftButton.setImage(#imageLiteral(resourceName: "back_button.png").withRenderingMode(.alwaysTemplate), for: .normal)
        mainView.headerView.leftButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        mainView.headerView.headerTitleLabel.text = "Товар"
        
        setupDataSource()
    }
    
    func setupDataSource() {
        let valueTitles = [
            "Товар",
            "Товар",
            "Обучение",
            "Поддержка",
            "Бот",
            "Матчей",
            "Проходимость"
        ]
        let propertyKeys = [
            "strategy",
            "quantity",
            "lessonsCount",
            "support",
            "bot",
            "matches_a_day",
            "patency"
        ]
        let keyValueAdapers = zip(valueTitles, propertyKeys).flatMap { (title, key) -> KeyValueAdapter? in
            if key == "strategy" {
                return KeyValueAdapter(model: model, cellClass: UITableViewCell.self, keyInObject: key, keyTitle: title)
            }
            
            if (model as? AnyObject)?.value(forKey: key) as? String == nil {
                return nil
            }
            return KeyValueAdapter(model: model, cellClass: UITableViewCell.self, keyInObject: key, keyTitle: title)
        }
        
        
        var adapters = [
            GoodAdapter(model: model, cellClass: GoodDetailsImageCell.self),
            GoodActionAdapter(model: model, cellClass: DetailsInfoCell.self, action: payAction),
            TextWithTitleAdapter(model: model, cellClass: TextWithTitleCell.self),
            TKBaseAdapter(model: keyValueAdapers, cellClass: CharacteristicsCell.self),
        ]
        
        if
            let reviews = model.reviews,
            reviews.count > 0
        {
            adapters += [
                TKBaseAdapter(model: "Отзывы", cellClass: TitleCell.self),
                ReviewActionAdapter(model: model, cellClass: ReviewsCell.self, action: photoAction)
            ]
        }
        
        if
            let faq = model.faq,
            faq.count > 0
        {
            let faqAdapters: [TKBaseAdapter] = faq.flatMap {
                TextWithTitleAdapter(model: $0, cellClass: TextWithTitleCell.self)
            }
            adapters += [ TKBaseAdapter(model: "Вопрос-ответ", cellClass: TitleCell.self) ]
            adapters += faqAdapters
        }
        dataSource = ResultsDataSource(tableView: mainView.tableView, adapters: adapters)
        dataSource?.delegate = self
    }
    
    // MARK: Actions
    
    func backAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func photoAction(url: URL) {
        let vc = IDMPhotoBrowser(photoURLs: [url])
        present(vc!, animated: true, completion: nil)
    }
    
    func payAction() {
        if DCNetworkCore.sharedInstance().authProvider.hasAuth == false {
            let vc = LoginViewController()
            vc.delegate = self
            navigationController?.present(vc, animated: true, completion: nil)
            return
        }
        if model.purchased?.boolValue == true {
            SVProgressHUD.show()
            purchasesImporter.importDetails(forObjectWithId: model.id) { success in
                SVProgressHUD.dismiss()
                
                if success {
                    let vc = PurchaseDetailsController(withPurchseId: self.model.id)
                    self.navigationController?.pushViewController(vc, animated: true)
                    return
                }
                
                RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Попробуйте позже", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
            }
            return
        }
        
        runPayment(for: model)
    }
    
    func runPayment(for model: Good) {
        if model.purchased?.boolValue == true {
            return
        }
        
        SVProgressHUD.show()
        
        provider.generateOrderToken(forGoodId: model.id) { [unowned self] token in
            SVProgressHUD.dismiss()
            guard let token = token else {
                return
            }
            
            let auth = DCNetworkCore.sharedInstance().authProvider!
            let vc = YandexMoneyController(modelId: self.model.id, orderId: token, customerId: auth.currentUser.login, sum: self.model.price!.doubleValue)
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

extension GoodDetailsViewController: ResultsDataSourceDelegate {
    func didSelectRow(at indexPath: IndexPath) {
        guard
            let adapterCell = mainView.tableView.cellForRow(at: indexPath) as? AdapterProtocol,
            let adapter = adapterCell.adapter as? TextWithTitleAdapter,
            adapter.model is Good || adapter.model is Faq
        else {
            return
        }
        
        adapter.type = adapter.type == .collapsed ? .expanded : .collapsed
        mainView.tableView.reloadData()
    }
}

extension GoodDetailsViewController: LoginViewControllerDelegate {
    func didLogIn() {
        SVProgressHUD.show()
        goodImporter.importDetails(forObjectWithId: self.goodId) { success in
            SVProgressHUD.dismiss()
            
            if !success {
                return
            }
            
            self.model = DCPersistentStack.sharedInstance().context.getOrCreateById(self.goodId)
            self.setupDataSource()
            self.dataSource?.tableView.reloadData()
            self.runPayment(for: self.model)
        }
    }
}

extension GoodDetailsViewController: YandexMoneyControllerDelegate {
    func didPay(withSuccess success: Bool, modelId: String) {
        if success == false {
            return
        }
        
        SVProgressHUD.show()
        purchasesImporter.importDetails(forObjectWithId: modelId) { success in
            SVProgressHUD.dismiss()
            
            if success {
                let vc = PurchaseDetailsController(withPurchseId: modelId)
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
            
            RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Попробуйте позже", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
        }
        
        model.purchased = true
        setupDataSource()
    }
}
