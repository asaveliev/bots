//
//  DetailsView.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class TableContentView: DCBaseView {
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.clear
        view.separatorStyle = .none
        view.allowsSelection = false
        view.contentInset = UIEdgeInsets(top: -20, left: 0.0, bottom: 0.0, right: 0.0)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(tableView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        tableView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    override func headerViewClass() -> AnyClass! {
        return DCClassicHeaderView.self
    }
}
