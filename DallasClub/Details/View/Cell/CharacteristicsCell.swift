//
//  CharacteristicsCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class KeyValueView: UIView {
    
    lazy var keyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.tkColorFromHex(0x8A8A8F)
        label.textAlignment = .right
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(keyLabel)
        addSubview(valueLabel)
        
        translatesAutoresizingMaskIntoConstraints = false
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        keyLabel.snp.remakeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.width.equalTo(105)
            make.leading.equalToSuperview().offset(16)
        }
        
        valueLabel.snp.remakeConstraints { make in
            make.centerY.equalTo(keyLabel.snp.centerY)
            make.trailing.equalToSuperview().offset(-16)
            make.leading.equalTo(keyLabel.snp.trailing).offset(12)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class CharacteristicsCell: UITableViewCell, AdapterProtocol {

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.text = "Информация"
        return label
    }()
    
    lazy var divederView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC8C7CC)
        return view
    }()
    
    var keyValueViews: [KeyValueView] = []
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let views: [UIView] = [
            titleLabel,
            divederView
        ]
        
        views.forEach {
            contentView.addSubview($0)
        }
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        titleLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().offset(16)
        }
        
        keyValueViews.enumerated().forEach { (index, element) in
            element.snp.remakeConstraints { make in
                make.leading.trailing.equalToSuperview()
                
                if index == 0 {
                    make.top.equalTo(titleLabel.snp.bottom).offset(10)
                    return
                }
                
                let prevView = keyValueViews[index - 1]
                make.top.equalTo(prevView.snp.bottom).offset(7)
                
                if index + 1 == keyValueViews.count {
                    make.bottom.equalToSuperview().offset(-17)
                }
            }
        }
        
        divederView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1 / UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    func updaterKeyValueViews(withAdapters adapters: [KeyValueAdapter]) {
        keyValueViews = adapters.map { adapter -> KeyValueView in
            let view = KeyValueView()
            view.keyLabel.text = adapter.keyTitle
            view.valueLabel.text = adapter.value
            return view
        }
        keyValueViews.forEach {
            contentView.addSubview($0)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var adapter: TKBaseAdapter! {
        didSet {
            guard let keyValueAdapters = adapter.model as? [KeyValueAdapter] else {
                return
            }
            
            updaterKeyValueViews(withAdapters: keyValueAdapters)
            setNeedsUpdateConstraints()
        }
    }
    
}
