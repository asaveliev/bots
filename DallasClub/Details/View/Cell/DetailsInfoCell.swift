//
//  DetailsInfoCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class DetailsInfoCell: UITableViewCell, AdapterProtocol {
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.tkColorFromHex(0x8A8A8F)
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 22.0)
        return label
    }()
    
    lazy var headlineLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15.0)
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        return label
    }()
    
    lazy var buyButton: UIButton = {
        let button = UIButton()
        button.clipsToBounds = true
        button.layer.cornerRadius = 4.0
        button.backgroundColor = UIColor.tkColorFromHex(0x007AFF)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
        button.addTarget(self, action: #selector(buyAction), for: .touchUpInside)
        return button
    }()
    
    lazy var divederView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC8C7CC)
        return view
    }()
    
    func buyAction() {
        guard let actionAdapter = adapter as? GoodActionAdapter else {
            return
        }
        
        actionAdapter.action()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let views = [
            typeLabel,
            titleLabel,
            headlineLabel,
            priceLabel,
            buyButton,
            divederView
        ]
        
        views.forEach {
            contentView.addSubview($0)
        }
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        
        typeLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.trailing.equalToSuperview().inset(16)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(typeLabel.snp.bottom).offset(1)
        }
        
        priceLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
        }
        
        headlineLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(priceLabel.snp.bottom).offset(8)
        }
        
        buyButton.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(headlineLabel.snp.bottom).offset(16)
            make.height.equalTo(50)
        }
        
        divederView.snp.remakeConstraints { make in
            make.top.equalTo(buyButton.snp.bottom).offset(16)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1 / UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
        
        super.updateConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: AdapterProtocol
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let cellAdapter = adapter as? GoodAdapter else {
                return
            }
            
            typeLabel.text = cellAdapter.subtitle
            titleLabel.text = cellAdapter.title
            priceLabel.attributedText = cellAdapter.price
            headlineLabel.text = cellAdapter.headline
            buyButton.setTitle(cellAdapter.buyButtonTitle, for: .normal)
        }
    }
    
}
