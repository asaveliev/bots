//
//  ReviewsCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class ReviewsCell: UITableViewCell, AdapterProtocol {
    
    var dataSource: ReviewsDataSource?
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0.0, left: 16.0, bottom: 0.0, right: 16.0)
        layout.minimumInteritemSpacing = 16.0
        layout.itemSize = CGSize(width: 122.0, height: 122.0)
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.backgroundColor = UIColor.clear
        collection.showsHorizontalScrollIndicator = false
        return collection
    }()
    
    lazy var divederView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC8C7CC)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(collectionView)
        contentView.addSubview(divederView)
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        collectionView.snp.remakeConstraints { make in
            make.height.equalTo(122)
            make.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-16)
            make.leading.trailing.equalToSuperview()
        }
        
        divederView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1 / UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
        
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let model = adapter.model as? Good else {
                return
            }
            
            dataSource = ReviewsDataSource(withGood: model)
            dataSource?.collectionView = collectionView
            dataSource?.delegate = self
        }
    }
    
}

extension ReviewsCell: DCDataSourceDelegate {
    func dataSource(_ dataSource: Any!, didSelectObjectAt indexPath: IndexPath!) {
        guard
            let adapter = adapter as? ReviewActionAdapter,
            let review = self.dataSource?.object(at: indexPath) as? Review else {
            return
        }
        
        if
            let url = review.url,
            let photoUrl = URL(string: url)
        {
            adapter.action(photoUrl)
        }
    }
}
