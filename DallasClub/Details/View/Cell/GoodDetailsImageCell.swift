//
//  GoodDetailsImageCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class GoodDetailsImageCell: UITableViewCell, AdapterProtocol {
    
    lazy var avatarImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.cornerRadius = 4.0
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(avatarImageView)
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        avatarImageView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(8)
            make.height.equalTo(avatarImageView.snp.width)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: AdapterProtocol 
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let cellAdapter = adapter as? GoodAdapter else {
                return
            }
            
            if let url = URL(string: cellAdapter.imageUrl) {
                avatarImageView.sd_setImage(with: url)
            }
        }
    }
    
}
