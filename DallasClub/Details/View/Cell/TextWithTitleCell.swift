//
//  TextWithTitleCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class TextWithTitleCell: UITableViewCell, AdapterProtocol {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 17.0)
        return label
    }()
    
    lazy var valueLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15.0)
        label.textColor = UIColor.tkColorFromHex(0x8A8A8F)
        return label
    }()
    
    lazy var divederView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC8C7CC)
        return view
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let views = [
            titleLabel,
            valueLabel,
            divederView
        ]
        
        views.forEach {
            contentView.addSubview($0)
        }
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        titleLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().offset(12)
        }
        
        valueLabel.snp.remakeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(16)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        
        divederView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1 / UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    // MARK: AdapterProtocol
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let cellAdapter = adapter as? TextWithTitleAdapter else {
                return
            }
            
            if cellAdapter.type == .expanded || cellAdapter.text.characters.count < 287 {
                valueLabel.text = cellAdapter.text
            } else {
                let textPreview = cellAdapter.text.substring(to: cellAdapter.text.index(cellAdapter.title.startIndex, offsetBy: 287)) + "..."
                let moreString = NSAttributedString(string: "more", attributes: [
                    NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x097FFF),
                    NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)
                    ])
                let attrString = NSMutableAttributedString(string: textPreview, attributes: [
                    NSFontAttributeName: UIFont.systemFont(ofSize: 15.0),
                    NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x8A8A8F)
                    ])
                attrString.append(moreString)
                
                valueLabel.attributedText = attrString
            }
            
            titleLabel.text = cellAdapter.title
        }
    }
    
}
