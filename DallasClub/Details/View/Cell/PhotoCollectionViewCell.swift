//
//  PhotoCollectionViewCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 16.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell, AdapterProtocol {
    
    lazy var photoImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.layer.cornerRadius = 4.0
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(photoImageView)
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        photoImageView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard
                let model = adapter.model as? Review,
                let urlString = model.url,
                let url = URL(string: urlString)
            else {
                return
            }
            
            photoImageView.sd_setImage(with: url)
        }
    }
    
}
