//
//  DCStartupCell.m
//  DallasClub
//
//  Created by Anton Savelev on 08/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStartupCell.h"

@implementation DCStartupCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.font = [UIFont regularFontOfSize:15.0];
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.cardView addSubview:self.titleLabel];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setCardConstraints {
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[v(>=47@1000)]-4-|" options:0 metrics:nil views:@{ @"v": self.shadowedView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[v(>=47@1000)]-4-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-13-[v]-13-|" options:0 metrics:nil views:@{ @"v": self.shadowedView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-13-[v]-13-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
}

- (void)setConstraints {
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[title]-10-|" options:0 metrics:nil views:@{ @"title": self.titleLabel }]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[title]-15-|" options:0 metrics:nil views:@{ @"title": self.titleLabel }]];
}

@end
