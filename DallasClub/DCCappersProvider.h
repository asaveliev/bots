//
//  DCCappersProvider.h
//  DallasClub
//
//  Created by Anton Savelev on 17.04.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@interface DCCappersProvider : DCApiProvider

- (void)fetchCappersPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion;

@end
