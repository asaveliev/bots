//
//  DCPersistentStack.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCPersistentStack.h"

@implementation DCPersistentStack

+ (instancetype)sharedInstance {
    static DCPersistentStack *shared;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [DCPersistentStack new];
    });
    
    return shared;
}

- (instancetype)init {
    if (self = [super init]) {
        [self initialize];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)initialize {
    NSManagedObjectModel *mom = [NSManagedObjectModel mergedModelFromBundles:nil];
    NSArray *urls = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *dbUrl = [urls.lastObject URLByAppendingPathComponent:@"Bets.sqlite"];
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    _context.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    
    _backgroundContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    _backgroundContext.parentContext = self.context;
    
    NSDictionary *sourceMetadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:dbUrl options:nil error:nil];
    if (sourceMetadata && ![mom isConfiguration:nil compatibleWithStoreMetadata:sourceMetadata]) {
        [self removeDataBaseFromDisk:dbUrl];
    }
    
    NSError *error = nil;
    NSPersistentStoreCoordinator *psc = self.context.persistentStoreCoordinator;
    NSPersistentStore *store = [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:dbUrl options:nil error:&error];
    if (!store) {
        NSLog(@"ERROR: initializing PSC %@\n%@", [error localizedDescription], [error userInfo]);
    }
}

- (void)removeDataBaseFromDisk:(NSURL *)storeURL {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    if (![fileManager fileExistsAtPath:storeURL.path])
        return;
    
    NSURL *storeDirectory = [storeURL URLByDeletingLastPathComponent];
    NSDirectoryEnumerator *enumerator = [fileManager enumeratorAtURL:storeDirectory includingPropertiesForKeys:nil options:0 errorHandler:NULL];
    NSString *storeName = [storeURL.lastPathComponent stringByDeletingPathExtension];
    for (NSURL *url in enumerator) {
        if (![url.lastPathComponent hasPrefix:storeName]) continue;
        [fileManager removeItemAtURL:url error:&error];
    }
}

#pragma mark - observer

- (void)didEnterBackground {
    [self.context performBlock:^{
        [self.context save:nil];
    }];
}

@end
