//
//  DCNotificationsApiProvider.h
//  DallasClub
//
//  Created by Anton on 15/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@interface DCNotificationsApiProvider : DCApiProvider

- (void)registerToken:(NSString *)token;
- (void)removeToken:(NSString *)token;

@property (nonatomic, readwrite) BOOL notificationsEnabled;

@end
