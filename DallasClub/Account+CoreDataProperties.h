//
//  Account+CoreDataProperties.h
//  
//
//  Created by Anton on 06/08/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Account.h"

NS_ASSUME_NONNULL_BEGIN

@interface Account (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *balance;
@property (nullable, nonatomic, retain) NSNumber *change;
@property (nullable, nonatomic, retain) NSNumber *failed_bets;
@property (nullable, nonatomic, retain) NSNumber *returned_bets;
@property (nullable, nonatomic, retain) NSNumber *success_bets;
@property (nullable, nonatomic, retain) NSNumber *month_change;
@property (nullable, nonatomic, retain) NSNumber *week_change;

@end

NS_ASSUME_NONNULL_END
