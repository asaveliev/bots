//
//  DCAudioManager.h
//  DallasClub
//
//  Created by Anton Savelev on 08.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EZRecorder;
@class EZAudioPlayer;

@interface DCAudioManager : NSObject

@property (nonatomic, readonly) EZRecorder      *recorder;
@property (nonatomic, readonly) EZAudioPlayer   *player;
@property (nonatomic, readonly) NSString        *filePath;

- (NSData *)generateMp3Data;

- (instancetype)initWithFilePath:(NSString *)path;

- (void)startRecording;
- (void)stopRecording;
- (void)playAudio;

@end
