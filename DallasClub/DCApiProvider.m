//
//  DCProvider.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

#ifndef DEV
NSString *const DCApiRootOld = @"https://dallas-push.herokuapp.com//api/v1/";
NSString *const DCApiRoot = @"https://dallas-push.herokuapp.com//api/v1/";
NSString *const DCApiHost = @"https://dallas-push.herokuapp.com/";
#else
NSString *const DCApiRootOld = @"https://dallas-push.herokuapp.com//api/v1/";
NSString *const DCApiRoot = @"https://dallas-push.herokuapp.com//api/v1/";
NSString *const DCApiHost = @"https://dallas-push.herokuapp.com/";
#endif

@implementation DCApiProvider

- (instancetype)init {
    if (self = [super init]) {
        _sessionManager = [AFHTTPSessionManager manager];
    }
    
    return self;
}

@end
