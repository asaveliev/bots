//
//  Capper+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 17.04.17.
//
//

#import "Capper.h"


NS_ASSUME_NONNULL_BEGIN

@interface Capper (CoreDataProperties)

+ (NSFetchRequest<Capper *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *balance;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSString *capper_description;
@property (nullable, nonatomic, copy) NSDate *created;
@property (nullable, nonatomic, copy) NSString *coverUrl;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *ordering;

@end

NS_ASSUME_NONNULL_END
