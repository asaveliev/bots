//
//  DCStatisticsDataSource.m
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatisticsDataSource.h"
#import "DCFullStatiticsCell.h"
#import "NSDate+Utilities.h"
#import "DCPersistentStack.h"
#import "DCAccountDataSource.h"
#import "Bet.h"
#import "Account.h"
#import "DCStatsFetcher.h"
#import "Capper.h"

@interface DCStatisticsDataSource () <DCAccountDataSourceDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray       *colors;
@property (nonatomic, strong) NSArray       *images;
@property (nonatomic, strong) NSArray       *titles;

@property (nonatomic, strong) UITableView   *tableView;

@property (nonatomic, strong) DCStatsFetcher        *fetcher;
@property (nonatomic, strong) DCAccountDataSource   *accountSource;
@property (nonatomic, readonly) Capper              *capper;

@end

@implementation DCStatisticsDataSource

- (instancetype)initWithTableView:(UITableView *)tableView graphView:(DCGraphView *)graph {
    return [self initWithTableView:tableView graphView:graph capper:nil];
}

- (instancetype)initWithTableView:(UITableView *)tableView graphView:(DCGraphView *)graph capper:(Capper *)capper {
    if (self = [super init]) {
        _capper = capper;
        _graphView = graph;
        _tableView = tableView;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        
        [self registerClasses];
        
        _accountSource = [[DCAccountDataSource alloc] initWithAccountView:nil];
        self.accountSource.accountDelegate = self;
        _fetcher = [[DCStatsFetcher alloc] initWithCapperName:capper.name];
        
        _colors = @[[UIColor statsPostiveBubbleColor], [UIColor statsNeutralBubbleColor], [UIColor statsNegativeBubbleColor], [UIColor statsNegativeBubbleColor], UIColorFromHex(0x357FDD)];
        _titles = @[NSLocalizedString(@"Выиграно ставок", nil), NSLocalizedString(@"Возврат", nil), NSLocalizedString(@"Проиграно ставок", nil), NSLocalizedString(@"Прирост банка за месяц", nil), NSLocalizedString(@"Прибыль за все время", nil)];
        _images = @[@"check", @"return", @"close", @"arrow", @"arrow"];
    }
    
    return self;
}

- (void)setSelectedDay:(NSDate *)selectedDay {
    if (![selectedDay isSameMonthAsDate:_selectedDay]) {
        _selectedDay = selectedDay;
        [self reloadData];
    } else if(self.delegate) {
        [self.delegate statisticsDataSourceDidUpdate];
    }
    
    _selectedDay = selectedDay;
}

- (void)setMode:(DCGraphMode)mode {
    _mode = mode;
    [self reloadData];
}

- (void)reloadData {
    [self.graphView setGraphElementsWithValues:@[] titles:@[]];
    if (self.mode == DCGraphModeDays) {
        [self.fetcher fetchDataOfMonth:self.selectedDay ?: [NSDate date] completion:^(NSError *error) {
            [self.tableView reloadData];
            [self configureGraphView];
            
            if (self.delegate) {
                [self.delegate statisticsDataSourceDidUpdate];
            }
        }];
    } else {
        NSMutableArray *years = [NSMutableArray new];
        for (int i = 2016; i <= [NSDate date].year; i++) {
            NSDateComponents *components = [NSDateComponents new];
            components.month = 1;
            components.year = i;
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDate *date = [calendar dateFromComponents:components];
            [years addObject:date];
        }

        [self.fetcher fetchDataOfYears:years completion:^(NSError *error) {
            [self.tableView reloadData];
            [self configureGraphView];
            
            if (self.delegate) {
                [self.delegate statisticsDataSourceDidUpdate];
            }
        }];
    }
}

#pragma mark - GraphView

- (void)configureGraphView {
    if (self.mode == DCGraphModeDays) {
        [self configureGraphViewForDays];
    } else {
        [self configureGraphViewForMonths];
    }
}

- (void)configureGraphViewForDays {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:self.selectedDay];
    components.day = 1;
    NSDate *date = [calendar dateFromComponents:components];
    
    NSMutableArray *values = [NSMutableArray new];
    NSMutableArray *titles = [NSMutableArray new];
    while ([date isSameMonthAsDate:self.selectedDay]) {
        NSString *dayString = [date stringWithFormat:@"d"];
        NSInteger summary = 0;
        if (self.fetcher.changeByDays[dayString]) {
            summary = [self.fetcher.changeByDays[dayString] doubleValue];
        }
        
        [values addObject:@(summary)];
        [titles addObject:[NSString stringWithFormat:@"%ld", (long)date.day]];
        
        date = [date dateByAddingDays:1];
    }
    
    [self.graphView setGraphElementsWithValues:values titles:titles];
}

- (void)configureGraphViewForMonths {
    NSMutableArray *values = [NSMutableArray new];
    NSMutableArray *titles = [NSMutableArray new];
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.locale = [NSLocale new];
    formatter.dateFormat = @"MMM";
    NSArray *sortedKeys = [self.fetcher.changeByMonths.allKeys sortedArrayUsingComparator:^NSComparisonResult(NSDate*  _Nonnull obj1, NSDate*  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSInteger summary = _capper.balance.integerValue - self.accountSource.account.change.integerValue;
    for (NSDate *key in sortedKeys) {
        if ([sortedKeys indexOfObject:key] % 12 == 0) {
            [titles addObject:[NSString stringWithFormat:@"%ld", (long)key.year]];
            [values addObject:@0];
        }
        
        if (self.fetcher.changeByMonths[key]) {
            summary += [self.fetcher.changeByMonths[key] integerValue];
        }
        if ([key compare:[NSDate date]] == NSOrderedAscending) {
            [values addObject:@(summary)];
        } else {
            [values addObject:@0];
        }
        [titles addObject:[[[formatter stringFromDate:key] capitalizedString] substringToIndex:3]];
    }
    
    [self.graphView setGraphElementsWithValues:values titles:titles];
}

#pragma mark - TableView

- (void)registerClasses {
    [self.tableView registerClass:[DCFullStatiticsCell class] forCellReuseIdentifier:@"Cell"];
}

- (NSString *)sectionName {
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    DCFullStatiticsCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(DCFullStatiticsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.bubbleView.backgroundColor = self.colors[indexPath.row];
    cell.priceLabel.textColor = [UIColor blackColor];
    cell.titleLabel.text = self.titles[indexPath.row];
    ((DCBubbleImageView *)cell.bubbleView).imageView.image = [UIImage imageNamed:self.images[indexPath.row]];
    
    double initial = _capper.balance.doubleValue - self.accountSource.account.change.doubleValue;
    switch (indexPath.row) {
        case 0: {
            NSInteger count = [self countOfBetsWithState:DCBetStateSuccess];
            cell.priceLabel.text = [NSString stringWithFormat:@"%ld", self.mode == DCGraphModeDays ? (long)count : self.accountSource.account.success_bets.integerValue];
            break;
        }
        case 1: {
            NSInteger count = [self countOfBetsWithState:DCBetStateReturned];
            cell.priceLabel.text = [NSString stringWithFormat:@"%ld", self.mode == DCGraphModeDays ? (long)count : self.accountSource.account.returned_bets.integerValue];
            break;
        }
        case 2: {
            NSInteger count = [self countOfBetsWithState:DCBetStateFailed];
            cell.priceLabel.text = [NSString stringWithFormat:@"%ld", self.mode == DCGraphModeDays ? (long)count : self.accountSource.account.failed_bets.integerValue];
            break;
        }
        case 3: {
            double change = self.fetcher.monthChange;
            cell.priceLabel.text = [NSString stringWithFormat:@"%.0f%%", change / initial * 100.0];
            if ([cell.priceLabel.text isEqualToString:@"-0%"]) {
                cell.priceLabel.text = @"0%";
            }
            break;
        }
        case 4: {
            cell.priceLabel.text = [NSString stringWithFormat:@"%.0f%%", self.accountSource.account.change.doubleValue / initial * 100.0];
            break;
        }
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return 80.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)countOfBetsWithState:(DCBetState)state {
    return [self.fetcher countOfBetsByDay:self.selectedDay withState:state];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

#pragma mark - DCAccountDataSourceDelegate

- (void)dataSourceDidChangeContent {
    [self.tableView reloadData];
}

@end
