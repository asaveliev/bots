//
//  AppDelegate.h
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property () BOOL restrictRotation;


@end

