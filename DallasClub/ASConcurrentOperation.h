//
//  ASBaseOperation.h
//  AlfaSense
//
//  Created by Рамис Ямилов on 30.03.16.
//  Copyright © 2016 Alfa-Bank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCNetworkCore.h"
#import "DCPersistentStack.h"

@interface ASConcurrentOperation : NSOperation

@property (nonatomic, strong) NSError *operationError;

- (void)completeOperationWithError:(NSError*)error;
@end
