//
//  DCMonthsControl.m
//  DallasClub
//
//  Created by Anton on 05/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCMonthsControl.h"
#import "DCMonthControlViewModel.h"
#import "NSDate+Utilities.h"

@interface DCMonthsControl ()

@property (strong, nonatomic) UIView * selectionView;

@end

@implementation DCMonthsControl

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _selectionView = [[UIView alloc] initWithFrame:CGRectZero];
        self.selectionView.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:self.selectionView];
    }
    return self;
}

- (void)configureButton:(UIButton *)button {
    button.titleLabel.font = [UIFont statsTitleFontRegular];
    [button setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.3] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithWhite:1.0 alpha:1.0] forState:UIControlStateSelected];
}

- (void)buttonClicked:(UIButton *)button {
    DCMonthControlViewModel *selectedModel = self.models[[self.buttons indexOfObject:button]];
    if (!selectedModel.selectable) {
        return;
    }
    [super buttonClicked:button];
    [self.scrollView setContentOffset:[self scrollOffsetForButton:button] animated:YES];
    
    if (!self.selectionView.superview) {
        [self.scrollView addSubview:self.selectionView];
    }
    [UIView animateWithDuration:CGRectEqualToRect(self.selectionView.frame, CGRectZero) ? 0.0 : 0.3 animations:^{
        CGRect buttonRect = button.frame;
        buttonRect.origin = (CGPoint) {buttonRect.origin.x, buttonRect.origin.y + buttonRect.size.height + 4.0};
        buttonRect.size.height = 2.0;
        
        self.selectionView.frame = buttonRect;
    }];
    
    selectedModel.isSelected = !selectedModel.isSelected;
}

- (CGPoint)scrollOffsetForButtonAtIndex:(NSInteger)index {
    UIButton *button = self.buttons[index];
    return [self scrollOffsetForButton:button];
}

- (CGPoint)scrollOffsetForButton:(UIButton *)button {
    CGFloat x;
    if (self.buttons.firstObject == button) {
        x = button.center.x - button.frame.size.width / 2.0;
    }
    else if (self.buttons.lastObject == button) {
        x = self.scrollView.contentSize.width - self.scrollView.frame.size.width;
    }
    else {
        x = button.center.x - self.scrollView.frame.size.width / 2.0;
    }
    return CGPointMake(x, -button.center.y + 15.0);
}

- (void)setSelectedButtonAtIndex:(NSInteger)index {
    UIButton *button = self.buttons[index];
    [self buttonClicked:button];
}

- (NSInteger)selectedIndex {
    for (UIButton *b in self.buttons) {
        if (b.selected) {
            return [self.buttons indexOfObject:b];
        }
    }
    
    return -1;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    [self setSelectedButtonAtIndex:selectedIndex];
}

- (void)setModels:(NSArray *)models {
    _models = models;
    
    NSMutableArray *titles = [NSMutableArray new];
    for (DCMonthControlViewModel *model in models) {
        [titles addObject:[[model.date stringWithFormat:model.type == DCMonthControlElementTypeYear ? @"yyyy" : @"LLLL"] capitalizedString]];
    }
    
    [super setButtonsWithTitles:titles andSizes:[self getMonthsSize:titles]];
}

- (NSArray *)getMonthsSize:(NSArray *)titles {
    NSMutableArray *sizes = [NSMutableArray new];
    
    for (NSString *title in titles) {
        CGSize size = [title sizeWithAttributes:@{ NSFontAttributeName: [UIFont statsTitleFontRegular] }];
        size.width += 20.0;
        
        NSDictionary *sizeData = @{
                                   @"width": @(size.width),
                                   @"height": @(30.0)
                                   };
        
        [sizes addObject:sizeData];
    }
    
    return sizes;
}

@end
