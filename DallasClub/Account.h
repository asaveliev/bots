//
//  Account.h
//  
//
//  Created by Anton on 01/08/16.
//
//

#import <Foundation/Foundation.h>
#import "BaseManagedObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface Account : BaseManagedObject<Serializable>

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Account+CoreDataProperties.h"
