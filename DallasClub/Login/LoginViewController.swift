//
//  LoginViewController.swift
//  DallasClub
//
//  Created by Anton Savelev on 18.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SVProgressHUD
import RMUniversalAlert

protocol LoginViewControllerDelegate: class {
    func didLogIn()
}

class LoginViewController: UIViewController {
    
    weak var delegate: LoginViewControllerDelegate?
    
    let banProvider = BanProvider()
    
    let mainView = LoginView()
    
    override func loadView() {
        view = mainView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.segmentedView.leftButton.isSelected = true
        mainView.segmentedView.leftButton.addTarget(self, action: #selector(leftAction), for: .touchUpInside)
        mainView.segmentedView.rightButton.addTarget(self, action: #selector(rightAction), for: .touchUpInside)
        mainView.cancelButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        mainView.actionButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    // MARK: Actions
    
    func leftAction() {
        mainView.type = .login
        
        mainView.segmentedView.leftButton.isSelected = true
        mainView.segmentedView.rightButton.isSelected = false
    }
    
    func rightAction() {
        mainView.type = .registration
        
        mainView.segmentedView.leftButton.isSelected = false
        mainView.segmentedView.rightButton.isSelected = true
    }
    
    func finishFlow(withLogin login: String, password: String) {
        SVProgressHUD.show()
        
        DCNetworkCore.sharedInstance().authProvider.authUser(withLogin: login, andPass: password, completion: { success in
            if !success {
                SVProgressHUD.dismiss()
                RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Невернй логин или пароль", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                return
            }
            
            self.banProvider.checkBan { banned in
                SVProgressHUD.dismiss()
                guard banned == false else {
                    DCNetworkCore.sharedInstance()?.authProvider.removeUser(DCNetworkCore.sharedInstance()?.authProvider.currentUser)
                    RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Ваш аккаунт забанен. По всем вопросам пишите в группу ВК.", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: { _ in
                            self.dismiss(animated: true, completion: nil)
                        })
                    NotificationCenter.default.post(name: NSNotification.Name("DCUpdateTabBarNotification"), object: nil)
                    return
                }
                
                DCNetworkCore.sharedInstance()?.notificationsProvider.notificationsEnabled = true
                self.dismiss(animated: true, completion: {
                    self.delegate?.didLogIn()
                })
            }
        })
    }
    
    func buttonAction() {
        mainView.endEditing(true)
        
        if mainView.type == .login {
            guard
                let password = mainView.passwordField.text,
                let login = mainView.emailField.text,
                login.characters.count > 0,
                password.characters.count > 0
            else {
                RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Заполните все необходимые поля", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                return
            }
        
            self.finishFlow(withLogin: login, password: password)
        }
        
        if mainView.type == .registration {
            guard
                let password = mainView.passwordField.text,
                let login = mainView.emailField.text,
                let name = mainView.nameField.text,
                name.characters.count > 0,
                login.characters.count > 0,
                password.characters.count > 0
                else {
                    RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Заполните все необходимые поля", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                    return
            }
            
            SVProgressHUD.show()
            DCNetworkCore.sharedInstance().authProvider.registerUser(withLogin: login, pass: password, name: name, completion: { success, errors in
                SVProgressHUD.dismiss()
                if !success {
                    let errorString = errors ?? "Попробуйте позже"
                    RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: errorString, cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
                    return
                }
                
                self.finishFlow(withLogin: login, password: password)
            })
        }
    }
    
    func closeAction() {
        dismiss(animated: true, completion: nil)
    }
    
}
