//
//  SegmentedView.swift
//  DallasClub
//
//  Created by Anton Savelev on 20.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class SegmentedView: UIView {
    
    lazy var leftButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
        button.setTitleColor(UIColor.black, for: .selected)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.clear
        button.selectedBackgroundColor = UIColor.white
        button.setTitle("Войти", for: .normal)
        return button
    }()
    
    lazy var rightButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13.0)
        button.setTitleColor(UIColor.black, for: .selected)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = UIColor.clear
        button.selectedBackgroundColor = UIColor.white
        button.setTitle("Регистрация", for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(leftButton)
        addSubview(rightButton)
        
        clipsToBounds = true
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1 / UIScreen.main.scale
        layer.cornerRadius = 4.0
        translatesAutoresizingMaskIntoConstraints = false
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        leftButton.snp.remakeConstraints { make in
            make.top.bottom.leading.equalToSuperview()
            make.trailing.equalTo(snp.centerX)
        }
        
        rightButton.snp.remakeConstraints { make in
            make.top.bottom.trailing.equalToSuperview()
            make.leading.equalTo(snp.centerX)
        }
        
        super.updateConstraints()
    }
    
}
