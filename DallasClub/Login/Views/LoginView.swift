//
//  LoginView.swift
//  DallasClub
//
//  Created by Anton Savelev on 18.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

func isIpad() -> Bool {
    return UIDevice.current.userInterfaceIdiom == .pad
}

enum LoginViewType {
    case login
    case registration
}

class LoginView: TKBaseContentView {
    
    private var gradientLayer: CAGradientLayer!
    
    var type: LoginViewType = .login {
        didSet {
            update(type: type)
        }
    }
    
    override var hideHeaderView: Bool {
        return true
    }
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Отмена", for: .normal)
        return button
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "АВТОРИЗАЦИЯ"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 32.0, weight: UIFontWeightSemibold)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Введите ваши данные, чтобы\nиспользовать все функции приложения"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    lazy var nameField: UITextField = {
        let field = UITextField()
        field.textAlignment = .center
        field.font = UIFont.systemFont(ofSize: 17.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        field.attributedPlaceholder = NSAttributedString(string: "username", attributes: [
            NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x8A8A8F),
            NSFontAttributeName: UIFont.systemFont(ofSize: 17.0),
            NSParagraphStyleAttributeName: paragraph
            ])
        field.backgroundColor = UIColor.white
        field.clipsToBounds = true
        field.layer.cornerRadius = 8.0
        return field
    }()
    
    lazy var emailField: UITextField = {
        let field = UITextField()
        field.autocapitalizationType = .none
        field.textAlignment = .center
        field.font = UIFont.systemFont(ofSize: 17.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        field.attributedPlaceholder = NSAttributedString(string: "email", attributes: [
            NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x8A8A8F),
            NSFontAttributeName: UIFont.systemFont(ofSize: 17.0),
            NSParagraphStyleAttributeName: paragraph
        ])
        field.backgroundColor = UIColor.white
        field.clipsToBounds = true
        field.layer.cornerRadius = 8.0
        return field
    }()
    
    lazy var passwordField: UITextField = {
        let field = UITextField()
        field.isSecureTextEntry = true
        field.textAlignment = .center
        field.font = UIFont.systemFont(ofSize: 17.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        field.attributedPlaceholder = NSAttributedString(string: "password", attributes: [
            NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x8A8A8F),
            NSFontAttributeName: UIFont.systemFont(ofSize: 17.0),
            NSParagraphStyleAttributeName: paragraph
            ])
        field.backgroundColor = UIColor.white
        field.clipsToBounds = true
        field.layer.cornerRadius = 8.0
        return field
    }()
    
    lazy var actionButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.tkColorFromHex(0x2e3035)
        button.clipsToBounds = true
        button.layer.cornerRadius = 8.0
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
        return button
    }()
    
    lazy var segmentedView: SegmentedView = SegmentedView(frame: CGRect.zero)
    
    override func initialSetup() {
        super.initialSetup()
        
        segmentedView.isHidden = true
        
        let views: [UIView] = [
            cancelButton,
            titleLabel,
            subtitleLabel,
            nameField,
            emailField,
            passwordField,
            actionButton,
            segmentedView
        ]
        
        views.forEach {
            contentView.addSubview($0)
        }
        
        setGradient()
        
        update(type: .login)
    }
    
    func setGradient() {
        let colorTop = UIColor.tkColorFromHex(0x50535b).withAlphaComponent(0.5).cgColor
        let colorBottom = UIColor.tkColorFromHex(0x161616).withAlphaComponent(0.5).cgColor
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = UIScreen.main.bounds
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        contentView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = UIScreen.main.bounds
    }
    
    override func updateConstraints() {
        let fieldHeight = 50.0
        
        cancelButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(31)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(22)
            make.width.equalTo(62)
        }
        
        titleLabel.snp.remakeConstraints { make in
            let offset = isIpad() ? 254 : 100 * UIScreen.main.scale / 3
            make.top.equalToSuperview().offset(offset)
            make.leading.trailing.equalToSuperview().inset(16)
        }
        
        subtitleLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalTo(titleLabel.snp.bottom).offset(15)
        }
        
        emailField.snp.remakeConstraints { make in
            make.top.equalTo(nameField.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(isIpad() ? 134 : 16)
            make.height.equalTo(fieldHeight)
        }
        
        nameField.snp.remakeConstraints { make in
            make.top.equalTo(subtitleLabel.snp.bottom).offset(45)
            make.leading.trailing.equalToSuperview().inset(isIpad() ? 134 : 16)
            make.height.equalTo(type == .registration ? fieldHeight : 0)
        }
        
        passwordField.snp.remakeConstraints { make in
            make.top.equalTo(emailField.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(isIpad() ? 134 : 16)
            make.height.equalTo(fieldHeight)
        }
        
        actionButton.snp.remakeConstraints { make in
            make.top.equalTo(passwordField.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(isIpad() ? 134 : 16)
            make.height.equalTo(fieldHeight)
        }
        
        segmentedView.snp.remakeConstraints { make in
            make.trailing.leading.bottom.equalToSuperview().inset(16)
            make.height.equalTo(29)
        }
        
        super.updateConstraints()
    }
    
    func update(type: LoginViewType) {
        switch type {
        case .login:
            let colorTop = UIColor.tkColorFromHex(0x333438).cgColor
            let colorBottom = UIColor.tkColorFromHex(0x161616).cgColor
            self.gradientLayer.colors = [
                colorTop,
                colorBottom
            ]
            titleLabel.text = "Авторизация"
            actionButton.setTitle("Авторизация", for: .normal)
            break
        case .registration:
            self.gradientLayer.colors = [
                UIColor(red:0.92, green:0.13, blue:0.33, alpha:1).cgColor,
                UIColor(red:0.97, green:0.23, blue:0.45, alpha:1).cgColor
            ]
            titleLabel.text = "Регистрация"
            actionButton.setTitle("Регистрация", for: .normal)
            break
        }
        
        nameField.isHidden = type == .login
        
        setNeedsUpdateConstraints()
    }
    
}
