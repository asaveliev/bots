//
//  NSString+MD5.h
//  DallasClub
//
//  Created by Anton Savelev on 03.03.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)md5;

@end
