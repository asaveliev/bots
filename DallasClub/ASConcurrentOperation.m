//
//  ASBaseOperation.m
//  AlfaSense
//
//  Created by Рамис Ямилов on 30.03.16.
//  Copyright © 2016 Alfa-Bank. All rights reserved.
//

#import "ASConcurrentOperation.h"

@interface ASConcurrentOperation ()
@property (atomic, assign) BOOL as_executing;
@property (atomic, assign) BOOL as_finished;
@end

@implementation ASConcurrentOperation

- (BOOL)isAsynchronous {
    return YES;
}

- (BOOL)isExecuting {
    return self.as_executing;
}

- (BOOL)isFinished {
    return self.as_finished;
}

- (void)start {
    if ([self isCancelled]) {
        // Move the operation to the finished state if it is canceled.
        [self willChangeValueForKey:@"isFinished"];
        self.as_finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    // If the operation is not canceled, begin executing the task.
    [self willChangeValueForKey:@"isExecuting"];
    self.as_executing = YES;
    [self main];
    [self didChangeValueForKey:@"isExecuting"];
    
    NSLog(@"%@ is started", NSStringFromClass([self class]));
}

- (void)completeOperationWithError:(NSError *)error {
    NSLog(@"%@ is finished with error %@", NSStringFromClass([self class]), error);
    
    self.operationError = error;
    
    [self willChangeValueForKey:@"isFinished"];
    [self willChangeValueForKey:@"isExecuting"];
    
    self.as_executing = NO;
    self.as_finished = YES;
    
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

@end
