//
//  DCExpandableBackgroundView.h
//  DallasClub
//
//  Created by Anton Savelev on 24.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCExpandableBackgroundView : DCBaseView

@property (nonatomic, readonly) UIImageView *backgroundImageView;
@property (nonatomic, readonly) UILabel     *backTitle;
@property (nonatomic, readonly) UIButton    *backButton;
@property (nonatomic, readonly) UIButton    *infoButton;

- (void)bindTableViewToResizeBackground:(UITableView *)tableView;

@end
