//
//  DCFeedView.h
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatisticsView.h"

@interface DCFeedView : DCStatisticsView

- (instancetype)initWithSelectedItem:(BOOL)selected;

@end
