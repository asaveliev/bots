//
//  DCMoneyFormatter.h
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCMoneyFormatter : NSObject

- (NSString *)stringFromAmount:(NSNumber *)amount;

@end
