//
//  MarketView.swift
//  DallasClub
//
//  Created by Anton Savelev on 01.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SnapKit

class MarketView: TKBaseContentView {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsetsMake(15.0, 8.0, 0.0, 8.0)
        
        let collection = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collection.showsVerticalScrollIndicator = false
        collection.contentInset = UIEdgeInsetsMake(64.0, 0.0, 64.0, 0.0)
        collection.backgroundColor = UIColor.white
        collection.alwaysBounceVertical = true
        return collection
    }()
    
    override func initialSetup() {
        super.initialSetup()
        
        headerViewType = .normal
        contentView.addSubview(collectionView)
    }
    
    override func updateConstraints() {
        collectionView.snp.remakeConstraints { [unowned self] make in
            make.edges.equalTo(self)
        }
        
        super.updateConstraints()
    }
    
}
