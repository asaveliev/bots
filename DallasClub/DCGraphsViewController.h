//
//  DCGraphsViewController.h
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Capper;

@interface DCGraphsViewController : UIViewController

- (instancetype)initWithCapper:(Capper *)capper;

@end

