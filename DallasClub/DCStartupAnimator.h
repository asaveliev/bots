//
//  DCStartupAnimator.h
//  DallasClub
//
//  Created by Anton Savelev on 08/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCStartupAnimator : NSObject

@property (assign, nonatomic) NSTimeInterval animationDuration;

- (instancetype)initWithView:(UIView *)view;

- (void)animateWithCompletion:(void (^)(BOOL finished))completion;

@end
