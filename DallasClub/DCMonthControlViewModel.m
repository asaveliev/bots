//
//  DCMonthViewModel.m
//  DallasClub
//
//  Created by Anton Savelev on 04.01.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMonthControlViewModel.h"

@implementation DCMonthControlViewModel

- (instancetype)initWithDate:(NSDate *)date {
    if (self = [super init]) {
        _date = date;
        _selectable = YES;
        _isSelected = NO;
    }
    
    return self;
}

@end
