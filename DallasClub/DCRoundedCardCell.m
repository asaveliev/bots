//
//  DCRoundedCardCell.m
//  DallasClub
//
//  Created by Anton on 19/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRoundedCardCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation DCRoundedCardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _cardView = [UIView new];
        _shadowedView = [UIView new];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.cardView.backgroundColor = UIColorFromHex(0x2e3035);
        self.shadowedView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.shadowedView];
        [self.contentView addSubview:self.cardView];
        
        self.contentView.clipsToBounds = NO;
        self.cardView.clipsToBounds = YES;
        self.cardView.layer.cornerRadius = 8.0;
        
        self.shadowedView.clipsToBounds = NO;
        self.shadowedView.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.2].CGColor;
        self.shadowedView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
        self.shadowedView.layer.shadowOpacity = 1.0;
        
        self.shadowedView.translatesAutoresizingMaskIntoConstraints = NO;
        self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self setCardConstraints];
    }
    return self;
}

- (void)setCardConstraints {
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[v(>=70@1000)]-4-|" options:0 metrics:nil views:@{ @"v": self.shadowedView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[v(>=70@1000)]-4-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-13-[v]-13-|" options:0 metrics:nil views:@{ @"v": self.shadowedView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-13-[v]-13-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    
    self.shadowedView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.shadowedView.bounds cornerRadius:8.0].CGPath;
}

@end
