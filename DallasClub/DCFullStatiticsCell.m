//
//  DCFullStatiticsCell.m
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFullStatiticsCell.h"

@implementation DCFullStatiticsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
    }
    
    return self;
}

- (Class)bubbleClass {
    return [DCBubbleImageView class];
}

@end
