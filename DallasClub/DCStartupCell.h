//
//  DCStartupCell.h
//  DallasClub
//
//  Created by Anton Savelev on 08/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRoundedCardCell.h"

@interface DCStartupCell : DCRoundedCardCell

@property (nonatomic, readonly) UILabel *titleLabel;

@end
