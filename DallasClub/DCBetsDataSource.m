//
//  DCBetsDataSource.m
//  DallasClub
//
//  Created by Anton on 25/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBetsDataSource.h"
#import "Bet.h"
#import "NSDate+Utilities.h"
#import "DCStatisticsCell.h"
#import "DCAccountDataSource.h"
#import "DCTableHeaderView.h"
#import "DCMoneyFormatter.h"
#import "DCImporter.h"
#import "SVPullToRefresh.h"
#import <objc/runtime.h>


static void *UITableViewFixKey;
@interface UITableView(Fix)

@property BOOL ignoreOffsets;

@end

@implementation UITableView(Fix)

- (void)setIgnoreOffsets:(BOOL)ignoreOffsets {
    objc_setAssociatedObject(self, UITableViewFixKey, @(ignoreOffsets), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)ignoreOffsets {
    return [objc_getAssociatedObject(self, UITableViewFixKey) boolValue];
}

- (void)setContentOffset:(CGPoint)contentOffset {
    if (self.ignoreOffsets) {
        return;
    }
    [super setContentOffset:contentOffset];
}

@end

@interface DCBetsDataSource()

@property (nonatomic, readonly) NSString            *capperName;

@property (nonatomic, readonly) DCAccountDataSource *accountDataSource;

@end

@implementation DCBetsDataSource
@dynamic delegate;

- (instancetype)initWithTableView:(UITableView *)tableView capperName:(NSString *)name {
    _capperName = name;
    if (self = [super initWithTableView:tableView]) {
        __weak DCBetsDataSource *weakSelf = self;
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            [[DCImporter betsImporter] loadNextBetsPageWithCapperName:name];
            [[DCImporter betsImporter] loadAccountWithCapperName:name];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.tableView.infiniteScrollingView stopAnimating];
            });
        }];
    }
    
    return self;
}

- (instancetype)initWithTableView:(UITableView *)tableView {
    if (self = [super initWithTableView:tableView]) {
        __weak DCBetsDataSource *weakSelf = self;
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            [[DCImporter importer] loadNextBetsPage];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.tableView.infiniteScrollingView stopAnimating];
            });
        }];
    }
    
    return self;
}

- (void)setTableView:(UITableView *)tableView {
    tableView.ignoreOffsets = YES;
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    [super setTableView:tableView];
    [self registerClasses];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.tableView reloadData];
    
    [self.tableView.infiniteScrollingView stopAnimating];
    __weak DCBetsDataSource *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [[DCImporter betsImporter] loadNextBetsPageWithCapperName:weakSelf.capperName];
        [[DCImporter betsImporter] loadAccountWithCapperName:weakSelf.capperName];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        });
    }];
    [tableView setContentOffset:CGPointZero animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.tableView.ignoreOffsets = NO;
    });
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Bet entityName]];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"ordering" ascending:NO],
                                [NSSortDescriptor sortDescriptorWithKey:@"number" ascending:NO]];
    request.predicate = [NSPredicate predicateWithValue:YES];
    if (self.capperName) {
        request.predicate = [NSPredicate predicateWithFormat:@"capper_name = %@", self.capperName];
    }
    return request;
}

- (NSString *)sectionName {
    return @"ordering";
}

- (NSString *)reuseIdentifier {
    return @"BetsCell";
}

- (void)registerClasses {
    [self.tableView registerClass:[DCStatisticsCell class] forCellReuseIdentifier:self.reuseIdentifier];
}

- (void)configureCell:(DCStatisticsCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Bet *bet = [self objectAtIndexPath:indexPath];
    
    cell.state = bet.state.integerValue;
    NSMutableAttributedString *attrText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Ставка #%d:\n", bet.number.intValue] attributes:@{ NSFontAttributeName: [UIFont statsTitleFontMedium] }];
    [attrText appendAttributedString:[[NSAttributedString alloc] initWithString:bet.readableStatus attributes:@{ NSFontAttributeName: [UIFont statsTitleFontRegular] }]];
    cell.titleLabel.attributedText = attrText;
    NSString *sign = bet.balance_change.doubleValue > 0 ? @"+" : @"";
    DCMoneyFormatter *formatter = [DCMoneyFormatter new];
    cell.priceLabel.text = [NSString stringWithFormat:@"%@%@", sign, [formatter stringFromAmount:bet.balance_change]];
    cell.bubbleView.percentLabel.text = [NSString stringWithFormat:@"%.0f%%", bet.amout.doubleValue];
    
    if (self.capperName.length) {
        cell.backgroundColor = cell.contentView.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0 && self.capperName.length) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 226.0)];
        DCTableHeaderView *headerView = [[DCTableHeaderView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 201.0)];
        if (!self.accountDataSource) {
            _accountDataSource = [[DCAccountDataSource alloc] initWithAccountView:headerView];
        } else {
            self.accountDataSource.headerView = headerView;
        }
        [view addSubview:headerView];
        
        UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleCardPress:)];
        press.minimumPressDuration = 0.0;
        [headerView addGestureRecognizer:press];
        self.tableView.delaysContentTouches = NO;
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 209.5, [UIScreen mainScreen].bounds.size.width, 15.0)];
        dateLabel.textColor = UIColorFromHex(0x8798A4);
        dateLabel.textAlignment = NSTextAlignmentCenter;
        dateLabel.font = [UIFont regularFontOfSize:13.0];
        [view addSubview:dateLabel];
        
        Bet *bet = [self objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
        dateLabel.text = [[bet.created stringWithFormat:@"LLLL dd"] uppercaseString];
        
        
        if (self.capperName.length) {
            view.backgroundColor = UIColorFromHex(0xF7F7F7);
        }
        
        return view;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 25.0)];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 8.5, [UIScreen mainScreen].bounds.size.width, 15.0)];
    dateLabel.textColor = UIColorFromHex(0x8798A4);
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.font = [UIFont regularFontOfSize:13.0];
    [view addSubview:dateLabel];
    
    Bet *bet = [self objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    dateLabel.text = [[bet.created stringWithFormat:@"LLLL dd"] uppercaseString];
    
    if (self.capperName.length) {
        view.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0 && self.capperName.length) {
        return 226.0;
    }
    return 25.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return 78.0;
}

- (void)handleCardPress:(UILongPressGestureRecognizer *)sender {
    [UIView animateWithDuration:0.15 animations:^{
        if (sender.state == UIGestureRecognizerStateEnded) {
            sender.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
            if (CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view]) && [self.delegate respondsToSelector:@selector(needsToDisplayGraphs)]) {
                [self.delegate needsToDisplayGraphs];
            }
        } else if (sender.state == UIGestureRecognizerStateBegan) {
            sender.view.transform = CGAffineTransformMakeScale(0.95, 0.95);
        } else if (sender.state == UIGestureRecognizerStateFailed || !CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view])) {
            sender.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } else if (CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view])) {
            sender.view.transform = CGAffineTransformMakeScale(0.95, 0.95);
        }
    }];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [super controllerDidChangeContent:controller];
    [self.accountDataSource updateAccountView];
}

@end
