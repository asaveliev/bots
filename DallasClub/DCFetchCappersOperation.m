//
//  DCFetchCappersOperation.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCFetchCappersOperation.h"
#import "Capper.h"

@implementation DCFetchCappersOperation

- (void)main {
    [[DCNetworkCore sharedInstance].cappersProvider fetchCappersPage:self.page ?: @1 withCompletion:^(NSArray *items, NSError *error) {
        if (error || ![items isKindOfClass:[NSArray class]]) {
            [self completeOperationWithError:error];
            return;
        }
        
        NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
        [context performBlock:^{
            NSMutableArray *existing = [[Capper getAllInContext:context] mutableCopy];
            for (NSDictionary *data in items) {
                Capper *item = [Capper getByField:@"uid" equalsTo:data[@"id"] fromContext:context];
                if (!item) {
                    item = [Capper createInContext:context];
                }
                [item setDictionaryRepresentation:data];
                
                [existing removeObject:item];
            }
            
            if (self.page.integerValue == 1 || !self.page) {
                for (Capper *toDelete in existing) {
                    [context deleteObject:toDelete];
                }
            }
            
            NSError *err = nil;
            [context save:&err];
            [self completeOperationWithError:err];
        }];
    }];
}

@end
