//
//  DCGraphView.h
//  DallasClub
//
//  Created by Anton on 04/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKGraphElementView.h"

extern const CGFloat DCGraphViewDefaultOffset;
extern const CGFloat DCGraphViewDefaultWidth;

@protocol DCGraphViewDelegate <NSObject>

- (void)graphViewDidSelectElementAtIndex:(NSInteger)index;
- (BOOL)graphViewCanSelectElementAtIndex:(NSInteger)index;

@end

@interface DCGraphView : UIView

@property (nonatomic, strong)   NSMutableArray            *elements;
@property (nonatomic, strong)   SKGraphElementView        *selectedElement;
@property (nonatomic, readonly) NSInteger                 selecteIndex;

@property (nonatomic, assign) CGFloat offset;
@property (nonatomic, assign) CGFloat elementWidth;

@property (nonatomic, weak) id<DCGraphViewDelegate> delegate;

- (void)setGraphElementsWithValues:(NSArray *)values titles:(NSArray *)titles;

@end
