//
//  DCDateTextField.m
//  DallasClub
//
//  Created by Anton on 21/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCDateTextField.h"
#import "NSDate+Utilities.h"

@interface DCDateTextField ()

@property (nonatomic, strong) AbstractActionSheetPicker *picker;

@end

@implementation DCDateTextField

@synthesize picker = _picker;

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setDatePicker];
    }
    
    return self;
}

- (void)setDatePicker {
    self.picker = [[ActionSheetDatePicker alloc] initWithTitle:@"" datePickerMode:UIDatePickerModeDateAndTime selectedDate:self.selectedDate ?: [NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        self.text = [selectedDate stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
        self.selectedPickerIndex = 0;
        [self resignFirstResponder];
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        [self resignFirstResponder];
    } origin:self];
    
    self.inputAccessoryView = [self.picker createPickerToolbarWithTitle:@""];
    self.inputView = [self.picker configuredPickerView];
    self.inputView.backgroundColor = [UIColor whiteColor];
}

- (void)setSelectedDate:(NSDate *)selectedDate {
    _selectedDate = selectedDate;
    
    [self setDatePicker];
}

@end
