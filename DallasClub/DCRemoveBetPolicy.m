//
//  DCRemoveBetPolicy.m
//  DallasClub
//
//  Created by Anton on 09/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRemoveBetPolicy.h"

@implementation DCRemoveBetPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    [[DCNetworkCore sharedInstance].statsProvider removeBetWithId:self.originalNumber completion:^(NSArray *items, NSError *error) {
        completion(error);
    }];
}

@end
