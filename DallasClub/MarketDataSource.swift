//
//  MarketDataSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 08.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

enum MarketDataSourceMode {
    case grid
    case table
}

class MarketDataSource: AdapterCollectionDataSource<GoodAdapter, ShopCollectionViewCell> {
    
    var mode: MarketDataSourceMode = .grid {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var bannerView: MarketBannerView?
    
    override var fetchRequest: NSFetchRequest<NSFetchRequestResult>! {
        return Good.sortedFetchRequest
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.size.width, height: 130.0)
    }
    
    override func registerClasses() {
        if collectionView == nil {
            return
        }
        
        super.registerClasses()
        
        collectionView.register(MarketBannerView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        collectionView.register(ShopTableCollectionViewCell.self, forCellWithReuseIdentifier: ShopTableCollectionViewCell.tkReuseIdentifier)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if mode == .grid {
            return super.collectionView(collectionView, cellForItemAt: indexPath)
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopTableCollectionViewCell.tkReuseIdentifier, for: indexPath)
        configureCollectionCell(cell, at: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! MarketBannerView
            header.subtitleLabel.text = "Лучшие стратегии"
            header.titleLabel.text = "ХОЧУ ПРОГНОЗ"
            
            bannerView = header
            
            return header
        }
        
        return UICollectionReusableView()
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if mode == .grid {
            let interitemOffset = 8.0
            let itemsPerRow = 2
            let itemWidth = (UIScreen.main.bounds.size.width - CGFloat(interitemOffset) * 3.0) / CGFloat(itemsPerRow)
            let ratio = 1.35
            let itemHeight = itemWidth * CGFloat(ratio)
            return CGSize(width: itemWidth, height: itemHeight)
        }
        
        return ShopTableCollectionViewCell.size(withAdapter: adapter(atIndexPath: indexPath))
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return mode == .table ? UIEdgeInsets.zero : UIEdgeInsetsMake(15.0, 8.0, 0.0, 8.0)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return mode == .grid ? 8.0 : 0.0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return mode == .grid ? 8.0 : 0.0
    }
}
