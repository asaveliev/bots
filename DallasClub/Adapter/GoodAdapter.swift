//
//  GoodAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 08.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class GoodAdapter: TKBaseAdapter {
    
    let title: String
    let subtitle: String
    let price: NSAttributedString
    let imageUrl: String
    
    let buyButtonTitle: String
    let headline: String
    let buttonEnabled: Bool
    
    let hasOffer: Bool
    let percentOffer: String
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        
        if let goodModel = model as? Good {
            title = goodModel.title ?? ""
            subtitle = "Стратегия"
            imageUrl = goodModel.imageUrl ?? ""
            headline = goodModel.headline ?? ""
            buyButtonTitle = goodModel.purchased?.boolValue == false || DCNetworkCore.sharedInstance().authProvider.hasAuth == false ? "Купить за \(DCMoneyFormatter().string(fromAmount: goodModel.price ?? 0)!)" : "Пройти курс"
            buttonEnabled = goodModel.purchased?.boolValue == false
            
            if let fullPrice = goodModel.fullPrice?.intValue, let price = goodModel.price?.intValue {
                if fullPrice == 0 {
                    hasOffer = false
                    percentOffer = ""
                    self.price = NSAttributedString(string: DCMoneyFormatter().string(fromAmount: goodModel.price ?? 0), attributes: [
                        NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)
                        ])
                } else {
                    hasOffer = true
                    let percent = Double(fullPrice - price) / Double(fullPrice)
                    percentOffer = String(format: "%.0f%%", percent * 100)
                    let price = NSMutableAttributedString(string: "\(DCMoneyFormatter().string(fromAmount: goodModel.price ?? 0)!)  ", attributes: [
                        NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)
                        ])
                    let fullPrice = NSAttributedString(string: DCMoneyFormatter().string(fromAmount: goodModel.fullPrice ?? 0), attributes: [
                        NSFontAttributeName: UIFont.systemFont(ofSize: 13.0),
                        NSForegroundColorAttributeName: UIColor.tkColorFromHex(0x8A8A8F),
                        NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
                        ])
                    price.append(fullPrice)
                    
                    self.price = price
                }
            } else {
                hasOffer = false
                percentOffer = ""
                price = NSAttributedString(string: DCMoneyFormatter().string(fromAmount: goodModel.price ?? 0), attributes: [
                    NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)
                    ])
            }
            
        } else {
            buttonEnabled = false
            title = ""
            subtitle = ""
            price = NSAttributedString()
            imageUrl = ""
            percentOffer = ""
            headline = ""
            buyButtonTitle = ""
            hasOffer = false
        }
        
        super.init(model: model, cellClass: cellClass)
    }
    
}

class ReviewActionAdapter: TKBaseAdapter {
    let action: (URL) -> ()
    
    init(model: Any?, cellClass: UITableViewCell.Type, action: @escaping (URL) -> ()) {
        self.action = action
        
        super.init(model: model, cellClass: cellClass)
    }
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        fatalError("init(model:cellClass:) has not been implemented")
    }
    
}

class GoodActionAdapter: GoodAdapter {
    let action: () -> ()
    
    init(model: Any?, cellClass: UITableViewCell.Type, action: @escaping () -> ()) {
        self.action = action
        
        super.init(model: model, cellClass: cellClass)
    }
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        fatalError("init(model:cellClass:) has not been implemented")
    }
}
