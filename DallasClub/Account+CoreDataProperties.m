//
//  Account+CoreDataProperties.m
//  
//
//  Created by Anton on 06/08/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Account+CoreDataProperties.h"

@implementation Account (CoreDataProperties)

@dynamic balance;
@dynamic change;
@dynamic failed_bets;
@dynamic returned_bets;
@dynamic success_bets;
@dynamic month_change;
@dynamic week_change;

@end
