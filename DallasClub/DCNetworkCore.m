//
//  DCNetworkCore.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCNetworkCore.h"
#import "DCHTTPSessionManager.h"

@interface DCNetworkCore ()

@end

@implementation DCNetworkCore

+ (instancetype)sharedInstance {
    static DCNetworkCore *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [DCNetworkCore new];
    });
    
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        _sessionManager = [DCHTTPSessionManager manager];
        self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
        [self updateAuthHeader];
    }
    
    return self;
}

- (void)updateAuthHeader {
    [self.sessionManager.requestSerializer setValue:self.authProvider.token forHTTPHeaderField:@"Authorization"];
}

#pragma mark - Providers

- (DCCappersProvider *)cappersProvider {
    static DCCappersProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCCappersProvider new];
        prov.sessionManager = self.sessionManager;
    });
    
    return prov;
}

- (DCMaterialsProvider *)materialsProvider {
    static DCMaterialsProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCMaterialsProvider new];
        prov.sessionManager = self.sessionManager;
    });
    
    return prov;
}

- (DCAuthProvider *)authProvider {
    static DCAuthProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCAuthProvider new];
        prov.sessionManager = self.sessionManager;
    });
    
    return prov;
}

- (DCFeedProvider *)feedProvider {
    static DCFeedProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCFeedProvider new];
        prov.sessionManager = self.sessionManager;
    });
    
    return prov;
}

- (DCStatisticsProvider *)statsProvider {
    static DCStatisticsProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCStatisticsProvider new];
        prov.sessionManager = self.sessionManager;
    });
    
    return prov;
}

- (DCNotificationsApiProvider *)notificationsProvider {
    static DCNotificationsApiProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCNotificationsApiProvider new];
        prov.sessionManager = [AFHTTPSessionManager manager];
    });
    
    return prov;
}

- (DCGoodsProvider *)goodsProvider {
    static DCGoodsProvider *prov;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        prov = [DCGoodsProvider new];
        prov.sessionManager = [AFHTTPSessionManager manager];
    });
    
    return prov;
}

@end
