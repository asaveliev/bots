//
//  DCRecordViewController.h
//  DallasClub
//
//  Created by Anton Savelev on 03.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCRecordViewController : UIViewController

@property (nonatomic, copy) void (^completion)(NSData *outputData);

@end
