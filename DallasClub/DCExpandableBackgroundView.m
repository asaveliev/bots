//
//  DCExpandableBackgroundView.m
//  DallasClub
//
//  Created by Anton Savelev on 24.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCExpandableBackgroundView.h"

@implementation DCExpandableBackgroundView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"capper-bg"]];
        self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.backgroundImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.backgroundImageView];
        
        _backButton = [UIButton new];
        self.backButton.titleLabel.font = [UIFont systemFontOfSize:16.0];
        [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.backButton setTitle:@"  Прогнозы" forState: UIControlStateNormal];
        self.backButton.tintColor = [UIColor whiteColor];
        self.backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [self.backButton setImage:[[UIImage imageNamed:@"back_button"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self addSubview:self.backButton];
        
        _infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        self.infoButton.tintColor = [UIColor whiteColor];
        self.infoButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [self addSubview:self.infoButton];
        
        [self setBgConstraints];
    }
    
    return self;
}

- (void)setBgConstraints {
    NSDictionary *views = @{
                            @"backButton": self.backButton,
                            @"bgImage": self.backgroundImageView,
                            @"info": self.infoButton
                            };
    
    NSDictionary *metrics = @{
                              @"height": @([UIApplication sharedApplication].statusBarFrame.size.height > 20 ? [UIScreen mainScreen].bounds.size.width * 0.7 : [UIScreen mainScreen].bounds.size.width * 0.55),
                              @"width": @([UIScreen mainScreen].bounds.size.width),
                              @"topInset": @([UIApplication sharedApplication].statusBarFrame.size.height > 20 ? [UIApplication sharedApplication].statusBarFrame.size.height + 7.0 : [UIApplication sharedApplication].statusBarFrame.size.height + 13.0)
                              };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-64)-[bgImage(==height)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[backButton(20)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bgImage(==width)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[backButton(99)]-(>=0)-[info(50)]-12-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:views]];
}

- (void)bindTableViewToResizeBackground:(UITableView *)tableView {
    [tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"contentOffset"]) {
        self.headerView.alpha = 0.0;
        CGFloat fadeOffset = [UIApplication sharedApplication].statusBarFrame.size.height + 44.0;
        CGPoint offset = [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue];
        
        if (offset.y >= -fadeOffset + 2.0) {
            CGFloat ratio = (offset.y + fadeOffset) / fadeOffset;
            self.headerView.alpha = fabs(ratio);
            [self bringSubviewToFront:self.headerView];
            self.headerView.hidden = NO;
        } else {
            self.headerView.hidden = YES;
            [self sendSubviewToBack:self.headerView];
        }
        
        self.backButton.hidden = offset.y >= -30.0;
        self.backgroundImageView.hidden = offset.y >= 0.0;
        CGFloat bgOffset = [UIScreen mainScreen].bounds.size.width * 0.55;
        if (offset.y <= -bgOffset) {
            self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = YES;
            
            CGRect frame = self.backgroundImageView.frame;
            frame.origin.y = -([UIApplication sharedApplication].statusBarFrame.size.height + 44.0);
            frame.size.height = fabs(offset.y);
            frame.size.width = [UIScreen mainScreen].bounds.size.width;
            self.backgroundImageView.frame = frame;
        }
    }
}


@end
