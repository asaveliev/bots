//
//  DCApiProvider.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@interface DCFeedProvider : DCApiProvider

- (void)fetchFeedWithCompletion:(void(^)(NSArray *items, NSError *error))completion;
- (void)fetchFeedPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion;
- (void)fetchFeedPage:(NSNumber *)pageNumber capperId:(NSNumber *)capperId withCompletion:(void (^)(NSArray *, NSError *))completion;

- (void)createPost:(NSDictionary *)parameters image:(UIImage *)image withCompletion:(void (^)(NSArray *, NSError *))completion;
- (void)removePostWithId:(NSNumber *)postId completion:(void (^)(NSError *error))completion;
- (void)updatePostWithId:(NSNumber *)postId image:(UIImage *)image parameters:(NSDictionary *)params completion:(void (^)(NSError *error))completion;

@end
