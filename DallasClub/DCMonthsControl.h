//
//  DCMonthsControl.h
//  DallasClub
//
//  Created by Anton on 05/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "HAScrollableSegmentedControll.h"

@interface DCMonthsControl : HAScrollableSegmentedControll

@property (nonatomic, readwrite) NSInteger  selectedIndex;
@property (nonatomic, readwrite) NSArray    *models;

@end
