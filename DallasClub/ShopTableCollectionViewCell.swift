//
//  ShopTableCollectionViewCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class ShopTableCollectionViewCell: UICollectionViewCell, AdapterProtocol {
    
    static let priceWidth = 54.0
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let cellAdapter = adapter as? GoodAdapter else {
                return
            }
            
            typeLabel.text = cellAdapter.subtitle
            priceLabel.attributedText = cellAdapter.price
            titleLabel.text = cellAdapter.title
            
            if let url = URL(string: cellAdapter.imageUrl) {
                titleImageView.sd_setImage(with: url)
            }
        }
    }
    
    lazy var titleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        imageView.layer.cornerRadius = 4.0
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.tkColorFromHex(0x8A8A8F)
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 15.0)
        return label
    }()
    
    lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textAlignment = .right
        return label
    }()
    
    lazy var divederView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xC8C7CC)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(titleImageView)
        contentView.addSubview(typeLabel)
        contentView.addSubview(titleLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(divederView)
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        
        titleImageView.snp.remakeConstraints { make in
            make.width.height.equalTo(40)
            make.leading.top.equalToSuperview().inset(16)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(18)
            make.leading.equalTo(titleImageView.snp.trailing).offset(8)
            make.trailing.equalTo(priceLabel.snp.leading).offset(-8)
        }
        
        typeLabel.snp.remakeConstraints { make in
            make.leading.equalTo(titleImageView.snp.trailing).offset(8)
            make.top.equalTo(titleLabel.snp.bottom).offset(2)
            make.bottom.equalToSuperview().offset(-16)
            make.trailing.equalTo(priceLabel.snp.leading).offset(-8)
        }
        
        priceLabel.snp.remakeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.width.equalTo(ShopTableCollectionViewCell.priceWidth)
        }
        
        divederView.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(1 / UIScreen.main.scale)
            make.bottom.equalToSuperview()
        }
        
        super.updateConstraints()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func size(withTitle title: String, subtitle: String) -> CGSize {
        let boundingWidth = UIScreen.main.bounds.width - CGFloat(64.0 - 16.0 - priceWidth)
        let verticalInsets = 32.0
        let height = title.height(withConstrainedWidth: boundingWidth, font: UIFont.systemFont(ofSize: 15.0)) + subtitle.height(withConstrainedWidth: boundingWidth, font: UIFont.systemFont(ofSize: 13.0)) + CGFloat(verticalInsets) + 2.0
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }

    static func size(withAdapter adapter: TKBaseAdapter) -> CGSize {
        guard let cellAdapter = adapter as? GoodAdapter else {
            return CGSize.zero
        }
        
        return size(withTitle: cellAdapter.title, subtitle: cellAdapter.subtitle)
    }
    
}
