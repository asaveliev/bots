//
//  BaseManagedObject.m
//  StocksApp
//
//  Created by Anton on 09/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "BaseManagedObject.h"
#import <CoreData/CoreData.h>

@implementation BaseManagedObject

+ (NSString *)entityName {
    return NSStringFromClass(self);
}

+ (instancetype)createInContext:(NSManagedObjectContext *)context {
    return [NSEntityDescription insertNewObjectForEntityForName:[self entityName] inManagedObjectContext:context];
}

+ (instancetype)getByField:(NSString *)fieldName equalsTo:(id)value fromContext:(NSManagedObjectContext *)context {
    return [self getAllByField:fieldName equalsTo:value fromContext:context].lastObject;
}

+ (NSArray *)getAllByField:(NSString *)fieldName equalsTo:(id)value fromContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    if (!fieldName) {
        fetchRequest.predicate = [NSPredicate predicateWithValue:YES];
    } else {
        NSString *format = [NSString stringWithFormat:@"%@=%%@", fieldName];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:format, value];
    }
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
        return @[];
    }
    return result;
}

+ (NSArray *)getAllInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:[self entityName]];
    fetchRequest.predicate = [NSPredicate predicateWithValue:YES];
    NSError *error = nil;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
        return @[];
    }
    return result;
}

@end
