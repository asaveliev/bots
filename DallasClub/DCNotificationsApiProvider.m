//
//  DCNotificationsApiProvider.m
//  DallasClub
//
//  Created by Anton on 15/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCNotificationsApiProvider.h"
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "DCNetworkCore.h"

static NSString *const DCNotificationsDisabledKey = @"DCNotificationsDisabled";

static NSString *const DCPostEndpoint = @"devices";

@implementation DCNotificationsApiProvider

- (void)registerToken:(NSString *)token {
    if (!token) {
        return;
    }
    
    /* {
     "device": {
     "platform": "android",
     "token": "device_token_goes_here"
     }
     } */
    
    NSDictionary *params = @{
                             @"device": @{
                                 @"platform": @"ios",
                                 @"token": token
                                 }
                             };
    self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [self.sessionManager.requestSerializer setValue:[DCNetworkCore sharedInstance].authProvider.token forHTTPHeaderField:@"Authorization"];
    [self.sessionManager POST:[DCApiRoot stringByAppendingString:DCPostEndpoint] parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail");
    }];
}


- (void)removeToken:(NSString *)token {
    if (token == nil) {
        return;
    }
    self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [self.sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [self.sessionManager DELETE:[NSString stringWithFormat:@"%@/%@", [DCApiRoot stringByAppendingString:DCPostEndpoint], token] parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"fail");
    }];
}

- (BOOL)notificationsEnabled {
    return ![[NSUserDefaults standardUserDefaults] boolForKey:DCNotificationsDisabledKey];
}

- (void)setNotificationsEnabled:(BOOL)notificationsEnabled {
    if (notificationsEnabled) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    } else {
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:!notificationsEnabled forKey:DCNotificationsDisabledKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
