//
//  DCFetchBetsOperation.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFetchBetsOperation.h"
#import "Bet.h"
#import "Capper.h"

@interface DCFetchBetsOperation()

@property (nonatomic, readonly) NSString *capperName;

@end

@implementation DCFetchBetsOperation

- (instancetype)initWithPageNumber:(NSNumber *)page capperName:(NSString *)name {
    if (self = [super initWithPageNumber:page]) {
        _capperName = name;
    }
    
    return self;
}

- (void)main {
    NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
    NSNumber *capperId;
    Capper *capper = [Capper getByField:@"name" equalsTo:self.capperName fromContext:context];
    capperId = capper.uid;
    
    [[DCNetworkCore sharedInstance].statsProvider fetchStatisticsPage:self.page ?: @1 capperId:capperId withCompletion:^(NSArray *items, NSError *error) {
        if (error || ![items isKindOfClass:[NSArray class]]) {
            [self completeOperationWithError:error];
            return;
        }
        
        NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
        [context performBlock:^{
            NSMutableArray *existing = [[Bet getAllInContext:context] mutableCopy];
            for (NSDictionary *data in items) {
                Bet *item = [Bet getByField:@"uid" equalsTo:data[@"id"] fromContext:context];
                if (!item) {
                    item = [Bet createInContext:context];
                }
                [item setDictionaryRepresentation:data];
                
                [existing removeObject:item];
            }
            
            if ((self.page.integerValue == 1 || !self.page) && !self.capperName.length) {
                for (Bet *toDelete in existing) {
                    [context deleteObject:toDelete];
                }
            }
            
            NSError *err = nil;
            [context save:&err];
            [self completeOperationWithError:err];
        }];
    }];
}

@end
