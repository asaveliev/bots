//
//  DCProvider.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

extern NSString *const DCApiRootOld;
extern NSString *const DCApiRoot;
extern NSString *const DCApiHost;

@interface DCApiProvider : NSObject

@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;

@end
