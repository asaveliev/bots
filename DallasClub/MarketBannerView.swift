//
//  MarketBannerView.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class MarketBannerView: UICollectionReusableView {
    
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        view.clipsToBounds = true
        view.image = #imageLiteral(resourceName: "shop_baner_img.png")
        return view
    }()
    
    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.5
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28.0, weight: UIFontWeightSemibold)
        label.textColor = UIColor.white
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        addSubview(overlayView)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }
    
    override func updateConstraints() {
        imageView.snp.remakeConstraints { [unowned self] make in
            make.top.bottom.leading.trailing.equalTo(self)
        }
        
        titleLabel.snp.remakeConstraints { [unowned self] make in
            make.leading.trailing.equalTo(self).inset(10.0)
            make.top.equalTo(self).offset(37.0)
            make.centerX.equalTo(self)
        }
        
        subtitleLabel.snp.remakeConstraints { [unowned self] make in
            make.leading.trailing.equalTo(self).inset(10.0)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(4.0)
            make.centerX.equalTo(self)
        }
        
        overlayView.snp.remakeConstraints { make in
            make.edges.equalTo(imageView)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
