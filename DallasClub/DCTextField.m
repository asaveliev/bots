//
//  DCTextField.m
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCTextField.h"

@interface DCTextField ()

@property (nonatomic, readonly) UIView  *bottomLine;
@property (nonatomic, readonly) UIView  *topLine;

@end

@implementation DCTextField

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _bottomLine = [UIView new];
        [self addSubview:self.bottomLine];
        
        _selectedPickerIndex = -1;
        
        _topLine = [UIView new];
        [self addSubview:self.topLine];
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.topLine.backgroundColor = self.bottomLine.backgroundColor = UIColorFromHex(0xC3CDD4);
    }
    
    return self;
}

- (void)setSelectedPickerIndex:(NSInteger)selectedPickerIndex {
    _selectedPickerIndex = selectedPickerIndex;
    
    [self setupPickerWithTitles:self.titles];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.bottomLine.frame = (CGRect){ 0.0, self.bounds.size.height - 1.0, self.bounds.size.width, 1.0 };
    self.topLine.frame = (CGRect){ 0.0, 0.0, self.bounds.size.width, 1.0 };
}

- (void)setTitles:(NSArray *)titles {
    _titles = titles;
    [self setupPickerWithTitles:self.titles];
}

- (void)setupPickerWithTitles:(NSArray *)titles {
    _picker = [[ActionSheetStringPicker alloc] initWithTitle:@"" rows:titles initialSelection:self.selectedPickerIndex != -1 ? self.selectedPickerIndex : 0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.text = selectedValue;
        self.selectedPickerIndex = selectedIndex;
        [self resignFirstResponder];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        [self resignFirstResponder];
    } origin:self];
    
    self.inputAccessoryView = [self.picker createPickerToolbarWithTitle:@""];
    self.inputView = [self.picker configuredPickerView];
    self.inputView.backgroundColor = [UIColor whiteColor];
}

@end
