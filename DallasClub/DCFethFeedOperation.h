//
//  DCFethFeedOperation.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCPaginableOperation.h"

@interface DCFethFeedOperation : DCPaginableOperation

- (instancetype)initWithPageNumber:(NSNumber *)page capperName:(NSString *)name;

@end
