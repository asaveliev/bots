//
//  HAScrollableSegmentedControll.h
//  HistoryApp
//
//  Created by Anton on 14/05/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HASelectionBlock)(NSInteger selectedIndex, BOOL selected);

@interface HAScrollableSegmentedControll : UIView

@property (strong, nonatomic, readonly) UIScrollView *scrollView;
@property (nonatomic, copy) HASelectionBlock selectionBlock;
@property (strong, nonatomic) NSMutableArray<UIButton *> *buttons;

- (void)setButtonsWithTitles:(NSArray *)titles andSizes:(NSArray <NSDictionary *>*)sizes;
- (void)configureButton:(UIButton *)button;
- (void)buttonClicked:(UIButton *)button;

@end
