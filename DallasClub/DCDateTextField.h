//
//  DCDateTextField.h
//  DallasClub
//
//  Created by Anton on 21/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCTextField.h"

@interface DCDateTextField : DCTextField

@property (nonatomic, strong) NSDate *selectedDate;

@end
