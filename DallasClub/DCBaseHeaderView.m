//
//  HABaseHeaderView.m
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseHeaderView.h"

@interface DCBaseHeaderView ()

@end

@implementation DCBaseHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _headerTitleLabel = [UILabel new];
        self.headerTitleLabel.textColor = [UIColor blackColor];
        self.headerTitleLabel.font = [UIFont headerViewTitleFont];
        self.headerTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.headerTitleLabel];
        
        _rightButton = [UIButton new];
        [self addSubview:self.rightButton];
        
        _leftButton = [UIButton new];
        [self addSubview:self.leftButton];
        
        self.rightButton.titleLabel.font = self.leftButton.titleLabel.font = [UIFont headerButtonsFont];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"title": self.headerTitleLabel,
                            @"rightButton": self.rightButton,
                            @"leftButton": self.leftButton
                            };
    NSDictionary *metrics = @{
                              @"topInset": @([UIApplication sharedApplication].statusBarFrame.size.height / 1.5)
                              };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }

    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[title]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[rightButton(>=28)]-12-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[leftButton(>=28)]" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[title]-0-|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[rightButton]-0-|" options:0 metrics:metrics views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topInset-[leftButton]-0-|" options:0 metrics:metrics views:views]];
}

@end
