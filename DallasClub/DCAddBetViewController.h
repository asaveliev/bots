//
//  DCAddBetViewController.h
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DCBetsPolicy;

@interface DCAddBetViewController : UIViewController

@property (nonatomic, assign) BOOL needsUpdateAfterComplete;

- (instancetype)initWithPolicy:(DCBetsPolicy *)policy;

@end
