//
//  DCFeedDataSource.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCCappersDataSource.h"
#import "DCFeedDataSource.h"
#import "Post.h"
#import "DCFeedCell.h"
#import "DCFeedSectionHeaderView.h"
#import "NSDate+Utilities.h"
#import "DCImporter.h"
#import "DCAudioCell.h"
#import "DCAudioStreamer.h"
#import "DCCappersHeaderView.h"
#import "DCPhotoFeedCell.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "SVPullToRefresh.h"
#import "STKAudioPlayer.h"
#import "DallasClub-Swift.h"

@interface DCFeedDataSource () <DCAudioCellProtocol, DCstreamerDelegate, DCPhotoFeedCellDelegate>

@property (strong, nonatomic) NSNumber              *uid;
@property (strong, nonatomic) NSString              *capperName;

@property (strong, nonatomic) DCAudioStreamer       *streamer;

@end

@implementation DCFeedDataSource
    
@dynamic delegate;

- (instancetype)initWithTableView:(UITableView *)tableView postId:(NSNumber *)uid {
    _uid = uid;
    if (self  = [super initWithTableView:tableView]) {
        if (uid) {
            return self;
        }
        
        __weak DCFeedDataSource *weakSelf = self;
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            [[DCImporter importer] loadNextFeedPage];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.tableView.infiniteScrollingView stopAnimating];
            });
        }];
        
        _streamer = [DCAudioStreamer new];
        self.streamer.delegate = self;
        
        _cappersDataSource = [[DCCappersDataSource alloc] initWithTableView:nil];
    }
    
    return self;
}

- (void)setTableView:(UITableView *)tableView {
    self.tableView.delegate = nil;
    self.tableView.dataSource = nil;
    [super setTableView:tableView];
    [self registerClasses];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.tableView reloadData];
    __weak DCFeedDataSource *weakSelf = self;
    [self.tableView.infiniteScrollingView stopAnimating];
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [[DCImporter feedImporter] loadNextFeedPageWithCapperName:weakSelf.capperName];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        });
    }];
}

- (instancetype)initWithTableView:(UITableView *)tableView postId:(NSNumber *)uid capperName:(NSString *)name {
    _capperName = name;
    if (self = [super initWithTableView:tableView]) {
        _streamer = [DCAudioStreamer new];
        self.streamer.delegate = self;
        
        __weak DCFeedDataSource *weakSelf = self;
        [self.tableView addInfiniteScrollingWithActionHandler:^{
            [[DCImporter feedImporter] loadNextFeedPageWithCapperName:name];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakSelf.tableView.infiniteScrollingView stopAnimating];
            });
        }];
    }
    
    return self;
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Post entityName]];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"ordering" ascending:NO],
                                [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO]];
    request.predicate = self.uid ? [NSPredicate predicateWithFormat:@"uid = %@", self.uid] : [NSPredicate predicateWithValue:YES];
    if (self.capperName) {
        request.predicate = [NSPredicate predicateWithFormat:@"capper_name = %@", self.capperName];
    }
    
    return request;
}

- (NSString *)sectionName {
    return @"ordering";
}

- (void)registerClasses {
    [self.tableView registerClass:[DCFeedCell class] forCellReuseIdentifier:self.reuseIdentifier];
    [self.tableView registerClass:[DCPhotoFeedCell class] forCellReuseIdentifier:@"DCPhotoFeedCell"];
    [self.tableView registerClass:[DCAudioCell class] forCellReuseIdentifier:@"audioCell"];
    [self.tableView registerClass:[DCFeedSectionHeaderView class] forHeaderFooterViewReuseIdentifier:@"Header"];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([cell isKindOfClass:[DCFeedCell class]]) {
        [self configureFeedCell:(DCFeedCell *)cell atIndexPath:indexPath];
    } else {
        [self configureAudioCell:(DCAudioCell *)cell atIndexPath:indexPath];
    }
}

- (void)configureAudioCell:(DCAudioCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Post *post = [self objectAtIndexPath:indexPath];
    cell.delegate = self;
    cell.audioUrl = post.audioUrl;
    cell.audioButton.selected = NO;
    cell.slider.slider.value = 0.0;
    cell.slider.slider.maximumValue = 1.0;
    cell.slider.slider.minimumValue = 0.0;
    cell.slider.slider.userInteractionEnabled = NO;
    cell.slider.minLabel.hidden = YES;
    cell.slider.minLabel.text = @"00:00";
    cell.nameLabel.text = post.capper_name;
    if ([cell.audioUrl isEqualToString:self.streamer.playingAudioUrl]) {
        cell.audioButton.selected = !self.streamer.paused;
        cell.slider.slider.userInteractionEnabled = YES;
        self.streamer.slider = cell.slider;
    } else {
        cell.audioButton.selected = NO;
    }
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:post.capperPhotoUrl]];
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"HH:mm";
    cell.timeLabel.text = [formatter stringFromDate:post.created];
}

- (void)configureFeedCell:(DCFeedCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Post *post = [self objectAtIndexPath:indexPath];
    cell.messageLabel.text = post.content;
    cell.nameLabel.text = post.capper_name;
    
    cell.avatarImageView.hidden = !post.capperPhotoUrl.length;
    [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:post.capperPhotoUrl]];
    
    cell.messageLabel.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        if ([self.delegate respondsToSelector:@selector(dataSource:didSelectLink:)]) {
            [self.delegate dataSource:self didSelectLink:[NSURL URLWithString:string]];
        }
    };
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"HH:mm";
    cell.timeLabel.text = [formatter stringFromDate:post.created];
    
    if ([cell isKindOfClass:[DCPhotoFeedCell class]]) {
        DCPhotoFeedCell *photoCell = (DCPhotoFeedCell *)cell;
        photoCell.delegate = self;
        [photoCell.photoImageView sd_setImageWithURL:[NSURL URLWithString:post.imageUrl]];
    }
    
    if (self.capperName.length) {
        cell.backgroundColor = cell.contentView.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
}

#pragma mark - UITableView

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    Post *post = [self objectAtIndexPath:indexPath];
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:post.audioUrl.length ? @"audioCell" : self.reuseIdentifier];
    if (post.imageUrl.length) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DCPhotoFeedCell" forIndexPath:indexPath];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Post *post = [self objectAtIndexPath:indexPath];
    if (post.audioUrl.length) {
        return UITableViewAutomaticDimension;
    }
    CGFloat height = [post.content heightWithConstrainedWidth: [UIScreen mainScreen].bounds.size.width - (IPAD ? 240 : 70) font: [UIFont feedFont]];
    return post.imageUrl.length ? height + (IPAD ? 590 : 290) : height + 100;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return self.uid ? CGFLOAT_MIN : 25.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil;
    }
    
    if (self.uid) {
        return nil;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, 25.0)];
    view.backgroundColor = [UIColor clearColor];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 8.5, [UIScreen mainScreen].bounds.size.width, 15.0)];
    dateLabel.textColor = UIColorFromHex(0x8798A4);
    dateLabel.textAlignment = NSTextAlignmentCenter;
    dateLabel.font = [UIFont regularFontOfSize:13.0];
    [view addSubview:dateLabel];
    
    Post *post = [self objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    dateLabel.text = [[post.created stringWithFormat:@"LLLL dd"] uppercaseString];
    
    if (self.capperName.length) {
        view.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
    
    return view;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[DCAudioCell class]]) {
        DCAudioCell *audioCell = (DCAudioCell *)cell;
        if ([audioCell.audioUrl isEqualToString:self.streamer.playingAudioUrl]) {
            self.streamer.slider = audioCell.slider;
        }
    }
    
    if (self.capperName.length) {
        cell.backgroundColor = cell.contentView.backgroundColor = UIColorFromHex(0xF7F7F7);
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[DCAudioCell class]]) {
        DCAudioCell *audioCell = (DCAudioCell *)cell;
        if ([audioCell.audioUrl isEqualToString:self.streamer.playingAudioUrl]) {
            audioCell.slider.slider.userInteractionEnabled = YES;
            self.streamer.slider = nil;
        }
    }
}

#pragma mark - DCAudioCellProtocol

- (void)audioCellDidPlay:(DCAudioCell *)cell {
    if ([cell.audioUrl isEqualToString:self.streamer.playingAudioUrl]) {
        self.streamer.paused = NO;
        return;
    }
    
    [self.streamer playAudioWithUrl:cell.audioUrl];
    self.streamer.slider = cell.slider;
    cell.slider.userInteractionEnabled = YES;
    [self.tableView reloadData];
}

- (void)audioCellDidPause:(DCAudioCell *)cell {
    self.streamer.paused = YES;
}

- (void)audioCellSliderChanged:(DCAudioCell *)cell toValue:(CGFloat)value {
    [self.streamer.player seekToTime:value];
}

#pragma mark - DCstreamerDelegate

-(void)streamerDidEnd {
    [self.tableView reloadData];
}

#pragma mark - DCPhotoFeedCellDelegate

- (void)photoCellDidSelectImageWithUrl:(NSString *)url {
    [self.delegate dataSource:self didSelectPhoto:url];
}

#pragma mark -

- (void)infoAction {
    [self.delegate dataSourceDidSelectInfo];
}

@end
