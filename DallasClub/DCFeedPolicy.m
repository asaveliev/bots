//
//  DCFeedPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "Bet.h"
#import "DCFeedPolicy.h"
#import "NSDateFormatter+ISO8601.h"

@interface DCFeedPolicy ()

@property (nonatomic, strong) Post *post;

@end

@implementation DCFeedPolicy

- (instancetype)initWithPostId:(NSNumber *)postId {
    if (self = [super init]) {
        _post = [Post getByField:@"uid" equalsTo:postId fromContext:[DCPersistentStack sharedInstance].context];
        _text = self.post.content;
        _date = self.post.created;
        _betNumber =  self.post.bet.number ? [NSString stringWithFormat:@"%@", self.post.bet.number] : nil;
    }
    
    return self;
}

- (NSString *)dateString {
    return [NSDateFormatter stringFromDateForISO8601:self.date];
}

#pragma mark - Action

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    completion(nil);
}

#pragma mark -

- (NSNumber *)postId {
    return self.post.uid;
}

@end
