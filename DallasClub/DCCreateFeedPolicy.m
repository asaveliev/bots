//
//  DCCreateFeedPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCCreateFeedPolicy.h"

@implementation DCCreateFeedPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    NSMutableDictionary *post = [NSMutableDictionary new];
    if (self.dateString.length) {
        [post setObject:self.dateString forKey:@"date"];
    }
    if (self.betNumber.length) {
        [post setObject:self.betNumber forKey:@"number"];
    }
    if (self.audioData) {
        [post setObject:self.audioData forKey:@"audio"];
    } else {
        [post setObject:self.text ?: @"" forKey:@"content"];
    }
    
    [post setObject:@(self.isTgHidden) forKey:@"tg_hidden"];

    NSDictionary *parameters = @{
                                 @"post": post
                                 };
    [[DCNetworkCore sharedInstance].feedProvider createPost:parameters image:self.image withCompletion:^(NSArray *response, NSError *error) {
        completion(error);
    }];
}

@end
