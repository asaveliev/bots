//
//  DCBetsPolicy.h
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCNetworkCore.h"

@interface DCBetsPolicy : NSObject

@property (nonatomic, strong)   NSString    *percent;
@property (nonatomic, strong)   NSString    *numberString;
@property (nonatomic, readonly) NSNumber    *originalNumber;
@property (nonatomic, strong)   NSString    *statusString;
@property (nonatomic, readonly) NSNumber    *betId;
@property (nonatomic, strong)   NSString    *coefficient;
@property (nonatomic, readonly) NSString    *status;
@property (nonatomic, assign)   NSInteger   state;

@property (nonatomic, strong)   NSDate      *date;
@property (nonatomic, readonly) NSString    *dateString;

- (instancetype)initWithBetId:(NSNumber *)uid;

- (void)performActionWithCompletion:(void(^)(NSError *error))completion;

@end
