//
//  DCStatisticsViewController.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatisticsViewController.h"
#import "DCGraphsViewController.h"
#import "DCAddBetViewController.h"
#import "DCBetsDataSource.h"
#import "DCAccountDataSource.h"
#import "DCStatisticsView.h"
#import "DCAboutViewController.h"
#import "DCFeedViewController.h"

#import "Bet.h"
#import "Post.h"
#import "DCImporter.h"

#import "SVProgressHUD.h"
#import "RMUniversalAlert.h"

#import "DCBetsPolicy.h"
#import "DCAddBetPolicy.h"
#import "DCRemoveBetPolicy.h"
#import "DCEditBetPolicy.h"

@interface DCStatisticsViewController () <DCBetsDataSourceDelegate>

@property (strong, nonatomic) DCBetsDataSource      *dataSource;
@property (strong, nonatomic) DCAccountDataSource   *accountSource;

@property (strong, nonatomic) DCStatisticsView      *mainView;

@end

@implementation DCStatisticsViewController

- (void)loadView {
    self.view = self.mainView = [DCStatisticsView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.headerTitleLabel.text = @"Статистика Далласа";
    
    _dataSource = [[DCBetsDataSource alloc] initWithTableView:self.mainView.tableView];
    self.dataSource.delegate = self;
     [self.dataSource.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    self.dataSource.delegate = self;
    _accountSource = [[DCAccountDataSource alloc] initWithAccountView:self.mainView.tableHeader];
    
    [self.mainView.rightButton addTarget:self action:@selector(addPlusButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    UILongPressGestureRecognizer *press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleCardPress:)];
    press.minimumPressDuration = 0.0;
    [self.mainView.tableHeader addGestureRecognizer:press];
    self.mainView.tableView.delaysContentTouches = NO;
    
    [self.mainView.headerView.leftButton addTarget:self action:@selector(info) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.headerView.rightButton.hidden = ![DCNetworkCore sharedInstance].authProvider.isAdmin;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.mainView.headerView.rightButton.hidden = ![DCNetworkCore sharedInstance].authProvider.isAdmin;
}

- (void)refreshData {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.dataSource.refreshControl endRefreshing];
    });
    [[DCImporter importer] updateBets];
}

- (void)info {
    NSString *text = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texts" ofType:@"plist"]][@"bets"];
    [self.navigationController pushViewController:[[DCAboutViewController alloc] initWithText:text] animated:YES];
}

- (void)addPlusButtonTapped {
    DCAddBetViewController *vc = [[DCAddBetViewController alloc] initWithPolicy:[DCAddBetPolicy new]];
    [self.tabBarController.navigationController pushViewController:vc animated:YES];
}

- (void)handleCardPress:(UILongPressGestureRecognizer *)sender {
    [UIView animateWithDuration:0.15 animations:^{
        if (sender.state == UIGestureRecognizerStateEnded) {
            self.mainView.tableHeader.transform = CGAffineTransformMakeScale(1.0, 1.0);
            if (CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view])) {
                [self presentViewController:[DCGraphsViewController new] animated:YES completion:nil];
            }
        } else if (sender.state == UIGestureRecognizerStateBegan) {
            self.mainView.tableHeader.transform = CGAffineTransformMakeScale(0.95, 0.95);
        } else if (sender.state == UIGestureRecognizerStateFailed || !CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view])) {
            self.mainView.tableHeader.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } else if (CGRectContainsPoint(sender.view.frame, [sender locationInView:sender.view])) {
            self.mainView.tableHeader.transform = CGAffineTransformMakeScale(0.95, 0.95);
        }
    }];
}

#pragma mark - DCDataSourceDelegate

- (void)dataSource:(id)dataSource didSelectObjectAtIndexPath:(NSIndexPath *)indexPath {
    Bet *bet = [self.dataSource objectAtIndexPath:indexPath];
    if (![DCNetworkCore sharedInstance].authProvider.isAdmin) {
        if (!bet.post) {
            return;
        }
        UIViewController *vc = [[DCFeedViewController alloc] initWithPostId:bet.post.uid];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }

    [RMUniversalAlert showActionSheetInViewController:self withTitle:@"" message:@"Выберите действие" cancelButtonTitle:@"Отмена" destructiveButtonTitle:nil otherButtonTitles:@[@"Изменить", @"Удалить"] popoverPresentationControllerBlock:^(RMPopoverPresentationController * _Nonnull popover) {
        
    } tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
        DCBetsPolicy *policy;
        if (buttonIndex == 2) {
            policy = [[DCEditBetPolicy alloc] initWithBetId:bet.uid];
            [self.navigationController pushViewController:[[DCAddBetViewController alloc] initWithPolicy:policy] animated:YES];
        } else if (buttonIndex == 3) {
            policy = [[DCRemoveBetPolicy alloc] initWithBetId:bet.uid];
            
            [SVProgressHUD show];
            [policy performActionWithCompletion:^(NSError *error) {
                [SVProgressHUD dismiss];
                if (!error) {
                    [[DCImporter importer] updateBets];
                    return;
                }
                
                [RMUniversalAlert showAlertInViewController:self withTitle:@"Ошибка!" message:@"Попробуйте позже" cancelButtonTitle:@"Ок" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                    
                }];
            }];
        }
    }];
}

@end
