//
//  DCEditFeedPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCEditFeedPolicy.h"

@implementation DCEditFeedPolicy

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    NSMutableDictionary *post = [NSMutableDictionary new];
    [post setObject:self.text ?: @"" forKey:@"content"];
    if (self.dateString.length) {
        [post setObject:self.dateString forKey:@"date"];
    }
    if (self.betNumber.length) {
        [post setObject:self.betNumber forKey:@"number"];
    }
    
    NSDictionary *parameters = @{
                                 @"post": post
                                 };
    
    [[DCNetworkCore sharedInstance].feedProvider updatePostWithId:self.postId image:self.image parameters:parameters completion:^(NSError *error) {
        completion(error);
    }];
}

@end
