//
//  UIColor+DCColors.h
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#define UIColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#import <UIKit/UIKit.h>

@interface UIColor (DCColors)

+ (instancetype)okButtonColor;

+ (instancetype)bgColor;

+ (instancetype)tabBarTintColor;

+ (instancetype)statsNegativeBalanceColor;
+ (instancetype)statsPositiveBalanceColor;
+ (instancetype)statsPostiveBubbleColor;
+ (instancetype)statsNegativeBubbleColor;
+ (instancetype)statsNeutralBubbleColor;
+ (instancetype)statsHeaderSubtitleColor;

+ (instancetype)audioBlueColor;
+ (instancetype)audioGrayColor;

@end
