//
//  DCAddPhotoView.h
//  DallasClub
//
//  Created by Anton Savelev on 04/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCAddPhotoView : UIView

@property (strong, nonatomic) UIImage *image;

@end
