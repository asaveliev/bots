//
//  DCAccountDataSource.m
//  DallasClub
//
//  Created by Anton on 01/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAccountDataSource.h"
#import "DCTableHeaderView.h"
#import "DCMoneyFormatter.h"
#import "Account.h"

@interface DCAccountDataSource ()

@end

@implementation DCAccountDataSource

- (instancetype)initWithAccountView:(DCTableHeaderView *)view {
    if (self = [super initWithTableView:nil]) {
        _headerView = view;
        
        [self updateAccountView];
    }
    
    return self;
}

- (void)setHeaderView:(DCTableHeaderView *)headerView {
    _headerView = headerView;
    
    [self updateAccountView];
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:[Account entityName]];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"change" ascending:NO]];
    request.predicate = [NSPredicate predicateWithValue:YES];
    
    return request;
}

- (void)updateAccountView {
    Account *acc = self.account;
    if (!acc) {
        return;
    }
    
    DCMoneyFormatter *formatter = [DCMoneyFormatter new];
    
    self.headerView.titleLabel.text = [formatter stringFromAmount:acc.balance];
    //self.headerView.subtitleLabel.text = [NSString stringWithFormat:@"прибыль %@", [formatter stringFromAmount:acc.change]];
    self.headerView.successLabel.text = [NSString stringWithFormat:@"%@",acc.success_bets];
    self.headerView.returnLabel.text = [NSString stringWithFormat:@"%@",acc.returned_bets];
    self.headerView.failedLabel.text = [NSString stringWithFormat:@"%@",acc.failed_bets];
}

- (Account *)account {
    return [self objectAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

#pragma mak - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self updateAccountView];
    
    if (self.accountDelegate) {
        [self.accountDelegate dataSourceDidChangeContent];
    }
}

@end
