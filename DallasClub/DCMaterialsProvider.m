//
//  DCMaterialsProvider.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMaterialsProvider.h"

NSString *const DCMaterialsEndpoint = @"/blog/posts";

@implementation DCMaterialsProvider

- (void)fetchMaterialsWithCompletion:(void (^)(NSArray *, NSError *))completion {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"page"] = @1;
    parameters[@"per_page"] = @(INT_MAX);
    
    [self.sessionManager GET:[DCApiRoot stringByAppendingString:DCMaterialsEndpoint] parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

@end
