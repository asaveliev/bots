//
//  DCAddBetViewController.m
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "Bet.h"
#import "DCPersistentStack.h"
#import "NSDate+Utilities.h"

#import "DCAddBetViewController.h"
#import "DCAddBetView.h"

#import "DCNetworkCore.h"
#import "DCImporter.h"

#import "RMUniversalAlert.h"
#import "SVProgressHUD.h"

#import "DCBetsPolicy.h"
#import "DCAddBetPolicy.h"

@interface DCAddBetViewController ()

@property (strong, nonatomic) DCAddBetView *mainView;

@property (strong, nonatomic) DCBetsPolicy *policy;

@end

@implementation DCAddBetViewController

- (instancetype)initWithPolicy:(DCBetsPolicy *)policy {
    if (self = [super init]) {
        _needsUpdateAfterComplete = YES;
        _policy = policy;
    }
    
    return self;
}

- (void)loadView {
    self.view = _mainView = [DCAddBetView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureFields];
    
    self.mainView.coefTextField.text = self.policy.coefficient;
    self.mainView.percentTextField.text = self.policy.percent;
    self.mainView.statusTextField.text = self.policy.statusString;
    self.mainView.dateTextField.selectedDate = self.policy.date;
    self.mainView.dateTextField.text = [self.policy.date stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
    self.mainView.statusTextField.selectedPickerIndex = self.policy.state;
    
    [self.mainView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.rightButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureFields {
    NSMutableArray *numbers = [NSMutableArray new];
    
    [SVProgressHUD show];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSMutableArray *numbers = [NSMutableArray new];
        for (NSInteger i = 0; i < 10e4; i++) {
            [numbers addObject:[NSString stringWithFormat:@"%ld", (long)i]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.mainView.numberTextField.titles = numbers;
            
            self.mainView.numberTextField.text = self.policy.numberString;
            Bet *lastBet = [[Bet getAllInContext:[DCPersistentStack sharedInstance].context] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"ordering" ascending:NO], [NSSortDescriptor sortDescriptorWithKey:@"number" ascending:NO]]].firstObject;
            if ([self.policy isKindOfClass:[DCAddBetPolicy class]]) {
                self.mainView.numberTextField.text = [NSString stringWithFormat:@"%ld", lastBet.number.integerValue];
            }
            self.mainView.numberTextField.selectedPickerIndex = [self.mainView.numberTextField.titles indexOfObject:self.mainView.numberTextField.text] != NSNotFound ? [self.mainView.numberTextField.titles indexOfObject:self.mainView.numberTextField.text] : 0;
            
            [SVProgressHUD dismissWithDelay:0.1];
        });
    });
    
    numbers = [NSMutableArray new];
    for (int i = 1; i <= 100; i++) {
        [numbers addObject:[NSString stringWithFormat:@"%d %%", i]];
    }
    self.mainView.percentTextField.titles = numbers;
    
    numbers = [NSMutableArray new];
    for (NSInteger i = 100; i <= 3000; i++) {
        [numbers addObject:[NSString stringWithFormat:@"%.2f", i / 100.0]];
    }
    self.mainView.coefTextField.titles = numbers;
    
    self.mainView.statusTextField.titles = @[@"Cтавка прошла 💪", @"Cтавка не прошла 😢", @"Возврат 🤔"];
}

- (void)done {
    if (!self.mainView.numberTextField.text.length || !self.mainView.percentTextField.text.length || !self.mainView.statusTextField.text.length || !self.mainView.coefTextField.text.length || !self.mainView.dateTextField.text.length) {
        [RMUniversalAlert showAlertInViewController:self withTitle:@"Ошибка!" message:@"Одно или несколько полей не заполнены" cancelButtonTitle:@"Ок" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
            
        }];
        
        return;
    }

    NSString *stateString;
    if (self.mainView.statusTextField.selectedPickerIndex == 0) {
        stateString = @"successful";
    } else if (self.mainView.statusTextField.selectedPickerIndex == 1) {
        stateString = @"failed";
    } else {
        stateString = @"returned";
    }
    
    self.policy.numberString = self.mainView.numberTextField.text;
    self.policy.percent = [self.mainView.percentTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"% "]];
    self.policy.coefficient = self.mainView.coefTextField.text;
    self.policy.state = self.mainView.statusTextField.selectedPickerIndex;
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterMediumStyle;
    formatter.timeStyle = NSDateFormatterShortStyle;
    self.policy.date = [formatter dateFromString:self.mainView.dateTextField.text];
    
    [SVProgressHUD show];
    [self.policy performActionWithCompletion:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        if (error) {
            [RMUniversalAlert showAlertInViewController:self withTitle:@"Ошибка!" message:@"Попробуйте позже" cancelButtonTitle:@"Ок" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                
            }];
            
            return;
        }
        
        if (self.needsUpdateAfterComplete) {
            [[DCImporter importer] updateBets];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
