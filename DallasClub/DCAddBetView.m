//
//  DCAddBetView.m
//  DallasClub
//
//  Created by Anton on 30/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAddBetView.h"
#import "DCClassicHeaderView.h"

@implementation DCAddBetView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.headerTitleLabel.text = @"Результат";
        
        [self.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
        [self.leftButton setTitle:@"Назад" forState:UIControlStateNormal];
        [self.leftButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
        [self.rightButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
        [self.rightButton setTitle:@"Добавить" forState:UIControlStateNormal];
        
        _numberTitleLabel = [self titleLabelWithTitle:@"НОМЕР СТАВКИ"];
        [self.contentView addSubview:self.numberTitleLabel];
        
        _percentTitleLabel = [self titleLabelWithTitle:@"ПРОЦЕНТ ОТ БАНКА"];
        [self.contentView addSubview:self.percentTitleLabel];
        
        _statusTitleLabel = [self titleLabelWithTitle:@"СТАТУС"];
        [self.contentView addSubview:self.statusTitleLabel];
        
        _coefTitleLabel = [self titleLabelWithTitle:@"коэффициент"];
        [self.contentView addSubview:self.coefTitleLabel];
        
        _dateTitleLabel = [self titleLabelWithTitle:@"дата"];
        [self.contentView addSubview:self.dateTitleLabel];
        
        _numberTextField = [self configureTextField];
        [self.contentView addSubview:self.numberTextField];
        
        _percentTextField = [self configureTextField];
        [self.contentView addSubview:self.percentTextField];
        
        _statusTextField = [self configureTextField];
        [self.contentView addSubview:self.statusTextField];
        
        _coefTextField = [self configureTextField];
        [self.contentView addSubview:self.coefTextField];
        
        _dateTextField = [DCDateTextField new];
        self.dateTextField.font = [UIFont regularFontOfSize:17.0];
        self.dateTextField.textColor = [UIColor blackColor];
        self.dateTextField.leftView = [[UIView alloc] initWithFrame:(CGRect){ CGPointZero, 15.0, 48.0 }];
        self.dateTextField.leftViewMode = UITextFieldViewModeAlways;
        [self.contentView addSubview:self.dateTextField];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"percent": self.percentTitleLabel,
                            @"percentField": self.percentTextField,
                            @"number": self.numberTitleLabel,
                            @"numberField": self.numberTextField,
                            @"status": self.statusTitleLabel,
                            @"statusField": self.statusTextField,
                            @"coef": self.coefTitleLabel,
                            @"coefField": self.coefTextField,
                            @"date": self.dateTitleLabel,
                            @"dateField": self.dateTextField
                            };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    NSDictionary *metrics = @{
                              @"vSpacing": @20.0,
                              @"fieldHeight": @48.0,
                              @"labelSpacing": @9.0
                              };
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[number]-labelSpacing-[numberField(==fieldHeight)]-vSpacing-[percent]-labelSpacing-[percentField(==fieldHeight)]-vSpacing-[status]-labelSpacing-[statusField(==fieldHeight)]-vSpacing-[coef]-labelSpacing-[coefField(==fieldHeight)]-vSpacing-[date]-labelSpacing-[dateField(==fieldHeight)]" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[percentField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[numberField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[statusField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[coefField]-0-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[dateField]-0-|" options:0 metrics:metrics views:views]];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[status]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[number]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[percent]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[coef]-12-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[date]-12-|" options:0 metrics:metrics views:views]];
    
}

- (DCTextField *)configureTextField {
    DCTextField *field = [DCTextField new];
    field.font = [UIFont regularFontOfSize:17.0];
    field.textColor = [UIColor blackColor];
    field.leftView = [[UIView alloc] initWithFrame:(CGRect){ CGPointZero, 15.0, 48.0 }];
    field.leftViewMode = UITextFieldViewModeAlways;
    
    return field;
}

- (UILabel *)titleLabelWithTitle:(NSString *)title {
    UILabel *label = [UILabel new];
    label.textColor = UIColorFromHex(0x6D6D72);
    label.font = [UIFont regularFontOfSize:13.0];
    label.text = [title uppercaseString];
    
    return label;
}

- (Class)headerViewClass {
    return [DCClassicHeaderView class];
}

@end
