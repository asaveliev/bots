
//
//  DCAddPhotoView.m
//  DallasClub
//
//  Created by Anton Savelev on 04/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAddPhotoView.h"

@interface DCAddPhotoView()

@property (strong, nonatomic)   UIImageView *imageView;
@property (nonatomic, readonly) UIView      *bottomLine;
@property (nonatomic, readonly) UIView      *topLine;
@property (nonatomic, strong)   UILabel     *label;
@property (nonatomic, strong)   UIView      *shadowView;

@end

@implementation DCAddPhotoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _bottomLine = [UIView new];
        [self addSubview:self.bottomLine];
        _topLine = [UIView new];
        [self addSubview:self.topLine];
        
        _label = [UILabel new];
        self.label.font = [UIFont regularFontOfSize:17.0];
        self.label.textColor = [UIColor blackColor];
        self.label.text = @"Выбрать фото";
        [self addSubview:self.label];
        
        _shadowView = [UIView new];
        self.shadowView.layer.masksToBounds = NO;
        self.shadowView.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.4].CGColor;
        self.shadowView.layer.shadowOffset = CGSizeMake(0.0, 2.0);
        self.shadowView.layer.shadowOpacity = 1.0;
        [self addSubview:self.shadowView];
        
        _imageView = [UIImageView new];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.imageView.layer.cornerRadius = 2.0;
        [self addSubview:self.imageView];
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.topLine.backgroundColor = self.bottomLine.backgroundColor = UIColorFromHex(0xC3CDD4);
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
       @"label": self.label,
       @"image": self.imageView,
       @"shadow": self.shadowView
    };
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[label]-[image(22)]-15-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[label]-0-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[image]-13-|" options:0 metrics:nil views:views]];
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[label]-[shadow(22)]-15-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-13-[shadow]-13-|" options:0 metrics:nil views:views]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.bottomLine.frame = (CGRect){ 0.0, self.bounds.size.height - 1.0, self.bounds.size.width, 1.0 };
    self.topLine.frame = (CGRect){ 0.0, 0.0, self.bounds.size.width, 1.0 };
    self.shadowView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.shadowView.bounds cornerRadius:2.0].CGPath;
}

#pragma maek - 

- (void)setImage:(UIImage *)image {
    self.imageView.image = image;
    self.shadowView.hidden = image == nil;
}

- (UIImage *)image {
    return self.imageView.image;
}

@end
