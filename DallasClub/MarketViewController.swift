//
//  MarketViewController.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import RMUniversalAlert
import SVProgressHUD

@objc
public class MarketViewController: UIViewController {
    
    let mainView = MarketView()
    
    let importer = ManagedObjectsImporter<Good>()
    let dataSource = MarketDataSource()
    
    public override func loadView() {
        view = mainView
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        importer.sync()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        
        mainView.headerView.titleLabel.text = "Магазин"
        dataSource.collectionView = mainView.collectionView
        dataSource.delegate = self
        
        mainView.collectionView.addObserver(self, forKeyPath: "contentOffset", options: [.new], context: nil)
        
        mainView.headerView.rightButton.setImage(#imageLiteral(resourceName: "navbar_list_icon"), for: .normal)
        mainView.headerView.rightButton.addTarget(self, action: #selector(changeTypeAction), for: .touchUpInside)
    }
    
    func changeTypeAction() {
        mainView.headerView.rightButton.setImage(dataSource.mode == .grid ? #imageLiteral(resourceName: "navbar_grid_icon") : #imageLiteral(resourceName: "navbar_list_icon"), for: .normal)
        dataSource.mode = dataSource.mode == .grid ? .table : .grid
    }
    
    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentOffset" {
            guard let offset = change?[.newKey] as? CGPoint else {
                return
            }
            
            if offset.y >= -64.0 {
                return
            }
            
            guard let bannerView = dataSource.bannerView else {
                return
            }
            
            bannerView.translatesAutoresizingMaskIntoConstraints = true
            var frame = bannerView.frame
            frame.size.height = fabs(offset.y) + 64.0
            frame.size.width = UIScreen.main.bounds.width
            frame.origin.y  = -fabs(offset.y + 64.0)
            bannerView.frame = frame
            
        }
    }
    
}

extension MarketViewController: DCDataSourceDelegate {
    public func dataSource(_ dataSource: Any!, didSelectObjectAt indexPath: IndexPath!) {        
        guard let good = self.dataSource.object(at: indexPath) as? Good else {
            return
        }
        
        SVProgressHUD.show()
        importer.importDetails(forObjectWithId: good.id) { success in
            SVProgressHUD.dismiss()
            
            if success {
                self.navigationController?.pushViewController(GoodDetailsViewController(withGoodId: good.id), animated: true)
                return
            }
            
            RMUniversalAlert.show(in: self, withTitle: "Ошибка", message: "Попробуйте позже", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
        }
    }
}
