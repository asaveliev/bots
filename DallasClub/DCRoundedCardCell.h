//
//  DCRoundedCardCell.h
//  DallasClub
//
//  Created by Anton on 19/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCRoundedCardCell : UITableViewCell

@property (strong, nonatomic, readonly) UIView *cardView;
@property (strong, nonatomic, readonly) UIView *shadowedView;

- (void)setCardConstraints;

@end
