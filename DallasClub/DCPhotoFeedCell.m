//
//  DCPhotoFeedCell.m
//  DallasClub
//
//  Created by Anton Savelev on 26.02.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCPhotoFeedCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TKNewRoundedCardCell ()
@property (strong, nonatomic) UIView  *cardView;
@end

@interface DCPhotoFeedCell ()

@property (strong, nonatomic) UITapGestureRecognizer *recognizer;

@end

@implementation DCPhotoFeedCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _photoImageView = [UIImageView new];
        self.photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.photoImageView.clipsToBounds = YES;
        self.photoImageView.userInteractionEnabled = YES;
        self.photoImageView.layer.cornerRadius = 3.0;
        self.photoImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.cardView addSubview:self.photoImageView];
        
        _recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePhotoTap)];
        [self.photoImageView addGestureRecognizer:self.recognizer];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    if (!self.photoImageView) {
        return;
    }
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[text]-15-|" options:0 metrics:nil views:@{ @"text": self.messageLabel }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[image]-15-|" options:0 metrics:nil views:@{ @"image": self.photoImageView }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[v(30)]-8-[name]-5-[time]-15-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{ @"v": self.avatarImageView, @"time": self.timeLabel, @"name": self.nameLabel }]];
    if (IPAD) {
        [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[v(30)]-15-[image(470)]-15-[text]-15-|" options:0 metrics:nil views:@{ @"v": self.avatarImageView, @"text": self.messageLabel, @"image": self.photoImageView }]];
    } else {
        [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[v(30)]-15-[image(170)]-15-[text]-15-|" options:0 metrics:nil views:@{ @"v": self.avatarImageView, @"text": self.messageLabel, @"image": self.photoImageView }]];
    }
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[time]" options:0 metrics:nil views:@{ @"v": self.avatarImageView, @"text": self.messageLabel, @"time": self.timeLabel }]];
}

#pragma mark - Action

- (void)handlePhotoTap {
    [self.delegate photoCellDidSelectImageWithUrl:self.photoImageView.sd_imageURL.absoluteString];
}

@end
