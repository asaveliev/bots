//
//  DCFeedPolicy.h
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"
#import "DCPersistentStack.h"
#import "DCNetworkCore.h"

@interface DCFeedPolicy : NSObject

@property (nonatomic, readonly) NSNumber    *postId;
@property (nonatomic, strong)   NSString    *text;
@property (nonatomic)           BOOL        isTgHidden;

@property (nonatomic, strong)   NSString    *betNumber;

@property (nonatomic, strong)   NSDate      *date;
@property (nonatomic, readonly) NSString    *dateString;

@property (nonatomic, strong)   UIImage     *image;
@property (nonatomic, readonly) NSString    *imageUrl;

- (instancetype)initWithPostId:(NSNumber *)postId;

- (void)performActionWithCompletion:(void(^)(NSError *error))completion;

@end
