//
//  TKNewRoundedCardCell.m
//  DallasClub
//
//  Created by Anton Savelev on 25.02.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "TKNewRoundedCardCell.h"

@interface TKNewRoundedCardCell ()
@property (strong, nonatomic) UIView  *cardView;
@end

@implementation TKNewRoundedCardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _cardView = [UIView new];
        self.cardView.translatesAutoresizingMaskIntoConstraints = NO;
        self.cardView.backgroundColor = UIColorFromHex(0x2e3035);
        self.cardView.layer.masksToBounds = YES;
        self.cardView.layer.cornerRadius = 6.0;
        [self.contentView addSubview:self.cardView];
        
        [self setCardConstraints];
        
        self.contentView.backgroundColor = self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setCardConstraints {
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[v(>=70@1000)]-10-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[v]-20-|" options:0 metrics:nil views:@{ @"v": self.cardView }]];
}

@end
