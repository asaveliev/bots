//
//  Post+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 04/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Post.h"

@class Bet;

NS_ASSUME_NONNULL_BEGIN

@interface Post (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSDate *created;
@property (nullable, nonatomic, retain) NSDate *updated_at;
@property (nullable, nonatomic, retain) NSString *ordering;
@property (nullable, nonatomic, retain) NSNumber *uid;
@property (nullable, nonatomic, retain) NSString *imageUrl;
@property (nullable, nonatomic, retain) NSString *audioUrl;
@property (nullable, nonatomic, retain) Bet *bet;
@property (nullable, nonatomic, retain) NSString *capperPhotoUrl;
@property (nullable, nonatomic, retain) NSString *capper_name;

@end

NS_ASSUME_NONNULL_END
