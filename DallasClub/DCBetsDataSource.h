//
//  DCBetsDataSource.h
//  DallasClub
//
//  Created by Anton on 25/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFetchedDataSource.h"

@protocol DCBetsDataSourceDelegate <DCDataSourceDelegate>

@optional
- (void)needsToDisplayGraphs;

@end

@interface DCBetsDataSource : DCFetchedDataSource

@property (nonatomic, weak) id<DCBetsDataSourceDelegate> delegate;

- (instancetype)initWithTableView:(UITableView *)tableView capperName:(NSString *)name;

@end
