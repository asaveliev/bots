//
//  DCUserModel.m
//  DallasClub
//
//  Created by Anton Savelev on 11.06.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCUserModel.h"
#import "Lockbox.h"

NSString *const DCKeychainTokenPrefix = @"DCKeychainToken";
NSString *const DCKeychainPasswordPrefix = @"DCKeychainPassword";

@implementation DCUserModel

@dynamic supportsSecureCoding;

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[DCUserModel class]]) {
        return NO;
    }
    
    DCUserModel *userModel = (DCUserModel *)object;
    return [userModel.userId isEqualToNumber:self.userId];
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _userId = [aDecoder decodeObjectForKey:@"id"];
        _name = [aDecoder decodeObjectForKey:@"name"];
        _isSuperUser = [aDecoder decodeObjectForKey:@"admin"];
        _avatarUrl = [aDecoder decodeObjectForKey:@"photo_url"];
        _login = [aDecoder decodeObjectForKey:@"login"];
        _isCapper = [aDecoder decodeObjectForKey:@"isCapper"];
        self.password = [aDecoder decodeObjectForKey:@"password"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.userId forKey:@"id"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.isSuperUser forKey:@"admin"];
    [aCoder encodeObject:self.avatarUrl forKey:@"photo_url"];
    [aCoder encodeObject:self.password forKey:@"password"];
    [aCoder encodeObject:self.login forKey:@"login"];
    [aCoder encodeObject:self.isCapper forKey:@"isCapper"];
}

- (void)setDictionaryRepresentation:(NSDictionary *)dictionaryRepresentation {
    _userId = dictionaryRepresentation[@"id"];
    _name = dictionaryRepresentation[@"name"];
    _isSuperUser = dictionaryRepresentation[@"admin"];
    _avatarUrl = dictionaryRepresentation[@"photo_url"];
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict[@"id"] = self.userId;
    dict[@"name"] = self.name;
    dict[@"admin"] = self.isSuperUser;
    dict[@"photo_url"] = self.avatarUrl;
    return dict;
}

- (void)setPassword:(NSString *)password {
    [Lockbox archiveObject:password forKey:[DCKeychainPasswordPrefix stringByAppendingString:[NSString stringWithFormat:@"%ld", self.userId.integerValue]]];
}

- (NSString *)password {
    return [Lockbox unarchiveObjectForKey:[DCKeychainPasswordPrefix stringByAppendingString:[NSString stringWithFormat:@"%ld", self.userId.integerValue]]];
}

- (void)setToken:(NSString *)token {
    [Lockbox archiveObject:token forKey:[DCKeychainTokenPrefix stringByAppendingString:[NSString stringWithFormat:@"%ld", self.userId.integerValue]]];
}

- (NSString *)token {
    return [Lockbox unarchiveObjectForKey:[DCKeychainTokenPrefix stringByAppendingString:[NSString stringWithFormat:@"%ld", self.userId.integerValue]]];
}

- (void)removeFromKeychain {
    self.password = nil;
    self.token = nil;
}

@end
