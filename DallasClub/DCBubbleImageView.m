//
//  DCBubbleImageView.m
//  DallasClub
//
//  Created by Anton on 06/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBubbleImageView.h"

@implementation DCBubbleImageView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _imageView = [UIImageView new];
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.imageView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[img]-0-|" options:0 metrics:nil views:@{ @"img": self.imageView }]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[img]-0-|" options:0 metrics:nil views:@{ @"img": self.imageView }]];
    }
    
    return self;
}

@end
