//
//  DCFeedView.m
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedView.h"
#import "DCClassicHeaderView.h"

@interface DCFeedView ()

@property (nonatomic, assign) BOOL selected;
@property (nonatomic, readonly) UIView *topView;

@end

@implementation DCFeedView

- (instancetype)initWithSelectedItem:(BOOL)selected {
    if (self = [self initWithFrame:CGRectZero]) {
        if (!selected) {
            [self.leftButton setContentEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
            [self.leftButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
            [self.leftButton setImage:[UIImage imageNamed:@"info_icon"] forState:UIControlStateNormal];
        } else {
            [self.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
            [self.leftButton setTitle:@"Назад" forState:UIControlStateNormal];
            [self.leftButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
        }
    }
    
    return self;
}

- (Class)headerViewClass {
    return nil;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self.rightButton setImage:[UIImage imageNamed:@"add-message-icon"] forState:UIControlStateNormal];
        
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.backgroundColor = UIColorFromHex(0x161616);
        self.tableView.tableHeaderView = self.tableHeader;
        
        self.contentView.backgroundColor = UIColorFromHex(0x161616);
        self.backgroundColor = UIColorFromHex(0x161616);
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.topView.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (IPAD) {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-85-[v]-85-|" options:0 metrics:nil views:@{ @"v": self.tableView }]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[v]-49-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:nil views:@{ @"v": self.tableView}]];
    } else {
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[v]-0-|" options:0 metrics:nil views:@{ @"v": self.tableView }]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[v]-49-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:nil views:@{ @"v": self.tableView}]];
    }
}

@end
