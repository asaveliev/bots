//
//  Bet+CoreDataProperties.m
//  
//
//  Created by Anton Savelev on 30/08/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bet+CoreDataProperties.h"

@implementation Bet (CoreDataProperties)

@dynamic amout;
@dynamic balance_change;
@dynamic coefficient;
@dynamic created;
@dynamic number;
@dynamic ordering;
@dynamic state;
@dynamic uid;
@dynamic post;
@dynamic capper_name;

@end
