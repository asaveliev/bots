//
//  DCSettingsViewController.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCSettingsViewController.h"
#import "DCSettingsView.h"
#import "DCSettingsCell.h"
#import "DCNetworkCore.h"
#import "RMUniversalAlert.h"
#import "SVProgressHUD.h"
#import "DCTabBarController.h"
#import "DallasClub-Swift.h"
#import "DCUserModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *const appStoreURLFormat = @"itms-apps://itunes.apple.com/us/app/itunes-u/id%@?action=write-review";
static NSString *const appStoreID = @"1147785664";

@interface DCSettingsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) DCSettingsView    *mainView;

@property (strong, nonatomic) NSArray           *subtitles;
@property (strong, nonatomic) NSArray           *titles;

@end

@implementation DCSettingsViewController

- (void)loadView {
    self.view = _mainView = [DCSettingsView new];
}
    
- (NSArray *)subtitles {
    BOOL auth = [DCNetworkCore sharedInstance].authProvider.hasAuth;
    return auth ? @[@"", @"", @"", @"В группе мы пишем новости и проводим различные розыгрыши. Присоединяйтесь!", @"", @"", @""] : @[@"", @"", @"В группе мы пишем новости и проводим различные розыгрыши  Присоединяйтесь!", @"", @"", @"", @""];
    
}

- (NSArray *)titles {
    BOOL auth = [DCNetworkCore sharedInstance].authProvider.hasAuth;
    return auth ? @[@"", auth ? @"Выход" : @"Авторизация", @"Оповещения"] : @[ auth ? @"Выход" : @"Авторизация", @"Оповещения"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mainView.tableView registerClass:[DCSettingsCell class] forCellReuseIdentifier:@"Cell"];
    [self.mainView.tableView registerClass:[SettingsIconCell class] forCellReuseIdentifier:@"IconCell"];
    [self.mainView.tableView registerClass:[UserCell class] forCellReuseIdentifier:@"User"];
    
    self.mainView.tableView.dataSource = self;
    self.mainView.tableView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:@"DCUpdateTabBarNotification" object:nil];
}

- (void)reload {
    [self.mainView.tableView reloadData];
}
    
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.mainView.tableView reloadData];
    
    self.tabBarController.title = @"Настройки";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titles.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([DCNetworkCore sharedInstance].authProvider.hasAuth) {
        DCUserModel *user = [DCNetworkCore sharedInstance].authProvider.currentUser;
        if (indexPath.row == 0) {
            UserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
            cell.titleLabel.text = user.name;
            cell.subtitleLabel.text = user.isCapper.boolValue ? @"Прогнозист" : @"Пользователь";
            [cell.avaView sd_setImageWithURL:[NSURL URLWithString: user.avatarUrl] placeholderImage:[UIImage imageNamed: @"ava"]];
            return cell;
        }
    }
    
    DCSettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.titleLabel.text = self.titles[indexPath.row];
    cell.subtitleLabel.text = self.subtitles[indexPath.row];
    BOOL auth = [DCNetworkCore sharedInstance].authProvider.hasAuth;
    int offset = auth ? 1 : 2;
    cell.switchControl.hidden = indexPath.row != 3 - offset;
    [cell.switchControl setOn:[DCNetworkCore sharedInstance].notificationsProvider.notificationsEnabled];
    
    [cell.switchControl addTarget:self action:@selector(notificationsSwitched:) forControlEvents:UIControlEventValueChanged];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.titleLabel.textColor = [UIColor whiteColor];
    cell.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL auth = [DCNetworkCore sharedInstance].authProvider.hasAuth;
    int offset = auth ? 1 : 2;
    
    switch (indexPath.row - 1 + offset) {
        case 3:
            [self rateAction];
            break;
        case 4:
            [self rateAction];
            break;
        case 5:
            [self openVkAction];
            break;
        case 6:
            [self pushAction];
            break;
        case 7:
            [self agreementAction];
            break;
        case 1:
            [self loginAction];
            break;
        default:
            break;
    }
}

#pragma mark Actions

- (void)questionAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://vk.com/ayratdallas"]];
}

- (void)marketAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://hochuprognoz.ru/shop/"]];
}

- (void)freeStrategyAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://hochuprognoz.ru/shop/tennis-40-40-no"]];
}

- (void)openPurchases {
    PurchasesController *vc = [PurchasesController new];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)loginAction {
    if ([DCNetworkCore sharedInstance].authProvider.hasAuth) {
        DCAuthProvider *auth = [DCNetworkCore sharedInstance].authProvider;
        [auth removeUser:[DCNetworkCore sharedInstance].authProvider.currentUser];
        if (auth.currentUser.isCapper.boolValue) {
            if (!auth.users.lastObject) {
                [[DCNetworkCore sharedInstance] updateAuthHeader];
                [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
                [self.mainView.tableView reloadData];
                return;
            }
            [auth selectUser:auth.users.lastObject completion:^(BOOL success) {
                [self.mainView.tableView reloadData];
                [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
            }];
            return;
        }
        [auth selectUser:nil completion:nil];
        [self.mainView.tableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:DCUpdateTabBarNotification object:nil];
        return;
    }
    
    [self.tabBarController presentViewController:[LoginViewController new] animated:true completion:nil];
}

- (void)notificationsSwitched:(UISwitch *)sender {
    BOOL auth = [DCNetworkCore sharedInstance].authProvider.hasAuth;
    if (!auth) return;
    [DCNetworkCore sharedInstance].notificationsProvider.notificationsEnabled = sender.isOn;
}

- (void)openTelegramAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://telegram.me/hpbets"]];
}

- (void)openVkAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://vk.com/hochuprognoz"]];
}

- (void)rateAction {
    NSString *urlString = [NSString stringWithFormat:appStoreURLFormat, appStoreID];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (void)openYouTubeAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/channel/UCsS_wa7ga92S2-Yk6eYT18Q"]];
}

- (void)pushAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://hochuprognoz.ru/blog/posts/push"]];
}

- (void)agreementAction {
    NSString *text = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"texts" ofType:@"plist"]][@"agreement"];
    [RMUniversalAlert showAlertInViewController:self withTitle:@"Соглашение" message:text cancelButtonTitle:@"Я согласен" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

@end
