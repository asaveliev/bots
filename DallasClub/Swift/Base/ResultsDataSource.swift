//
//  ResultsDataSource.swift
//  Unistroy
//
//  Created by Admin on 22.05.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

protocol ResultsDataSourceDelegate: class {
    func didSelectRow(at indexPath: IndexPath)
}

class ResultsDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    var tableView: UITableView!
    var adapters: [TKBaseAdapter]!
    weak var delegate: ResultsDataSourceDelegate?
    
    init(tableView: UITableView, adapters: [TKBaseAdapter]) {
        super.init()
        self.adapters = adapters
        for adapter in adapters {
            tableView.register(adapter.cellClass, forCellReuseIdentifier: adapter.cellReuseIdentifier)
        }
        self.tableView = tableView
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adapters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: adapters[indexPath.row].cellReuseIdentifier, for: indexPath) as! AdapterProtocol
        cell.adapter = adapters[indexPath.row]
        (cell as? UITableViewCell)?.selectionStyle = .none
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }

}
