//
//  TKBaseAdapter.swift
//  Orthodont
//
//  Created by Anton Savelev on 15.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

class TKBaseAdapter {

    var cellClass: UITableViewCell.Type

    var cellReuseIdentifier: String {
        return cellClass.tkReuseIdentifier
    }

    var model: Any?

    required init(model: Any?, cellClass:UITableViewCell.Type) {
        self.model = model
        self.cellClass = cellClass
    }

}
