//
//  TKBaseViewController.swift
//  Orthodont
//
//  Created by Anton Savelev on 13.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit
import SVProgressHUD

class TKBaseViewController<T: TKBaseContentView>: UIViewController {
    
    var hideHeader: Bool {
        return false
    }

    var hideBackButton: Bool {
        return self.navigationController?.viewControllers.first == self
    }

    var mainView: T! {
        return view as! T
    }
    
    override var title: String? {
        set { self.mainView.headerView.titleLabel.text = newValue }
        get { return self.mainView.headerView.titleLabel.text }
    }
    
    override func loadView() {
        view = T(headerViewHidden: hideHeader)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.headerView.leftButton.isHidden = hideBackButton
        self.mainView.headerView.leftButton.addTarget(self, action: #selector(back), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    func back() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    func showError(text: String) {
        let alert = UIAlertController(title: "Ошибка!", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ок", style: .default, handler: { _ in
            alert.dismiss(animated: true, completion: nil)
        }))

        present(alert, animated: true, completion: nil)
    }

    func showWaiting() {
        SVProgressHUD.show()
    }

    func hideWaiting() {
        SVProgressHUD.dismiss()
    }
    
}
