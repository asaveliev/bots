//
//  AdapterDataSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 08.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class AdapterDataSource<A: TKBaseAdapter, C: UITableViewCell>: DCFetchedDataSource {
    override init!(tableView: UITableView!) {
        super.init(tableView: tableView)
        refreshControl.removeFromSuperview()
    }
    
    override func registerClasses() {
        if tableView == nil {
            return
        }
        tableView.register(C.self, forCellReuseIdentifier: C.tkReuseIdentifier)
        tableView.register(C.self, forCellReuseIdentifier: "Cell")
    }
    
    func adapter(atIndexPath indexPath: IndexPath) -> A {
        let object = self.object(at: indexPath)
        let adapter = A(model: object, cellClass: C.self)
        return adapter
    }
    
    override var reuseIdentifier: String! {
        return C.tkReuseIdentifier
    }
    
    override func configureCell(_ cell: UITableViewCell!, at indexPath: IndexPath!) {
        guard let adapterCell = cell as? AdapterProtocol else {
            return
        }
        
        adapterCell.adapter = adapter(atIndexPath: indexPath)
    }
}

class AdapterCollectionDataSource<A: TKBaseAdapter, C: UICollectionViewCell>: DCFetchedColectionViewDataSource {
    func adapter(atIndexPath indexPath: IndexPath) -> A {
        let object = self.object(at: indexPath)
        let adapter = A(model: object, cellClass: UITableViewCell.self)
        return adapter
    }
    
    override var reuseIdentifier: String! {
        return C.tkReuseIdentifier
    }
    
    override init() {
        super.init(tableView: UITableView())
    }
    
    override func registerClasses() {
        if collectionView == nil {
            return
        }
        collectionView.register(C.self, forCellWithReuseIdentifier: C.tkReuseIdentifier)
    }
    
    override func configureCollectionCell(_ cell: UICollectionViewCell!, at indexPath: IndexPath!) {
        guard let adapterCell = cell as? AdapterProtocol else {
            return
        }
        
        adapterCell.adapter = adapter(atIndexPath: indexPath)
    }

}
