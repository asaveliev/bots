//
//  TKBaseHeaderView.swift
//  Orthodont
//
//  Created by Anton Savelev on 13.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

enum TKBaseHeaderViewType: Int {
    case normal
    case transparent
    case forMap
}

class TKBaseHeaderView: UIView {
    
    fileprivate(set) var titleLabel: UILabel!
    fileprivate(set) var leftButton: UIButton!
    fileprivate(set) var rightButton: UIButton!
    fileprivate var dividerView: UIView!
    fileprivate var dividerViewHeight: NSLayoutConstraint!
    var type: TKBaseHeaderViewType! {
        didSet {
            updateViewType()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        titleLabel = UILabel()
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
        titleLabel.textAlignment = .center
        addSubview(titleLabel)
        
        leftButton = UIButton()
        addSubview(leftButton)
        
        rightButton = UIButton()
        rightButton.contentHorizontalAlignment = .right
        addSubview(rightButton)

        // leftButton.setImage(#imageLiteral(resourceName: "back-arrow").withRenderingMode(.alwaysTemplate), for: .normal)
        leftButton.contentHorizontalAlignment = .left

        dividerView = UIView()
        dividerView.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        addSubview(dividerView)
        setConstraints()

        type = .normal
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setConstraints() {
        let views: [String : UIView] = ["back": leftButton, "title": titleLabel, "right": rightButton, "dividerView": dividerView]
        for view in views.values {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        let buttonSize: CGFloat = 75.0
        let metrics = ["buttonSize": buttonSize, "vMargin": UIApplication.shared.statusBarFrame.size.height]
        
        dividerViewHeight = NSLayoutConstraint.init(item: dividerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 0.5)
        NSLayoutConstraint.activate([dividerViewHeight])
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-9-[back(==buttonSize)]-[title]-[right(==buttonSize)]-9-|",
            options: .alignAllCenterY,
            metrics: metrics,
            views: views
        ))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-(<=vMargin)-[back]-0-[dividerView]-0-|",
            options: [],
            metrics: metrics,
            views: views
        ))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[dividerView]-0-|", options: [], metrics: nil, views: views))
        NSLayoutConstraint(item: rightButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonSize).isActive = true
    }
    
    func updateViewType() {
        if type == .normal {
            dividerViewHeight.constant = 0.5
            backgroundColor = UIColor.tkColorFromHex(0xF7F7F7)
            titleLabel.textColor = UIColor.black
        }
        else if type == .transparent {
            dividerViewHeight.constant = 0.0
            backgroundColor = UIColor.clear
            leftButton.tintColor = UIColor.white
            titleLabel.textColor = UIColor.white
        }
        else {
            dividerViewHeight.constant = 0.0
            backgroundColor = UIColor.clear
            titleLabel.textColor = UIColor.white
        }
    }
    
}
