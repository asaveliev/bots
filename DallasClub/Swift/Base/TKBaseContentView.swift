//
//  TKBaseContenView.swift
//  Orthodont
//
//  Created by Anton Savelev on 13.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

class TKBaseContentView: UIView {
    
    fileprivate(set) var contentView: UIView!
    fileprivate(set) var headerView: TKBaseHeaderView!
    fileprivate(set) var hideHeaderView: Bool = false
    var headerViewType: TKBaseHeaderViewType! {
        didSet {
            headerView.type = headerViewType
        }
    }
    
    required init(headerViewHidden: Bool) {
        super.init(frame: CGRect.zero)
        hideHeaderView = headerViewHidden
        self.initialSetup()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialSetup()
    }
    
    func initialSetup() {
        backgroundColor = UIColor.white
        
        contentView = UIView()
        addSubview(contentView)
        
        headerView = TKBaseHeaderView()
        addSubview(headerView)
        
        setBaseConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: layout
    
    func setBaseConstraints() {
        let views: [String: UIView] = ["content": contentView, "header": headerView]
        let metrics: [String: Double]  = ["headerHeight": hideHeaderView == true ? 0.0 : 64.0]
        
        for view in views.values {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[header]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[content]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[header(headerHeight)]-0-[content]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: metrics, views: views))
    }
    
}
