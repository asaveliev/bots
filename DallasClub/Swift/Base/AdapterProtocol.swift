//
//  AdapterProtocol.swift
//  Unistroy
//
//  Created by Robot Dream on 28.04.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

protocol AdapterProtocol: class {
    var adapter: TKBaseAdapter! { get set }

}
