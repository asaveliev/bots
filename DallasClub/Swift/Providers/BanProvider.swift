//
//  BanProvider.swift
//  DallasClub
//
//  Created by Anton Savelev on 18.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class BanProvider: NSObject {
    
    private let warningsCountKey = "WarningsCountKey"
    
    func checkBan(completion:@escaping (_ banned: Bool) -> Void) {
        let urlString = "\(DCApiRoot)user"
        DCNetworkCore.sharedInstance()?.updateAuthHeader()
        DCNetworkCore.sharedInstance().sessionManager.get(urlString, parameters: nil, progress: nil, success: { _, response in
            guard
                let response = response as? [String: Any],
                let user = response["user"] as? [String: Any],
                let banned = user["banned"] as? Bool
            else { return }
            completion(banned)
        }) { _, _ in
            completion(false)
        }
    }
    
    func warnUser(completion:@escaping (_ warningCount: Int) -> Void) {
        let urlString = "\(DCApiRoot)warn"
        DCNetworkCore.sharedInstance().sessionManager.post(urlString, parameters: nil, progress: nil, success: { _, _ in
            let warningsCount = UserDefaults.standard.integer(forKey: self.warningsCountKey)
            UserDefaults.standard.set(warningsCount + 1, forKey: self.warningsCountKey)
            UserDefaults.standard.synchronize()
            completion(warningsCount + 1)
        }) { _, _ in
            let warningsCount = UserDefaults.standard.integer(forKey: self.warningsCountKey)
            completion(warningsCount)
        }
    }
}
