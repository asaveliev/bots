//
//  ModelProvider.swift
//  DallasClubProd
//
//  Created by Anton on 21.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

import AFNetworking

struct ModelProvider<T: Decodable> {
    let sessionManager = DCNetworkCore.sharedInstance().sessionManager
    
    func getModel(from endpoint: String, completion: @escaping (_ model: T?, _ error: Error?) -> Void) {
        sessionManager?.get(DCApiRoot.appending(endpoint), parameters: nil, progress: nil, success: { _, response in
            guard
                let dict = (response as? [String: Any]),
                let data = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            else { return }
            
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let model = try? decoder.decode(T.self, from: data)
            completion(model, nil)
        }, failure: { _, error in
            completion(nil, error)
        })
    }
}
