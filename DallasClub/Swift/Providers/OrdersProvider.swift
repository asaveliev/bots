//
//  OrdersProvider.swift
//  DallasClub
//
//  Created by Anton Savelev on 22.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class OrdersProvider {
    func generateOrderToken(forGoodId id: String, completion:@escaping (_ token: String?) -> Void) {
        DCNetworkCore.sharedInstance().sessionManager.post(DCApiRoot.appending("orders"), parameters: ["good_id": id], progress: nil, success: { (_, response) in
            guard let token = (response as? [String: Any])?["token"] as? String else {
                completion(nil)
                return
            }
            completion(token)
        }) { (_, _) in
            completion(nil)
        }
    }
}
