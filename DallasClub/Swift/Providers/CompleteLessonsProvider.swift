//
//  CompleteLessonsProvider.swift
//  DallasClub
//
//  Created by Anton Savelev on 02.08.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class CompleteLessonsProvider {
    func markStepAsComplete(byId id: String) {
        let urlString = "\(DCApiRoot)steps/\(id)/complete"
        _ = DCNetworkCore.sharedInstance().sessionManager.post(urlString, parameters: nil, progress: nil, success: nil, failure: nil)
    }
}
