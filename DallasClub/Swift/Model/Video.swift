//
//  Video.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class Video: EVBaseManagedObject {
    @NSManaged public var title: String?
    @NSManaged public var duration: NSNumber?
    @NSManaged public var imageUrl: String?
    @NSManaged public var url: String?
    @NSManaged public var purchase: Purchase?
}
