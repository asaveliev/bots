//
//  BaseManagedObject.swift
//  Unistroy
//
//  Created by Anton Savelev on 10.05.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import Foundation
import CoreData
import EVReflection

class EVBaseManagedObject: EVManagedObject {
    @NSManaged public var id: String
    
    class var apiEndpoint: String {
        return ""
    }

    class var entityName: String {
        return String(describing: self)
    }

    class var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "id", ascending: true)]
    }

    class var sortedFetchRequest: NSFetchRequest<NSFetchRequestResult> {
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        request.sortDescriptors = defaultSortDescriptors
        request.predicate = NSPredicate(value: true)
        return request
    }

    class func sortedFetchRequestWithPredicateFormat(_ format: String, _ args: CVarArg...) -> NSFetchRequest<NSFetchRequestResult> {
        let request = sortedFetchRequest
        request.predicate = withVaList(args) { NSPredicate(format: format, arguments: $0) }
        return request
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if value as? String == "" {
            return
        }
        
        super.setValue(value, forKey: key)
    }
    
    override func setValue(_ value: Any!, forUndefinedKey key: String) {
        if let mapping = propertyMapping().filter({$0.keyInObject == key}).first {
            if let key = mapping.keyInResource {
                setValue(value, forKey: key)
                return
            }
            return
        }
        
        if key == key.underscoreToCamelCase {
            return
        }
        
        if value as? String == "" {
            return
        }
        
        setValue(value, forKey: key.underscoreToCamelCase)
    }
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                key: "id"
                , decodeConverter: { self.id = "\($0!)" }
                , encodeConverter: { Int(self.id) }
            )
        ]
    }
}

extension NSManagedObjectContext {
    func insertObject<A: EVBaseManagedObject>() -> A  {
        let x = NSEntityDescription.insertNewObject(forEntityName: A.entityName, into: self)
        guard let obj = x as? A else {
            fatalError("Entity \(A.entityName) does not correspond to \(A.self)")
        }
        return obj
    }

    func getById<A: EVBaseManagedObject>(_ id: String) -> A? {
        let result = try? self.fetch(A.sortedFetchRequestWithPredicateFormat("id = %@", id)) as! [A]
        return result?.first
    }
    
    func getAllObjects<A: EVBaseManagedObject>() -> [A]? {
        let result = try? self.fetch(A.sortedFetchRequest) as! [A]
        return result
    }


    func getOrCreateById<A: EVBaseManagedObject>(_ id: String) -> A {
        let result: A? = getById(id)
        if result != nil {
            return result!
        }
        let post: A = insertObject()
        post.id = id
        return post
    }

    func lastUpdatedObject<A: EVBaseManagedObject>() -> A? {
        let fetchRequest = A.sortedFetchRequest
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "updatedDate", ascending: false)]
        fetchRequest.fetchLimit = 1
        guard let results = try? self.fetch(fetchRequest) else { return nil }
        return results.first as? A
    }
}
