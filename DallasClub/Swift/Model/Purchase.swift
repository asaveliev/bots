//
//  Purchase.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation
import  EVReflection

class Purchase: EVBaseManagedObject {
    @NSManaged public var fullPrice: NSNumber?
    @NSManaged public var imageUrl: String?
    @NSManaged public var price: NSNumber?
    @NSManaged public var title: String?
    @NSManaged public var videosCount: NSNumber?
    @NSManaged public var videos: NSSet?
    @NSManaged public var lessons: NSSet?
    @NSManaged public var color: String
    
    override class var apiEndpoint: String {
        return "purchases/"
    }
    
    private func videos(fromValue: Any?) -> NSSet? {
        guard
            let array = fromValue as? [[String: Any]],
            let context = DCPersistentStack.sharedInstance().backgroundContext
            else {
                return nil
        }
        
        let existing: [Video]? = context.getAllObjects()?.filter { $0.purchase == self }
        if existing != nil {
            existing!.forEach {
                context.delete($0)
            }
        }
        
        let managedObjects = array.flatMap { item -> Video in
            let video: Video = context.insertObject()
            EVReflection.setPropertiesfromDictionary(item as NSDictionary, anyObject: video)
            return video
        }
        
        return NSSet(array: managedObjects)
    }
    
    private func lessons(fromValue: Any?) -> NSSet? {
        guard
            let array = fromValue as? [[String: Any]],
            let context = DCPersistentStack.sharedInstance().backgroundContext
            else {
                return nil
        }
        
        let existing: [Lesson]? = context.getAllObjects()?.filter { $0.purchase == self }
        if existing != nil {
            existing!.forEach {
                context.delete($0)
            }
        }
        
        let managedObjects = array.enumerated().flatMap { index, item -> Lesson in
            let lesson: Lesson = context.insertObject()
            lesson.ordering = NSNumber(value: index)
            EVReflection.setPropertiesfromDictionary(item as NSDictionary, anyObject: lesson)
            return lesson
        }
        
        return NSSet(array: managedObjects)
    }
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                key: "id"
                , decodeConverter: { self.id = "\($0!)" }
                , encodeConverter: { Int(self.id) }
            ),
            (
                key: "videos"
                , decodeConverter: { self.videos = self.videos(fromValue: $0) }
                , encodeConverter: { return nil }
            ),
            (
                key: "lessons"
                , decodeConverter: { self.lessons = self.lessons(fromValue: $0) }
                , encodeConverter: { return nil }
            )
        ]
    }
}
