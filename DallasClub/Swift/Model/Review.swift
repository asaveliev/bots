//
//  Review.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class Review: EVBaseManagedObject {
    @NSManaged public var url: String?
    @NSManaged public var good: Good?
}
