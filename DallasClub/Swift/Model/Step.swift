//
//  Steps.swift
//  DallasClub
//
//  Created by Anton Savelev on 31.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class Step: EVBaseManagedObject {
    @NSManaged var imageUrl: String?
    @NSManaged var audioUrl: String?
    @NSManaged var thumbnailUrl: String?
    @NSManaged var videoUrl: String?
    @NSManaged var text: String?
    @NSManaged var lesson: Lesson?
    @NSManaged var completed: NSNumber?
    @NSManaged var ordering: NSNumber?
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                key: "id"
                , decodeConverter: { self.id = "\($0!)" }
                , encodeConverter: { Int(self.id) }
            )
        ]
    }
}
