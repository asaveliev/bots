//
//  Step.swift
//  DallasClub
//
//  Created by Anton Savelev on 31.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation
import EVReflection

class Lesson: EVBaseManagedObject {
    @NSManaged public var title: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var purchase: Purchase?
    @NSManaged public var steps: NSSet?
    @NSManaged public var itemsCompleted: NSNumber?
    @NSManaged public var ordering: NSNumber?
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                key: "id"
                , decodeConverter: { self.id = "\($0!)" }
                , encodeConverter: { Int(self.id) }
            ),
            (
                key: "steps"
                , decodeConverter: { self.steps = self.steps(fromValue: $0) }
                , encodeConverter: { return nil }
            )
        ]
    }
    
    private func steps(fromValue: Any?) -> NSSet? {
        guard
            let array = fromValue as? [[String: Any]],
            let context = DCPersistentStack.sharedInstance().backgroundContext
        else {
            return nil
        }
        
        let existing: [Step]? = context.getAllObjects()?.filter { $0.lesson == self }
        if existing != nil {
            existing!.forEach {
                context.delete($0)
            }
        }
        
        let managedObjects = array.enumerated().flatMap { index, item -> Step in
            let step: Step = context.insertObject()
            step.ordering = NSNumber(value: index)
            EVReflection.setPropertiesfromDictionary(item as NSDictionary, anyObject: step)
            return step
        }
        
        return NSSet(array: managedObjects)
    }
}
