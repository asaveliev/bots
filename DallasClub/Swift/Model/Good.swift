//
//  Good+CoreDataClass.swift
//  
//
//  Created by Anton Savelev on 08.07.17.
//
//

import Foundation
import CoreData
import EVReflection

@objc(Good)
class Good: EVBaseManagedObject {
    @NSManaged public var fullPrice: NSNumber?
    @NSManaged public var goodDescription: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var price: NSNumber?
    @NSManaged public var quantity: NSNumber?
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var videosCount: NSNumber?
    @NSManaged public var headline: String?
    @NSManaged public var support: NSNumber?
    @NSManaged public var bot: NSNumber?
    @NSManaged public var matches_a_day: String?
    @NSManaged public var patency: String?
    @NSManaged public var duration: String?
    @NSManaged public var faq: NSSet?
    @NSManaged public var reviews: NSSet?
    @NSManaged public var purchased: NSNumber?
    @NSManaged public var lessonsCount: String?
    
    override class var apiEndpoint: String {
        return "shop/"
    }
    
    override public func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
        return [
            (keyInObject: "description", keyInResource: "goodDescription"),
            (keyInObject: "good_type", keyInResource: "type")
        ]
    }
    
    override func propertyConverters() -> [(key: String, decodeConverter: ((Any?) -> ()), encodeConverter: (() -> Any?))] {
        return [
            (
                key: "id"
                , decodeConverter: { self.id = "\($0!)" }
                , encodeConverter: { Int(self.id) }
            ),
            (
                key: "reviews"
                , decodeConverter: { self.reviews = self.reviews(fromValue: $0) }
                , encodeConverter: { return nil }
            ),
            (
                key: "faq"
                , decodeConverter: { self.faq = self.faq(fromValue: $0) }
                , encodeConverter: { return nil }
            )
        ]
    }
    
    func faq(fromValue: Any?) -> NSSet? {
        guard
            let array = fromValue as? [[String: String]],
            let context = DCPersistentStack.sharedInstance().backgroundContext
            else {
                return nil
        }
        
        let existing: [Faq]? = context.getAllObjects()
        if existing != nil {
            existing!.forEach {
                context.delete($0)
            }
        }
        
        let managedObjects = array.flatMap { item -> Faq in
            let faq: Faq = context.insertObject()
            EVReflection.setPropertiesfromDictionary(item as NSDictionary, anyObject: faq)
            return faq
        }
        
        return NSSet(array: managedObjects)
    }
    
    func reviews(fromValue: Any?) -> NSSet? {
        guard
            let array = fromValue as? [String],
            let context = DCPersistentStack.sharedInstance().backgroundContext
        else {
            return nil
        }
        
        let existing: [Review]? = context.getAllObjects()
        if existing != nil {
            existing!.forEach {
                context.delete($0)
            }
        }
        
        let managedReviews = array.flatMap { url -> Review in
            let review: Review = context.insertObject()
            review.url = url
            return review
        }
        
        return NSSet(array: managedReviews)
    }
}
