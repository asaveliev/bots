
//
//  Faq.swift
//  DallasClub
//
//  Created by Anton Savelev on 15.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class Faq: EVBaseManagedObject {
    @NSManaged public var question: String?
    @NSManaged public var answer: String?
    @NSManaged public var good: Good?
}
