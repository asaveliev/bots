//
//  String+CamelCase.swift
//  DallasClub
//
//  Created by Anton Savelev on 09.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

extension String {
    
    var underscoreToCamelCase: String {
        let items = components(separatedBy: "_")
        var camelCase = ""
        items.enumerated().forEach {
            camelCase += 0 == $0 ? $1 : $1.capitalized
        }
        return camelCase
    }
}
