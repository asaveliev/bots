//
//  UITableViewCell+TKCell.swift
//  Orthodont
//
//  Created by Anton Savelev on 14.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

extension UITableViewCell {
    class var tkReuseIdentifier: String {
        return String(describing: self)
    }
}
