//
//  UIButton+SelectedBackground.swift
//  Unistroy-new
//
//  Created by Anton Savelev on 17.07.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import Foundation
import ObjectiveC

extension UIButton {
    static private var selectedColorAssociation: UInt8 = 0
    static private var oldBackgroundAssociation: UInt8 = 1
    var selectedBackgroundColor: UIColor? {
        get { return objc_getAssociatedObject(self, &UIButton.selectedColorAssociation) as? UIColor }
        set { objc_setAssociatedObject(self, &UIButton.selectedColorAssociation, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN) }
    }

    private var oldBackgroundColor: UIColor? {
        get { return objc_getAssociatedObject(self, &UIButton.oldBackgroundAssociation) as? UIColor }
        set { objc_setAssociatedObject(self, &UIButton.oldBackgroundAssociation, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN) }
    }

    open override var isSelected: Bool {
        didSet {
            guard let color = selectedBackgroundColor else {
                return
            }

            if isSelected && color != backgroundColor {
                if backgroundColor != nil {
                    oldBackgroundColor = backgroundColor
                }
                backgroundColor = color
            } else if !isSelected {
                if oldBackgroundColor != nil {
                    backgroundColor = oldBackgroundColor
                }
                titleLabel?.textColor = titleColor(for: .normal)
            }
        }
    }
}
