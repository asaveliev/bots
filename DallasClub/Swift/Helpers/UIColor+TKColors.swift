//
//  UIColor+TKColors.swift
//  Orthodont
//
//  Created by Anton Savelev on 13.03.17.
//  Copyright © 2017 Technokratos. All rights reserved.
//

import UIKit

extension UIColor {
    static func tkColorFromHex(_ rgbValue: UInt) -> UIColor {
        return UIColor(
            red: ((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0,
            green: ((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0,
            blue: ((CGFloat)(rgbValue & 0xFF))/255.0,
            alpha: 1
        )
    }
    
    static func hexStringToUIColor(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class var tkDefaultBackgroundColor: UIColor {
        return tkColorFromHex(0x36aa25)
    }

    class var tkDarkBackgroundColor: UIColor {
        return tkColorFromHex(0x007534)
    }

    class var tkLightGrayColor: UIColor {
        return tkColorFromHex(0xD8D8D8)
    }

    class var tkSeparatorColor: UIColor {
        return tkColorFromHex(0xcccccc)
    }

    class var tkTextPlaceholderColor: UIColor {
        return tkColorFromHex(0x999999)
    }

    class var tkInspectionNotFreeColor: UIColor {
        return tkColorFromHex(0xffcc32)
    }

    class var tkInspectionFreeColor: UIColor {
        return tkColorFromHex(0xadd5e8)
    }

    class var tkInspectionReservedColor: UIColor {
        return tkColorFromHex(0x22aa4a)
    }

}
