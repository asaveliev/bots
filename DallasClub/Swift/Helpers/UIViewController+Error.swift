//
//  UIViewController+Error.swift
//  DallasClubProd
//
//  Created by Anton on 22.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

import UIKit

extension UIViewController {
    func showTryLater() {
        let alertController = UIAlertController(title: "Ошибка", message: "Попробуйте позже", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
