//
//  ManagedObjectsImporter.swift
//  DallasClub
//
//  Created by Anton Savelev on 08.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation
import EVReflection

let bannedNotification = NSNotification.Name("DCBannedNotification")

class ManagedObjectsImporter<T: EVBaseManagedObject> {
    
    func importDetails(forObjectWithId id: String, completion: @escaping ((Bool) -> Void)) {
        guard let context = DCPersistentStack.sharedInstance().backgroundContext else {
            completion(false)
            return
        }
        
        context.perform {
            DCNetworkCore.sharedInstance().sessionManager.get(DCApiRoot.appending(T.apiEndpoint.appending(id)), parameters: nil, progress: nil, success: { _, response in
                let storedItem: T = context.getOrCreateById(id)
                guard let data = response as? [String: Any] else {
                    completion(false)
                    return
                }
                
                if data["banned"] as? Bool == true {
                    NotificationCenter.default.post(name: bannedNotification, object: nil)
                    return
                }
                
                EVReflection.setPropertiesfromDictionary(data as NSDictionary, anyObject: storedItem)
                do {
                    try context.save()
                } catch {
                    print(error)
                }
                completion(true)
            }, failure: { _, error in
                completion(false)
                print(error)
            })
        }
    }
    
    func sync() {
        guard let context = DCPersistentStack.sharedInstance().backgroundContext else {
            return
        }
        context.perform {
            let existing: [T]? = context.getAllObjects()
            DCNetworkCore.sharedInstance().sessionManager.get(DCApiRoot.appending(T.apiEndpoint), parameters: nil, progress: nil, success: { _, response in
                
                if let data = response as? [String: Any] {
                    if data["banned"] as? Bool == true {
                        NotificationCenter.default.post(name: bannedNotification, object: nil)
                        return
                    }
                }

                guard let array = response as? [[String: Any]] else {
                    return
                }
                let objects = array.flatMap { item -> T? in
                    guard let id = item["id"] as? Int else {
                        return nil
                    }
                    
                    let storedItem: T = context.getOrCreateById("\(id)")
                    EVReflection.setPropertiesfromDictionary(item as NSDictionary, anyObject: storedItem)
                    return storedItem
                }
                
                if existing == nil {
                    return
                }
                
                let toDelete = existing!.filter {
                    objects.map { $0.id }.contains($0.id) == false
                }
                
                toDelete.forEach {
                    context.delete($0)
                }
                
                do {
                    try context.save()
                } catch {
                    print(error)
                }
            }, failure: { _, error in
                print(error)
            })
            
        }
    }
    
}
