//
//  DCAudioCell.h
//  DallasClub
//
//  Created by Anton Savelev on 10.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "TKNewRoundedCardCell.h"
#import "DCSlider.h"

@class DCAudioCell;

@protocol DCAudioCellProtocol <NSObject>

- (void)audioCellDidPlay:(DCAudioCell *)cell;
- (void)audioCellDidPause:(DCAudioCell *)cell;
- (void)audioCellSliderChanged:(DCAudioCell *)cell toValue:(CGFloat)value;

@end

@interface DCAudioCell : TKNewRoundedCardCell

@property (nonatomic, strong)   NSString        *audioUrl;
@property (strong, nonatomic)   UIImageView     *avatarImageView;
@property (strong, nonatomic)   UILabel         *timeLabel;
@property (strong, nonatomic)   UILabel         *nameLabel;

@property (nonatomic, readonly) DCSlider    *slider;
@property (nonatomic, readonly) UIButton    *audioButton;

@property (nonatomic, weak) id<DCAudioCellProtocol> delegate;

@end
