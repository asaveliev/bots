//
//  DCPaginableOperation.h
//  DallasClub
//
//  Created by Anton Savelev on 18.09.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "ASConcurrentOperation.h"

@interface DCPaginableOperation : ASConcurrentOperation

@property (nonatomic, strong) NSNumber *page;

- (instancetype)initWithPageNumber:(NSNumber *)page;

@end
