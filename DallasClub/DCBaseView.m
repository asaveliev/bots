//
//  DCBaseView.m
//  DallasClub
//
//  Created by Anton on 20/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBaseView.h"

@interface DCBaseView()

@property (nonatomic, assign) CGFloat fadeOffset;

@end

@implementation DCBaseView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor bgColor];
        
        _contentView = [UIView new];
        self.contentView.backgroundColor = UIColorFromHex(0x161616);
        [self addSubview:self.contentView];
        
        _headerView = [self.headerViewClass new];
        self.headerView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.headerView];
        
        if (self.headerView) {
            [self setBaseConstraints];
        } else {
            [self setBaseWithoutHeaderConstraints];
        }
    }
    
    return self;
}

- (Class)headerViewClass {
    return [DCBaseHeaderView class];
}

- (void)setBaseConstraints {
    NSDictionary *views = @{
                            @"header": self.headerView,
                            @"content": self.contentView
                            };
    NSDictionary *metrics = @{
                              @"headerHeight": @([UIApplication sharedApplication].statusBarFrame.size.height + 44.0)
                              };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[header]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[content]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[header(headerHeight)]-0-[content]-0-|" options:0 metrics:metrics views:views]];
}

- (void)setBaseWithoutHeaderConstraints {
    NSDictionary *views = @{
                            @"content": self.contentView
                            };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[content]-0-|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[content]-0-|" options:0 metrics:nil views:views]];
}


- (UILabel *)headerTitleLabel {
    return self.headerView.headerTitleLabel;
}

- (UIButton *)rightButton {
    return self.headerView.rightButton;
}

- (UIButton *)leftButton {
    return self.headerView.leftButton;
}

#pragma mark - TableViewFade

- (void)bindTableView:(UITableView *)table forFadeWithOffset:(CGFloat)offset {
    self.fadeOffset = offset;
    [table addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"contentOffset"]) {
        self.headerView.alpha = 0.0;
        CGPoint offset = [[change valueForKey:NSKeyValueChangeNewKey] CGPointValue];
        
        if (offset.y >= -self.fadeOffset + 2.0) {
            CGFloat ratio = (offset.y + self.fadeOffset) / self.fadeOffset;
            self.headerView.alpha = fabs(ratio);
            [self bringSubviewToFront:self.headerView];
            self.headerView.hidden = NO;
        } else {
            self.headerView.hidden = YES;
            [self sendSubviewToBack:self.headerView];
        }
    }
}


@end
