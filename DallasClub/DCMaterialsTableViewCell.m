//
//  DCMaterialsTableViewCell.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMaterialsTableViewCell.h"

@implementation DCMaterialsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _thumbImageView = [UIImageView new];
        self.thumbImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.thumbImageView.clipsToBounds = YES;
        [self.contentView addSubview:self.thumbImageView];
        
        _title = [UILabel new];
        self.title.numberOfLines = 0;
        self.title.lineBreakMode = NSLineBreakByWordWrapping;
        self.title.font = [UIFont systemFontOfSize:15.0 weight:UIFontWeightSemibold];
        [self.contentView addSubview:self.title];
        
        _subtitle = [UILabel new];
        self.subtitle.font = [UIFont systemFontOfSize:12.0];
        self.subtitle.textColor = UIColorFromHex(0x8A8A8F);
        [self.contentView addSubview:self.subtitle];
        
        _separatorView = [UIView new];
        self.separatorView.backgroundColor = UIColorFromHex(0xd6d6d6);
        [self.contentView addSubview:self.separatorView];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"title": self.title,
        @"subtitle": self.subtitle,
        @"image": self.thumbImageView,
        @"sep": self.separatorView
    };
    
    NSDictionary *metrics = @{
        @"imageHeght": @75,
        @"imageWidth": @100
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-16-[image(==imageWidth)]-16-[title]-16-|" options:NSLayoutFormatAlignAllTop metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[image(==imageHeght)]-(>=16)-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-16-[title]-6-[subtitle]-(>=16)-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sep(1)]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[sep]-0-|" options:0 metrics:metrics views:views]];
}

@end
