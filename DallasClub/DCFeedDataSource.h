//
//  DCFeedDataSource.h
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFetchedDataSource.h"

@class DCFeedDataSource;
@class DCCappersDataSource;

@protocol DCFeedDataSourceDelegate <DCDataSourceDelegate>

- (void)dataSource:(DCFeedDataSource *)dataSource didSelectLink:(NSURL *)link;
- (void)dataSource:(DCFeedDataSource *)dataSource didSelectPhoto:(NSString *)link;
- (void)dataSourceDidSelectInfo;

@end

@interface DCFeedDataSource : DCFetchedDataSource
    
@property (weak, nonatomic) id<DCFeedDataSourceDelegate>    delegate;
@property (strong, readonly) DCCappersDataSource            *cappersDataSource;

- (instancetype)initWithTableView:(UITableView *)tableView postId:(NSNumber *)uid;
- (instancetype)initWithTableView:(UITableView *)tableView postId:(NSNumber *)uid capperName:(NSString *)name;

@end
