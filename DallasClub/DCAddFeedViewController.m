//
//  DCAddFeedViewController.m
//  DallasClub
//
//  Created by Anton on 31/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAddFeedViewController.h"
#import "DCRecordViewController.h"
#import "DCAddFeedView.h"
#import "DCNetworkCore.h"
#import "DCImporter.h"
#import "Bet.h"
#import "DCPersistentStack.h"

#import "DCFeedPolicy.h"
#import "DCCreateFeedPolicy.h"
#import "DCEditFeedPolicy.h"
#import "DCRemoveFeedPolicy.h"
#import "NSDate+Utilities.h"
#import "UIImage+Rotation.h"

#import "RMUniversalAlert.h"
#import "SVProgressHUD.h"

@interface DCAddFeedViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, readonly) DCAddFeedView   *mainView;
@property (nonatomic, readonly) DCFeedPolicy    *policy;

@property (nonatomic, strong)   UIImagePickerController *imagePicker;

@end

@implementation DCAddFeedViewController

- (instancetype)initWithPolicy:(DCFeedPolicy *)policy {
    if (self = [super init]) {
        _policy = policy;
    }
    return self;
}

- (void)loadView {
    self.view = _mainView = [DCAddFeedView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mainView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.rightButton addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    
    self.mainView.dateTextField.text = [self.policy.date stringWithDateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterShortStyle];
    self.mainView.dateTextField.selectedDate = self.policy.date;
    self.mainView.numberTextField.text = self.policy.betNumber;
    
    self.mainView.textView.text = self.policy.text;
    
    [self configureFields];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoTapped)];
    [self.mainView.addPhotoView addGestureRecognizer:tap];
    [self.mainView.recordButton addTarget:self action:@selector(recordAction) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.recordButton.hidden = ![self.policy isKindOfClass:[DCCreateFeedPolicy class]];
    
}

- (void)configureFields {
    NSMutableArray *titles = [NSMutableArray new];
    [titles addObject:@"0"];
    NSArray *bets = [[Bet getAllInContext:[DCPersistentStack sharedInstance].context] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"number" ascending:YES]]];
    for (Bet *bet in bets) {
        [titles addObject:[NSString stringWithFormat:@"%ld", (long)bet.number.integerValue]];
    }
    self.mainView.numberTextField.titles = titles;
    
    self.mainView.addPhotoView.image = self.policy.image;
}

#pragma mark - Actions

- (void)switchAction:(UISwitch *)sender {
    self.policy.isTgHidden = sender.isOn;
}

- (void)recordAction {
    DCRecordViewController *vc = [DCRecordViewController new];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    vc.completion = ^void(NSData *ouput) {
        if ([self.policy isKindOfClass:[DCCreateFeedPolicy class]]) {
            DCCreateFeedPolicy *policy = (DCCreateFeedPolicy *)self.policy;
            policy.audioData = ouput;
            [self done];
        };
    };
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)done {
    self.policy.text = self.mainView.textView.text;
    self.policy.betNumber = [self.mainView.numberTextField.text isEqualToString:@"0"] ? nil : self.mainView.numberTextField.text;
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateStyle = NSDateFormatterMediumStyle;
    formatter.timeStyle = NSDateFormatterShortStyle;
    self.policy.date = [formatter dateFromString:self.mainView.dateTextField.text];
    
    [SVProgressHUD show];
    [self.policy performActionWithCompletion:^(NSError *error) {
        [SVProgressHUD dismiss];
        
        if (error) {
            [RMUniversalAlert showAlertInViewController:self withTitle:@"Ошибка!" message:@"Попробуйте позже" cancelButtonTitle:@"Ок" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
                
            }];
            
            return;
        }
        
        [[DCImporter importer] updateFeed];
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)photoTapped {
    _imagePicker = [UIImagePickerController new];
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    self.imagePicker.allowsEditing = NO;
    self.imagePicker.delegate = self;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    self.policy.image = [image removeRotationForImage:image];
    self.mainView.addPhotoView.image = image;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
