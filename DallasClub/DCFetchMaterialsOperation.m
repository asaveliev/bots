//
//  DCFetchMaterialsOperation.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCFetchMaterialsOperation.h"
#import "Material.h"
#import "DCNetworkCore.h"

@implementation DCFetchMaterialsOperation

- (void)main {
    [[DCNetworkCore sharedInstance].materialsProvider fetchMaterialsWithCompletion:^(NSArray *items, NSError *error) {
        if (error) {
            [self completeOperationWithError:error];
            return;
        }
        
        NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
        [context performBlock:^{
            NSMutableArray *existing = [[Material getAllInContext:context] mutableCopy];
            for (NSDictionary *data in items) {
                Material *item = [Material getByField:@"uid" equalsTo:data[@"id"] fromContext:context];
                if (!item) {
                    item = [Material createInContext:context];
                }
                [item setDictionaryRepresentation:data];
                
                [existing removeObject:item];
            }
            
            for (Material *item in existing) {
                [context deleteObject:item];
            }
            
            NSError *err = nil;
            [context save:&err];
            [self completeOperationWithError:err];
        }];
    }];
}

@end
