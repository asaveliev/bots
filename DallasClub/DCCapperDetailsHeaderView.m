//
//  DCCapperDetailsHeaderView.m
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCCapperDetailsHeaderView.h"

@implementation DCCapperDetailsHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _avatarImage = [UIImageView new];
        self.avatarImage.clipsToBounds = YES;
        self.avatarImage.contentMode = UIViewContentModeScaleAspectFill;
        self.avatarImage.layer.cornerRadius = 50.0;
        self.avatarImage.layer.borderWidth = 4.0;
        self.avatarImage.layer.borderColor = [UIColor whiteColor].CGColor;
        [self addSubview: self.avatarImage];
        
        _titleLabel = [UILabel new];
        self.titleLabel.font = [UIFont systemFontOfSize:24.0 weight:UIFontWeightHeavy];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        _descriptionLabel = [UILabel new];
        self.descriptionLabel.textAlignment = NSTextAlignmentCenter;
        self.descriptionLabel.textColor = UIColorFromHex(0x929292);
        self.descriptionLabel.font = [UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium];
        [self addSubview:self.descriptionLabel];
        
        self.backgroundColor = [UIColor whiteColor];
        
        _segmentedControl = [HMSegmentedControl new];
        self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromHex(0x929292), NSFontAttributeName: [UIFont systemFontOfSize:12.0 weight:UIFontWeightSemibold]};
        self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName: UIColorFromHex(0x007AFF), NSFontAttributeName: [UIFont systemFontOfSize:12.0 weight:UIFontWeightSemibold]};
        self.segmentedControl.sectionTitles = @[[@"Прогнозы" uppercaseString], [@"Статистика" uppercaseString]];
        self.segmentedControl.selectionIndicatorHeight = 2.0;
        self.segmentedControl.selectionIndicatorColor = UIColorFromHex(0x007AFF);
        self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        [self addSubview:self.segmentedControl];
        
        [self setConstraints];
        self.clipsToBounds = NO;
    }
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"title": self.titleLabel,
        @"descr": self.descriptionLabel,
        @"ava": self.avatarImage,
        @"segmented": self.segmentedControl
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:@[[NSLayoutConstraint constraintWithItem:self.avatarImage attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[ava(100)]" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-50)-[ava(100)]" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-60-[title]-14-[descr]" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[title]-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[segmented]-0-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[segmented(40)]-0-|" options:0 metrics:nil views:views]];
}

@end
