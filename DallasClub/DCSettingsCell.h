//
//  DCSettingsCell.h
//  DallasClub
//
//  Created by Anton on 19/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KILabel/KILabel.h>

@interface DCSettingsCell : UITableViewCell

@property (nonatomic, readonly) KILabel     *titleLabel;
@property (nonatomic, readonly) UILabel     *subtitleLabel;
@property (nonatomic, readonly) UISwitch    *switchControl;

@end
