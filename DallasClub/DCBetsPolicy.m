//
//  DCBetsPolicy.m
//  DallasClub
//
//  Created by Anton on 08/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBetsPolicy.h"
#import "Bet.h"
#import "DCPersistentStack.h"
#import "NSDateFormatter+ISO8601.h"

@interface DCBetsPolicy ()

@property (strong, nonatomic) Bet *bet;

@end

@implementation DCBetsPolicy

- (instancetype)initWithBetId:(NSNumber *)uid {
    if (self = [super init]) {
        _bet = [Bet getByField:@"uid" equalsTo:uid fromContext:[DCPersistentStack sharedInstance].context];
        _statusString = self.bet.readableStatus;
        _percent = [NSString stringWithFormat:@"%.0f%%", self.bet.amout.doubleValue];
        _numberString = [NSString stringWithFormat:@"%@", self.bet.number];
        _state = self.bet.state.integerValue;
        _coefficient = [NSString stringWithFormat:@"%.2f", self.bet.coefficient.doubleValue];
        _date = self.bet.created;
    }
    
    return self;
}

- (NSNumber *)originalNumber {
    return self.bet.number;
}

- (NSNumber *)betId {
    return self.bet.uid;
}

- (NSString *)status {
    switch (self.state) {
        case DCBetStateSuccess:
            return @"successful";
            break;
        case DCBetStateFailed:
            return @"failed";
            break;
        case DCBetStateReturned:
            return @"returned";
            break;
        default:
            break;
    }
    
    return nil;
}

- (NSString *)dateString {
    return [NSDateFormatter stringFromDateForISO8601:self.date];
}

#pragma mark - Action

- (void)performActionWithCompletion:(void (^)(NSError *))completion {
    completion(nil);
}

@end
