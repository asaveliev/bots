//
//  Post+CoreDataProperties.m
//  
//
//  Created by Anton Savelev on 04/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Post+CoreDataProperties.h"

@implementation Post (CoreDataProperties)

@dynamic content;
@dynamic created;
@dynamic ordering;
@dynamic uid;
@dynamic imageUrl;
@dynamic audioUrl;
@dynamic bet;
@dynamic updated_at;
@dynamic capperPhotoUrl;
@dynamic capper_name;

@end
