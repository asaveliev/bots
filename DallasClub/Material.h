//
//  Material+CoreDataClass.h
//  
//
//  Created by Anton Savelev on 07.05.17.
//
//

#import "BaseManagedObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface Material : BaseManagedObject<Serializable>

@end

NS_ASSUME_NONNULL_END

#import "Material+CoreDataProperties.h"
