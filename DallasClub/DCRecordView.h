//
//  DCRecordView.h
//  DallasClub
//
//  Created by Anton Savelev on 03.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCRecordView : UIView

@property (nonatomic, readonly) UILabel     *timeLabel;
@property (nonatomic, readonly) UILabel     *stateLabel;

@property (nonatomic, readonly) UIButton    *recordButton;
@property (nonatomic, readonly) UIButton    *closeButton;
@property (nonatomic, readonly) UIButton    *doneButton;

@end
