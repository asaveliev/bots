//
//  DCChatViewController.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCChatViewController.h"
#import "JivoSdk.h"

@interface DCChatViewController ()<JivoDelegate>

@property (nonatomic, strong) UIWebView *chatView;
@property (nonatomic, strong) JivoSdk   *sdk;

@end

@implementation DCChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _chatView = [UIWebView new];
    [self.view addSubview:self.chatView];
    _sdk = [[JivoSdk alloc] initWith:self.chatView :@"ru"];
    self.sdk.delegate = self;
    [self.sdk prepare];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.sdk start];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.sdk stop];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.chatView.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)onEvent:(NSString *)name :(NSString*)data;{
    NSLog(@"event:%@, data:%@", name, data);
    if([[name lowercaseString] isEqualToString:@"url.click"]){
        if([data length] > 2){
            NSString *urlStr = [data substringWithRange:NSMakeRange(1,[data length] - 2)];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
        }
    }
}

@end
