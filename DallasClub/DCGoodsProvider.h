//
//  DCGoodsProvider.h
//  
//
//  Created by Anton Savelev on 03.03.17.
//
//

#import "DCApiProvider.h"

@interface DCGoodsProvider : DCApiProvider

- (void)getGoodsWithCompletion:(void (^)(NSArray *, NSError *))completion;

@end
