//
//  DCImporter.h
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCImporter : NSObject

+ (instancetype)importer;
+ (instancetype)feedImporter;
+ (instancetype)betsImporter;

- (void)syncAll;
- (void)updateBets;
- (void)updateFeed;
- (void)updateFeed:(void (^ _Nullable)())completion;

- (void)loadFeedPage:(NSNumber *)page;
- (void)loadNextFeedPage;
- (void)loadFeedPage:(NSNumber *)page withCapperName:(NSString *)capperName;
- (void)loadNextFeedPageWithCapperName:(NSString *)capperName;
- (void)loadAccountWithCapperName:(NSString *)name;

- (void)loadBetsPage:(NSNumber *)page;
- (void)loadNextBetsPage;
- (void)loadBetsPage:(NSNumber *)page withCapperName:(NSString *)capperName;
- (void)loadNextBetsPageWithCapperName:(NSString *)capperName;


@end
