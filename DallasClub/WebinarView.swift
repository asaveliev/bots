//
//  WebinarView.swift
//  DallasClubProd
//
//  Created by Anton on 19.08.2018.
//  Copyright © 2018 Savelev. All rights reserved.
//

import UIKit
import UITextView_Placeholder
import JMMessageInput

class WebinarMessageView: UIView {
    lazy var messageInput: JMMessageInputBar = {
        let view = JMMessageInputBar()
        view.textView.layer.cornerRadius = 8
        view.textView.placeholder = "Задать вопрос"
        view.textView.placeholderColor = UIColor(red: 151/255, green: 151/255, blue: 151/255, alpha: 1)
        view.textView.font = .systemFont(ofSize: 14)
        view.backgroundColor = .clear
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(messageInput)
        messageInput.snp.makeConstraints { make in
            make.height.equalTo(53)
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 2, left: 4, bottom: 4, right: 2))
        }
        
        backgroundColor = UIColor(red: 195/255, green: 205/255, blue: 212/255, alpha: 0.3)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class WebinarView: UIView {
    lazy var messageInput = WebinarMessageView()
    lazy var textView: UITextView = {
        let view = UITextView()
        view.isEditable = false
        view.isSelectable = false
        view.textContainer.lineFragmentPadding = 0
        view.textContainerInset = .zero
        view.font = .systemFont(ofSize: 16)
        return view
    }()
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20, weight: UIFontWeightBold)
        return label
    }()
    lazy var buttonTitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = .systemFont(ofSize: 16, weight: UIFontWeightBold)
        label.textAlignment = .center
        return label
    }()
    lazy var actionButton: UIButton = {
        let button = UIButton()
        button.clipsToBounds = true
        button.layer.cornerRadius = 8
        button.backgroundColor = UIColor(red: 1, green: 45/255, blue: 85/255, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: UIFontWeightSemibold)
        return button
    }()
    lazy var videoContainerView = UIView()
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 15
        button.clipsToBounds = true
        button.backgroundColor = UIColor(red: 0, green: 122/255, blue: 1, alpha: 1)
        button.setImage(UIImage(named: "arrow"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        return button
    }()
    lazy var overlayView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.0
        return view
    }()
    lazy var successView = SuccessView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(messageInput)
        addSubview(titleLabel)
        addSubview(textView)
        addSubview(buttonTitleLabel)
        addSubview(actionButton)
        addSubview(videoContainerView)
        addSubview(overlayView)
        messageInput.addSubview(sendButton)
        messageInput.sendSubview(toBack: sendButton)
        makeConstraints()
        
        successView.alpha = 0.0
                
        backgroundColor = .white
    }
    
    func makeConstraints() {
        videoContainerView.snp.makeConstraints { make in
            make.top.equalTo(layoutMarginsGuide)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(211)
        }
        
        messageInput.snp.makeConstraints { make in
            make.top.equalTo(videoContainerView.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(messageInput.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview().inset(15)
        }
        
        textView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview().inset(15)
        }
        
        actionButton.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.bottom.equalTo(layoutMarginsGuide).offset(-20)
            make.height.equalTo(50)
            make.top.equalTo(buttonTitleLabel.snp.bottom).offset(15)
        }
        
        buttonTitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(15)
            make.top.equalTo(textView.snp.bottom).offset(15)
            make.height.lessThanOrEqualTo(48)
        }
        
        sendButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-10)
            make.trailing.equalToSuperview().offset(-10)
            make.size.equalTo(30)
        }
        
        overlayView.snp.makeConstraints { make in
            make.top.equalTo(messageInput.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func setSendButtonHidden(_ hidden: Bool, keyboardHeight: CGFloat = 1000) {
        messageInput.messageInput.snp.updateConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 2, left: 4, bottom: 4, right: hidden ? 2 : 46))
        }
        
        setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
            self.overlayView.alpha = hidden ? 0 : 0.9
        }
    }
    
    func showSuccessView() {
        (UIApplication.shared.delegate as? AppDelegate)?.window.addSubview(successView)
        successView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        UIView.animate(withDuration: 0.3) {
            self.successView.alpha = 1
        }
    }
    
    func hideSuccessView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.successView.alpha = 0
        }) { _ in
            self.successView.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
