//
//  DCStatsFetcher.m
//  DallasClub
//
//  Created by Anton Savelev on 17.09.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatsFetcher.h"
#import "DCNetworkCore.h"
#import "NSDateFormatter+ISO8601.h"
#import "NSDate+Utilities.h"
#import "Capper.h"
#import "DCPersistentStack.h"

@interface DCStatsFetcher ()

@property (strong, nonatomic)   NSArray         *fetchedBets;
@property (readonly, nonatomic) NSString        *capperName;

@end

@implementation DCStatsFetcher

- (instancetype)initWithCapperName:(NSString *)name {
    _capperName = name;
    if (self = [super init]) {
        
    }
    
    return self;
}

- (void)fetchDataOfMonth:(NSDate *)month completion:(void (^)(NSError *))completion {
    NSManagedObjectContext *context = [DCPersistentStack sharedInstance].backgroundContext;
    NSNumber *capperId;
    Capper *capper = [Capper getByField:@"name" equalsTo:self.capperName fromContext:context];
    capperId = capper.uid;
    
    [[DCNetworkCore sharedInstance].statsProvider getStatisticsByMonths:month capperId:capperId completion:^(NSDictionary *response, NSError *error) {
        if (error) {
            completion(error);
            return;
        }
        
        self.fetchedBets = response[@"bets"];
        
        NSMutableDictionary *changes = [NSMutableDictionary new];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components: NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:month];
        components.day = 1;
        NSDate *date = [calendar dateFromComponents:components];
        while ([date isSameMonthAsDate:month]) {
            double change = 0.0;
            for (NSDictionary *bet in self.fetchedBets) {
                NSDate *betDate = [NSDateFormatter dateFromStringForISO8601:bet[@"date"]];
                if ([betDate isEqualToDateIgnoringTime:date]) {
                    change += [bet[@"balance_change"] doubleValue];
                }
            }
            
            changes[[NSString stringWithFormat:@"%ld", (long)date.day]] = @(change);
            
            date = [date dateByAddingDays:1];
        }
        self.changeByDays = [changes copy];
        
        completion(nil);
    }];
}

- (void)fetchDataOfYears:(NSArray *)years completion:(void (^)(NSError *))completion {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSMutableArray *monthsToFetch = [NSMutableArray new];
    NSMutableDictionary *changes = [NSMutableDictionary new];
    
    for (NSDate *year in years) {
        NSDateComponents *components = [NSDateComponents new];
        components.year = year.year;
        for (int i = 0; i < 12; i++) {
            components.month = i + 1;
            [monthsToFetch addObject:[calendar dateFromComponents:components]];
        }
    }
    
    for (NSDate *month in monthsToFetch) {
        [self fetchDataOfMonth:month completion:^(NSError *error) {
            if (error) {
                completion(error);
                return;
            }
            
            double change = 0.0;
            for (NSNumber *dayChange in self.changeByDays.allValues) {
                change += dayChange.doubleValue;
            }
            
            @synchronized (changes) {
                changes[month] = @(change);
                if (changes.allKeys.count == monthsToFetch.count) {
                    self.changeByMonths = [changes copy];
                    completion(nil);
                }
            }
        }];
    }
}

- (NSInteger)countOfBetsByDay:(NSDate *)day withState:(DCBetState)state {
    NSInteger count = 0;
    for (NSDictionary *bet in self.fetchedBets) {
        //TODO: make temporary object
        NSDate *betDate = [NSDateFormatter dateFromStringForISO8601:bet[@"created_at"]];
        DCBetState betState;
        NSString *statusString = bet[@"status"];
        if ([statusString isEqualToString:@"successful"]) {
            betState = DCBetStateSuccess;
        } else if ([statusString isEqualToString:@"failed"]) {
            betState = DCBetStateFailed;
        } else {
            betState = DCBetStateReturned;
        }
        if ([betDate isSameMonthAsDate:day] && betState == state) {
            count++;
        }
    }
    
    return count;
}

- (double)monthChange {
    double change = 0.0;
    for (NSNumber *changeValue in self.changeByDays.allValues) {
        if (![changeValue isKindOfClass:[NSNumber class]]) {
            continue;
        }
        change += changeValue.doubleValue;
    }
    
    return change;
}

@end
