//
//  DCStartupView.m
//  DallasClub
//
//  Created by Anton Savelev on 07/09/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStartupView.h"
#import "DCStartupCell.h"

@interface DCStartupView ()

@end

@implementation DCStartupView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.showsVerticalScrollIndicator = NO;
        self.tableView.bounces = NO;
        [self addSubview:self.tableView];
        
        _avatarImageView = [UIImageView new];
        self.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:self.avatarImageView];
        
        _nameLabel = [UILabel new];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.textColor = [UIColor blackColor];
        self.nameLabel.font = [UIFont lightFontOfSize:26.0];
        [self addSubview:self.nameLabel];
        
        _okButton = [UIButton new];
        self.okButton.backgroundColor = [UIColor okButtonColor];
        self.okButton.clipsToBounds = YES;
        self.okButton.titleLabel.font = [UIFont regularFontOfSize:15.0];
        [self addSubview:self.okButton];
        
        self.backgroundColor = [UIColor bgColor];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.okButton.layer.cornerRadius = self.okButton.bounds.size.height / 2.0;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"table": self.tableView,
        @"avatar": self.avatarImageView,
        @"name": self.nameLabel,
        @"ok": self.okButton
    };
    NSDictionary *metrics = @{
        @"avaSize": @150
    };
    
    for (UIView *v in views.allValues) {
        v.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[avatar(==avaSize)]-10-[name]-0-[table]-10-[ok(40)]-10-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[table]-0-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[name]-|" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[avatar(==avaSize)]" options:0 metrics:metrics views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-77-[ok]-77-|" options:0 metrics:nil views:views]];

    NSLayoutConstraint *xConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.avatarImageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[xConstraint]];
}

@end
