//
//  DCMaterialsProvider.h
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCApiProvider.h"

@interface DCMaterialsProvider : DCApiProvider

- (void)fetchMaterialsWithCompletion:(void(^)(NSArray *items, NSError *error))completion;

@end
