//
//  DCMaterialsView.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMaterialsView.h"
#import "DCClassicHeaderView.h"

@implementation DCMaterialsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [UITableView new];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.contentView addSubview:self.tableView];
        
        [self setConstraints];
    }
    
    return self;
}

- (Class)headerViewClass {
    return [DCClassicHeaderView class];
}

- (void)setConstraints {
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[v]-0-|" options:0 metrics:nil views:@{ @"v": self.tableView }]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[v]-49-|" options:NSLayoutFormatAlignAllLeading | NSLayoutFormatAlignAllTrailing metrics:nil views:@{ @"v": self.tableView}]];
}

@end
