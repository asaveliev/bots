//
//  CapperInfoViewController.h
//  DallasClub
//
//  Created by Anton Savelev on 10.06.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Capper;

@interface CapperInfoViewController : UIViewController

- (instancetype)initWithCapperModel:(Capper *)capper;

@end
