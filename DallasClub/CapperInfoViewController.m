//
//  CapperInfoViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 10.06.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "CapperInfoViewController.h"
#import "DCSettingsView.h"
#import "DCInfoCell.h"
#import "DallasClub-Swift.h"

#import "Capper.h"

@interface CapperInfoViewController () <UITableViewDelegate, UITableViewDataSource>

@property (readonly, nonatomic) DCSettingsView *mainView;

@property (readonly, nonatomic) Capper          *capper;

@end

@implementation CapperInfoViewController

- (instancetype)initWithCapperModel:(Capper *)capper {
    if (self = [super initWithNibName:nil bundle:nil]) {
        _capper = capper;
    }
    
    return self;
}

- (void)loadView {
    self.view = _mainView = [DCSettingsView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.mainView.tableView registerClass:[DCInfoCell class] forCellReuseIdentifier:@"Cell"];
    self.mainView.tableView.dataSource = self;
    self.mainView.tableView.delegate = self;
    self.mainView.tableView.contentInset = UIEdgeInsetsMake([UIApplication sharedApplication].statusBarFrame.size.height > 20 ? [UIApplication sharedApplication].statusBarFrame.size.height - 44.0 : [UIApplication sharedApplication].statusBarFrame.size.height - 24.0, 0.0, 0.0, 0.0);
    
    [self.mainView.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
    [self.mainView.leftButton setTitle:@"Назад" forState:UIControlStateNormal];
    [self.mainView.leftButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
    [self.mainView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    self.mainView.headerTitleLabel.text = @"Информация";
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.capper.capper_description heightWithConstrainedWidth:UIScreen.mainScreen.bounds.size.width - 45 font: [UIFont regularFontOfSize:17.0]] + 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DCInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.subtitleLabel.text = @"О СЕБЕ";
    cell.titleLabel.text = self.capper.capper_description;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
}

@end
