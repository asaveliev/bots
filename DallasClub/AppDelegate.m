//
//  AppDelegate.m
//  DallasClub
//
//  Created by Anton on 17/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "AppDelegate.h"
#import "DCTabBarController.h"
#import "DCStartupViewController.h"
#import "DCPersistentStack.h"
#import "DCImporter.h"
#import "DCNetworkCore.h"
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>
#import "SVProgressHUD.h"
#import "DCUserModel.h"
#import "LNNotificationCenter.h"
#import "LNNotificationAppSettings.h"
#import "LNNotification.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Firebase.h>
#import <YandexMobileMetrica/YandexMobileMetrica.h>
#import "DCCapperDetailsViewController.h"
#import <IQKeyboardManagerSwift/IQKeyboardManagerSwift-Swift.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Buglife/Buglife.h>
#import "DCNetworkingConsoleLogger.h"
#import <RMUniversalAlert/RMUniversalAlert.h>

static NSString *const DCFirstLaunchKey = @"DCFirstLaunch";

@interface AppDelegate ()

@end

@implementation AppDelegate
    
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if(!self.restrictRotation) {
       return UIInterfaceOrientationMaskPortrait;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];
    
    [[UITabBar appearance] setBackgroundColor:UIColorFromHex(0x161616)];
    [[UITabBar appearance] setBackgroundImage:[UIImage new]];
    
    DCNetworkingConsoleLogger *consoleLogger = [DCNetworkingConsoleLogger new];
    [[AFNetworkActivityLogger sharedLogger] addLogger:consoleLogger];
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    for (id<AFNetworkActivityLoggerProtocol> logger in [AFNetworkActivityLogger sharedLogger].loggers) {
        logger.level = AFLoggerLevelDebug;
    }
    
    [YMMYandexMetrica activateWithApiKey:@"53d2d81b-7200-44d4-b454-156f0008b058"];
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    UIViewController *rootVC = [DCTabBarController new];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    UINavigationController *nav = [UINavigationController new];
    [nav setViewControllers:@[rootVC]];
    nav.navigationBar.prefersLargeTitles = YES;
    nav.navigationBar.shadowImage = [UIImage new];
    nav.navigationBar.barTintColor = UIColorFromHex(0x161616);
    [nav.navigationBar setTranslucent:NO];
    NSMutableParagraphStyle *style = [NSMutableParagraphStyle new];
    style.alignment = NSTextAlignmentJustified;
    if (IPAD) {
        style.firstLineHeadIndent = 80;
    }
    nav.navigationBar.largeTitleTextAttributes = @{
                                                   NSForegroundColorAttributeName: [UIColor whiteColor],
                                                   NSFontAttributeName: [UIFont systemFontOfSize:28 weight:UIFontWeightBold],
                                                   NSParagraphStyleAttributeName: style
                                                   };
    nav.navigationBar.titleTextAttributes = @{
                                              NSForegroundColorAttributeName: [UIColor whiteColor],
                                              };
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DCFirstLaunchKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [DCPersistentStack sharedInstance];
    
    [[DCImporter importer] syncAll];
    
    [[DCNetworkCore sharedInstance].authProvider authorize];

    [[LNNotificationCenter defaultCenter] registerApplicationWithIdentifier:@"Dallas Club" name:@"Dallas Club" icon:[UIImage imageNamed:@"AppIcon"] defaultSettings:[LNNotificationAppSettings defaultNotificationAppSettings]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    application.applicationIconBadgeNumber = 0;
    
    [IQKeyboardManager sharedManager].enable = YES;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[Buglife sharedBuglife] startWithAPIKey:@"3eSONGcH5w4AOjbRSI6Nhgtt"];
    [[Buglife sharedBuglife] setUserIdentifier:[DCNetworkCore sharedInstance].authProvider.currentUser.name];
    [Buglife sharedBuglife].invocationOptions = [DCNetworkCore sharedInstance].authProvider.isSuperAdmin || [DCNetworkCore sharedInstance].authProvider.isAdmin ? LIFEInvocationOptionsShake : LIFEInvocationOptionsNone;
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [self hexStringFromData:deviceToken];
    [[DCNetworkCore sharedInstance].notificationsProvider registerToken:token];
//    [RMUniversalAlert showAlertInViewController: self.window.rootViewController withTitle:@"" message:token cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(RMUniversalAlert * _Nonnull alert, NSInteger buttonIndex) {
//        [UIPasteboard generalPasteboard].string = token;
//    }];
}

- (void)checkTopicAccess {
}

- (void)repeatSubscription {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[FIRMessaging messaging] subscribeToTopic:@"/topics/posts-ios"];
        
        [self checkTopicAccess];
    });
}
    
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    if ([((UINavigationController *)self.window.rootViewController).topViewController isKindOfClass:[DCCapperDetailsViewController class]]) {
        return;
    }
    [[DCImporter importer] syncAll];
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    [self connectToFcm];
    
    if ([DCNetworkCore sharedInstance].authProvider.hasAuth) {
        NSString *token = [[FIRInstanceID instanceID] token];
        [[DCNetworkCore sharedInstance].notificationsProvider registerToken:token];
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[FIRMessaging messaging] subscribeToTopic:@"/topics/posts-ios"];
    
    [self checkTopicAccess];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if (application.applicationState == UIApplicationStateActive && userInfo[@"aps"][@"alert"][@"body"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DCApplicationDidRecieveNotification" object:nil];
        [[DCImporter importer] syncAll];
        LNNotification *localNotification = [[LNNotification alloc] init];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.message = userInfo[@"aps"][@"alert"][@"body"];

        [[LNNotificationCenter defaultCenter] presentNotification:localNotification forApplicationIdentifier:@"Dallas Club"];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    if (application.applicationState == UIApplicationStateActive && userInfo[@"aps"][@"alert"][@"body"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DCApplicationDidRecieveNotification" object:nil];
        [[DCImporter importer] syncAll];
        LNNotification *localNotification = [[LNNotification alloc] init];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.message = userInfo[@"aps"][@"alert"][@"body"];
        
        [[LNNotificationCenter defaultCenter] presentNotification:localNotification forApplicationIdentifier:@"Dallas Club"];
    }
    
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)connectToFcm {
    if ([[FIRInstanceID instanceID] token] != nil) {
        return;
    }
    [[FIRMessaging messaging] disconnect];
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

- (NSString *)hexStringFromData:(NSData*)data {
    const unsigned char *dataBuffer = (const unsigned char *)data.bytes;
    if (!dataBuffer) return [NSString string];
    
    NSUInteger          dataLength  = data.length;
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self checkTopicAccess];
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
