//
//  DCAccountDataSource.h
//  DallasClub
//
//  Created by Anton on 01/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFetchedDataSource.h"

@class DCTableHeaderView;
@class Account;

@protocol DCAccountDataSourceDelegate <NSObject>

- (void)dataSourceDidChangeContent;

@end

@interface DCAccountDataSource : DCFetchedDataSource

@property (nonatomic, readonly) Account *account;
@property (nonatomic, weak)     id<DCAccountDataSourceDelegate> accountDelegate;
@property (strong, nonatomic)   DCTableHeaderView *headerView;

- (instancetype)initWithAccountView:(DCTableHeaderView *)view;
- (void)updateAccountView;

@end
