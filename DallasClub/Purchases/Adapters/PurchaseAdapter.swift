//
//  PurchaseAdapter.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class PurchaseAdapter: TKBaseAdapter {
    
    let type: String = "СТРАТЕГИЯ"
    let title: String
    let imageUrl: String?
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        if
            let purchase = model as? Purchase,
            let title = purchase.title
        {
            self.title = title
            self.imageUrl = purchase.imageUrl
        }
        else {
            title = ""
            imageUrl = nil
        }
        
        super.init(model: model, cellClass: cellClass)
    }
    
}
