//
//  PurchasesDAtaSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 25.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class LessonsDataSource: AdapterDataSource<LessonAdapter, LessonCell> {
    
    let purchase: Purchase
    init(tableView: UITableView, purchase: Purchase) {
        self.purchase = purchase
        
        super.init(tableView: tableView)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.dataSource(self, didSelectObjectAt: indexPath)
    }
    
    override var fetchRequest: NSFetchRequest<NSFetchRequestResult>! {
        return Lesson.sortedFetchRequestWithPredicateFormat("purchase == %@", purchase)
    }
}
