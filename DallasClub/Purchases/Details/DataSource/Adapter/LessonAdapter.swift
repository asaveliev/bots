//
//  File.swift
//  DallasClub
//
//  Created by Anton Savelev on 25.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

class LessonAdapter: TKBaseAdapter {
    let title: String
    let subtitle: String
    let imageUrl: String
    
    required init(model: Any?, cellClass: UITableViewCell.Type) {
        if let lesson = model as? Lesson {
            title = lesson.title ?? ""
            if
                let count = lesson.steps?.count,
                let completed = lesson.itemsCompleted?.intValue
            {
                subtitle = count > completed ? "Пройдено \(completed) из \(count)" : "Пройден"
            } else {
                subtitle = ""
            }
            
            imageUrl = lesson.imageUrl ?? ""
        } else {
            title = ""
            subtitle = ""
            imageUrl = ""
        }
        
        super.init(model: model, cellClass: cellClass)
    }

}
