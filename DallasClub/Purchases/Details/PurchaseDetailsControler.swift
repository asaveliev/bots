//
//  PurchaseDetailsControler.swift
//  DallasClub
//
//  Created by Anton Savelev on 24.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PurchaseDetailsController: UIViewController {
    let mainView = PurchaseDetailsView()
    
    var dataSource: LessonsDataSource?
    let model: Purchase
    
    init(withPurchseId id: String) {
        model = DCPersistentStack.sharedInstance().context.getOrCreateById(id)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainView.headerTitleLabel.text = "Покупки"
        mainView.headerTitleLabel.textColor = UIColor.black
        mainView.headerView.leftButton.setTitle("Назад", for: .normal)
        mainView.headerView.leftButton.setTitleColor(UIColor.statsNegativeBalance(), for: .normal)
        mainView.headerView.leftButton.setImage(#imageLiteral(resourceName: "back_button.png"), for: .normal)
        mainView.headerView.leftButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        mainView.backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        mainView.backgroundImageView.image = nil
        mainView.backgroundImageView.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        if let string = model.imageUrl, let url = URL(string: string) {
            mainView.backgroundImageView.sd_setImage(with: url)
        }
        
        mainView.tableView.addObserver(self, forKeyPath: "contentOffset", options: .new, context: nil)
        mainView.bindTableView(toResizeBackground: mainView.tableView)
        mainView.infoButton.isHidden = true
        
        dataSource = LessonsDataSource(tableView: mainView.tableView, purchase: model)
        dataSource?.delegate = self
        
        mainView.typeLabel.text = "СТРАТЕГИЯ"
        mainView.titleLabel.text = model.title?.uppercased()
    }
    
    deinit {
        mainView.tableView.removeObserver(mainView, forKeyPath: "contentOffset")
        mainView.tableView.removeObserver(self, forKeyPath: "contentOffset")
    }
    
    func back() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return mainView.tableView.contentOffset.y > -62.0 ? .default : .lightContent
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "contentOffset") {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
}

extension PurchaseDetailsController: DCDataSourceDelegate {
    func dataSource(_ dataSource: Any!, didSelectObjectAt indexPath: IndexPath!) {
        guard
            let dataSource = dataSource as? LessonsDataSource,
            let lesson = dataSource.object(at: indexPath) as? Lesson
        else {
            return
        }
        
        let vc = LessonViewController(withModel: lesson)
        navigationController?.pushViewController(vc, animated: true)
    }
}
