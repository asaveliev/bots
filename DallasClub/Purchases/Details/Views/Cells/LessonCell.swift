//
//  LessonCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 30.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class LessonCell: UITableViewCell, AdapterProtocol {
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let adapter = adapter as? LessonAdapter else {
                return
            }
            
            titleLabel.text = adapter.title
            subtitleLabel.text = adapter.subtitle
            if let url = URL(string: adapter.imageUrl) {
                iconImageView.sd_setImage(with: url)
            }
        }
    }
    
    lazy var cardContentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.tkColorFromHex(0xEEEEEE)
        view.clipsToBounds = true
        view.layer.cornerRadius = 6.0
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.boldSystemFont(ofSize: 15.0)
        return label
    }()
    
    lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.tkColorFromHex(0x3C8C40)
        label.font = UIFont.systemFont(ofSize: 12.0)
        return label
    }()
    
    lazy var iconImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 22.0
        image.clipsToBounds = true
        image.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return image
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let views: [UIView] = [
            titleLabel,
            subtitleLabel,
            iconImageView
        ]
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        contentView.addSubview(cardContentView)
        views.forEach {
            cardContentView.addSubview($0)
        }
        
        selectionStyle = .none
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        iconImageView.snp.remakeConstraints { make in
            make.leading.equalToSuperview().offset(15)
            make.height.width.equalTo(44)
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(10)
            make.top.equalToSuperview().offset(20)
            make.height.greaterThanOrEqualTo(18)
            make.trailing.equalToSuperview().offset(-12)
        }
        
        subtitleLabel.snp.remakeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(12)
            make.top.equalTo(titleLabel.snp.bottom).offset(3)
            make.height.greaterThanOrEqualTo(14)
            make.trailing.equalToSuperview().offset(-12)
            make.bottom.equalToSuperview().offset(-20)
        }
        
        cardContentView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 15.0, left: 15.0, bottom: 0.0, right: 15.0))
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
