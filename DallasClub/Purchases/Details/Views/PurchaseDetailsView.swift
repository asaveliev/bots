//
//  PurchaseDetailsView.swift
//  DallasClub
//
//  Created by Anton Savelev on 24.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PurchaseDetailsView: DCExpandableBackgroundView {
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightSemibold)
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28.0, weight: UIFontWeightHeavy)
        label.textColor = UIColor.white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var fakeBgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = UIColor.clear
        view.separatorStyle = .none
        let bgOffset = UIScreen.main.bounds.size.width * 0.55;
        view.contentInset = UIEdgeInsets(top: CGFloat(bgOffset), left: 0.0, bottom: 20.0, right: 0.0)
        view.contentOffset = CGPoint(x: 0.0, y: -bgOffset)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.clipsToBounds = false
        contentView.addSubview(titleLabel)
        contentView.addSubview(typeLabel)
        contentView.addSubview(fakeBgView)
        contentView.addSubview(tableView)
        
        backButton.tintColor = UIColor.white
        backButton.setTitleColor(UIColor.white, for: .normal)
        backButton.setTitle(" Назад", for: .normal)
        
        headerView.backgroundColor = UIColor.tkColorFromHex(0xF7F7F7)
        headerView.headerTitleLabel.textColor = UIColor.black
        
        backgroundColor = UIColor.white
    }
    
    override func headerViewClass() -> AnyClass! {
        return DCClassicHeaderView.self
    }
    
    override func updateConstraints() {
        tableView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: -64.0, left: 0.0, bottom: 0.0, right: 0.0))
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(40)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        typeLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.equalTo(titleLabel.snp.bottom).offset(7)
        }
        
        super.updateConstraints()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        guard let offset = change?[.newKey] as? CGPoint else {
            return
        }
        
        var frame = tableView.frame
        if frame.origin.x < 0 {
            return
        }
        frame.origin.y += 64
        frame.origin.y -= offset.y
        frame.origin.y = max(frame.origin.y, -64.0)
        frame.size.height = tableView.frame.size.height + 64.0
        fakeBgView.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
