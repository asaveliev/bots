//
//  PurchaseView.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PurchaseView: TableContentView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        headerView.isHidden = true
        tableView.showsVerticalScrollIndicator = false
        tableView.removeFromSuperview()
        addSubview(tableView)
        bringSubview(toFront: tableView)
        tableView.allowsSelection = true
        tableView.contentInset = UIEdgeInsets.zero
        
        headerView.backgroundColor = UIColor.tkColorFromHex(0xF7F7F7)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
