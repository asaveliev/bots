//
//  PurchasesController.swift
//  DallasClub
//
//  Created by Anton Savelev on 22.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit
import SVProgressHUD
import RMUniversalAlert

class PurchasesController: UIViewController {
    
    let mainView = PurchaseView()
    let importer = ManagedObjectsImporter<Purchase>()
    
    var dataSource: PurchasesDataSource?
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        importer.sync()
        dataSource = PurchasesDataSource.init(tableView: mainView.tableView)
        dataSource?.purchasesDelegate = self
        dataSource?.delegate = self
        
        mainView.bindTableView(mainView.tableView, forFadeWithOffset: -10.0)
        mainView.headerTitleLabel.text = "Покупки"
        mainView.headerView.leftButton.setTitle("Назад", for: .normal)
        mainView.headerView.leftButton.setTitleColor(UIColor.statsNegativeBalance(), for: .normal)
        mainView.headerView.leftButton.setImage(#imageLiteral(resourceName: "back_button.png"), for: .normal)
        mainView.headerView.leftButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(back), name: NSNotification.Name(rawValue: "DCUnathorizedNotificationKey"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(banAlert), name: bannedNotification, object: nil)
    }
    
    func banAlert() {
        SVProgressHUD.dismiss()
        RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Ваш аккаунт забанен. По всем вопросам пишите в группу ВК.", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: { _, _ in
            self.back()
        })
    }
    
    func back() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    deinit {
        mainView.tableView.removeObserver(mainView, forKeyPath: "contentOffset")
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension PurchasesController: PurchasesDataSourceDelegate {
    func didSeletBackButton() {
        back()
    }
}

extension PurchasesController: DCDataSourceDelegate {
    func dataSource(_ dataSource: Any!, didSelectObjectAt indexPath: IndexPath!) {
        if indexPath.row == 0 {
            return
        }
        
        guard let dataSource = dataSource as? PurchasesDataSource else {
            return
        }
        
        guard let model = dataSource.object(at: IndexPath(row: indexPath.row - 1, section: 0)) as? Purchase else {
            return
        }
        
        SVProgressHUD.show()
        importer.importDetails(forObjectWithId: model.id) { success in
            SVProgressHUD.dismiss()
            
            if success {
                let vc = PurchaseDetailsController(withPurchseId: model.id)
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
            
            RMUniversalAlert.show(in: self, withTitle: "Внимание!", message: "Произошла ошибка. Попробуйте позже", cancelButtonTitle: "Ок", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
            
        }
    }
}
