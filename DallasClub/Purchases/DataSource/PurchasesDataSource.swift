//
//  PurchasesDataSource.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import Foundation

protocol PurchasesDataSourceDelegate: class {
    func didSeletBackButton()
}

class PurchasesDataSource: AdapterDataSource<PurchaseAdapter, PurchaseCell> {
    
    weak var purchasesDelegate: PurchasesDataSourceDelegate?
    
    override var fetchRequest: NSFetchRequest<NSFetchRequestResult>! {
        return Purchase.sortedFetchRequest
    }
    
    override func registerClasses() {
        if tableView == nil {
            return
        }
        
        super.registerClasses()
        tableView.register(PurchasesHeaderCell.self, forCellReuseIdentifier: PurchasesHeaderCell.tkReuseIdentifier)
    }
    
    func backAction() {
        purchasesDelegate?.didSeletBackButton()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: PurchasesHeaderCell.tkReuseIdentifier, for: indexPath) as! PurchasesHeaderCell
            cell.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
            return cell
        }
        
        return super.tableView(tableView, cellForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(self.tableView, numberOfRowsInSection: section) + 1
    }
    
    override func adapter(atIndexPath indexPath: IndexPath) -> PurchaseAdapter {
        if indexPath.row == 0 {
            return PurchaseAdapter(model: nil, cellClass: PurchasesHeaderCell.self)
        }
        
        return super.adapter(atIndexPath: IndexPath(row: indexPath.row - 1, section: 0))
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.dataSource(self, didSelectObjectAt: indexPath)
    }
    
}
