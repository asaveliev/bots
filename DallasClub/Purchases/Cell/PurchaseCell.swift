//
//  PurchaseCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PurchaseCell: UITableViewCell, AdapterProtocol {
    
    var adapter: TKBaseAdapter! {
        didSet {
            guard let adapter = adapter as? PurchaseAdapter else {
                return
            }
            
            typeLabel.text = adapter.type
            titleLabel.text = adapter.title
            
            if
                let imageUrl = adapter.imageUrl,
                let url = URL(string: imageUrl)
            {
                backgroundImageView.sd_setImage(with: url)
            }
            
        }
    }
    
    lazy var innerContentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        view.layer.cornerRadius = 16.0
        view.backgroundColor = UIColor.tkColorFromHex(0xC3CDD4)
        return view
    }()
    
    lazy var backgroundImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = UIColor.clear
        return image
    }()
    
    lazy var typeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightSemibold)
        label.textColor = UIColor.white
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 28.0, weight: UIFontWeightHeavy)
        label.textColor = UIColor.white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        contentView.addSubview(innerContentView)
        let views: [UIView] = [
            backgroundImageView,
            typeLabel,
            titleLabel
        ]
        views.forEach {
            innerContentView.addSubview($0)
        }
        
        setNeedsUpdateConstraints()
        
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
    }
    
    override func updateConstraints() {
        innerContentView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0.0, left: 20.0, bottom: 20.0, right: 20.0))
            make.height.equalTo(220)
        }
        
        backgroundImageView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        typeLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.equalToSuperview().offset(18)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.top.equalTo(typeLabel.snp.bottom).offset(7)
            make.leading.trailing.equalToSuperview().inset(20)
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
