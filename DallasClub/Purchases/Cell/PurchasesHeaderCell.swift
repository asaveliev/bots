//
//  PurchasesHeaderCell.swift
//  DallasClub
//
//  Created by Anton Savelev on 23.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

import UIKit

class PurchasesHeaderCell: UITableViewCell {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 34.0, weight: UIFontWeightHeavy)
        label.text = "Мои курсы"
        return label
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "back_button.png").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle(" Назад", for: .normal)
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    let innerView = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        innerView.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        backgroundColor = UIColor.clear
        contentView.addSubview(innerView)
        
        backButton.setTitleColor(backButton.tintColor, for: .normal)
        
        let views: [UIView] = [
            titleLabel,
            backButton
        ]
        views.forEach {
            innerView.addSubview($0)
        }
        
        setNeedsUpdateConstraints()
    }
    
    override func updateConstraints() {
        backButton.snp.remakeConstraints { make in
            make.top.equalToSuperview().offset(33)
            make.left.equalToSuperview().offset(8)
            make.height.equalTo(20)
            make.width.equalTo(99)
        }
        
        titleLabel.snp.remakeConstraints { make in
            make.top.equalTo(backButton.snp.bottom).offset(17)
            make.leading.trailing.equalToSuperview().inset(19)
            make.bottom.equalToSuperview().offset(-12)
        }
        
        innerView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        super.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
