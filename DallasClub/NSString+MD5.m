//
//  NSString+MD5.m
//  DallasClub
//
//  Created by Anton Savelev on 03.03.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <CommonCrypto/CommonCrypto.h>
#import "NSString+MD5.h"

@implementation NSString (MD5)

- (NSString *)md5 {
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (unsigned)strlen(cStr), result);
    NSString *s = [NSString  stringWithFormat:
                   @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                   result[0], result[1], result[2], result[3], result[4],
                   result[5], result[6], result[7],
                   result[8], result[9], result[10], result[11], result[12],
                   result[13], result[14], result[15]
                   ];
    
    return s;
}

@end
