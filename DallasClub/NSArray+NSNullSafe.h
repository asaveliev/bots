//
//  NSArray+NSNullSafe.h
//  RCDOptics
//
//  Created by Anton on 07/04/16.
//  Copyright © 2016 Robot Dream. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (NSNullSafe)

- (NSArray *)arrayByReplacingNull;

@end
