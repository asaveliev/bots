//
//  Material+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 07.05.17.
//
//

#import "Material.h"


NS_ASSUME_NONNULL_BEGIN

@interface Material (CoreDataProperties)

+ (NSFetchRequest<Material *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *caption;
@property (nullable, nonatomic, copy) NSString *image_url;
@property (nullable, nonatomic, copy) NSDate *created_at;
@property (nullable, nonatomic, copy) NSString *url;

@end

NS_ASSUME_NONNULL_END
