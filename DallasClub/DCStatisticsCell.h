//
//  DCStatisticsCell.h
//  DallasClub
//
//  Created by Anton on 18/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCBubbleView.h"
#import "DCRoundedCardCell.h"

typedef enum : NSUInteger {
    DCBubbleViewStatePositive,
    DCBubbleViewStateNegative,
    DCBubbleViewStateNeutral
} DCBubbleViewState;

@interface DCStatisticsCell : DCRoundedCardCell

@property (strong, nonatomic, readonly) UILabel *titleLabel;
@property (strong, nonatomic, readonly) UILabel *priceLabel;
@property (strong, nonatomic, readonly) DCBubbleView *bubbleView;

@property (nonatomic) DCBubbleViewState state;

- (Class)bubbleClass;

@end
