//
//  DCMaterialDataSource.m
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import "DCMaterialDataSource.h"
#import "DCMaterialsTableViewCell.h"
#import "Material.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation DCMaterialDataSource

- (NSString *)reuseIdentifier {
    return @"Cell";
}

- (void)registerClasses {
    [self.tableView registerClass:[DCMaterialsTableViewCell class] forCellReuseIdentifier:self.reuseIdentifier];
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *requet = [NSFetchRequest fetchRequestWithEntityName:[Material entityName]];
    requet.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:NO]];
    requet.predicate = [NSPredicate predicateWithValue:YES];
    return requet;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Material *model = [self objectAtIndexPath:indexPath];
    DCMaterialsTableViewCell *materialCell = (DCMaterialsTableViewCell *)cell;
    materialCell.title.text = model.caption;
    
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"d MMM yyyy в HH:mm";
    materialCell.subtitle.text = [formatter stringFromDate:model.created_at];
    
    [materialCell.thumbImageView sd_setImageWithURL:[NSURL URLWithString:model.image_url]];
}

@end
