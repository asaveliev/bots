//
//  DCCapperBetsHeaderView.h
//  DallasClub
//
//  Created by Anton Savelev on 07.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCTableHeaderView.h"

@interface DCCapperBetsHeaderView : UITableViewHeaderFooterView

@property (nonatomic, readonly) DCTableHeaderView *headerView;

@end
