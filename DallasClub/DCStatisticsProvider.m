//
//  DCBetsProvider.m
//  DallasClub
//
//  Created by Anton on 24/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

/* POST {
 "bet": {
 "name": "Название',
 "description": "Описание", //опционально
 "amount": "10.5", //размер ставки
 "coefficient", "1.2",
 "number": 128,
 "status": "successful"
 }
 } */

#import "DCStatisticsProvider.h"
#import "NSDate+Utilities.h"
#import "DCNetworkCore.h"

static NSString *const DCApiBetsEndpoint = @"bets";
static NSString *const DCApiAccountEndpoint = @"account";
static NSString *const DCApiFullStatsEndpoint = @"statistics/full";
static NSString *const DCApiMothStatsEndpoint = @"statistics/monthly";

NSInteger const DCBetsPageSize = 5;

@implementation DCStatisticsProvider

#pragma mark - Bets

- (void)fetchStatisticsPage:(NSNumber *)pageNumber capperId:(NSNumber *)capperId withCompletion:(void (^)(NSArray *, NSError *))completion {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    if (pageNumber) {
        parameters[@"page"] = pageNumber;
        parameters[@"per_page"] = @(DCBetsPageSize);
    }
    
    NSString *url = !capperId ? [DCApiRootOld stringByAppendingString:DCApiBetsEndpoint] : [NSString stringWithFormat:@"%@/cappers/%@/%@", DCApiRoot, capperId, DCApiBetsEndpoint];
    [self.sessionManager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)fetchStatisticsPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion {
    [self fetchStatisticsPage:pageNumber capperId:nil withCompletion:completion];
}

- (void)fetchStatisticsWithCompletion:(void (^)(NSArray *, NSError *))completion {
    [self fetchStatisticsPage:nil withCompletion:completion];
}

- (void)removeBetWithId:(NSNumber *)betId completion:(void (^)(NSArray *, NSError *))completion {
    NSString *url = [DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/bets/%@", (long)[DCNetworkCore sharedInstance].authProvider.userId, betId]];
    @synchronized(self) {
        self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.sessionManager DELETE:url parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
            NSLog(@"%@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            completion(responseObject, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
            completion(nil, error);
        }];
    }
}

- (void)updateBetWithDictionary:(NSDictionary *)dict betId:(NSNumber *)betId completion:(void (^)(NSArray *, NSError *))completion {
    NSString *url = [DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/bets/%@", (long)[DCNetworkCore sharedInstance].authProvider.userId, betId]];
    [self.sessionManager PUT:url parameters:dict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)createBetWithDictionary:(NSDictionary *)dict completion:(void (^)(NSArray *, NSError *))completion {
    [self.sessionManager POST:[DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/bets", (long)[DCNetworkCore sharedInstance].authProvider.userId]] parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

#pragma mark - Account statistics

- (void)getStatisticsByMonths:(NSDate *)month capperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *url = !capperId ? [DCApiRootOld stringByAppendingString:DCApiMothStatsEndpoint] : [NSString stringWithFormat:@"%@/cappers/%@/%@", DCApiRoot, capperId, DCApiMothStatsEndpoint];
    NSString *monthString = [month stringWithFormat:@"MM/yyyy"];
    NSDictionary *params = @{
                             @"month": monthString
                             };
    [self.sessionManager GET:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)getStatisticsByMonths:(NSDate *)month completion:(void (^)(NSDictionary *, NSError *))completion {
    [self getStatisticsByMonths:month capperId:nil completion:completion];
}

- (void)getAccountWithCapperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *url = !capperId ? [DCApiRootOld stringByAppendingString:DCApiAccountEndpoint] : [NSString stringWithFormat:@"%@/cappers/%@/%@", DCApiRoot, capperId, DCApiAccountEndpoint];
    [self.sessionManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)getFullStatisticsWithCapperId:(NSNumber *)capperId completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *url = !capperId ? [DCApiRootOld stringByAppendingString:DCApiFullStatsEndpoint] : [NSString stringWithFormat:@"%@/cappers/%@/%@", DCApiRoot, capperId, DCApiFullStatsEndpoint];
    [self.sessionManager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)getAccountWithcompletion:(void (^)(NSDictionary *, NSError *))completion {
    [self getAccountWithCapperId:nil completion:completion];
}

- (void)getFullStatisticsWithcompletion:(void (^)(NSDictionary *, NSError *))completion {
    [self getFullStatisticsWithCapperId:nil completion:completion];
}

@end
