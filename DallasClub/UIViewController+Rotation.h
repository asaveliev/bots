//
//  UIViewController+Rotation.h
//  DallasClub
//
//  Created by Anton Savelev on 27.07.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Rotation)
    
- (void)restrictRotation:(BOOL) restriction;

@end
