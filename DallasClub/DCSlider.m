//
//  DCSlider.m
//  DallasClub
//
//  Created by Anton Savelev on 12.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCSlider.h"

@interface DCResizableSlider : UISlider

@property (nonatomic, assign) CGFloat prefferedHeight;

@end

@implementation DCResizableSlider

- (CGRect)trackRectForBounds:(CGRect)bounds {
    return CGRectMake(0.0, 0.0, bounds.size.width, self.prefferedHeight ?: bounds.size.height);
}

@end

@implementation DCSlider

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        DCResizableSlider *slider = [DCResizableSlider new];
        slider.prefferedHeight = 4.0;
        _slider = slider;
        [self.slider setThumbImage:[UIImage new] forState:UIControlStateNormal];
        self.slider.minimumTrackTintColor = [UIColor audioBlueColor];
        self.slider.maximumTrackTintColor = [UIColor audioGrayColor];
        self.slider.userInteractionEnabled = NO;
        [self addSubview:self.slider];
        
        _minLabel = [UILabel new];
        self.minLabel.font = [UIFont regularFontOfSize:11.0];
        self.minLabel.textColor = UIColorFromHex(0xA6B2BC);
        self.minLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.minLabel];
        
        [self setConstraints];
    }
    
    return self;
}

- (void)setConstraints {
    NSDictionary *views = @{
        @"slider": self.slider,
        @"min": self.minLabel
    };
    
    for (UIView *view in views.allValues) {
        view.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    NSLayoutConstraint *centerConstraint = [NSLayoutConstraint constraintWithItem:self.slider attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0];
    [NSLayoutConstraint activateConstraints:@[centerConstraint]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[min]-(-3)-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[slider]-0-|" options:0 metrics:nil views:views]];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[min]-0-|" options:0 metrics:nil views:views]];
}

@end
