//
//  DCAboutView.m
//  DallasClub
//
//  Created by Anton Savelev on 25/08/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCAboutView.h"
#import "DCClassicHeaderView.h"

@implementation DCAboutView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.tableView];
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[t]-0-|" options:0 metrics:nil views:@{ @"t": self.tableView }]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-20)-[t]-0-|" options:0 metrics:nil views:@{ @"t": self.tableView }]];
        [self sendSubviewToBack:self.contentView];
        self.contentView.clipsToBounds = YES;
        
        [self.leftButton setTitleColor:[UIColor statsNegativeBalanceColor] forState:UIControlStateNormal];
        [self.leftButton setTitle:@"Назад" forState:UIControlStateNormal];
        [self.leftButton setImage:[UIImage imageNamed:@"back_button"] forState:UIControlStateNormal];
        
        self.backgroundColor = [UIColor whiteColor];
        self.headerView.backgroundColor = [UIColor bgColor];
    }
    
    return self;
}

- (Class)headerViewClass {
    return [DCClassicHeaderView class];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end
