//
//  DCStatisticsCell.m
//  DallasClub
//
//  Created by Anton on 18/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCStatisticsCell.h"

@implementation DCStatisticsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [UILabel new];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.cardView addSubview:self.titleLabel];
        
        _bubbleView = [[self bubbleClass] new];
        [self.cardView addSubview:self.bubbleView];
        
        _priceLabel = [UILabel new];
        self.priceLabel.font = [UIFont statsPriceFont];
        self.priceLabel.textAlignment = NSTextAlignmentRight;
        [self.cardView addSubview:self.priceLabel];
        
        [self setConstraints];
    }
    
    return self;
}

- (Class)bubbleClass {
    return [DCBubbleView class];
}

- (void)setConstraints {
    NSDictionary *views = @{
                            @"title": self.titleLabel,
                            @"bubble": self.bubbleView,
                            @"price": self.priceLabel
                            };
    NSDictionary *metrics = @{
                              @"bubbleSize": @40
                              };
    for (UIView *v in views.allValues) {
        v.translatesAutoresizingMaskIntoConstraints = NO;
    }
    
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-12-[bubble(==bubbleSize)]-10-[title(<=187@251)]-10-[price(>=81@1000)]-15-|" options:0 metrics:metrics views:views]];
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.bubbleView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.cardView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bubble(==bubbleSize)]" options:0 metrics:metrics views:views]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(>=10)-[title]-(>=10)-|" options:0 metrics:nil views:views]];
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.priceLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.cardView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [self.cardView addConstraint:[NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.cardView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

- (void)setState:(DCBubbleViewState)state {
    switch (state) {
        case DCBubbleViewStatePositive:
            self.bubbleView.backgroundColor = [UIColor statsPostiveBubbleColor];
            self.priceLabel.textColor = [UIColor statsPositiveBalanceColor];
            break;
        case DCBubbleViewStateNegative:
            self.bubbleView.backgroundColor = [UIColor statsNegativeBubbleColor];
            self.priceLabel.textColor = [UIColor statsNegativeBalanceColor];
            break;
        case DCBubbleViewStateNeutral:
            self.priceLabel.textColor = [UIColor statsNegativeBalanceColor];
            self.bubbleView.backgroundColor = [UIColor statsNeutralBubbleColor];
            break;
        default:
            break;
    }
    
    self.priceLabel.hidden = state == DCBubbleViewStateNeutral;
}

@end
