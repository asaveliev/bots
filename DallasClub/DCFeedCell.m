//
//  DCFeedCell.m
//  DallasClub
//
//  Created by Anton on 20/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedCell.h"

@interface TKNewRoundedCardCell ()
@property (strong, nonatomic) UIView  *cardView;
@end

@implementation DCFeedCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.cardView setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.cardView setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        
        _messageLabel = [KILabel new];
        self.messageLabel.font = [UIFont feedFont];
        self.messageLabel.textColor = [UIColor whiteColor];
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.messageLabel.numberOfLines = 0;
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.messageLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.messageLabel setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
        [self.cardView addSubview:self.messageLabel];
        
        _avatarImageView = [UIImageView new];
        self.avatarImageView.backgroundColor = [UIColor clearColor];
        [self.cardView addSubview:self.avatarImageView];
        self.avatarImageView.clipsToBounds = YES;
        self.avatarImageView.layer.cornerRadius = 15.0;
        self.avatarImageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.cardView.layer.cornerRadius = 14.0;
        
        _timeLabel = [UILabel new];
        self.timeLabel.font = [UIFont regularFontOfSize:14.0];
        self.timeLabel.textColor = UIColorFromHex(0x929292);
        self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        [self.cardView addSubview:self.timeLabel];
        
        _nameLabel = [UILabel new];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.nameLabel.textColor = [UIColor whiteColor];
        [self.cardView addSubview:self.nameLabel];
        
        [self setConstraints];
        
        self.contentView.backgroundColor = self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (void)setConstraints {
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[text]-15-|" options:0 metrics:nil views:@{ @"text": self.messageLabel }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[v(30)]-8-[name]-5-[time]-15-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:@{ @"v": self.avatarImageView, @"time": self.timeLabel, @"name": self.nameLabel }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-15-[v(30)]-15-[text]-15-|" options:0 metrics:nil views:@{ @"v": self.avatarImageView, @"text": self.messageLabel, @"time": self.timeLabel }]];
    [self.cardView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-22-[time]" options:0 metrics:nil views:@{ @"v": self.avatarImageView, @"text": self.messageLabel, @"time": self.timeLabel }]];
}

@end
