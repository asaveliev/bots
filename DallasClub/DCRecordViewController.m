//
//  DCRecordViewController.m
//  DallasClub
//
//  Created by Anton Savelev on 03.10.16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCRecordViewController.h"
#import "DCRecordView.h"
#import "DCAudioManager.h"
#import "EZAudio.h"

@interface DCRecordViewController () <EZRecorderDelegate, EZAudioPlayerDelegate>

@property (nonatomic, readonly) DCRecordView    *mainView;
@property (nonatomic, strong)   DCAudioManager  *audioManager;
@property (nonatomic, assign)   BOOL            recording;

@end

@implementation DCRecordViewController

- (void)loadView {
    self.view = _mainView = [DCRecordView new];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mainView.stateLabel.text = @"Запись сообщения";
    self.mainView.timeLabel.text = @"00:00";
    
    [self.mainView.closeButton addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView.recordButton addTarget:self action:@selector(recordAction:) forControlEvents:UIControlEventTouchUpInside];
    self.mainView.recordButton.selected = YES;
    
    self.mainView.doneButton.hidden = YES;
    [self.mainView.doneButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self setupRecorder];
}

- (void)setupRecorder {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *uuid = [[[NSUUID UUID] UUIDString] stringByAppendingPathExtension:@"m4a"];
    _audioManager = [[DCAudioManager alloc] initWithFilePath:[basePath stringByAppendingPathComponent:uuid]];
    [self.audioManager startRecording];
    self.audioManager.recorder.delegate = self;
    self.recording = YES;
}

#pragma mark - Actions

- (void)doneAction {
    [self.audioManager.player pause];
    [self.audioManager stopRecording];
    NSData *outputData = [self.audioManager generateMp3Data];
    if (self.completion) {
        self.completion(outputData);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)recordAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    if (self.recording) {
        self.recording = NO;
        [self.audioManager stopRecording];
        self.mainView.stateLabel.text = @"Сообщение записано";
        self.mainView.doneButton.hidden = NO;
        return;
    }
    
    if (!sender.selected) {
        [self.audioManager.player pause];
    } else {
        [self.audioManager playAudio];
        self.audioManager.player.delegate = self;
    }
}

- (void)closeAction {
    [self.audioManager.player pause];
    [self.audioManager stopRecording];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - EZRecorderDelegate

- (void)recorderUpdatedCurrentTime:(EZRecorder *)recorder {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mainView.timeLabel.text = recorder.formattedCurrentTime;
    });
}

#pragma mark - EZAudioPlayerDelegate

- (void)audioPlayer:(EZAudioPlayer *)audioPlayer reachedEndOfAudioFile:(EZAudioFile *)audioFile {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mainView.recordButton.selected = NO;
    });
}

- (void)audioPlayer:(EZAudioPlayer *)audioPlayer updatedPosition:(SInt64)framePosition inAudioFile:(EZAudioFile *)audioFile {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mainView.timeLabel.text = audioPlayer.formattedCurrentTime;
    });
}

@end
