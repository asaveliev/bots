//
//  DCCapperCollectionViewCell.h
//  DallasClub
//
//  Created by Anton Savelev on 05.05.17.
//  Copyright © 2017 Savelev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCCapperCollectionViewCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *avatarImageView;

@end
