//
//  DCApiProvider.m
//  DallasClub
//
//  Created by Anton on 23/07/16.
//  Copyright © 2016 Savelev. All rights reserved.
//

#import "DCFeedProvider.h"
#import "DCNetworkCore.h"

static NSString *const DCApiFeedEndpoint = @"posts";
NSInteger const DCFeedPageSize = 5;

@interface DCFeedProvider ()

@end

@implementation DCFeedProvider

- (void)fetchFeedPage:(NSNumber *)pageNumber withCompletion:(void (^)(NSArray *, NSError *))completion {
    [self fetchFeedPage:pageNumber capperId:nil withCompletion:completion];
}

- (void)fetchFeedPage:(NSNumber *)pageNumber capperId:(NSNumber *)capperId withCompletion:(void (^)(NSArray *, NSError *))completion {
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    if (pageNumber) {
        parameters[@"page"] = pageNumber;
        parameters[@"per_page"] = @(DCFeedPageSize);
    }
    
    NSString *url = !capperId ? [DCApiRoot stringByAppendingString:DCApiFeedEndpoint] : [NSString stringWithFormat:@"%@/cappers/%@/%@", DCApiRoot, capperId, DCApiFeedEndpoint];
    [self.sessionManager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)fetchFeedWithCompletion:(void (^)(NSArray *, NSError *))completion {
    [self fetchFeedPage:nil withCompletion:completion];
}

- (void)createPost:(NSDictionary *)parameters image:(UIImage *)image withCompletion:(void (^)(NSArray *, NSError *))completion {
    NSString *base64 = [UIImageJPEGRepresentation(image, 0.5) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength | NSDataBase64Encoding76CharacterLineLength];
    if (base64) {
        NSString *string = [NSString stringWithFormat:@"data:image/png;base64,%@", base64];
        NSMutableDictionary *mutableParams = [parameters mutableCopy];
        mutableParams[@"post"][@"photo"] = string;
        parameters = [mutableParams copy];
    }
    NSData *photoData = parameters[@"post"][@"audio"];
    if (photoData) {
        NSMutableDictionary *mutableParams = [parameters mutableCopy];
        [mutableParams[@"post"] removeObjectForKey:@"audio"];
        parameters = [mutableParams copy];
    }
    NSString *url = [DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/posts", (long)[DCNetworkCore sharedInstance].authProvider.userId]];
    [self.sessionManager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (photoData) {
            [formData appendPartWithFileData:photoData name:@"post[audio]" fileName:@"audio.mp3" mimeType:@"audio/mp3"];
        }
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(responseObject, nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, error);
    }];
}

- (void)updatePostWithId:(NSNumber *)postId image:(UIImage *)image parameters:(NSDictionary *)params completion:(void (^)(NSError *error))completion {
    NSString *base64 = [UIImageJPEGRepresentation(image, 0.5) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength | NSDataBase64Encoding76CharacterLineLength];
    if (base64) {
        NSString *string = [NSString stringWithFormat:@"data:image/png;base64,%@", base64];
        NSMutableDictionary *mutableParams = [params mutableCopy];
        mutableParams[@"post"][@"photo"] = string;
        params = [mutableParams copy];
    }
    NSString *url = [DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/posts/%@", (long)[DCNetworkCore sharedInstance].authProvider.userId, postId]];
    [self.sessionManager PUT:url parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completion(nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(error);
    }];
}

- (void)removePostWithId:(NSNumber *)postId completion:(void (^)(NSError *error))completion {
    NSString *url = [DCApiRoot stringByAppendingString:[NSString stringWithFormat:@"cappers/%ld/posts/%@", (long)[DCNetworkCore sharedInstance].authProvider.userId, postId]];
    @synchronized(self) {
        self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [self.sessionManager DELETE:url parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
            NSLog(@"%@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            completion(nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
            completion(error);
        }];
    }
}

@end
