//
//  Material+CoreDataProperties.m
//  
//
//  Created by Anton Savelev on 07.05.17.
//
//

#import "Material+CoreDataProperties.h"

@implementation Material (CoreDataProperties)

+ (NSFetchRequest<Material *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Material"];
}

@dynamic uid;
@dynamic caption;
@dynamic image_url;
@dynamic created_at;
@dynamic url;

@end
