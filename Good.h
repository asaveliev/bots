//
//  Good+CoreDataClass.h
//  
//
//  Created by Anton Savelev on 01.01.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Good : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Good+CoreDataProperties.h"
