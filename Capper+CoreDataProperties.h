//
//  Capper+CoreDataProperties.h
//  
//
//  Created by Anton Savelev on 17.04.17.
//
//

#import "Capper+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Capper (CoreDataProperties)

+ (NSFetchRequest<Capper *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *uid;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *balance;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSDate *created;

@end

NS_ASSUME_NONNULL_END
