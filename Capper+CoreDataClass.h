//
//  Capper+CoreDataClass.h
//  
//
//  Created by Anton Savelev on 17.04.17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Capper : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Capper+CoreDataProperties.h"
